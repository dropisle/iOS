//
//  TabBarViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-01-26.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol SelectedViewControllerDelegate: class {
	func SelectedViewController(viewController: UIViewController)
	func HomeProductReload()
}

class TabBarViewController: UITabBarController, UITabBarControllerDelegate, UIViewControllerTransitioningDelegate {

	override func viewDidLoad() {
		super.viewDidLoad()
		self.delegate = self
		tabBar.barTintColor = .secondarySystemGroupedBackground //UIColor.tabBarTintColor()
		UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.tabBarItemColor()], for: .selected)
		tabBar.unselectedItemTintColor = UIColor.tabBarItemColor()
		tabBar.tintColor = UIColor.tabBarItemColor()
	}

	override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
		let _ = tabBar.items?.firstIndex(of: item)
	}

	///Check if user is logged in
	func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
		//viewControllerdelegate?.SelectedViewController(viewController: viewController)
		let tabBarIndex = tabBarController.selectedIndex
//		if CurrentUserInfo.shared.userId().isEmpty {
//			if tabBarIndex == 9 || tabBarIndex == 9 {
//				selectedIndex = 9
//				let storyboard = UIStoryboard(name: "GuestUserSignUpStoryboard", bundle: nil)
//				let pvc = storyboard.instantiateVC() as! GuestSignUpPanelViewController
//				pvc.modalPresentationStyle = UIModalPresentationStyle.custom
//				pvc.modalTransitionStyle = .crossDissolve
//				pvc.transitioningDelegate = self
//				self.present(pvc, animated: true, completion: nil)
//			}
//		}
	}
}
