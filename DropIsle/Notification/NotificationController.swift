//
//  NotificationController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-06-13.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class NotificationController: UIViewController {

	var notificationData = ["Push Notification"]
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "Notification"
		self.tabBarController?.tabBar.isHidden = true
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
	}
}

extension NotificationController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return notificationData.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell" , for: indexPath) as? NotificationCell else { return UITableViewCell() }
		cell.notificationTitles.text = notificationData[indexPath.row]
		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if indexPath.row == 0 {
			let storyBoard: UIStoryboard = UIStoryboard(name: "SettingsStoryboard", bundle: nil)
			if let newViewController = storyBoard.instantiateViewController(withIdentifier: "PushNotificationSettingsController") as? PushNotificationSettingsController {
				navigationController?.pushViewController(newViewController, animated: true)
			}
		}
	}
}
