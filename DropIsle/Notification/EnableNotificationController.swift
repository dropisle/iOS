//
//  NotificationController.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-12.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit
import UserNotifications

class EnableNotificationController: UIViewController, UIApplicationDelegate {

	static let tabBarViewController = "TabBarViewController"
	var application = UIApplication.shared

    override func viewDidLoad() {
        super.viewDidLoad()
    }

	@IBAction func notNow(_ sender: UIButton) {
		DispatchQueue.main.async(execute: {
			self.performSegue(withIdentifier: EnableNotificationController.tabBarViewController, sender: self)
		})
	}

	@IBAction func allow(_ sender: UIButton) {
		if #available(iOS 10, *) {
			UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .alert, .badge]) { (granted, _) in
				if granted {
					print("Notification Enable Successfully")
					DispatchQueue.main.async {
						self.application.registerForRemoteNotifications()
					}
					DispatchQueue.main.async(execute: {
						self.performSegue(withIdentifier: EnableNotificationController.tabBarViewController, sender: self)
					})
				} else {
					print("Some Error Occure")
					DispatchQueue.main.async(execute: {
						self.performSegue(withIdentifier: EnableNotificationController.tabBarViewController, sender: self)
					})
				}
			}
		} else {
			application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert],
																																							categories: nil))
			application.registerForRemoteNotifications()
		}
		application.registerForRemoteNotifications()
	}
}
