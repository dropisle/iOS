//
//  PaintingsCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-08.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class PaintingsCell: UICollectionViewCell, AppConfigurable {
	
	let imageView = UIImageView()
	let backView = UIView()
	let bottomView = UIView()
	let headerTitle = UILabel()
	let headerSubTitle = UILabel()
	let imageContainerView = UIView()

	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}

	private func setup() {
		backView.translatesAutoresizingMaskIntoConstraints = false
		imageView.translatesAutoresizingMaskIntoConstraints = false
		headerTitle.translatesAutoresizingMaskIntoConstraints = false
		bottomView.translatesAutoresizingMaskIntoConstraints = false
		imageContainerView.translatesAutoresizingMaskIntoConstraints = false
		headerSubTitle.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(backView)
		backView.addSubview(imageContainerView)
		backView.addSubview(bottomView)
		imageContainerView.addSubview(imageView)
		bottomView.addSubview(headerTitle)
		bottomView.addSubview(headerSubTitle)

		NSLayoutConstraint.activate([
			headerTitle.topAnchor.constraint(equalTo: bottomView.topAnchor, constant: 10),
			headerTitle.leadingAnchor.constraint(equalTo: bottomView.leadingAnchor, constant: 8),

			headerSubTitle.topAnchor.constraint(equalTo: headerTitle.bottomAnchor, constant: 8),
			headerSubTitle.leadingAnchor.constraint(equalTo: bottomView.leadingAnchor, constant: 8),

			imageView.leadingAnchor.constraint(equalTo: imageContainerView.leadingAnchor),
			imageView.trailingAnchor.constraint(equalTo: imageContainerView.trailingAnchor),
			imageView.topAnchor.constraint(equalTo: imageContainerView.topAnchor),
			imageView.bottomAnchor.constraint(equalTo: imageContainerView.bottomAnchor),

			bottomView.leadingAnchor.constraint(equalTo: backView.leadingAnchor),
			bottomView.trailingAnchor.constraint(equalTo: backView.trailingAnchor),
			bottomView.topAnchor.constraint(equalTo: imageContainerView.bottomAnchor),
			bottomView.bottomAnchor.constraint(equalTo: bottomAnchor),
			bottomView.heightAnchor.constraint(equalToConstant: 70),

			imageContainerView.leadingAnchor.constraint(equalTo: backView.leadingAnchor),
			imageContainerView.trailingAnchor.constraint(equalTo: backView.trailingAnchor),
			imageContainerView.topAnchor.constraint(equalTo: backView.topAnchor),
			imageContainerView.bottomAnchor.constraint(equalTo: bottomView.topAnchor),

			backView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
			backView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
			backView.topAnchor.constraint(equalTo: contentView.topAnchor),
			backView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
		])
		style()
	}

	private func style() {
		bottomView.backgroundColor = .clear

		headerTitle.font = UIFont(name: "AppleSDGothicNeo-Medium-Bold", size: 16)
		headerTitle.textColor = .label

		headerSubTitle.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14)
		headerSubTitle.textColor = .label

		imageView.clipsToBounds = true
		imageView.layer.cornerRadius = 10
		imageView.contentMode = .scaleAspectFill

		imageContainerView.backgroundColor = .yellow
		imageContainerView.layer.cornerRadius = 10

		imageContainerView.layer.shadowColor = UIColor.black.cgColor
		imageContainerView.layer.shadowOpacity = 0.3
		imageContainerView.layer.shadowOffset = CGSize.zero
		imageContainerView.layer.shadowRadius = 10
	}

	func configure(with app: App) {
		headerTitle.text = app.name
		headerSubTitle.text = app.subTitle
		imageView.image = app.image
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
