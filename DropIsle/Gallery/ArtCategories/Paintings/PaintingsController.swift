//
//  PaintingsController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-07.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class PaintingsController: UIViewController {

	private var customConstraints = [NSLayoutConstraint]()

	let containerView = UIView()
	let searchContainer = UIView()
	let searchButton = UIButton()
	let backButton = UIButton()
	let filterButton = UIButton()

	override func viewDidLoad() {
		super.viewDidLoad()
		setupCollectionView()
		collectionView.delegate = self
	}

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
	}

	// MARK: - Properties -
	private var presenter = RecommendedPresenter()         // Handles all the data
	var collectionView: UICollectionView!

	// MARK: - Setup methods -
	/// Constructs the UICollectionView and adds it to the view.
	/// Registers all the Cells and Views that the UICollectionView will need
	private func setupCollectionView() {
		// Initialises the collection view with a CollectionViewLayout which we will define
		collectionView = UICollectionView.init(frame: .zero, collectionViewLayout: makeLayout())
		// Assigning data source and background color
		collectionView.dataSource = self
		collectionView.backgroundColor = .secondarySystemGroupedBackground

		// Adding the collection view to the view
		collectionView.contentInsetAdjustmentBehavior = .never
		collectionView.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 70, right: 0)
		view.addSubview(collectionView)

		// This line tells the system we will define our own constraints
		collectionView.translatesAutoresizingMaskIntoConstraints = false
		// Constraining the collection view to the 4 edges of the view
		NSLayoutConstraint.activate([
			collectionView.topAnchor.constraint(equalTo: view.topAnchor),
			collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])

		// Registering all Cells and Classes we will need
		collectionView.register(PaintingsCell.self,
								forCellWithReuseIdentifier: PaintingsCell.identifier)
	}

	// MARK: - Collection View Helper Methods -
	// In this section you can find all the layout related code

	/// Creates the appropriate UICollectionViewLayout for each section type
	private func makeLayout() -> UICollectionViewLayout {
		// Constructs the UICollectionViewCompositionalLayout
		let layout = UICollectionViewCompositionalLayout { (sectionIndex: Int, layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
			let isWideView = layoutEnvironment.container.effectiveContentSize.width > 500
			switch self.presenter.sectionType(for: sectionIndex) {
				case .singleSectionList:   return self.createRecommendedForYouSection(isWide: isWideView)
			}
		}

		/// Configure the Layout with interSectionSpacing
		let config = UICollectionViewCompositionalLayoutConfiguration()
		config.interSectionSpacing = 20
		layout.configuration = config
		return layout
	}

	/// Creates a layout that shows 3 items per group and scrolls horizontally
	private func createRecommendedForYouSection(isWide: Bool) -> NSCollectionLayoutSection {
		// Defining the size of a single item in this layout
		let itemSize = NSCollectionLayoutSize (
		  widthDimension: .fractionalWidth(1.0),
		  heightDimension: .fractionalHeight(1.0))
		let item = NSCollectionLayoutItem(layoutSize: itemSize)
		item.contentInsets = NSDirectionalEdgeInsets(top: 8, leading: 8, bottom: 8, trailing: 8)

		let groupHeight = NSCollectionLayoutDimension.fractionalWidth(isWide ? 0.55 : 0.85)
		let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: groupHeight)
		let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: isWide ? 2 : 1)
		let section = NSCollectionLayoutSection(group: group)

		return section
	}

	///Animate custom Navbar
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let pan = scrollView.panGestureRecognizer
		let velocity = pan.velocity(in: scrollView).y
		if velocity < -300 {
		}
	}
}

// MARK: - UICollectionViewDataSource -

extension PaintingsController: UICollectionViewDataSource {
	/// Tells the UICollectionView how many sections are needed
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return presenter.numberOfSections
	}

	/// Tells the UICollectionView how many items the requested sections needs
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return presenter.numberOfItems(for: section)
	}

	/// Constructs and configures the item needed for the requested IndexPath
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		// Checks what section type we should use for this indexPath so we use the right cells for that section
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PaintingsCell.identifier, for: indexPath) as? PaintingsCell else {
			fatalError("Could not dequeue FeatureCell")
		}
		switch presenter.sectionType(for: indexPath.section) {
			case .singleSectionList:
				presenter.configure(item: cell, for: indexPath)
				return cell
		}
	}
}

extension PaintingsController: UICollectionViewDelegate {

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		switch presenter.sectionType(for: indexPath.section) {
			case .singleSectionList:
				print(presenter.dataSource[indexPath.section].data[indexPath.row])
		}
	}
}

