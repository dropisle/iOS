//
//  GalleryController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-06.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit
import Tabman
import Pageboy

class GalleryController: TabmanViewController {

	let filterButton = UIButton()
	let searchButton = UIButton()
	let barContainer = UIView()

	lazy var viewControllers: [UIViewController] = {
		var viewControllers = [UIViewController]()
		viewControllers.append(modernArtController())
		viewControllers.append(grafitiController())
		viewControllers.append(abstractController())
		viewControllers.append(popArtController())
		viewControllers.append(paintingsController())
		viewControllers.append(sculptureController())
		viewControllers.append(photographyController())
		return viewControllers
	}()

    override func viewDidLoad() {
        super.viewDidLoad()
		setUpViews()

		barContainer.backgroundColor = .secondarySystemGroupedBackground

		self.dataSource = self
		let bar = TMBar.ButtonBar()
		bar.layout.transitionStyle = .progressive
		bar.layout.contentInset = UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 10.0)
		bar.layout.alignment = .centerDistributed
		bar.layout.contentMode = .intrinsic
		bar.indicator.overscrollBehavior = .bounce
		bar.indicator.weight = .medium
		bar.layout.interButtonSpacing = 20
		bar.backgroundView.style = .clear
		bar.indicator.backgroundColor = .label
		bar.buttons.customize { (button) in
			button.tintColor = .lightGray
			button.selectedTintColor = .label
			button.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 14)!
		}
		addBar(bar, dataSource: self, at: .custom(view: barContainer, layout: nil))
	}

	@objc func filterGallery(sender: UIButton) {
		self.performSegue(withIdentifier: segues.filterArtCollections.rawValue, sender: self)
	}
}

extension GalleryController {
	func modernArtController() -> ModernArtController {
	  let storyboard = UIStoryboard(name: "Gallery", bundle: .main)
		return storyboard.instantiateViewController(withIdentifier: "ModernArtController") as! ModernArtController
	}

	func grafitiController() -> GrafitiController {
	  let storyboard = UIStoryboard(name: "Gallery", bundle: .main)
		return  storyboard.instantiateViewController(withIdentifier: "GrafitiController") as! GrafitiController
	}

	func abstractController() -> AbstractController {
	  let storyboard = UIStoryboard(name: "Gallery", bundle: .main)
		return storyboard.instantiateViewController(withIdentifier: "AbstractController") as! AbstractController
	}

	func popArtController() -> PopArtController {
	  let storyboard = UIStoryboard(name: "Gallery", bundle: .main)
		return  storyboard.instantiateViewController(withIdentifier: "PopArtController") as! PopArtController
	}

	func paintingsController() -> PaintingsController {
	  let storyboard = UIStoryboard(name: "Gallery", bundle: .main)
		return storyboard.instantiateViewController(withIdentifier: "PaintingsController") as! PaintingsController
	}

	func sculptureController() -> SculptureController {
	  let storyboard = UIStoryboard(name: "Gallery", bundle: .main)
		return  storyboard.instantiateViewController(withIdentifier: "SculptureController") as! SculptureController
	}

	func photographyController() -> PhotographyController {
	  let storyboard = UIStoryboard(name: "Gallery", bundle: .main)
		return storyboard.instantiateViewController(withIdentifier: "PhotographyController") as! PhotographyController
	}
}

extension GalleryController: PageboyViewControllerDataSource, TMBarDataSource {

  func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
	let title = ["Modern Art", "Grafiti", "Abstract", "Pop Art", "Paintings", "Sculpture", "Photography"]
	return TMBarItem(title: title[index])
  }

  func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
	return viewControllers.count
  }

  func viewController(for pageboyViewController: PageboyViewController,
					  at index: PageboyViewController.PageIndex) -> UIViewController? {
	return viewControllers[index]
  }

  func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
	return nil
  }

  func barItem(for tabViewController: TabmanViewController, at index: Int) -> TMBarItemable {
	let title = "Page \(index)"
	return TMBarItem(title: title)
  }
}

extension GalleryController {

	func setUpViews() {
		let filterButtonImage = UIImage(systemName: "slider.horizontal.3", withConfiguration: UIImage.SymbolConfiguration(pointSize: 15, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		filterButton.setImage(filterButtonImage, for: .normal)
		filterButton.addTarget(self, action: #selector(filterGallery), for: .touchDown)

		filterButton.translatesAutoresizingMaskIntoConstraints = false
		searchButton.translatesAutoresizingMaskIntoConstraints = false
		barContainer.translatesAutoresizingMaskIntoConstraints = false

		searchButton.backgroundColor = .clear
		searchButton.layer.cornerRadius = 22.5
		searchButton.setTitle("Search by artist, country, style etc.", for: .normal)
		searchButton.setTitleColor(.label, for: .normal)
		searchButton.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14)

		view.addSubview(barContainer)
		navigationController?.navigationBar.addSubview(searchButton)
		navigationController?.navigationBar.barTintColor = .secondarySystemGroupedBackground
		searchButton.addSubview(filterButton)

		barContainer.backgroundColor = .blue
		guard let navBar = navigationController?.navigationBar else  { return }
		NSLayoutConstraint.activate([
			barContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
			barContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			barContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			barContainer.heightAnchor.constraint(equalToConstant: 50),

			searchButton.trailingAnchor.constraint(equalTo: navBar.trailingAnchor, constant: -10),
			searchButton.leadingAnchor.constraint(equalTo: navBar.leadingAnchor, constant: 10),

			searchButton.heightAnchor.constraint(equalToConstant: 45),
			searchButton.centerXAnchor.constraint(equalTo: navBar.centerXAnchor),
			searchButton.centerYAnchor.constraint(equalTo: navBar.centerYAnchor),

			filterButton.widthAnchor.constraint(equalToConstant: 30),
			filterButton.heightAnchor.constraint(equalToConstant: 30),
			filterButton.trailingAnchor.constraint(equalTo: searchButton.trailingAnchor, constant: -10),
			filterButton.centerYAnchor.constraint(equalTo: searchButton.centerYAnchor)
		])
	}
}
