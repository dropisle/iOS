//
//  AnalyticsEnums.swift
//  DropIsle
//
//  Created by CtanLI on 2020-02-20.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

enum AnalyticsEvent {
	case onBoarding
	case guestUser
	case createAccount
	case signInAccount
	case emailCreateAccountSelected
	case phoneCreateAccountSelected
	case emailSignInAccountSelected
	case phoneSignInAccountSelected
	case signup
	case login
	case emailSignupScreen
	case emailLoginScreen
	case phoneSignupScreen
	case phoneLoginScreen
	case phoneValidationScreen
	case launch
	case tokenRefresh
	case home

	case guestUserSignUpOrDynamicLinkClicked
	case guestUserSignInOptions
	case guestUserSignUpOptions

	case showDetailScreen

	case wishlist
	case addToWishlist
	case removeFromWishlist

	case guestUserCreateOrSignInScreen
	case networkStateOff
	case searchValues
	case search
	case searchResult
	case showFilterScreen
	case showChatScreen
	case filter

	case productDetail
	case makeOfferConfirmed
	case makeOfferClicked
	case shareProduct
	case like
	case unlike
	case mapView
	case showConversation
	case showUserProfile

	case profile
	case settings

	case otherUsersOnSale

	case editProfile
	case updatedRegion

	case listingProduct
	case listedProductConfirmed
	case listedProductEditConfirmed

	case dropCode
	case shareProfile

	case conversation
	case messageNotificationClicked
	case messageNotificationAutomaticTrigger

	var name: String {
		switch self {
			///onBoarding
			case .onBoarding:
				return "PageView:Onboard_Screen"
			case .guestUser:
				return "Action:Guest_User_Home_Screen"
			case .createAccount:
				return "Action:Create_Account"
			case .signInAccount:
				return "Action:sign_In_Account"

			///Account Options
			case .emailCreateAccountSelected:
				return "Action:email_Create_Account_Selected"
			case .phoneCreateAccountSelected:
				return "Action:phone_Create_Account_Selected"
			case .emailSignInAccountSelected:
				return "Action:email_SignIn_AccountSelected"
			case .phoneSignInAccountSelected:
				return "Action:phone_SignIn_AccountSelected"
			case .signup:
				return "Confirmed:Sign_Up_Completed"
			case .login:
				return "Confirmed:Log_In_Completed"
			case .emailSignupScreen:
				return "PageView:Signup_Screen"
			case .emailLoginScreen:
				return "PageView:Login_Screen"
			case .phoneSignupScreen:
				return "PageView:Phone_Signup_Screen"
			case .phoneLoginScreen:
				return "PageView:Phone_Login_Screen"
			case .phoneValidationScreen:
				return "PageView:Phone_Validation_Screen"

			///Launch Screen
			case .launch:
				return "PageView:Launch_Screen"
			case .tokenRefresh:
				return "Action:token_Refreshed"

			///Home Screen
			case .home:
				return "PageView:Home_Screen"
			case .guestUserSignUpOrDynamicLinkClicked:
				return "Action:Guest_User_SignUp_Or_DynamicLink_Clicked"
			case .guestUserCreateOrSignInScreen:
				return "PageView:Guest_User_Create_Or_SignIn_Screen"

			case .guestUserSignInOptions:
					return "Action:Guest_User_SignIn_Options_Clicked"
			case .guestUserSignUpOptions:
					return "Action:Guest_User_SignUp_Options_Clicked"

			case .showDetailScreen:
				return "Action:show_Detail_Screen"

			///Add To Wishlist
			case .wishlist:
			return "PageView:Show_Wishlist_Screen"
			case .addToWishlist:
				return "Action:Add_To_Wishlist"
			case .removeFromWishlist:
				return "Action:Remove_From_Wishlist"

			///Network State
			case .networkStateOff:
				return "Action:Network_State_Off"

			///Searches
			case .search:
				return "PageView:Search_Screen"
			case .searchValues:
				return "Action:Search_Values"
			case .searchResult:
				return "PageView:Search_Result_Screen"

			///Filter
			case .showFilterScreen:
				return "Action:Filter_Screen"

			///Chat
			case .showChatScreen:
				return "Action:Chat_Screen"

			///Filter
			case .filter:
				return "PageView:filter_Screen"

			///Product Detail
			case .productDetail:
				return "PageView:Product_Detail_Screen"
			case .makeOfferConfirmed:
				return "Action: Make_Offer_Confirmed"
			case .makeOfferClicked:
				return "Action: Make_Offer_Clicked"
			case .shareProduct:
				return "Action:Shared_Product"
			case .like:
				return "Action:Like_Product"
			case .unlike:
				return "Action:Unliked_Product"
			case .mapView:
				return "Action:View_Product_Region"
			case .showConversation:
				return "Action:Show_Conversation_Screen"
			case .showUserProfile:
				return "Action:Show_User_Profile"

			///Profile
			case .profile:
				return "PageView:User_Profile_Screen"
			case .settings:
				return "Action:Show_Settings_Screen"

			case .otherUsersOnSale:
				return "PageView:Other_Users_OnSale_Screen"

		 ///Edit product
			case .editProfile:
				return "PageView:Edit_Profile_Screen"
			case .updatedRegion:
				return "Action:Updated_Region"

			///Listing product
			case .listingProduct:
				return "PageView:Listing_Product_Screen"
			case .listedProductConfirmed:
				return "Action:Listed_Product"
			case .listedProductEditConfirmed:
				return "Action:Listed_Product_Edit_Confirmed"

			case .dropCode:
				return "PageView:DropCode_Screen"
			case .shareProfile:
				return "Action:Shared_Dropcode_Screen"

			///Mesaages
			case .conversation:
				return "PageView:Show_Coversation_Screen"
			case .messageNotificationClicked:
				return "Action:Message_Notification_Clicked"
			case .messageNotificationAutomaticTrigger:
				return "Action:Message_Notification_Automatic_Trigger"
		}
	}
}

