//
//  NetworkManager.swift
//  DropIsle
//
//  Created by CtanLI on 2019-09-26.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation
import Alamofire

struct NetworkState {
	var isConnected: Bool {
		// isReachable checks for wwan, ethernet, and wifi, if
		// you only want 1 or 2 of these, the change the .isReachable
		// at the end to one of the other options.
		return NetworkReachabilityManager(host: "www.google.com")!.isReachable
	}
}
