//
//  Analytics.swift
//  DropIsle
//
//  Created by CtanLI on 2019-12-05.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import FirebaseAnalytics
import Amplitude_iOS

protocol LoggerProtocol {
	func log(event: AnalyticsEvent, parameters: [String: Any]?)
}

struct FirebaseLogger : LoggerProtocol {
	func log(event: AnalyticsEvent, parameters: [String : Any]? = nil) {
		Analytics.logEvent(event.name, parameters: parameters)
		Amplitude.instance()?.logEvent(event.name, withEventProperties: parameters)
		AMPIdentify.init().setOnce(event.name, value: parameters as NSObject?)
	}
}

class LoggerManager : LoggerProtocol {
	private var loggers : [LoggerProtocol] = []
	static let shared : LoggerManager = {
		var manager = LoggerManager()
		manager.loggers.append(FirebaseLogger())
		return manager
	}()

	func log(event: AnalyticsEvent, parameters: [String : Any]? = nil) {
		#if DEBUG
		//self.loggers.forEach({ $0.log(event: event, parameters: parameters) })
		#else
		self.loggers.forEach({ $0.log(event: event, parameters: parameters) })
		#endif
	}
}

