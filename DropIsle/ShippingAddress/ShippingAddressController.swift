//
//  ShippingAddressController.swift
//  DropIsle
//
//  Created by CtanLI on 2021-02-13.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import UIKit

class ShippingAddressController: UIViewController, UITextFieldDelegate {

	let scrollView = UIScrollView()
	let containerView = UIView()

	let emailAdd = UILabel()
	let emailTextField = UITextField()

	let emailMessages = UILabel()

	let firstName = UILabel()
	let firstNameField = UITextField()

	let lastName = UILabel()
	let lastNameField = UITextField()

	let mobile = UILabel()
	let mobileField = UITextField()

	let addressFinder = UILabel()
	let addressFinderField = UITextField()

	let address = UILabel()
	let addressField = UITextField()

	let city = UILabel()
	let cityField = UITextField()

	let province = UILabel()
	let provinceField = UITextField()

	let postalCode = UILabel()
	let postalCodeField = UITextField()

	let country = UILabel()
	let countryField = UITextField()

	let firstDivider = UIView()

	let confirmAddress = UIButton()
	var callback : (() -> ())?
	var billingAddressCallback : ((Address) -> ())?
	var addresses: [Address] = []
	var addressToEdit: Address?
	var isToEditAddress = Bool()
	var isFromPaymentScreen = Bool()
	var randomId = String()
	var validation = Validation()

	override func viewDidLoad() {
		super.viewDidLoad()
		emailTextField.setLeftPaddingPoints(10)
		firstNameField.setLeftPaddingPoints(10)
		lastNameField.setLeftPaddingPoints(10)
		mobileField.setLeftPaddingPoints(10)
		addressFinderField.setLeftPaddingPoints(10)
		cityField.setLeftPaddingPoints(10)
		addressField.setLeftPaddingPoints(10)
		provinceField.setLeftPaddingPoints(10)
		countryField.setLeftPaddingPoints(10)
		postalCodeField.setLeftPaddingPoints(10)

		confirmAddress.addTarget(self, action: #selector(validateForm), for: .touchUpInside)
		emailTextField.delegate = self
		postalCodeField.delegate = self
		mobileField.delegate = self

		emailTextField.keyboardType = .emailAddress
		mobileField.keyboardType = .phonePad

		setUp()

		//
		if let addressToEdit = addressToEdit {
			updateddressField(address: addressToEdit)
		}
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		//clear address data when leaving screen
		callback?()
	}

	@objc func validateForm() {
		if  let email = emailTextField.text,
			let fName = firstNameField.text,
			let lName = lastNameField.text,
			let _ = mobileField.text,
			let addressValue = addressField.text,
			let city = cityField.text,
			let province = provinceField.text,
			let country = countryField.text,
			let postalCode = postalCodeField.text {

			if validation.validateAnyOtherTextField(otherField: email) ||  !validation.validateEmailId(emailID: email) { self.alert(message: "Please enter a valid email address"); return }
			if validation.validateAnyOtherTextField(otherField: fName) { self.alert(message: "First name cannot be empty"); return }
			if validation.validateAnyOtherTextField(otherField: lName) { self.alert(message: "Last name cannot be empty"); return }
			//if validation.validateAnyOtherTextField(otherField: mobileNum) { self.alert(message: "Mobile cannot be empty"); return }
			if validation.validateAnyOtherTextField(otherField: addressValue) { self.alert(message: "Address cannot be empty"); return }
			if validation.validateAnyOtherTextField(otherField: city) { self.alert(message: "City cannot be empty"); return }
			if validation.validateAnyOtherTextField(otherField: province) { self.alert(message: "Province cannot be empty"); return }
			if validation.validateAnyOtherTextField(otherField: postalCode) { self.alert(message: "Postal Code cannot be empty"); return }
			if validation.validateAnyOtherTextField(otherField: country) { self.alert(message: "Country cannot be empty"); return }

			//
			confirmAddressAction()
		}
	}

	func confirmAddressAction() {
		if  let email = emailTextField.text,
			let fName = firstNameField.text,
			let lName = lastNameField.text,
			let mobileNum = mobileField.text,
			let addressValue = addressField.text,
			let city = cityField.text,
			let province = provinceField.text,
			let country = countryField.text,
			let postalCode = postalCodeField.text {

			if !isToEditAddress {
				randomId = String(arc4random())
			}
			let address = Address(addressId: randomId, email: email, firstName: fName, lastName: lName, address: addressValue, mobile: mobileNum, city: city, province: province, country: country, postalCode: postalCode)
			//
			isFromPaymentScreen ? billingAddressCallback?(address) : loadAddress(address: address)
		}
		callback?()
		navigationController?.popViewController(animated: true)
	}

	func loadAddress(address: Address) {
		guard let addressData = UserDefaults.standard.object(forKey: "address") as? NSData else {
			addresses.append(address)
			saveAddress(address: addresses)
			return
		}

		do {
			guard let addressArray = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(addressData as Data) as? [Address] else { return }
			addresses.append(contentsOf: addressArray)
			let _ = addresses.compactMap({ values in
				if let index = addresses.firstIndex(where: { $0.addressId == address.addressId }) {
					addresses.remove(at: index)
				}
			})
			addresses.append(address)
			saveAddress(address: addresses)
		} catch {
			print(error.localizedDescription)
		}
	}

	func saveAddress(address: [Address]) {
		do {
			let placesData = try NSKeyedArchiver.archivedData(withRootObject: address, requiringSecureCoding: false)
			UserDefaults.standard.set(placesData, forKey: "address")
		} catch {
			print(error.localizedDescription)
		}
	}

	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		guard let currentText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else { return true }
		if textField == postalCodeField {
			guard currentText.count != 8 else { return false }
			textField.setText(to: currentText.grouping(every: 3, with: " "), preservingCursor: true)
			return false
		}

		if textField == mobileField {
			 textField.text = format(with: "(XXX) XXX-XXXX", phone: currentText)
			 return false
		}
		return true
	}

	//mask example: `+X (XXX) XXX-XXXX`
	func format(with mask: String, phone: String) -> String {
		let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
		var result = ""
		var index = numbers.startIndex // numbers iterator

		// iterate over the mask characters until the iterator of numbers ends
		for ch in mask where index < numbers.endIndex {
			if ch == "X" {
				// mask requires a number in this place, so take the next one
				result.append(numbers[index])

				// move numbers iterator to the next index
				index = numbers.index(after: index)

			} else {
				result.append(ch) // just append a mask character
			}
		}
		return result
	}
}

extension ShippingAddressController {
	func updateddressField(address: Address) {
		emailTextField.text = address.email
		firstNameField.text = address.firstName
		lastNameField.text = address.lastName
		mobileField.text = address.mobile
		addressField.text = address.address
		cityField.text = address.city
		provinceField.text = address.province
		countryField.text = address.country
		postalCodeField.text = address.postalCode
		randomId = address.addressId
	}
}

extension ShippingAddressController {
	func setUp() {
		emailAdd.translatesAutoresizingMaskIntoConstraints = false
		emailTextField.translatesAutoresizingMaskIntoConstraints = false
		scrollView.translatesAutoresizingMaskIntoConstraints = false
		containerView.translatesAutoresizingMaskIntoConstraints = false
		confirmAddress.translatesAutoresizingMaskIntoConstraints = false

		emailMessages.translatesAutoresizingMaskIntoConstraints = false
		firstDivider.translatesAutoresizingMaskIntoConstraints = false

		firstName.translatesAutoresizingMaskIntoConstraints = false
		firstNameField.translatesAutoresizingMaskIntoConstraints = false

		lastName.translatesAutoresizingMaskIntoConstraints = false
		lastNameField.translatesAutoresizingMaskIntoConstraints = false

		mobile.translatesAutoresizingMaskIntoConstraints = false
		mobileField.translatesAutoresizingMaskIntoConstraints = false

		addressFinder.translatesAutoresizingMaskIntoConstraints = false
		addressFinderField.translatesAutoresizingMaskIntoConstraints = false

		address.translatesAutoresizingMaskIntoConstraints = false
		addressField.translatesAutoresizingMaskIntoConstraints = false

		city.translatesAutoresizingMaskIntoConstraints = false
		cityField.translatesAutoresizingMaskIntoConstraints = false

		province.translatesAutoresizingMaskIntoConstraints = false
		provinceField.translatesAutoresizingMaskIntoConstraints = false

		postalCode.translatesAutoresizingMaskIntoConstraints = false
		postalCodeField.translatesAutoresizingMaskIntoConstraints = false

		country.translatesAutoresizingMaskIntoConstraints = false
		countryField.translatesAutoresizingMaskIntoConstraints = false

		view.addSubview(scrollView)
		scrollView.addSubview(containerView)
		containerView.addSubview(emailAdd)
		containerView.addSubview(emailTextField)
		containerView.addSubview(confirmAddress)

		containerView.addSubview(emailMessages)
		containerView.addSubview(firstDivider)

		containerView.addSubview(firstName)
		containerView.addSubview(firstNameField)

		containerView.addSubview(lastName)
		containerView.addSubview(lastNameField)

		containerView.addSubview(mobile)
		containerView.addSubview(mobileField)

		containerView.addSubview(addressFinder)
		containerView.addSubview(addressFinderField)

		containerView.addSubview(address)
		containerView.addSubview(addressField)

		containerView.addSubview(city)
		containerView.addSubview(cityField)

		containerView.addSubview(province)
		containerView.addSubview(provinceField)

		containerView.addSubview(postalCode)
		containerView.addSubview(postalCodeField)

		containerView.addSubview(country)
		containerView.addSubview(countryField)

		NSLayoutConstraint.activate([

			scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			scrollView.widthAnchor.constraint(equalTo: view.widthAnchor),
			scrollView.topAnchor.constraint(equalTo: view.topAnchor),
			scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

			containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
			containerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
			containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
			containerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),

			emailAdd.heightAnchor.constraint(equalToConstant: 25),
			emailAdd.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 25),
			emailAdd.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			emailAdd.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			emailTextField.heightAnchor.constraint(equalToConstant: 40),
			emailTextField.topAnchor.constraint(equalTo: emailAdd.bottomAnchor, constant: 5),
			emailTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			emailTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			emailMessages.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 5),
			emailMessages.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			emailMessages.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			firstDivider.heightAnchor.constraint(equalToConstant: 0.3),
			firstDivider.topAnchor.constraint(equalTo: emailMessages.bottomAnchor, constant: 8),
			firstDivider.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
			firstDivider.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),

			firstName.heightAnchor.constraint(equalToConstant: 25),
			firstName.topAnchor.constraint(equalTo: firstDivider.bottomAnchor, constant: 10),
			firstName.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			firstName.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			firstNameField.heightAnchor.constraint(equalToConstant: 40),
			firstNameField.topAnchor.constraint(equalTo: firstName.bottomAnchor, constant: 5),
			firstNameField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			firstNameField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			lastName.heightAnchor.constraint(equalToConstant: 25),
			lastName.topAnchor.constraint(equalTo: firstNameField.bottomAnchor, constant: 10),
			lastName.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			lastName.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			lastNameField.heightAnchor.constraint(equalToConstant: 40),
			lastNameField.topAnchor.constraint(equalTo: lastName.bottomAnchor, constant: 5),
			lastNameField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			lastNameField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			mobile.heightAnchor.constraint(equalToConstant: 25),
			mobile.topAnchor.constraint(equalTo: lastNameField.bottomAnchor, constant: 10),
			mobile.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			mobile.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			mobileField.heightAnchor.constraint(equalToConstant: 40),
			mobileField.topAnchor.constraint(equalTo: mobile.bottomAnchor, constant: 5),
			mobileField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			mobileField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			addressFinder.heightAnchor.constraint(equalToConstant: 25),
			addressFinder.topAnchor.constraint(equalTo: mobileField.bottomAnchor, constant: 10),
			addressFinder.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			addressFinder.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			addressFinderField.heightAnchor.constraint(equalToConstant: 40),
			addressFinderField.topAnchor.constraint(equalTo: addressFinder.bottomAnchor, constant: 5),
			addressFinderField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			addressFinderField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			address.heightAnchor.constraint(equalToConstant: 25),
			address.topAnchor.constraint(equalTo: addressFinderField.bottomAnchor, constant: 10),
			address.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			address.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			addressField.heightAnchor.constraint(equalToConstant: 40),
			addressField.topAnchor.constraint(equalTo: address.bottomAnchor, constant: 5),
			addressField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			addressField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			city.heightAnchor.constraint(equalToConstant: 25),
			city.topAnchor.constraint(equalTo: addressField.bottomAnchor, constant: 10),
			city.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			city.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			cityField.heightAnchor.constraint(equalToConstant: 40),
			cityField.topAnchor.constraint(equalTo: city.bottomAnchor, constant: 5),
			cityField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			cityField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),


			province.heightAnchor.constraint(equalToConstant: 25),
			province.topAnchor.constraint(equalTo: cityField.bottomAnchor, constant: 10),
			province.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			province.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			provinceField.heightAnchor.constraint(equalToConstant: 40),
			provinceField.topAnchor.constraint(equalTo: province.bottomAnchor, constant: 5),
			provinceField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			provinceField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			postalCode.heightAnchor.constraint(equalToConstant: 25),
			postalCode.topAnchor.constraint(equalTo: provinceField.bottomAnchor, constant: 10),
			postalCode.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			postalCode.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			postalCodeField.heightAnchor.constraint(equalToConstant: 40),
			postalCodeField.topAnchor.constraint(equalTo: postalCode.bottomAnchor, constant: 5),
			postalCodeField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			postalCodeField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			country.heightAnchor.constraint(equalToConstant: 25),
			country.topAnchor.constraint(equalTo: postalCodeField.bottomAnchor, constant: 10),
			country.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			country.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			countryField.heightAnchor.constraint(equalToConstant: 40),
			countryField.topAnchor.constraint(equalTo: country.bottomAnchor, constant: 5),
			countryField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			countryField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			confirmAddress.heightAnchor.constraint(equalToConstant: 45),
			confirmAddress.topAnchor.constraint(equalTo: countryField.bottomAnchor, constant: 10),
			confirmAddress.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20),
			confirmAddress.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			confirmAddress.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
		])

		firstDivider.backgroundColor = .lightGray

		emailAdd.text = "Add Email Address"
		emailAdd.textColor = .label
		emailAdd.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		emailTextField.borderColor = .lightGray
		emailTextField.borderWidth = 0.2
		emailTextField.cornerRadius = 5
		emailTextField.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		firstName.text = "First Name"
		firstName.textColor = .label
		firstName.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		firstNameField.borderColor = .lightGray
		firstNameField.borderWidth = 0.2
		firstNameField.cornerRadius = 5
		firstNameField.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		lastName.text = "Last Name"
		lastName.textColor = .label
		lastName.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		lastNameField.borderColor = .lightGray
		lastNameField.borderWidth = 0.2
		lastNameField.cornerRadius = 5
		lastNameField.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		mobile.text = "Mobile"
		mobile.textColor = .label
		mobile.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		mobileField.borderColor = .lightGray
		mobileField.borderWidth = 0.2
		mobileField.cornerRadius = 5
		mobileField.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		addressFinder.text = "Address Finder"
		addressFinder.textColor = .label
		addressFinder.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		addressFinderField.borderColor = .lightGray
		addressFinderField.borderWidth = 0.2
		addressFinderField.cornerRadius = 5
		addressFinderField.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		address.text = "Address"
		address.textColor = .label
		address.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		addressField.borderColor = .lightGray
		addressField.borderWidth = 0.2
		addressField.cornerRadius = 5
		addressField.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		city.text = "City"
		city.textColor = .label
		city.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		cityField.borderColor = .lightGray
		cityField.borderWidth = 0.2
		cityField.cornerRadius = 5
		cityField.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		province.text = "Province"
		province.textColor = .label
		province.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		provinceField.borderColor = .lightGray
		provinceField.borderWidth = 0.2
		provinceField.cornerRadius = 5
		provinceField.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		postalCode.text = "Postal Code"
		postalCode.textColor = .label
		postalCode.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		postalCodeField.borderColor = .lightGray
		postalCodeField.borderWidth = 0.2
		postalCodeField.cornerRadius = 5
		postalCodeField.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		country.text = "Country"
		country.textColor = .label
		country.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		countryField.borderColor = .lightGray
		countryField.borderWidth = 0.2
		countryField.cornerRadius = 5
		countryField.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		emailMessages.text = "we will send you order notification to this Address"
		emailMessages.textColor = .label
		emailMessages.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 12)

		confirmAddress.cornerRadius = 7
		confirmAddress.borderColor = .secondarySystemGroupedBackground
		confirmAddress.borderWidth = 2
		confirmAddress.setTitle("Save this Address", for: .normal)
		confirmAddress.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 16)
		confirmAddress.backgroundColor = UIColor.NavyBlueDark
	}
}

