//
//  AddShippingAddressController.swift
//  DropIsle
//
//  Created by CtanLI on 2021-03-15.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import UIKit

class AddShippingAddressController: UIViewController, UITableViewDataSource, UITableViewDelegate {

	// MARK: - Properties -
	var tableView: UITableView!
	var cellHeights: [CGFloat] = []
	var addresses: [Address] = []
	var isToEditAddress = Bool()
	var addressToEdit: Address?
	
    override func viewDidLoad() {
        super.viewDidLoad()
		title = "Shipping address"

		setUpTableView()

		//
		loadAddress()
		cellHeights = ( 0..<addresses.count).map { _ in 120 }
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

	}

	func loadAddress() {
		guard let addressData = UserDefaults.standard.object(forKey: "address") as? NSData else { return }
		do {
			guard let addressArray = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(addressData as Data) as? [Address] else { return }
			addresses.removeAll()
			addresses.append(contentsOf: addressArray)
		} catch {
			print(error.localizedDescription)
		}
	}

	func setUpTableView() {
		tableView = UITableView.init(frame: .zero, style: .grouped)
		tableView.dataSource = self
		tableView.delegate = self
		tableView.separatorStyle = .singleLine
		tableView.separatorInset = UIEdgeInsets.zero
		tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.sectionHeaderHeight = .leastNormalMagnitude
		tableView.backgroundColor = .secondarySystemGroupedBackground
		tableView.estimatedRowHeight = 120
		tableView.rowHeight = UITableView.automaticDimension
		view.addSubview(tableView)

		NSLayoutConstraint.activate([
			tableView.topAnchor.constraint(equalTo: view.topAnchor),
			tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])

		tableView.register(AddShippingAddressCell.self, forCellReuseIdentifier: AddShippingAddressCell.identifier)
		tableView.register(AddShippingAddressFooter.self, forHeaderFooterViewReuseIdentifier: AddShippingAddressFooter.identifier)
	}

    // MARK: - Table view data source

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
		return addresses.count
    }

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: AddShippingAddressCell.identifier ) as! AddShippingAddressCell
		cell.selectionStyle = .none
		cell.addressValues = addresses[indexPath.row]
		cell.delegate = self
		cell.editAddress.isHidden = true
		cell.deliverToAddress.isHidden = true
		return cell
	}

	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		let footerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: AddShippingAddressFooter.identifier) as! AddShippingAddressFooter
		footerView.delegate = self
		addresses.count == 0 ? footerView.addNewAddress.setTitle("Add Shipping Address", for: .normal) : footerView.addNewAddress.setTitle("Add a New Address", for: .normal)
		return footerView
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		//tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
		let cell = tableView.cellForRow(at: indexPath) as? AddShippingAddressCell
		cell?.editAddress.isHidden = false
		cell?.deliverToAddress.isHidden = false
		cellHeights[indexPath.row] = 210
		UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: { () -> Void in
			tableView.beginUpdates()
			tableView.endUpdates()
		}, completion: nil)
	}

	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		//tableView.cellForRow(at: indexPath)?.accessoryType = .none
		let cell = tableView.cellForRow(at: indexPath) as? AddShippingAddressCell
		cell?.editAddress.isHidden = true
		cell?.deliverToAddress.isHidden = true
		cellHeights[indexPath.row] = 120
		UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: { () -> Void in
			tableView.beginUpdates()
			tableView.endUpdates()
		}, completion: nil)
	}

	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		guard let cell = tableView.cellForRow(at: indexPath) as? AddShippingAddressCell else { return }
		if editingStyle == .delete {
			addresses.remove(at: indexPath.row)
			tableView.deleteRows(at: [indexPath], with: .fade)

			//
			let _ = addresses.compactMap({ values in
				if let index = addresses.firstIndex(where: { $0.addressId == cell.addressValues?.addressId }) {
					addresses.remove(at: index)
				}
			})

			//
			saveAddress(address: addresses)
		} else if editingStyle == .insert {
			// Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
		}
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return cellHeights[indexPath.row]
	}

	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return CGFloat.leastNormalMagnitude
	}

	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 80
	}

	func saveAddress(address: [Address]) {
		do {
			let placesData = try NSKeyedArchiver.archivedData(withRootObject: address, requiringSecureCoding: false)
			UserDefaults.standard.set(placesData, forKey: "address")
		} catch {
			print(error.localizedDescription)
		}
	}

     //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier ==  segues.shippingAddress.rawValue {
			let shippingAddress = segue.destination as! ShippingAddressController
			shippingAddress.addressToEdit = addressToEdit
			shippingAddress.isToEditAddress = isToEditAddress
			shippingAddress.callback = { [weak self] in
				self?.addressToEdit = nil
				//
				self?.loadAddress()
				if let add = self?.addresses {
					self?.cellHeights = (0..<add.count).map { _ in 120 }
				}
				self?.tableView.reloadData()
			}
		}

		if segue.identifier ==  segues.payment.rawValue {
			let billingAddress = segue.destination as! PaymentController
			billingAddress.addressDetails = addressToEdit
			billingAddress.shippingAddressDetails = addressToEdit
		}
    }
}

extension AddShippingAddressController: AddNewAddressDelegate {
	func paymentAction(cell: AddShippingAddressCell) {
		addressToEdit = cell.addressValues
		self.performSegue(withIdentifier: segues.payment.rawValue, sender: self)
	}

	func editAddressAction(isToEdit: Bool, cell: AddShippingAddressCell) {
		isToEditAddress = isToEdit
		addressToEdit = cell.addressValues
		self.performSegue(withIdentifier: segues.shippingAddress.rawValue, sender: self)
	}

	func addNewAddressAction() {
		self.performSegue(withIdentifier: segues.shippingAddress.rawValue, sender: self) 
	}
}
