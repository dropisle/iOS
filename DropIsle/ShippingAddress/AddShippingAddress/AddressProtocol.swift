//
//  AddressProtocol.swift
//  DropIsle
//
//  Created by CtanLI on 2021-03-16.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import Foundation

protocol AddNewAddressDelegate {
	func addNewAddressAction()
	func editAddressAction(isToEdit: Bool, cell: AddShippingAddressCell)
	func paymentAction(cell: AddShippingAddressCell)
}
