//
//  AddShippingAddressCell.swift
//  DropIsle
//
//  Created by CtanLI on 2021-03-15.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import UIKit


class AddShippingAddressCell: UITableViewCell, Reusable {

	let userName = UILabel()
	let address = UILabel()
	let areaAddress = UILabel()
	let country = UILabel()
	let editAddress = UIButton()
	let deliverToAddress = UIButton()
	let containerView = UIView()
	
	var delegate: AddNewAddressDelegate?

	var addressValues: Address? {
		didSet {
			userName.text = "\(addressValues?.firstName ?? "")" + " \(addressValues?.lastName ?? "")"
			address.text = addressValues?.address
			areaAddress.text =  "\(addressValues?.city ?? "")," + "\(addressValues?.province ?? "")" + " \(addressValues?.postalCode ?? "")"
			country.text = addressValues?.country
		}
	}

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		//Initialization code
		editAddress.addTarget(self, action: #selector(editAddressAction), for: .touchUpInside)
		deliverToAddress.addTarget(self, action: #selector(paymentAction), for: .touchUpInside)
		deliverToAddress.isHidden = true
		editAddress.isHidden = true
		setup()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	@objc func editAddressAction() {
		delegate?.editAddressAction(isToEdit: true, cell: self)
	}

	@objc func paymentAction() {
		delegate?.paymentAction(cell: self)
	}

	func setup() {
		userName.translatesAutoresizingMaskIntoConstraints = false
		address.translatesAutoresizingMaskIntoConstraints = false
		areaAddress.translatesAutoresizingMaskIntoConstraints = false
		country.translatesAutoresizingMaskIntoConstraints = false
		editAddress.translatesAutoresizingMaskIntoConstraints = false
		deliverToAddress.translatesAutoresizingMaskIntoConstraints = false
		containerView.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(userName)
		contentView.addSubview(address)
		contentView.addSubview(areaAddress)
		contentView.addSubview(country)
		contentView.addSubview(editAddress)
		contentView.addSubview(deliverToAddress)

		NSLayoutConstraint.activate([
			userName.topAnchor.constraint(equalTo: topAnchor, constant: 10),
			userName.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			userName.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -40),

			address.topAnchor.constraint(equalTo: userName.bottomAnchor, constant: 7),
			address.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			address.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -40),

			areaAddress.topAnchor.constraint(equalTo: address.bottomAnchor, constant: 7),
			areaAddress.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			areaAddress.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -40),

			country.topAnchor.constraint(equalTo: areaAddress.bottomAnchor, constant: 7),
			country.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			country.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -40),

			deliverToAddress.heightAnchor.constraint(equalToConstant: 42),
			deliverToAddress.topAnchor.constraint(equalTo: country.topAnchor, constant: 25),
			deliverToAddress.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 40),
			deliverToAddress.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -40),

			editAddress.topAnchor.constraint(equalTo: deliverToAddress.bottomAnchor, constant: 5),
			editAddress.heightAnchor.constraint(equalToConstant: 42),
			editAddress.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 40),
			editAddress.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -40),
			//editAddress.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
		])

		containerView.backgroundColor = .red

		userName.text = "Li Chang"
		userName.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		userName.textColor = .label

		address.text = "800 Allen Dr"
		address.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		address.textColor = .label

		areaAddress.text = "Toronto, Ontario M4R 3t4"
		areaAddress.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		areaAddress.textColor = .label

		country.text = "Canada"
		country.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		country.textColor = .label

		editAddress.cornerRadius = 7
		editAddress.borderColor = .secondarySystemGroupedBackground
		editAddress.borderWidth = 2
		editAddress.setTitle("Edit Adrress", for: .normal)
		editAddress.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)
		editAddress.backgroundColor = UIColor.NavyBlueDark

		deliverToAddress.cornerRadius = 7
		deliverToAddress.borderColor = .secondarySystemGroupedBackground
		deliverToAddress.borderWidth = 2
		deliverToAddress.setTitle("Deliver to this Adrress", for: .normal)
		deliverToAddress.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 16)
		deliverToAddress.backgroundColor = UIColor.BuyNowColor
	}
}
