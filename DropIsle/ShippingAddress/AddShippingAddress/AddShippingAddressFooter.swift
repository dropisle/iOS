//
//  AddShippingAddressFooter.swift
//  DropIsle
//
//  Created by CtanLI on 2021-03-15.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import UIKit

class AddShippingAddressFooter: UITableViewHeaderFooterView, Reusable {

	let addNewAddress = UIButton()
	var delegate: AddNewAddressDelegate?

	override init(reuseIdentifier: String?) {
		super.init(reuseIdentifier: reuseIdentifier)
		self.contentView.backgroundColor = .secondarySystemGroupedBackground
		addNewAddress.addTarget(self, action: #selector(addNewAddressAction), for: .touchUpInside)
		setUp()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	@objc func addNewAddressAction() {
		delegate?.addNewAddressAction()
	}
	
	func setUp() {
		addNewAddress.translatesAutoresizingMaskIntoConstraints = false
		contentView.addSubview(addNewAddress)

		NSLayoutConstraint.activate([
			addNewAddress.heightAnchor.constraint(equalToConstant: 55),
			addNewAddress.topAnchor.constraint(equalTo: topAnchor, constant: 10),
			addNewAddress.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			addNewAddress.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
		])

		addNewAddress.cornerRadius = 7
		addNewAddress.borderColor = .secondarySystemGroupedBackground
		addNewAddress.borderWidth = 2
		addNewAddress.setTitle("Add a New Address", for: .normal)
		addNewAddress.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16)
		addNewAddress.backgroundColor = UIColor.NavyBlueDark
	}
}
