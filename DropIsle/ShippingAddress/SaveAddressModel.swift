//
//  SaveAddressModel.swift
//  DropIsle
//
//  Created by CtanLI on 2021-03-27.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import Foundation

class Address: NSObject, NSCoding {
	var addressId: String
	var email: String
	var firstName: String
	var lastName: String
	var address: String
	var mobile: String
	var city: String
	var province: String
	var country: String
	var postalCode: String

	init(addressId: String, email: String, firstName: String, lastName: String, address: String, mobile: String, city: String, province: String, country: String, postalCode: String) {
		self.email = email
		self.firstName = firstName
		self.lastName = lastName
		self.address = address
		self.mobile = mobile
		self.city = city
		self.province = province
		self.country = country
		self.postalCode = postalCode
		self.addressId = addressId
	}

	required init?(coder aDecoder: NSCoder) {
		self.addressId = aDecoder.decodeObject(forKey: "addressId") as? String ?? ""
		self.email = aDecoder.decodeObject(forKey: "email") as? String ?? ""
		self.firstName = aDecoder.decodeObject(forKey: "firstName") as? String ?? ""
		self.lastName = aDecoder.decodeObject(forKey: "lastName") as? String ?? ""
		self.address = aDecoder.decodeObject(forKey: "address") as? String ?? ""
		self.mobile = aDecoder.decodeObject(forKey: "mobile") as? String ?? ""
		self.city = aDecoder.decodeObject(forKey: "city") as? String ?? ""
		self.province = aDecoder.decodeObject(forKey: "province") as? String ?? ""
		self.country = aDecoder.decodeObject(forKey: "country") as? String ?? ""
		self.postalCode = aDecoder.decodeObject(forKey: "postalCode") as? String ?? ""
	}

	func encode(with aCoder: NSCoder) {
		aCoder.encode(addressId, forKey: "addressId")
		aCoder.encode(email, forKey: "email")
		aCoder.encode(firstName, forKey: "firstName")
		aCoder.encode(lastName, forKey: "lastName")
		aCoder.encode(address, forKey: "address")
		aCoder.encode(mobile, forKey: "mobile")
		aCoder.encode(city, forKey: "city")
		aCoder.encode(province, forKey: "province")
		aCoder.encode(country, forKey: "country")
		aCoder.encode(postalCode, forKey: "postalCode")
	}
}
