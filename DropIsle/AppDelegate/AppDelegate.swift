//
//  AppDelegate.swift
//  DropIsle
//
//  Created by CtanLI on 2018-11-10.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import FirebaseDynamicLinks
import UserNotifications
import KeychainAccess
import Amplitude_iOS
import GooglePlaces
import FirebaseInstallations
import Stripe

enum Enviroment: String {
	case development = "http://94.237.48.208/api"
	case production = "https://service.dropisle.com/api"
	case none = "none"
}
var enviroment: Enviroment = .none

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UIViewControllerTransitioningDelegate {

	var window: UIWindow?
	var endPointURL: String?
	let gcmMessageIDKey = "gcm.message_id"
	var firInstanceIDTokenRefresh = "firInstanceIDTokenRefresh"
	var trending = "Picks for you"
	var keychain = Keychain()

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		FirebaseApp.configure()
		//AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(true)
		Amplitude.instance()?.trackingSessionEvents = true
		Amplitude.instance()?.initializeApiKey("0a51261015b2a2c814e1fa30471e735a")
		GMSPlacesClient.provideAPIKey("AIzaSyDvnMWxzMMTsUrSG0LECoMn9ZyAShZjRbQ")
		//AIzaSyDcPiAoxNd1A8isi6s6CkKgQmovQGzyBng
		Stripe.setDefaultPublishableKey("pk_test_51HZyvvLoD6WUdDINzVpDyyItmFcpt33rJneFtv5IaksX2oWdBEMNghj5UoLlFXs9mmAc2tb8ZZDyJO7S4sdLW6wC00WyLa9tmk")

		UNUserNotificationCenter.current().delegate = self

		// [START set_messaging_delegate]
		Messaging.messaging().delegate = self
		// [END set_messaging_delegate]storedfilterArrays

		// Configure enviroments
		#if DEVELOPMENT
			enviroment = .development
		#else
			enviroment = .production
		#endif

		switch enviroment {
			case .development:
				endPointURL = Enviroment.development.rawValue
			case .production:
				endPointURL = Enviroment.production.rawValue
			case .none: break
		}

		// [START add_token_refresh_observer]
		// Add observer for InstanceID token refresh callback.
		NotificationCenter.default.addObserver(self,
												 selector: #selector(self.tokenRefreshNotification),
												 name: NSNotification.Name(rawValue: firInstanceIDTokenRefresh),
												 object: nil)
		// [END add_token_refresh_observer]

		//Disabling dark mode
		if #available(iOS 13.0, *) {
			// window?.overrideUserInterfaceStyle = .light
		} else {
			// Fallback on earlier versions
		}
		
		//
		IQKeyboardManager.shared.enable = true
		return true
	}

	func handleNotificationWhenAppIsKilled(_ launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
		// Check if launched from the remote notification and application is close
		if let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: AnyObject] {
			if let _ = userInfo["aps"] as? NSDictionary {
			}
		}

		if let _ = launchOptions?[.remoteNotification] as?  [AnyHashable: Any] {
		}
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
	}

	func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
		Messaging.messaging().apnsToken = deviceToken as Data
	}

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
			print("Disconnected from FCM.")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
			// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
			// Set default products when application becomes active

			///UIApplication.shared.applicationIconBadgeNumber = 0
			preferences[.categorySelected] = trending
			connectToFcm()
    }

    func applicationWillTerminate(_ application: UIApplication) {
			// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
			// Saves changes in the application's managed object context before the application terminates.

			//Set the value for reload to default if app is terminated
			preferences[.categorySelected] = trending
			PersistanceService.shared.saveContext()
    }

	func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
		// If you are receiving a notification message while your app is in the background,
		// this callback will not be fired till the user taps on the notification launching the application.
		// TODO: Handle data of notification

		// With swizzling disabled you must let Messaging know about the message, for Analytics
		Messaging.messaging().appDidReceiveMessage(userInfo)

		// Print message ID.
		if let messageID = userInfo[gcmMessageIDKey] {
			print("Message ID: \(messageID)")
		}

		// Print full message.
		print(userInfo)
	}

	func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
					 fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
		// If you are receiving a notification message while your app is in the background,
		// this callback will not be fired till the user taps on the notification launching the application.
		// TODO: Handle data of notification

		// With swizzling disabled you must let Messaging know about the message, for Analytics
		// Messaging.messaging().appDidReceiveMessage(userInfo)

		// Print message ID.
		if let messageID = userInfo[gcmMessageIDKey] {
			print("Message ID: \(messageID)")
		}
		// Print full message.
		print(userInfo)
		completionHandler(UIBackgroundFetchResult.newData)
	}

	@objc func tokenRefreshNotification(_ notification: Notification) {
		InstanceID.instanceID().instanceID { (result, error) in
			if let result = result {
				print("Remote instance ID token: \(result.token)")
			} else if let error = error {
				print("Error fetching remote instange ID: \(error)")
			}
		}

		let token = Messaging.messaging().fcmToken
		print("FCM token: \(token ?? "")")

		//Connect to FCM since connection may have failed when attempted before having a token.
		connectToFcm()
	}

	func connectToFcm() {
		InstanceID.instanceID().instanceID { (result, error) in
			if let result = result {
				print("Remote instance ID token: \(result.token)")
				self.keychain["FCMToken"] = result.token
			} else if let error = error {
				print("Error fetching remote instange ID: \(error)")
			}
		}

		//Disconnect previous FCM connection if it exists.
		Messaging.messaging().shouldEstablishDirectChannel = false
	}
}

@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {

	//Receive displayed notifications for iOS 10 devices.
	func userNotificationCenter(_ center: UNUserNotificationCenter,
								willPresent notification: UNNotification,
								withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
		completionHandler(UNNotificationPresentationOptions.alert)
		let userInfo = notification.request.content.userInfo

		let content = notification.request.content
		if let _ = content.badge as? Int {
			///UIApplication.shared.applicationIconBadgeNumber = badge
		}

		// With swizzling disabled you must let Messaging know about the message, for Analytics
		Messaging.messaging().appDidReceiveMessage(userInfo)

		// Print message ID.
		if let messageID = userInfo[gcmMessageIDKey] {
			print("Message ID: \(messageID)")
		}

		//post notification with info (url)
		NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceivedMessage"), object: nil, userInfo: userInfo)

		// Change this to your preferred presentation option
		completionHandler([])
	}

	func userNotificationCenter(_ center: UNUserNotificationCenter,
								didReceive response: UNNotificationResponse,
								withCompletionHandler completionHandler: @escaping () -> Void) {
		let userInfo = response.notification.request.content.userInfo
		if let messageID = userInfo[gcmMessageIDKey] { print("Message ID: \(messageID)") 	}
		if let rootViewController = UIApplication.topViewController() {
			 if (rootViewController.isKind(of: ConversationViewController.self)) {
				let params: [String: Bool] = ["isNotificationClicked": true]
				NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceivedMessage"), object: params, userInfo: userInfo)
				completionHandler()
			} else {
				DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
					guard let from = userInfo[AnyHashable("fromuser")] as? String, let to = userInfo[AnyHashable("touser")] as? String, let productId = userInfo[AnyHashable("productid")] as? String  else { return }
					guard let aps = userInfo["aps"] as? NSDictionary, let alert = aps["alert"] as? NSDictionary, let _ = alert["body"] as? String, let title = alert["title"] as? String else { return }
					let conversationController = ConversationViewController()
					///conversationController.MessageNotified(fromUser: from, toUser: to, productId: productId, title: title)
					if let tabController = self.window?.rootViewController as? LaunchScreenController {
						self.window?.makeKeyAndVisible()
						let navController = (tabController.presentedViewController as? TabBarViewController)?.selectedViewController as? UINavigationController
						navController?.pushViewController(conversationController, animated: true)
						completionHandler()
					}
				}
			}
		}
	}
}

extension AppDelegate: MessagingDelegate {

	func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
		// TODO: subscribe to topics here
	}

	func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
		print("Firebase registration token: \(fcmToken)")
		DispatchQueue.main.async {
			Messaging.messaging().subscribe(toTopic: "News") { _ in
				print("Subscribed to dropislePushNotification topic")
			}
		}
		connectToFcm()
	}
	// [END refresh_token]
	// [START ios_10_data_message]
	// Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
	// To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
	func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
		print("Received data message: \(remoteMessage.appData)")

		guard let data =
			try? JSONSerialization.data(withJSONObject: remoteMessage.appData, options: .prettyPrinted),
			let prettyPrinted = String(data: data, encoding: .utf8) else {
				return
		}
		print("Received direct channel message:\n\(prettyPrinted)")
	}
}

extension AppDelegate {

	func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
		print("Incoming url \(url.absoluteString)")
		if let link = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
			print(link)
			if !CurrentUserInfo.shared.userId().isEmpty {
				self.handleIncomingDynamicLink(link)
			}
			return true
		} else {
			return false
		}
	}

	func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
		if let incomingUrl = userActivity.webpageURL {
			print("Incoming url \(incomingUrl.absoluteString)")
			let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingUrl) { [weak self] (dynamicLink, error) in
				guard let self = self else { return }
				guard error == nil else {
					print("Found an error \(error?.localizedDescription ?? "")")
					return
				}
				if let dynamicLink  = dynamicLink {
					self.handleIncomingDynamicLink(dynamicLink)
				}
			}
			if linkHandled {
				return true
			} else {
				return false
			}
		}
		return false
	}

	func handleIncomingDynamicLink(_ dynamicLink: DynamicLink) {
		guard let url = dynamicLink.url else {
			print("No url found")
			return
		}
		print("Incomming link \(url.absoluteString)")
		guard (dynamicLink.matchType == .unique || dynamicLink.matchType == .default) else {
			print("Not strong enough match to continue")
			return
		}
		guard let components = URLComponents(url: url, resolvingAgainstBaseURL: false), let itemQuery = components.queryItems else { return }

			//Open product detail from shared link
			if components.path == APIEndPoints.productPath {
			if let productQueryItem = itemQuery.first(where: {$0.name == "id"}) {
				guard let productId = productQueryItem.value else { return }
				guard let productCategory = itemQuery.last(where: {$0.name == "category"})?.value else { return }
				let storyboard = UIStoryboard(name: "ProductDetailPage", bundle: nil)
			if let productDetail = storyboard.instantiateViewController(withIdentifier: "ProductDetailPageController") as? ProductDetailPageController {
					productDetail.productIdFromDeepLink = productId
					productDetail.productCategoryFromDeepLink = productCategory
			if !CurrentUserInfo.shared.userId().isEmpty {
						if let tabController = self.window?.rootViewController as? LaunchScreenController {
						let navController = (tabController.presentedViewController as? TabBarViewController)?.selectedViewController as? UINavigationController
						navController?.pushViewController(productDetail, animated: true) }
			}	else {
					let storyboard = UIStoryboard(name: "TabBarStoryboard", bundle: nil)
					let tabBars = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as? TabBarViewController
					window?.rootViewController = tabBars
					window?.makeKeyAndVisible()
					window?.rootViewController?.present(productDetail, animated: true, completion: nil)
					}
				}
			}
		}

		//Open profile from QRCode Scan or Shared profile
		if components.path == APIEndPoints.profilePath {
			if let productQueryItem = itemQuery.first(where: {$0.name == "userId"}) {
				guard let userId = productQueryItem.value else { return }
				let storyboard = UIStoryboard(name: "ProfilePage", bundle: nil)
				if let productDetail = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController {
					productDetail.userIdentifier = userId
					if !CurrentUserInfo.shared.userId().isEmpty {
							if let tabController = self.window?.rootViewController as? LaunchScreenController {
							let navController = (tabController.presentedViewController as? TabBarViewController)?.selectedViewController as? UINavigationController
							navController?.pushViewController(productDetail, animated: true) }
					} else {
						let storyboard = UIStoryboard(name: "TabBarStoryboard", bundle: nil)
						let tabBars = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as? TabBarViewController
						window?.rootViewController = tabBars
						window?.makeKeyAndVisible()
						window?.rootViewController?.present(productDetail, animated: true, completion: nil)
					}
				}
			}
		}
	}
}

extension UIApplication {
	class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
		if let nav = base as? UINavigationController {
			 return topViewController(base: nav.visibleViewController)
		 }
		if let tab = base as? UITabBarController {
				  let moreNavigationController = tab.moreNavigationController
			if let top = moreNavigationController.topViewController, top.view.window != nil {
				return topViewController(base: top)
				  } else if let selected = tab.selectedViewController {
				return topViewController(base: selected)
				  }
			  }
		if let presented = base?.presentedViewController {
				return topViewController(base: presented)
		}
		return base
	}
}
