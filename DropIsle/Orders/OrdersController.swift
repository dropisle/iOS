//
//  OrdersController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-25.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit
import Tabman
import Pageboy

class OrdersController: TabmanViewController {

	let barContainer = UIView()

	lazy var viewControllers: [UIViewController] = {
		var viewControllers = [UIViewController]()
		viewControllers.append(newOrdersController())
		viewControllers.append(deliveredOrdersController())
		viewControllers.append(cancelledOrdersController())
		return viewControllers
	}()

	override func viewDidLoad() {
		super.viewDidLoad()
		title = "Orders"
		setUpViews()
		barContainer.backgroundColor = .secondarySystemGroupedBackground

		self.dataSource = self
		let bar = TMBar.ButtonBar()
		bar.layout.transitionStyle = .snap
		bar.layout.contentInset = UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 10.0)
		bar.layout.alignment = .centerDistributed
		bar.layout.contentMode = .fit
		bar.indicator.overscrollBehavior = .bounce
		bar.indicator.weight = .medium
		bar.layout.interButtonSpacing = 20
		bar.backgroundView.style = .clear
		bar.indicator.backgroundColor = .label
		bar.buttons.customize { (button) in
			button.tintColor = .lightGray
			button.selectedTintColor = .label
			button.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 14)!
		}
		addBar(bar, dataSource: self, at: .custom(view: barContainer, layout: nil))
	}

	@objc func filterGallery(sender: UIButton) {
		self.performSegue(withIdentifier: segues.filterArtCollections.rawValue, sender: self)
	}
}

extension OrdersController {
	func newOrdersController() -> NewOrdersController {
	  let storyboard = UIStoryboard(name: "Orders", bundle: .main)
		return storyboard.instantiateViewController(withIdentifier: "NewOrdersController") as! NewOrdersController
	}

	func deliveredOrdersController() -> DeliveredOrdersController {
	  let storyboard = UIStoryboard(name: "Orders", bundle: .main)
		return  storyboard.instantiateViewController(withIdentifier: "DeliveredOrdersController") as! DeliveredOrdersController
	}

	func cancelledOrdersController() -> CancelledOrdersController {
	  let storyboard = UIStoryboard(name: "Orders", bundle: .main)
		return storyboard.instantiateViewController(withIdentifier: "CancelledOrdersController") as! CancelledOrdersController
	}
}

extension OrdersController: PageboyViewControllerDataSource, TMBarDataSource {

  func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
	let title = ["New", "Delivered", "Cancelled"]
	return TMBarItem(title: title[index])
  }

  func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
	return viewControllers.count
  }

  func viewController(for pageboyViewController: PageboyViewController,
					  at index: PageboyViewController.PageIndex) -> UIViewController? {
	return viewControllers[index]
  }

  func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
	return nil
  }

  func barItem(for tabViewController: TabmanViewController, at index: Int) -> TMBarItemable {
	let title = "Page \(index)"
	return TMBarItem(title: title)
  }
}

extension OrdersController {

	func setUpViews() {
		barContainer.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(barContainer)

		NSLayoutConstraint.activate([
			barContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
			barContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			barContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			barContainer.heightAnchor.constraint(equalToConstant: 50),
		])
	}
}
