//
//  NewOrdersController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-25.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class NewOrdersController: UIViewController, UITableViewDataSource, UITableViewDelegate {

	var tableView: UITableView!

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		title = "Cart"

		setUpTableView()
	}

	func setUpTableView() {
		tableView = UITableView.init(frame: .zero)
		tableView.dataSource = self
		tableView.delegate = self
		tableView.separatorStyle = .none
		tableView.contentInset = UIEdgeInsets(top: 60, left: 0, bottom: 0, right: 0)
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.backgroundColor = .secondarySystemGroupedBackground
		view.addSubview(tableView)

		NSLayoutConstraint.activate([
			tableView.topAnchor.constraint(equalTo: view.topAnchor),
			tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])

		tableView.register(NewOrdersCell.self, forCellReuseIdentifier: NewOrdersCell.identifier)
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 2
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: NewOrdersCell.identifier ) as! NewOrdersCell
		cell.selectionStyle = .none
		return cell
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 180
	}

	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return .leastNormalMagnitude
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		self.performSegue(withIdentifier: segues.ordersDetails.rawValue, sender: self)
	}
}
