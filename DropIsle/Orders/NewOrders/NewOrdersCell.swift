//
//  NewOrdersCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-25.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class NewOrdersCell: UITableViewCell, Reusable {

	let day = UILabel()
	let orderNumber = UILabel()
	let orderState = UILabel()
	let orderViewState = UIView()
	let datePlaced = UILabel()
	let deliveryDate = UILabel()
	let orderTotal = UILabel()
	let devider = UIView()

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		//Initialization code
		setUp()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func setUp() {
		day.translatesAutoresizingMaskIntoConstraints = false
		orderNumber.translatesAutoresizingMaskIntoConstraints = false
		orderState.translatesAutoresizingMaskIntoConstraints = false
		orderViewState.translatesAutoresizingMaskIntoConstraints = false
		datePlaced.translatesAutoresizingMaskIntoConstraints = false
		deliveryDate.translatesAutoresizingMaskIntoConstraints = false
		orderTotal.translatesAutoresizingMaskIntoConstraints = false
		devider.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(day)
		contentView.addSubview(orderNumber)
		contentView.addSubview(orderState)
		contentView.addSubview(orderViewState)
		contentView.addSubview(datePlaced)
		contentView.addSubview(deliveryDate)
		contentView.addSubview(orderTotal)
		contentView.addSubview(devider)

		NSLayoutConstraint.activate([
			day.topAnchor.constraint(equalTo: topAnchor, constant: 10),
			day.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),

			orderNumber.topAnchor.constraint(equalTo: day.bottomAnchor, constant: 10),
			orderNumber.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),

			orderViewState.trailingAnchor.constraint(equalTo: orderState.leadingAnchor, constant: -10),
			orderViewState.centerYAnchor.constraint(equalTo: orderState.centerYAnchor),
			orderViewState.heightAnchor.constraint(equalToConstant: 5),
			orderViewState.widthAnchor.constraint(equalToConstant: 5),

			orderState.topAnchor.constraint(equalTo: orderNumber.topAnchor),
			orderState.bottomAnchor.constraint(equalTo: orderNumber.bottomAnchor),
			orderState.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			orderState.heightAnchor.constraint(equalToConstant: 14),

			datePlaced.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			datePlaced.topAnchor.constraint(equalTo: orderNumber.bottomAnchor, constant: 10),

			deliveryDate.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			deliveryDate.topAnchor.constraint(equalTo: datePlaced.bottomAnchor, constant: 10),

			orderTotal.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			orderTotal.topAnchor.constraint(equalTo: deliveryDate.bottomAnchor, constant: 10),

			devider.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			devider.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			devider.topAnchor.constraint(equalTo: orderTotal.bottomAnchor, constant: 10),
			devider.heightAnchor.constraint(equalToConstant: 0.3)

		])

		devider.backgroundColor = .lightGray
		contentView.backgroundColor = .secondarySystemGroupedBackground
		
		day.textColor = .label
		day.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16)
		day.text = "Today"

		orderNumber.textColor = .label
		orderNumber.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 15)
		orderNumber.text = "Order #234"

		orderState.textColor = .label
		orderState.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 16)
		orderState.text = "Delivered"
		orderState.textAlignment = .natural

		orderViewState.backgroundColor = .systemGreen
		orderViewState.cornerRadius = 2.5

		datePlaced.textColor = .label
		datePlaced.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		datePlaced.text = "Placed: Oct 3, 2020"

		deliveryDate.textColor = .label
		deliveryDate.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		deliveryDate.text = "Estimated delivery: Oct 20, 2020 "

		orderTotal.textColor = .label
		orderTotal.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		orderTotal.text = "Order Total: CAD $350:67"
	}
}
