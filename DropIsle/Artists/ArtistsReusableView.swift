//
//  ArtistsReusableView.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-06.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class ArtistsReusableView: UICollectionReusableView {
	
	let backgroundImage: UIImageView = {
		let iv = UIImageView()
		iv.contentMode = .scaleAspectFill
		iv.backgroundColor = .green
		iv.clipsToBounds = true
		iv.image = UIImage(named: "nature5")
		return iv
	}()

	let smallDashView = UIView()
	let pageTitle = UILabel()
	let subPageTitle = UILabel()

	let title = UILabel()
	let subTitle = UILabel()
	let containerView = UIView()

	override init(frame: CGRect) {
		super.init(frame: frame)
		style()
		setup()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func setup() {
		backgroundImage.translatesAutoresizingMaskIntoConstraints = false
		containerView.translatesAutoresizingMaskIntoConstraints = false
		title.translatesAutoresizingMaskIntoConstraints = false
		subTitle.translatesAutoresizingMaskIntoConstraints = false
		pageTitle.translatesAutoresizingMaskIntoConstraints = false
		smallDashView.translatesAutoresizingMaskIntoConstraints = false

		addSubview(backgroundImage)
		addSubview(pageTitle)
		addSubview(containerView)
		containerView.addSubview(title)
		containerView.addSubview(subTitle)
		containerView.addSubview(smallDashView)

		NSLayoutConstraint.activate([
			backgroundImage.leadingAnchor.constraint(equalTo: leadingAnchor),
			backgroundImage.trailingAnchor.constraint(equalTo: trailingAnchor),
			backgroundImage.topAnchor.constraint(equalTo: topAnchor),
			backgroundImage.bottomAnchor.constraint(equalTo: containerView.topAnchor),

			pageTitle.topAnchor.constraint(equalTo: topAnchor, constant: 100),
			pageTitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),

			containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
			containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
			containerView.bottomAnchor.constraint(equalTo: bottomAnchor),
			containerView.heightAnchor.constraint(equalToConstant: 120),

			title.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 35),
			subTitle.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 5),

			smallDashView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 4),
			smallDashView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
			smallDashView.heightAnchor.constraint(equalToConstant: 4),
			smallDashView.widthAnchor.constraint(equalToConstant: 40)
		])
	}

	private func style() {
		smallDashView.backgroundColor = .label
		smallDashView.cornerRadius = 2

		pageTitle.text = "Artists"
		pageTitle.textColor = .label
		pageTitle.textAlignment = .natural
		pageTitle.lineBreakMode = .byWordWrapping
		pageTitle.numberOfLines = 0
		pageTitle.font = UIFont(name: "Papyrus-Condensed", size: 50)

		title.textColor = .label
		title.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18)

		subTitle.textColor = .label
		subTitle.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		backgroundImage.fillSuperview()
	}
}
