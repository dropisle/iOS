//
//  EditProfileController.swift
//  DropIsle
//
//  Created by CtanLI on 2021-03-06.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import UIKit

class EditProfileController: UIViewController {

	let scrollView = UIScrollView()
	let containerView = UIView()

	let firstName = UILabel()
	let firstNameField = UITextField()

	let lastName = UILabel()
	let lastNameField = UITextField()

	let gender = UILabel()
	let genderField = UITextField()

	let email = UILabel()
	let emailField = UITextField()

	let phoneNum = UILabel()
	let phoneNumField = UITextField()

    override func viewDidLoad() {
        super.viewDidLoad()
		title = "Edit Personal Information"
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: UIBarButtonItem.Style.plain, target: self, action: #selector(saveInfo))
		navigationItem.rightBarButtonItem?.tintColor = .label
		//
		setUp()
    }

	@objc func saveInfo() {
	}
}

extension EditProfileController {
	func setUp() {

		scrollView.translatesAutoresizingMaskIntoConstraints = false
		containerView.translatesAutoresizingMaskIntoConstraints = false

		firstName.translatesAutoresizingMaskIntoConstraints = false
		firstNameField.translatesAutoresizingMaskIntoConstraints = false

		lastName.translatesAutoresizingMaskIntoConstraints = false
		lastNameField.translatesAutoresizingMaskIntoConstraints = false

		gender.translatesAutoresizingMaskIntoConstraints = false
		genderField.translatesAutoresizingMaskIntoConstraints = false

		email.translatesAutoresizingMaskIntoConstraints = false
		emailField.translatesAutoresizingMaskIntoConstraints = false

		phoneNum.translatesAutoresizingMaskIntoConstraints = false
		phoneNumField.translatesAutoresizingMaskIntoConstraints = false

		view.addSubview(scrollView)
		scrollView.addSubview(containerView)

		containerView.addSubview(firstName)
		containerView.addSubview(firstNameField)

		containerView.addSubview(lastName)
		containerView.addSubview(lastNameField)

		containerView.addSubview(gender)
		containerView.addSubview(genderField)

		containerView.addSubview(email)
		containerView.addSubview(emailField)

		containerView.addSubview(phoneNum)
		containerView.addSubview(phoneNumField)

		NSLayoutConstraint.activate([
			scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			scrollView.widthAnchor.constraint(equalTo: view.widthAnchor),
			scrollView.topAnchor.constraint(equalTo: view.topAnchor),
			scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

			containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
			containerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
			containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
			containerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),

			firstName.heightAnchor.constraint(equalToConstant: 25),
			firstName.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 25),
			firstName.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			firstName.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			firstNameField.heightAnchor.constraint(equalToConstant: 40),
			firstNameField.topAnchor.constraint(equalTo: firstName.bottomAnchor, constant: 5),
			firstNameField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			firstNameField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			lastName.heightAnchor.constraint(equalToConstant: 25),
			lastName.topAnchor.constraint(equalTo: firstNameField.bottomAnchor, constant: 10),
			lastName.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			lastName.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			lastNameField.heightAnchor.constraint(equalToConstant: 40),
			lastNameField.topAnchor.constraint(equalTo: lastName.bottomAnchor, constant: 5),
			lastNameField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			lastNameField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			gender.heightAnchor.constraint(equalToConstant: 25),
			gender.topAnchor.constraint(equalTo: lastNameField.bottomAnchor, constant: 10),
			gender.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			gender.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			genderField.heightAnchor.constraint(equalToConstant: 40),
			genderField.topAnchor.constraint(equalTo: gender.bottomAnchor, constant: 5),
			genderField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			genderField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			email.heightAnchor.constraint(equalToConstant: 25),
			email.topAnchor.constraint(equalTo: genderField.bottomAnchor, constant: 10),
			email.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			email.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			emailField.heightAnchor.constraint(equalToConstant: 40),
			emailField.topAnchor.constraint(equalTo: email.bottomAnchor, constant: 5),
			emailField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			emailField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			phoneNum.heightAnchor.constraint(equalToConstant: 25),
			phoneNum.topAnchor.constraint(equalTo: emailField.bottomAnchor, constant: 10),
			phoneNum.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			phoneNum.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			phoneNumField.heightAnchor.constraint(equalToConstant: 40),
			phoneNumField.topAnchor.constraint(equalTo: phoneNum.bottomAnchor, constant: 5),
			phoneNumField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			phoneNumField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
			phoneNumField.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20)
		])

		firstName.text = "First Name"
		firstName.textColor = .label
		firstName.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		firstNameField.borderColor = .lightGray
		firstNameField.borderWidth = 0.2
		firstNameField.cornerRadius = 5

		lastName.text = "Last Name"
		lastName.textColor = .label
		lastName.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		lastNameField.borderColor = .lightGray
		lastNameField.borderWidth = 0.2
		lastNameField.cornerRadius = 5

		gender.text = "Gender"
		gender.textColor = .label
		gender.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		genderField.borderColor = .lightGray
		genderField.borderWidth = 0.2
		genderField.cornerRadius = 5

		email.text = "Email"
		email.textColor = .label
		email.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		emailField.borderColor = .lightGray
		emailField.borderWidth = 0.2
		emailField.cornerRadius = 5

		phoneNum.text = "Phone Number"
		phoneNum.textColor = .label
		phoneNum.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		phoneNumField.borderColor = .lightGray
		phoneNumField.borderWidth = 0.2
		phoneNumField.cornerRadius = 5
	}
}
