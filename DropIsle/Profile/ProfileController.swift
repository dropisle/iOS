//
//  ProfileController.swift
//  DropIsle
//
//  Created by CtanLI on 2021-02-14.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import UIKit

class ProfileController: UIViewController {

	let scrollView = UIScrollView()
	let viewContainer = UIView()

	let nameLabel = UILabel()
	let dateSince = UILabel()
	let dividerOne = UIView()

	let emailLabel = UILabel()
	let dividerTwo = UIView()

	let phoneNum = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
		title = "Profile"
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: UIBarButtonItem.Style.plain, target: self, action: #selector(editInfo))
		navigationItem.rightBarButtonItem?.tintColor = .label

		//
		setUp()
    }

	@objc func editInfo() {
		self.performSegue(withIdentifier: segues.editProfile.rawValue, sender: self)
	}
}

extension ProfileController {
	func setUp() {
		scrollView.translatesAutoresizingMaskIntoConstraints = false
		viewContainer.translatesAutoresizingMaskIntoConstraints = false

		nameLabel.translatesAutoresizingMaskIntoConstraints = false
		dateSince.translatesAutoresizingMaskIntoConstraints = false
		dividerOne.translatesAutoresizingMaskIntoConstraints = false

		emailLabel.translatesAutoresizingMaskIntoConstraints = false
		dividerTwo.translatesAutoresizingMaskIntoConstraints = false
		phoneNum.translatesAutoresizingMaskIntoConstraints = false

		view.addSubview(scrollView)
		scrollView.addSubview(viewContainer)
		viewContainer.addSubview(nameLabel)
		viewContainer.addSubview(dateSince)
		viewContainer.addSubview(dividerOne)
		viewContainer.addSubview(emailLabel)
		viewContainer.addSubview(dividerTwo)
		viewContainer.addSubview(phoneNum)

		NSLayoutConstraint.activate([
			scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			scrollView.widthAnchor.constraint(equalTo: view.widthAnchor),
			scrollView.topAnchor.constraint(equalTo: view.topAnchor),
			scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

			viewContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
			viewContainer.topAnchor.constraint(equalTo: scrollView.topAnchor),
			viewContainer.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
			viewContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),

			nameLabel.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			nameLabel.topAnchor.constraint(equalTo: viewContainer.topAnchor, constant: 20),

			dateSince.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			dateSince.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 10),

			dividerOne.heightAnchor.constraint(equalToConstant: 0.5),
			dividerOne.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			dividerOne.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),
			dividerOne.topAnchor.constraint(equalTo: dateSince.bottomAnchor, constant: 7),

			emailLabel.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			emailLabel.topAnchor.constraint(equalTo: dividerOne.bottomAnchor, constant: 40),

			dividerTwo.heightAnchor.constraint(equalToConstant: 0.5),
			dividerTwo.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			dividerTwo.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),
			dividerTwo.topAnchor.constraint(equalTo: emailLabel.bottomAnchor, constant: 7),

			phoneNum.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			phoneNum.topAnchor.constraint(equalTo: dividerTwo.bottomAnchor, constant: 40),
		])

		nameLabel.text = "Sandra Parker"
		nameLabel.textColor = .label
		nameLabel.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 16)

		dateSince.text = "since May, 2020"
		dateSince.textColor = .lightGray
		dateSince.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)
		dividerOne.backgroundColor = .label

		emailLabel.text = "Sandra@gmail.com"
		emailLabel.textColor = .label
		emailLabel.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 16)
		dividerTwo.backgroundColor = .label

		phoneNum.text = "+1 647-88-8888"
		phoneNum.textColor = .label
		phoneNum.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 16)
	}
}
