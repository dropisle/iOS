//
//  ExpirationDatePicker.swift
//  DropIsle
//
//  Created by CtanLI on 2021-03-17.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

enum PickerOption: Int {
	case months = 1, years
	func diliveryOptions() -> Int { return self.rawValue }
}

struct Months {
	static let jan = "1"
	static let feb = "2"
	static let march = "3"
	static let april = "4"
	static let may = "5"
	static let june = "6"
	static let july = "7"
	static let aug = "8"
	static let sept = "9"
	static let oct = "10"
	static let nov = "11"
	static let dec = "12"

	static var indexValues: [String] = {
		return [Months.jan, Months.feb, Months.march, Months.april, Months.may, Months.june, Months.july, Months.aug, Months.sept, Months.oct, Months.nov, Months.dec]
	}()
}

struct Years {
	static let twentyOne = "2021"
	static let twentyTwo = "2022"
	static let twentyThree = "2023"
	static let twentyFour = "2024"
	static let twentyFive = "2025"
	static let twentySix = "2026"
	static let twentySeven = "2027"
	static let twentyEight = "2028"
	static let twentyNine = "2029"
	static let twentyThirty = "2030"
	static let twentyThirtyOne = "2031"
	static let twentyThirtyTwo = "2032"

	static var indexValues: [String] = {
		return [Years.twentyOne, Years.twentyTwo, Years.twentyThree, Years.twentyFour, Years.twentyFive, Years.twentySix, Years.twentySeven, Years.twentyEight, Years.twentyNine, Years.twentyThirty, Years.twentyThirtyOne, Years.twentyThirtyTwo]
	}()
}
