//
//  PaymentController.swift
//  DropIsle
//
//  Created by CtanLI on 2021-03-16.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import UIKit
import Stripe

class PaymentController: UIViewController, UITextFieldDelegate {

	let scrollView = UIScrollView()
	let containerView = UIView()

	let cardNumberValue = UITextField()
	let cvvValue = UITextField()
	let cardNameValue = UITextField()
	let cardName = UILabel()

	let cardNumber = UILabel()
	let cvv = UILabel()
	let experation = UILabel()

	let dividerOne = UIView()
	let dividerTwo = UIView()

	let hStack = UIStackView()
	let innerHStack = UIStackView()
	let innerMonthContainer = UIView()
	let innerYearContainer = UIView()

	let monthValue = UIButton()
	let yearValue = UIButton()

	let monthSelector = UIImageView()
	let yearSelector = UIImageView()

	let vStack1 = UIView()
	let vStack2 = UIView()
	let vStack3 = UIView()
	let cvvInfo = UIButton()
	var isSelectedValue = Int()

	let toggleImage = UIButton()
	let radioButton = UIButton()
	let shippingAdd = UILabel()
	let addBillingAddress = UIButton()
	var isCurrentShippingAddress = Bool()

	var shippingAddressDetails: Address?
	var addressDetails: Address?
	var cardParams = STPPaymentMethodCardParams()
	var billingAddress = STPPaymentMethodBillingDetails()
	var validation = Validation()

	lazy var cardTextField: STPPaymentCardTextField = {
		let cardTextField = STPPaymentCardTextField()
		return cardTextField
	}()

	override func viewDidLoad() {
		super.viewDidLoad()
		title = "Add card details"
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Apply", style: UIBarButtonItem.Style.plain, target: self, action: #selector(saveCard))
		navigationItem.rightBarButtonItem?.tintColor = .label

		cardNumberValue.delegate = self
		cvvValue.delegate = self
		cardNameValue.delegate = self
		cardNameValue.setLeftPaddingPoints(10)

		addBillingAddress.isHidden = true
		monthValue.tag = 1
		monthValue.addTarget(self, action: #selector(showPicker(_:)), for: .touchUpInside)
		yearValue.tag = 2
		yearValue.addTarget(self, action: #selector(showPicker(_:)), for: .touchUpInside)
		radioButton.addTarget(self, action: #selector(billingAddressAction), for: .touchUpInside)
		addBillingAddress.addTarget(self, action: #selector(showBillingAddress), for: .touchUpInside)

		cardNumberValue.keyboardType = .phonePad
		cvvValue.keyboardType = .phonePad
		
		//
		setUp()
	}

	func validZipCode(postalCode: String)->Bool{
		let postalcodeRegex = "(^[a-zA-Z][0-9][a-zA-Z][- ]*[0-9][a-zA-Z][0-9]$)" //"^[0-9]{5}(-[0-9]{4})?$" (^[0-9]{5}(-[0-9]{4})?$)
		let pinPredicate = NSPredicate(format: "SELF MATCHES %@", postalcodeRegex)
		let bool = pinPredicate.evaluate(with: postalCode) as Bool
		return bool
	}

	@objc func saveCard() {
		if  let cardNum = cardNumberValue.text,
			let cardMonth = monthValue.titleLabel?.text,
			let cardYear = yearValue.titleLabel?.text,
			let cardKey = cvvValue.text,
			let name = cardNameValue.text {

			if validation.validateAnyOtherTextField(otherField: name) { self.alert(message: "Please enter name as shown on the card"); return }
			if validation.validateAnyOtherTextField(otherField: cardNum) { self.alert(message: "Card Number cannot be empty"); return }
			if validation.validateAnyOtherTextField(otherField: cardKey) { self.alert(message: "CVV cannot be empty"); return }

			cardParams.number = cardNum.whiteSpacesRemoved()
			cardParams.cvc = cardKey
			cardParams.expMonth = NSNumber(value: Int(cardMonth) ?? 0)
			cardParams.expYear = NSNumber(value: Int(cardYear) ?? 0)

			let billingDetails = STPPaymentMethodAddress()
			billingDetails.postalCode = addressDetails?.postalCode.whiteSpacesRemoved()
			billingDetails.line1 = addressDetails?.address
			billingDetails.city = addressDetails?.city
			billingDetails.country = addressDetails?.country
			billingDetails.state = addressDetails?.province

			billingAddress.name = name
			billingAddress.address = billingDetails
		}
		//
		self.performSegue(withIdentifier: segues.reviewOrder.rawValue, sender: self)
	}

	@objc func billingAddressAction() {
		if isCurrentShippingAddress {
			isCurrentShippingAddress = false
			addBillingAddress.isHidden = true
			toggleImage.setTitle("◉", for: .normal)
			addAddressToggle(view: addBillingAddress, hidden: true)
		} else {
			isCurrentShippingAddress = true
			addBillingAddress.isHidden = false
			toggleImage.setTitle("◎", for: .normal)
			addAddressToggle(view: addBillingAddress, hidden: false)
		}
	}

	@objc func showBillingAddress() {
		self.performSegue(withIdentifier: segues.shippingAddress.rawValue, sender: self)
	}

	func addAddressToggle(view: UIButton, hidden: Bool) {
		if let billingAddress = addressDetails {
			updateBillingAddress(billingAddress: billingAddress)
		}
		UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
			view.isHidden = hidden
		})
	}

	@objc func showPicker(_ sender: UIButton) {
		isSelectedValue = sender.tag
		performSegue(withIdentifier: segues.picker.rawValue, sender: self)
	}

	//MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier ==  segues.picker.rawValue {
			let picker = segue.destination as! PickerController
			picker.isSelected = isSelectedValue
			picker.delegate = self
		}

		if segue.identifier ==  segues.shippingAddress.rawValue {
			let shippingAddress = segue.destination as! ShippingAddressController
			shippingAddress.isFromPaymentScreen = true
			shippingAddress.billingAddressCallback = { [weak self] result in
				self?.updateBillingAddress(billingAddress: result)
			}
		}

		if segue.identifier ==  segues.reviewOrder.rawValue {
			let reviewOrder = segue.destination as! ReviewOrderController
			reviewOrder.cardParams = cardParams
			reviewOrder.billingAddress = billingAddress
			reviewOrder.shippingAddressDetails = shippingAddressDetails
		}
	}

	func updateBillingAddress(billingAddress: Address) {
		addressDetails = billingAddress
		shippingAdd.text = "\(billingAddress.address)," + " \(billingAddress.postalCode)" + " \(billingAddress.city)"  + " \(billingAddress.country)"
	}
}

extension PaymentController : PickerViewDelegate {
	func pickerOptionSelected(value: String, tag: Int) {
		tag == 1 ? monthValue.setTitle(value, for: .normal) : yearValue.setTitle(value, for: .normal)
	}
}

extension String {
  func whiteSpacesRemoved() -> String {
	return self.filter { $0 != Character(" ") }
  }
}

extension PaymentController {
	func setUp() {
		hStack.axis = .horizontal
		hStack.distribution = .fillEqually
		hStack.alignment = .fill
		hStack.spacing = 2

		innerHStack.axis = .horizontal
		innerHStack.distribution = .fillEqually
		innerHStack.alignment = .fill
		innerHStack.spacing = 0

		scrollView.translatesAutoresizingMaskIntoConstraints = false
		containerView.translatesAutoresizingMaskIntoConstraints = false

		cardName.translatesAutoresizingMaskIntoConstraints = false
		cardNameValue.translatesAutoresizingMaskIntoConstraints = false
		cardNumber.translatesAutoresizingMaskIntoConstraints = false
		cvv.translatesAutoresizingMaskIntoConstraints = false
		experation.translatesAutoresizingMaskIntoConstraints = false

		cardNumberValue.translatesAutoresizingMaskIntoConstraints = false
		cvvValue.translatesAutoresizingMaskIntoConstraints = false
		monthValue.translatesAutoresizingMaskIntoConstraints = false
		yearValue.translatesAutoresizingMaskIntoConstraints = false
		cvvInfo.translatesAutoresizingMaskIntoConstraints = false

		dividerOne.translatesAutoresizingMaskIntoConstraints = false
		dividerTwo.translatesAutoresizingMaskIntoConstraints = false

		hStack.translatesAutoresizingMaskIntoConstraints = false
		innerHStack.translatesAutoresizingMaskIntoConstraints = false

		vStack1.translatesAutoresizingMaskIntoConstraints = false
		vStack2.translatesAutoresizingMaskIntoConstraints = false
		vStack3.translatesAutoresizingMaskIntoConstraints = false

		innerMonthContainer.translatesAutoresizingMaskIntoConstraints = false
		innerYearContainer.translatesAutoresizingMaskIntoConstraints = false
		monthSelector.translatesAutoresizingMaskIntoConstraints = false
		yearSelector.translatesAutoresizingMaskIntoConstraints = false

		toggleImage.translatesAutoresizingMaskIntoConstraints = false
		radioButton.translatesAutoresizingMaskIntoConstraints = false
		addBillingAddress.translatesAutoresizingMaskIntoConstraints = false
		shippingAdd.translatesAutoresizingMaskIntoConstraints = false

		view.addSubview(scrollView)
		scrollView.addSubview(containerView)
		containerView.addSubview(cardNameValue)
		containerView.addSubview(cardName)
		containerView.addSubview(cardNumber)
		containerView.addSubview(cardNumberValue)

		containerView.addSubview(dividerOne)
		containerView.addSubview(dividerTwo)
		containerView.addSubview(hStack)

		containerView.addSubview(toggleImage)
		containerView.addSubview(radioButton)
		containerView.addSubview(addBillingAddress)
		containerView.addSubview(shippingAdd)
		
		hStack.addArrangedSubview(vStack1)
		hStack.addArrangedSubview(vStack2)
		vStack1.addSubview(experation)
		vStack2.addSubview(cvv)

		vStack1.addSubview(innerHStack)
		innerHStack.addArrangedSubview(innerMonthContainer)
		innerHStack.addArrangedSubview(innerYearContainer)

		innerMonthContainer.addSubview(monthSelector)
		innerMonthContainer.addSubview(monthValue)
		innerYearContainer.addSubview(yearSelector)
		innerYearContainer.addSubview(yearValue)

		vStack2.addSubview(cvvValue)
		vStack2.addSubview(cvvInfo)

		NSLayoutConstraint.activate([
			scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			scrollView.widthAnchor.constraint(equalTo: view.widthAnchor),
			scrollView.topAnchor.constraint(equalTo: view.topAnchor),
			scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

			containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
			containerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
			containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
			containerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),

			cardName.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 20),
			cardName.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			cardName.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			cardNameValue.heightAnchor.constraint(equalToConstant: 40),
			cardNameValue.topAnchor.constraint(equalTo: cardName.bottomAnchor, constant: 10),
			cardNameValue.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			cardNameValue.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			cardNumber.topAnchor.constraint(equalTo: cardNameValue.bottomAnchor, constant: 20),
			cardNumber.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),

			cardNumberValue.topAnchor.constraint(equalTo: cardNumber.bottomAnchor, constant: 15),
			cardNumberValue.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			cardNumberValue.heightAnchor.constraint(equalToConstant: 30),

			dividerOne.heightAnchor.constraint(equalToConstant: 0.3),
			dividerOne.topAnchor.constraint(equalTo: cardNumberValue.bottomAnchor, constant: 10),
			dividerOne.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			dividerOne.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			hStack.topAnchor.constraint(equalTo: dividerOne.topAnchor, constant: 20),
			hStack.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			hStack.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
			hStack.heightAnchor.constraint(equalToConstant: 70),

			innerHStack.topAnchor.constraint(equalTo: experation.bottomAnchor, constant: 5),
			innerHStack.leadingAnchor.constraint(equalTo: vStack1.leadingAnchor),
			innerHStack.trailingAnchor.constraint(equalTo: vStack1.trailingAnchor),
			innerHStack.heightAnchor.constraint(equalToConstant: 40),

			innerHStack.bottomAnchor.constraint(equalTo: hStack.bottomAnchor, constant: -1),

			monthValue.topAnchor.constraint(equalTo: innerMonthContainer.topAnchor),
			monthValue.leadingAnchor.constraint(equalTo: innerMonthContainer.leadingAnchor, constant: 10),
			monthValue.bottomAnchor.constraint(equalTo: innerMonthContainer.bottomAnchor),
			monthValue.trailingAnchor.constraint(equalTo: innerMonthContainer.trailingAnchor),

			monthSelector.centerYAnchor.constraint(equalTo: innerMonthContainer.centerYAnchor),
			monthSelector.trailingAnchor.constraint(equalTo: innerMonthContainer.trailingAnchor, constant: -10),
			monthSelector.heightAnchor.constraint(equalToConstant: 15),

			yearValue.topAnchor.constraint(equalTo: innerYearContainer.topAnchor),
			yearValue.leadingAnchor.constraint(equalTo: innerYearContainer.leadingAnchor, constant: 10),
			yearValue.bottomAnchor.constraint(equalTo: innerYearContainer.bottomAnchor),
			yearValue.trailingAnchor.constraint(equalTo: innerYearContainer.trailingAnchor),

			yearSelector.centerYAnchor.constraint(equalTo: innerYearContainer.centerYAnchor),
			yearSelector.trailingAnchor.constraint(equalTo: innerYearContainer.trailingAnchor, constant: -10),
			yearSelector.heightAnchor.constraint(equalToConstant: 15),

			cvv.leadingAnchor.constraint(equalTo: vStack2.leadingAnchor, constant: 20),
			cvvValue.topAnchor.constraint(equalTo: cvv.bottomAnchor, constant: 5),
			cvvValue.leadingAnchor.constraint(equalTo: vStack2.leadingAnchor, constant: 20),
			cvvValue.bottomAnchor.constraint(equalTo: vStack2.bottomAnchor, constant: -2),
			cvvValue.trailingAnchor.constraint(equalTo: cvvInfo.leadingAnchor, constant: -10),
			cvvValue.heightAnchor.constraint(equalToConstant: 40),

			cvvInfo.centerYAnchor.constraint(equalTo: cvvValue.centerYAnchor),
			cvvInfo.trailingAnchor.constraint(equalTo: vStack2.trailingAnchor, constant: -10),
			cvvInfo.heightAnchor.constraint(equalToConstant: 30),

			dividerTwo.heightAnchor.constraint(equalToConstant: 0.3),
			dividerTwo.topAnchor.constraint(equalTo: hStack.bottomAnchor, constant: 15),
			dividerTwo.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			dividerTwo.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
			//dividerTwo.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -5),

			toggleImage.heightAnchor.constraint(equalToConstant: 25),
			toggleImage.widthAnchor.constraint(equalToConstant: 25),
			toggleImage.topAnchor.constraint(equalTo: dividerTwo.bottomAnchor, constant: 20),
			toggleImage.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),

			//radioButton.heightAnchor.constraint(equalToConstant: 45),
			radioButton.topAnchor.constraint(equalTo: dividerTwo.bottomAnchor, constant: 20),
			radioButton.leadingAnchor.constraint(equalTo: toggleImage.trailingAnchor, constant: 10),
			radioButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			shippingAdd.topAnchor.constraint(equalTo: radioButton.bottomAnchor, constant: 8),
			shippingAdd.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
			shippingAdd.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

			addBillingAddress.heightAnchor.constraint(equalToConstant: 40),
			addBillingAddress.topAnchor.constraint(equalTo: shippingAdd.bottomAnchor, constant: 25),
			addBillingAddress.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 40),
			addBillingAddress.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -40),
			addBillingAddress.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -5),
		])

		toggleImage.setTitle("◉", for: UIControl.State())
		toggleImage.setTitleColor(.label, for: .normal)

		radioButton.setTitleColor(.label, for: .normal)
		radioButton.setTitle("My billing address is the same as my shipping address", for: .normal)
		radioButton.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)
		radioButton.contentHorizontalAlignment = .left
		radioButton.titleLabel?.lineBreakMode = .byWordWrapping
		radioButton.titleLabel?.numberOfLines = 0

		addBillingAddress.setTitle("Add a billing address", for: .normal)
		addBillingAddress.cornerRadius = 7
		addBillingAddress.borderColor = .secondarySystemGroupedBackground
		addBillingAddress.borderWidth = 2
		addBillingAddress.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 15)
		addBillingAddress.backgroundColor = UIColor.NavyBlueDark

		shippingAdd.text = "\(addressDetails?.address ?? "")," + " \(addressDetails?.postalCode ?? "")" + " \(addressDetails?.city ?? "")"  + " \(addressDetails?.country ?? "")"
		shippingAdd.textColor = .darkGray
		shippingAdd.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		vStack1.clipsToBounds = true
		vStack2.clipsToBounds = true
		vStack3.clipsToBounds = true
		innerHStack.clipsToBounds = true
		dividerOne.backgroundColor = .lightGray
		dividerTwo.backgroundColor = .lightGray

		cardNameValue.borderColor = .lightGray
		cardNameValue.borderWidth = 0.2
		cardNameValue.cornerRadius = 5

		cardName.text = "Name on card"
		cardName.textColor = .label
		cardName.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 15)

		cardNameValue.placeholder = "Enter card name"
		cardNameValue.textColor = .label
		cardNameValue.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		cardNumber.text = "Card number"
		cardNumber.textColor = .label
		cardNumber.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 15)

		cardNumberValue.placeholder = "0000 0000 0000 0000"
		cardNumberValue.textColor = .label
		cardNumberValue.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)
		cardNumberValue.textAlignment = .left

		experation.text = "Experation date"
		experation.textColor = .label
		experation.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 15)

		monthValue.setTitleColor(.label, for: .normal)
		monthValue.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)
		monthValue.contentHorizontalAlignment = .left
		monthValue.backgroundColor = .clear
		monthValue.setTitle("1", for: .normal)

		innerHStack.cornerRadius = 5
		innerHStack.borderWidth = 0.3
		innerHStack.borderColor = .lightGray
		innerMonthContainer.borderWidth = 0.3
		innerMonthContainer.borderColor = .lightGray
		innerYearContainer.borderWidth = 0.3
		innerYearContainer.borderColor = .lightGray

		yearValue.setTitleColor(.label, for: .normal)
		yearValue.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)
		yearValue.contentHorizontalAlignment = .left
		yearValue.backgroundColor = .clear
		yearValue.setTitle("2021", for: .normal)

		cvv.text = "CVV"
		cvv.textColor = .label
		cvv.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 15)

		cvvValue.placeholder = "000"
		cvvValue.textColor = .label
		cvvValue.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)
		cvvValue.textAlignment = .left

		let origImage = UIImage(systemName: "arrowtriangle.down.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 10, weight: .regular, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		monthSelector.image = origImage
		yearSelector.image = origImage

		let infoImage = UIImage(systemName: "info.circle.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 10, weight: .regular, scale: .large))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		cvvInfo.setImage(infoImage, for: .normal)
	}
}

extension PaymentController {
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		guard let currentText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else { return true }
		if textField == cardNumberValue  {
			guard currentText.count != 20 else { return false }
			textField.setText(to: currentText.grouping(every: 4, with: " "), preservingCursor: true)
			return false
		}

//		if textField {
//			guard currentText.count != 8 else { return false }
//			textField.setText(to: currentText.grouping(every: 3, with: " "), preservingCursor: true)
//			return false
//		}

		if textField == cvvValue  {
			guard currentText.count != 4 else { return false }
		}
		return true
	}
}

