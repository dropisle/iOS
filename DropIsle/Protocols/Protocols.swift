//
//  Protocols.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-06.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import Foundation
import UIKit

protocol ExploreAllDelegate {
	func exploreAll(isHeaderPressed: Bool)
}

protocol AppConfigurable {
	func configure(with app: App)
}

protocol CategoryConfigurable {
	func configure(with category: Category)
}
