//
//  ThirdSectionCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-21.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class ThirdSectionCell: UITableViewCell, Reusable {

	let subject$Styles = ["Landscape", "Abstract", "Eco friendly", "Anime & cartoon", "Flowers", "Travel & transportation", "Plants & trees", "Military" ]
	let materialsArr = ["Aluminium", "Ceramic", "Carbon fibre", "Steel", "Acryclic", "Canvas", "Cardboard", "Black & white paper" ]
	let scrollView = UIScrollView(frame: .zero)
	let subANdStyle = UILabel()
	let container = UIView()

	let scrollView2 = UIScrollView(frame: .zero)
	let materials = UILabel()
	let container2 = UIView()

	var stackView = UIStackView()
	var stackView2 = UIStackView()

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}

	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		// Configure the view for the selected state
	}

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		//Initialization code
		setup()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	@objc func showMoreDecription(_ sender: UIButton) {
	}

	func setup() {
		stackView.axis = .horizontal
		stackView.distribution = .fillProportionally
		stackView.alignment = .center
		stackView.spacing = 10

		stackView2.axis = .horizontal
		stackView2.distribution = .fillProportionally
		stackView2.alignment = .center
		stackView2.spacing = 10

		subANdStyle.translatesAutoresizingMaskIntoConstraints = false
		container.translatesAutoresizingMaskIntoConstraints = false
		scrollView.translatesAutoresizingMaskIntoConstraints = false
		stackView.translatesAutoresizingMaskIntoConstraints = false

		materials.translatesAutoresizingMaskIntoConstraints = false
		container2.translatesAutoresizingMaskIntoConstraints = false
		scrollView2.translatesAutoresizingMaskIntoConstraints = false
		stackView2.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(subANdStyle)
		contentView.addSubview(container)
		contentView.addSubview(materials)
		contentView.addSubview(container2)

		container.addSubview(scrollView)
		scrollView.addSubview(stackView)
		container2.addSubview(scrollView2)
		scrollView2.addSubview(stackView2)

		NSLayoutConstraint.activate([
			subANdStyle.heightAnchor.constraint(equalToConstant: 40.0),
			subANdStyle.topAnchor.constraint(equalTo: topAnchor, constant: 20),
			subANdStyle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			subANdStyle.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
			stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -0),
			stackView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 2),
			stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -2),

			scrollView.leadingAnchor.constraint(equalTo: container.leadingAnchor),
			scrollView.trailingAnchor.constraint(equalTo: container.trailingAnchor),
			scrollView.topAnchor.constraint(equalTo: container.topAnchor, constant: 5),
			scrollView.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -5),

			container.heightAnchor.constraint(equalToConstant: 45.0),
			container.topAnchor.constraint(equalTo: subANdStyle.bottomAnchor, constant: 10),
			container.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			container.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			container.bottomAnchor.constraint(equalTo: materials.topAnchor, constant: -10),

			//
			materials.heightAnchor.constraint(equalToConstant: 40.0),
			materials.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			materials.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			stackView2.leadingAnchor.constraint(equalTo: scrollView2.leadingAnchor, constant: 0),
			stackView2.trailingAnchor.constraint(equalTo: scrollView2.trailingAnchor, constant: -0),
			stackView2.topAnchor.constraint(equalTo: scrollView2.topAnchor, constant: 2),
			stackView2.bottomAnchor.constraint(equalTo: scrollView2.bottomAnchor, constant: -2),

			scrollView2.leadingAnchor.constraint(equalTo: container2.leadingAnchor),
			scrollView2.trailingAnchor.constraint(equalTo: container2.trailingAnchor),
			scrollView2.topAnchor.constraint(equalTo: container2.topAnchor, constant: 5),
			scrollView2.bottomAnchor.constraint(equalTo: container2.bottomAnchor, constant: -5),

			container2.heightAnchor.constraint(equalToConstant: 45.0),
			container2.topAnchor.constraint(equalTo: materials.bottomAnchor, constant: 10),
			container2.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			container2.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			container2.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
		])

		if UIDevice.current.userInterfaceIdiom == .pad {
			NSLayoutConstraint.activate([
				stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: 0),
				stackView2.widthAnchor.constraint(equalTo: scrollView2.widthAnchor, constant: 0)
			])
		}

		for value in subject$Styles {
			let actionWithText = PaddingLabel()
			actionWithText.translatesAutoresizingMaskIntoConstraints = false
			actionWithText.backgroundColor = UIColor.artDetailScrollColor
			actionWithText.text = value
			actionWithText.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 13)
			actionWithText.textColor = .black
			actionWithText.lineBreakMode = .byWordWrapping
			actionWithText.numberOfLines = 1
			actionWithText.textAlignment = .center
			actionWithText.clipsToBounds = true
			actionWithText.layer.cornerRadius = 15

			stackView2.addArrangedSubview(actionWithText)
			NSLayoutConstraint.activate([
				actionWithText.heightAnchor.constraint(equalToConstant: 30),
			])
		}

		for value in materialsArr {
			let actionWithText = PaddingLabel()
			actionWithText.translatesAutoresizingMaskIntoConstraints = false
			actionWithText.backgroundColor = UIColor.artDetailScrollColor
			actionWithText.text = value
			actionWithText.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 13)
			actionWithText.textColor = .black
			actionWithText.lineBreakMode = .byWordWrapping
			actionWithText.numberOfLines = 1
			actionWithText.textAlignment = .center
			actionWithText.clipsToBounds = true
			actionWithText.layer.cornerRadius = 15

			stackView.addArrangedSubview(actionWithText)
			NSLayoutConstraint.activate([
				actionWithText.heightAnchor.constraint(equalToConstant: 30),
			])
		}

		scrollView.showsHorizontalScrollIndicator = false
		scrollView2.showsHorizontalScrollIndicator = false

		subANdStyle.textColor = .label
		subANdStyle.text = "Subject & Styles"
		subANdStyle.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 20)

		materials.textColor = .label
		materials.text = "Materials"
		materials.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 20)
	}
}
