//
//  ProductDetailsController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-19.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit
import FSPagerView
import SDWebImage

class ProductDetailsController: UIViewController, FSPagerViewDataSource, FSPagerViewDelegate  {

	var kTableHeaderHeight:CGFloat = 500.0
	var desc =  "Contrary to popular belief, Lorem Ipsum is no It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum  by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham."
	var imageArray = [UIImage(named: "testing"), UIImage(named: "testing2"), UIImage(named: "testing3"), UIImage(named: "nature1"), UIImage(named: "nature2")]
	var tableView: UITableView!
	var pagerView = FSPagerView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIDevice.current.userInterfaceIdiom == .pad ? 650 : 500))
	var pageControl = FSPageControl()
	var descriptionHeight = CGFloat(150)

    override func viewDidLoad() {
        super.viewDidLoad()
		addLightBarButtonItems()
		addRightBarButtonItems()
		setUpTableView()
		setUpImageContainer(imageCount: 5)
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationItem.setHidesBackButton(true, animated: animated)
		navigationController?.setNavigationBarHidden(false, animated: animated)
	}

	func setUpTableView() {
		tableView = UITableView.init(frame: .zero, style: .grouped)
		tableView.dataSource = self
		tableView.delegate = self
		tableView.separatorStyle = .none
		tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.contentInsetAdjustmentBehavior = .never
		tableView.sectionFooterHeight = .leastNormalMagnitude
		tableView.backgroundColor = .secondarySystemGroupedBackground
		view.addSubview(tableView)

		NSLayoutConstraint.activate([
			tableView.topAnchor.constraint(equalTo: view.topAnchor),
			tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])

		tableView.register(HeaderSection.self, forHeaderFooterViewReuseIdentifier: HeaderSection.identifier)
		tableView.register(FifthSectionCell.self, forCellReuseIdentifier: FifthSectionCell.identifier)
		tableView.register(FourthSectionCell.self, forCellReuseIdentifier: FourthSectionCell.identifier)
		tableView.register(ThirdSectionCell.self, forCellReuseIdentifier: ThirdSectionCell.identifier)
		tableView.register(SecondSectionCell.self, forCellReuseIdentifier: SecondSectionCell.identifier)
		tableView.register(FirstSectionCell.self, forCellReuseIdentifier: FirstSectionCell.identifier)
	}

	func updateHeaderView() {
		guard let header = tableView.headerView(forSection: 0) as? HeaderSection else { return }

		var headerRect = CGRect(x: 0, y: -kTableHeaderHeight, width: tableView.bounds.width, height: kTableHeaderHeight)
		if tableView.contentOffset.y < -kTableHeaderHeight {
			headerRect.origin.y = tableView.contentOffset.y
			headerRect.size.height = -tableView.contentOffset.y
		}
		header.frame = headerRect
	}

	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		updateHeaderView()
	}

	//Set Up image Container
	func setUpImageContainer(imageCount: Int) {
		//Mark pageviewer
		pagerView.dataSource = self
		pagerView.delegate = self
		pagerView.itemSize = FSPagerView.automaticSize
		pagerView.interitemSpacing = 10
		pagerView.isInfinite = false
		pagerView.transformer = FSPagerViewTransformer(type: .crossFading)
		pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")

		// Create a page control
		pageControl = FSPageControl(frame: CGRect(x: 0, y: 0, width: 100, height: 10))
		pageControl.numberOfPages = imageCount
		pageControl.contentHorizontalAlignment = .center
		pageControl.hidesForSinglePage = true
		pageControl.backgroundColor = .clear 
		pageControl.setStrokeColor(.clear, for: .normal)
		pageControl.setStrokeColor(.clear, for: .selected)
		pageControl.setFillColor(UIColor.lightGray, for: .normal)
		pageControl.setFillColor(.label, for: .selected)
		pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
	}
    
	//Setting up right bar button
	func addLightBarButtonItems() {
		let backButton = UIButton.init(type: .custom)
		backButton.backgroundColor = .secondarySystemGroupedBackground
		let sImage = UIImage(systemName: "chevron.backward", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		backButton.setImage(sImage, for: .normal)
		backButton.cornerRadius = 15
		backButton.addTarget(self, action: #selector(backToPrevious ), for: .touchUpInside)

		let leftBarButton = UIBarButtonItem(customView: backButton)
		self.navigationItem.leftBarButtonItem = leftBarButton

		backButton.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			backButton.heightAnchor.constraint(equalToConstant: 30),
			backButton.widthAnchor.constraint(equalToConstant:  30)
		])
	}

	func addRightBarButtonItems() 	{
		let shareButton = UIButton.init(type: .custom)
		shareButton.backgroundColor = .secondarySystemGroupedBackground
		let sImage = UIImage(systemName: "square.and.arrow.up", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		shareButton.setImage(sImage, for: .normal)
		shareButton.cornerRadius = 15
		shareButton.addTarget(self, action: #selector(shareAction), for: .touchUpInside)

		let favouredButton = UIButton.init(type: .custom)
		favouredButton.backgroundColor = .secondarySystemGroupedBackground
		let fImage = UIImage(systemName: "suit.heart", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		favouredButton.setImage(fImage, for: .normal)
		favouredButton.cornerRadius = 15
		favouredButton.addTarget(self, action: #selector(favouredAction), for: .touchUpInside)

		let stackview = UIStackView.init(arrangedSubviews: [shareButton, favouredButton])
		stackview.distribution = .fillEqually
		stackview.axis = .horizontal
		stackview.alignment = .center
		stackview.spacing = 10

		shareButton.translatesAutoresizingMaskIntoConstraints = false
		favouredButton.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			shareButton.heightAnchor.constraint(equalToConstant: 30),
			shareButton.widthAnchor.constraint(equalToConstant:  30),
			favouredButton.heightAnchor.constraint(equalToConstant: 30),
			favouredButton.widthAnchor.constraint(equalToConstant:  30)
		])

		let rightBarButton = UIBarButtonItem(customView: stackview)
		self.navigationItem.rightBarButtonItem = rightBarButton
	}

	@objc func shareAction() {
	}

	@objc func favouredAction() {
	}

	@objc func backToPrevious() {
		navigationController?.popViewController(animated: true)
	}
}

extension ProductDetailsController: UITableViewDataSource, UITableViewDelegate {

	func numberOfSections(in tableView: UITableView) -> Int { return PDSections.numberOfSections }

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let section = PDSections(rawValue: section) else {
			return 0
		}
		switch section {
			case .firstSection:
				return 1
			case .secondSection:
				return 1
			case .thirdSection:
				return 1
			case .fourthSection:
				return 1
			case .fifthSection:
				return 1
		}
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let section = PDSections(rawValue: indexPath.section) else {
			let cell = UITableViewCell()
			return cell
		}
		switch section {
			case .firstSection:
				let cell = tableView.dequeueReusableCell(withIdentifier: FirstSectionCell.identifier ) as! FirstSectionCell
				cell.selectionStyle = .none
				return cell
			case .secondSection:
				let cell = tableView.dequeueReusableCell(withIdentifier: SecondSectionCell.identifier ) as! SecondSectionCell
				cell.selectionStyle = .none
				cell.delegate = self
				//Read more text added.
				let readmoreFont = UIFont(name: "AppleSDGothicNeo-Regular", size: 13)
				cell.descriptionText.addTrailing(with: "...", moreText: "more", moreTextFont: readmoreFont!, moreTextColor: .lightGray)
				return cell
			case .thirdSection:
				let cell = tableView.dequeueReusableCell(withIdentifier: ThirdSectionCell.identifier ) as! ThirdSectionCell
				cell.selectionStyle = .none
				return cell
			case .fourthSection:
				let cell = tableView.dequeueReusableCell(withIdentifier: FourthSectionCell.identifier ) as! FourthSectionCell
				cell.selectionStyle = .none
				return cell
			case .fifthSection:
				let cell = tableView.dequeueReusableCell(withIdentifier: FifthSectionCell.identifier ) as! FifthSectionCell
				cell.selectionStyle = .none
				return cell
		}
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		guard let section = PDSections(rawValue: indexPath.section) else { return 0 }
		switch section {
			case .firstSection:
			 	if let cell = tableView.dequeueReusableCell(withIdentifier:FirstSectionCell.identifier ) as? FirstSectionCell {
					let value = cell.title.textHeight(withWidth: tableView.bounds.size.width, fontSize: 20)
					return 400 + value
				}
				return 400
			case .secondSection:
				return descriptionHeight
			case .thirdSection:
				return 230
			case .fourthSection:
				return 270
			case .fifthSection:
				return  UIDevice.current.userInterfaceIdiom == .pad ? 400 : 230
		}
	}

	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: HeaderSection.identifier) as! HeaderSection
		if section == 0 {
			headerView.customView.addSubview(pagerView)
			headerView.scrollView.delegate = self
			headerView.scrollView.minimumZoomScale = 1.0
			headerView.scrollView.maximumZoomScale = 2.0

			pageControl.translatesAutoresizingMaskIntoConstraints = false
			headerView.pageControl.addSubview(pageControl)
			NSLayoutConstraint.activate([
				pageControl.centerYAnchor.constraint(equalTo: headerView.pageControl.centerYAnchor),
				pageControl.centerXAnchor.constraint(equalTo:headerView.pageControl.centerXAnchor)
			])
			return headerView
		}
		return headerView
	}

	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		guard let section = PDSections(rawValue: section) else { return 0 }
		switch section {
			case .firstSection:
				return 500
			case .secondSection:
				return .leastNormalMagnitude
			case .thirdSection:
				return .leastNormalMagnitude
			case .fourthSection:
				return .leastNormalMagnitude
			case .fifthSection:
				return .leastNormalMagnitude
		}
	}

	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return .leastNormalMagnitude
	}
}

// Handling images on detail page
extension ProductDetailsController {
	func numberOfItems(in pagerView: FSPagerView) -> Int {
		return 5
	}

	func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
		let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
		let imageLocation = imageArray[index]
		cell.imageView?.contentMode = .scaleAspectFill
		cell.imageView?.clipsToBounds = false
		cell.imageView?.sd_imageTransition = SDWebImageTransition.fade
		cell.imageView?.image = imageLocation
		return cell
	}

	func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
		pagerView.deselectItem(at: index, animated: true)
		pagerView.scrollToItem(at: index, animated: true)
	}

	func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
		self.pageControl.currentPage = targetIndex
	}
}

extension ProductDetailsController: ProductDescriptionDelegate {
	//show more action.
	func showMore(cell: SecondSectionCell, sender: UIButton) {
		guard let indexPath = self.tableView.indexPath(for: cell) else {
			return
		}
		if #available(iOS 11.0, *) {
			let cell = tableView.cellForRow(at: indexPath) as! SecondSectionCell
			tableView.performBatchUpdates({() -> Void in
				cell.descriptionText.text = desc
				let height = cell.descriptionText.textHeight(withWidth: tableView.bounds.size.width, fontSize: 13)
				descriptionHeight = height + 120
			}, completion: {(_ finished: Bool) -> Void in })
		} else {
			tableView.beginUpdates()
			cell.descriptionText.text = desc
			tableView.endUpdates()
		}
	}
}

enum PDSections: Int {
	case firstSection
	case secondSection
	case thirdSection
	case fourthSection
	case fifthSection
	static var numberOfSections: Int {
		return 5
	}
}

