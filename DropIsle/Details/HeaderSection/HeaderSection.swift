//
//  FirstSection.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-19.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class HeaderSection: UITableViewHeaderFooterView, Reusable {

	let imageViewer = UIView()
	let scrollView = UIScrollView()
	let pageControl = UIView()
	let customView = CustomView(frame: .zero)

	override init(reuseIdentifier: String?) {
		super.init(reuseIdentifier: reuseIdentifier)
		setup()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override var intrinsicContentSize: CGSize {
		return CGSize(width: 2000, height: 2000)
	}
	
	func setup() {
		customView.translatesAutoresizingMaskIntoConstraints = false
		scrollView.translatesAutoresizingMaskIntoConstraints = false
		pageControl.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(scrollView)
		scrollView.addSubview(customView)
		scrollView.addSubview(pageControl)

		NSLayoutConstraint.activate([
			scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
			scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
			scrollView.topAnchor.constraint(equalTo: topAnchor),
			scrollView.bottomAnchor.constraint(equalTo: bottomAnchor),

			customView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
			customView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
			customView.topAnchor.constraint(equalTo: scrollView.topAnchor),
			customView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),

			customView.heightAnchor.constraint(equalTo: scrollView.heightAnchor),
			customView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),

			pageControl.heightAnchor.constraint(equalToConstant: 10),
			pageControl.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
			pageControl.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
			pageControl.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -10)
		])
	}
}

class CustomView: UIView {
	override init(frame: CGRect) {
		super.init(frame: frame)
		commonInit()
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
		commonInit()
	}

	private func commonInit() {
	}
}
