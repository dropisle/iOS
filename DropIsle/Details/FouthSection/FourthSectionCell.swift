//
//  FourthSectionCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-22.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class FourthSectionCell: UITableViewCell, Reusable {

	let divider = UIView()
	let divider2 = UIView()
	let divider3 = UIView()
	let container = UIView()
	let hStack = UIStackView()
	let vStack1 = UIView()
	let vStack2 = UIView()
	let vStack3 = UIView()
	let rareArt = UILabel()
	let shipping = UILabel()
	let subShipping = UILabel()
	let cert = UILabel()
	let subCert = UILabel()

	//
	let name = UILabel()
	let country = UILabel()
	let briefDesc = UILabel()
	let avartar = UIImageView()
	let follow = UIButton()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		//Initialization code
		setUp()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func setUp() {
		hStack.axis = .horizontal
		hStack.distribution = .fillEqually
		hStack.alignment = .fill
		hStack.spacing = 0

		divider.translatesAutoresizingMaskIntoConstraints = false
		divider2.translatesAutoresizingMaskIntoConstraints = false
		container.translatesAutoresizingMaskIntoConstraints = false
		hStack.translatesAutoresizingMaskIntoConstraints = false
		vStack1.translatesAutoresizingMaskIntoConstraints = false
		vStack2.translatesAutoresizingMaskIntoConstraints = false
		vStack3.translatesAutoresizingMaskIntoConstraints = false
		rareArt.translatesAutoresizingMaskIntoConstraints = false
		shipping.translatesAutoresizingMaskIntoConstraints = false
		subShipping.translatesAutoresizingMaskIntoConstraints = false
		cert.translatesAutoresizingMaskIntoConstraints = false
		subCert.translatesAutoresizingMaskIntoConstraints = false

		name.translatesAutoresizingMaskIntoConstraints = false
		country.translatesAutoresizingMaskIntoConstraints = false
		briefDesc.translatesAutoresizingMaskIntoConstraints = false
		avartar.translatesAutoresizingMaskIntoConstraints = false
		follow.translatesAutoresizingMaskIntoConstraints = false
		divider3.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(divider)
		contentView.addSubview(divider2)
		contentView.addSubview(container)
		container.addSubview(hStack)

		hStack.addArrangedSubview(vStack1)
		hStack.addArrangedSubview(vStack2)
		hStack.addArrangedSubview(vStack3)
		vStack1.addSubview(rareArt)
		vStack2.addSubview(shipping)
		vStack2.addSubview(subShipping)
		vStack3.addSubview(cert)
		vStack3.addSubview(subCert)

		contentView.addSubview(name)
		contentView.addSubview(country)
		contentView.addSubview(briefDesc)
		contentView.addSubview(avartar)
		contentView.addSubview(follow)
		contentView.addSubview(divider3)

		NSLayoutConstraint.activate([
			divider.heightAnchor.constraint(equalToConstant: 0.3),
			divider.topAnchor.constraint(equalTo: topAnchor, constant: 10),
			divider.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			divider.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			container.heightAnchor.constraint(equalToConstant: 70),
			container.topAnchor.constraint(equalTo: divider.bottomAnchor, constant: 10),
			container.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			container.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			hStack.topAnchor.constraint(equalTo: container.topAnchor),
			hStack.bottomAnchor.constraint(equalTo: container.bottomAnchor),
			hStack.leadingAnchor.constraint(equalTo: container.leadingAnchor),
			hStack.trailingAnchor.constraint(equalTo: container.trailingAnchor),

			rareArt.centerYAnchor.constraint(equalTo: vStack1.centerYAnchor),
			rareArt.centerXAnchor.constraint(equalTo: vStack1.centerXAnchor),

			shipping.topAnchor.constraint(equalTo: vStack2.topAnchor, constant: 17.5),
			shipping.centerXAnchor.constraint(equalTo: vStack2.centerXAnchor),
			subShipping.centerXAnchor.constraint(equalTo: vStack2.centerXAnchor),
			subShipping.topAnchor.constraint(equalTo: shipping.bottomAnchor, constant: 5),

			cert.topAnchor.constraint(equalTo: vStack3.topAnchor, constant: 5),
			cert.leadingAnchor.constraint(equalTo: vStack3.leadingAnchor, constant: 0),
			cert.trailingAnchor.constraint(equalTo: vStack3.trailingAnchor, constant: -0),

			subCert.topAnchor.constraint(equalTo: cert.bottomAnchor, constant: 5),
			subCert.leadingAnchor.constraint(equalTo: vStack3.leadingAnchor, constant: 5),
			subCert.trailingAnchor.constraint(equalTo: vStack3.trailingAnchor, constant: -5),

			divider2.heightAnchor.constraint(equalToConstant: 0.3),
			divider2.topAnchor.constraint(equalTo: container.bottomAnchor, constant: 10),
			divider2.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			divider2.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			//
			name.topAnchor.constraint(equalTo: divider2.bottomAnchor, constant: 10),
			name.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			name.trailingAnchor.constraint(equalTo: avartar.leadingAnchor, constant: -5),

			country.topAnchor.constraint(equalTo: name.bottomAnchor, constant: 10),
			country.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			country.trailingAnchor.constraint(equalTo: avartar.leadingAnchor, constant: -5),

			briefDesc.topAnchor.constraint(equalTo: country.bottomAnchor, constant: 25),
			briefDesc.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),

			avartar.topAnchor.constraint(equalTo: divider2.bottomAnchor, constant: 10),
			avartar.heightAnchor.constraint(equalToConstant: 60),
			avartar.widthAnchor.constraint(equalToConstant: 60),
			avartar.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			follow.topAnchor.constraint(equalTo: briefDesc.bottomAnchor, constant: 10),
			follow.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			follow.widthAnchor.constraint(equalToConstant: 140),
			follow.heightAnchor.constraint(equalToConstant: 45),

			divider3.heightAnchor.constraint(equalToConstant: 0.3),
			divider3.topAnchor.constraint(equalTo: follow.bottomAnchor, constant: 10),
			divider3.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			divider3.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			
		])

		divider.backgroundColor = .lightGray
		divider2.backgroundColor = .lightGray
		divider3.backgroundColor = .lightGray

		vStack1.clipsToBounds = true
		vStack2.clipsToBounds = true
		vStack3.clipsToBounds = true

		rareArt.textColor = .label
		rareArt.lineBreakMode = .byWordWrapping
		rareArt.numberOfLines = 0
		rareArt.text = "Rare \nArt"
		rareArt.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16)

		shipping.textColor = .label
		shipping.lineBreakMode = .byWordWrapping
		shipping.numberOfLines = 0
		shipping.text = "Shipping"
		shipping.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16)

		subShipping.textColor = .label
		subShipping.lineBreakMode = .byWordWrapping
		subShipping.numberOfLines = 0
		subShipping.text = "included"
		subShipping.textAlignment = .center
		subShipping.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 13)

		cert.textColor = .label
		cert.lineBreakMode = .byWordWrapping
		cert.numberOfLines = 0
		cert.textAlignment = .center
		cert.text = "Certification of \nAuthentication"
		cert.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16)

		subCert.textColor = .label
		subCert.lineBreakMode = .byWordWrapping
		subCert.numberOfLines = 0
		subCert.text = "included"
		subCert.textAlignment = .center
		subCert.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 13)

		name.textColor = .label
		name.lineBreakMode = .byTruncatingTail
		name.numberOfLines = 0
		name.text = "Nadi Bay"
		name.textAlignment = .justified
		name.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 16)

		country.textColor = .lightGray
		country.lineBreakMode = .byTruncatingTail
		country.numberOfLines = 0
		country.text = "Canada"
		country.textAlignment = .justified
		country.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14)

		briefDesc.textColor = .label
		briefDesc.lineBreakMode = .byTruncatingTail
		briefDesc.numberOfLines = 0
		briefDesc.text = "16 year old enthusiast...."
		briefDesc.textAlignment = .justified
		briefDesc.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14)

		avartar.image = UIImage(named: "nature3")
		avartar.clipsToBounds = true
		avartar.layer.cornerRadius = 30
		avartar.contentMode = .scaleAspectFill

		follow.setTitle("Follow", for: .normal)
		follow.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18)
		follow.backgroundColor = UIColor.NavyBlueDark
		follow.cornerRadius = 4
		follow.setTitleColor(.lightGray, for: .normal)
	}
}
