//
//  SimilarArtworkHeaderView.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-23.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class SimilarArtworkHeaderView: UICollectionReusableView {

	let titleText = UILabel()

	override init(frame: CGRect) {
		super.init(frame: frame)
		setUp()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func setUp() {
		titleText.translatesAutoresizingMaskIntoConstraints = false
		addSubview(titleText)

		NSLayoutConstraint.activate([
			titleText.leadingAnchor.constraint(equalTo: leadingAnchor),
			titleText.trailingAnchor.constraint(equalTo: trailingAnchor),
			titleText.topAnchor.constraint(equalTo: topAnchor),
			titleText.bottomAnchor.constraint(equalTo: bottomAnchor),
		])

		titleText.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 16)
		titleText.textColor = .label
		titleText.text = "Similar Artwork"
		
	}
}
