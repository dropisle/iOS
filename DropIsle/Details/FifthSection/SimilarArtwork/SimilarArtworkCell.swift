//
//  SimilarArtworkCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-23.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class SimilarArtworkCell: UICollectionViewCell, AppConfigurable {

	let imageView = UIImageView()
	let favourited = HeartButton() 
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}

	private func setup() {
		imageView.translatesAutoresizingMaskIntoConstraints = false
		favourited.translatesAutoresizingMaskIntoConstraints = false
		contentView.addSubview(imageView)
		imageView.addSubview(favourited)

		NSLayoutConstraint.activate([
			imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
			imageView.trailingAnchor.constraint(equalTo: trailingAnchor),
			imageView.topAnchor.constraint(equalTo: topAnchor),
			imageView.bottomAnchor.constraint(equalTo: bottomAnchor),

			favourited.trailingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: -10),
			favourited.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 10),
			favourited.heightAnchor.constraint(equalToConstant: 20),
			favourited.widthAnchor.constraint(equalToConstant: 23),
		])
		style()
	}

	private func style() {
		imageView.clipsToBounds = true
		imageView.contentMode = .scaleAspectFill
		imageView.cornerRadius = 10

		favourited.filled = true
		favourited.strokeColor = .white
		favourited.fillColor = .systemRed
	}

	func configure(with app: App) {
		imageView.image = app.image
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

