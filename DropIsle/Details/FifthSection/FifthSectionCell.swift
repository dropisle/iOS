//
//  FifthSectionCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-23.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class FifthSectionCell: UITableViewCell, Reusable, UICollectionViewDelegate, UICollectionViewDataSource {

	var collectionView: UICollectionView!
	private var presenter = RecommendedPresenter() 
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
		setupCollectionView()
    }

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		//Initialization code
		setupCollectionView()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func setupCollectionView() {
		// Initialises the collection view with a CollectionViewLayout which we will define
		collectionView = UICollectionView.init(frame: .zero, collectionViewLayout: makeLayout())
		// Assigning data source and background color
		collectionView.dataSource = self
		collectionView.delegate = self
		collectionView.backgroundColor = .secondarySystemGroupedBackground
		collectionView.alwaysBounceVertical = false

		// Adding the collection view to the view
		collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		contentView.addSubview(collectionView)

		// This line tells the system we will define our own constraints
		collectionView.translatesAutoresizingMaskIntoConstraints = false
		// Constraining the collection view to the 4 edges of the view
		NSLayoutConstraint.activate([
			collectionView.topAnchor.constraint(equalTo: contentView.topAnchor),
			collectionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
			collectionView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
			collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
		])

		// Registering all Cells and Classes we will need
		collectionView.register(SimilarArtworkHeaderView.self,
								forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
								withReuseIdentifier: SimilarArtworkHeaderView.identifier)
		collectionView.register(SimilarArtworkCell.self,
								forCellWithReuseIdentifier: SimilarArtworkCell.identifier)
	}

	//Creates the appropriate UICollectionViewLayout for each section type
	private func makeLayout() -> UICollectionViewLayout {
		// Constructs the UICollectionViewCompositionalLayout
		let layout = UICollectionViewCompositionalLayout { (sectionIndex: Int, layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
			let isWideView = layoutEnvironment.container.effectiveContentSize.width > 500
			switch self.presenter.sectionType(for: sectionIndex) {
				case .singleSectionList:   return self.createRecommendedForYouSection(isWide: isWideView)
			}
		}

		//Configure the Layout with interSectionSpacing
		let config = UICollectionViewCompositionalLayoutConfiguration()
		config.interSectionSpacing = 20
		layout.configuration = config
		return layout
	}

	//Creates a layout that shows 3 items per group and scrolls horizontally
	private func createRecommendedForYouSection(isWide: Bool) -> NSCollectionLayoutSection {
		// Defining the size of a single item in this layout
		let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
											  heightDimension: .fractionalHeight(1))
		// Construct the Layout Item
		let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)

		// Configure the Layout Item
		layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 0)

		// Defining the size of a group in this layout
		let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .absolute(isWide ? 250 : 145), heightDimension: .estimated(isWide ? 290 : 137))

		// Constructing the Layout Group
		let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize, subitems: [layoutItem])

		// Constructing the Layout Section
		let layoutSection = NSCollectionLayoutSection(group: layoutGroup)

		// Configuring the Layout Section
		layoutSection.orthogonalScrollingBehavior = .continuous

		// Constructing the Section footer
		let layoutSectionFooter = createSectionHeader()

		// Adding the Section Header to the Layout Section
		layoutSection.boundarySupplementaryItems = [layoutSectionFooter]

		return layoutSection
	}

	//Creates a Layout for the SectionHeader
	private func createSectionHeader() -> NSCollectionLayoutBoundarySupplementaryItem {
		// Define size of Section Header
		let layoutSectionHeaderSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.90), heightDimension: .absolute(60))
		// Construct Section Header Layout
		let layoutSectionHeader = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: layoutSectionHeaderSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
		return layoutSectionHeader
	}

	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		if kind == UICollectionView.elementKindSectionHeader {
			guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SimilarArtworkHeaderView.identifier, for: indexPath) as? SimilarArtworkHeaderView else {
				fatalError("Could not dequeue SectionHeader")
			}
			return headerView
		}
		return UICollectionReusableView()
	}

	//Tells the UICollectionView how many sections are needed
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return presenter.numberOfSections
	}

	//Tells the UICollectionView how many items the requested sections needs
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return presenter.numberOfItems(for: section)
	}

	//Constructs and configures the item needed for the requested IndexPath
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		// Checks what section type we should use for this indexPath so we use the right cells for that section
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SimilarArtworkCell.identifier, for: indexPath) as? SimilarArtworkCell else {
			fatalError("Could not dequeue FeatureCell")
		}
		switch presenter.sectionType(for: indexPath.section) {
			case .singleSectionList:
				presenter.configure(item: cell, for: indexPath)
		return cell
		}
	}
}
