//
//  SecondSectionCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-19.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol ProductDescriptionDelegate: class {
	func showMore(cell: SecondSectionCell, sender: UIButton)
}

class SecondSectionCell: UITableViewCell, Reusable {

	var desc =  "Contrary to popular belief, Lorem Ipsum is no It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum  by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham."

	let descriptionTitle = UILabel()
	let descriptionText = UILabel()
	weak var delegate: ProductDescriptionDelegate?

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}

	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		// Configure the view for the selected state
	}

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		//Initialization code
		let tap = UITapGestureRecognizer(target: self, action: #selector(showMoreDecription))
		tap.numberOfTapsRequired = 1
		descriptionText.isUserInteractionEnabled = true
		descriptionText.addGestureRecognizer(tap)
		setup()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	@objc func showMoreDecription(_ sender: UIButton) {
		delegate?.showMore(cell: self, sender: sender)
	}

	func setup() {
		descriptionTitle.translatesAutoresizingMaskIntoConstraints = false
		descriptionText.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(descriptionTitle)
		contentView.addSubview(descriptionText)

		NSLayoutConstraint.activate([
			descriptionTitle.heightAnchor.constraint(equalToConstant: 40.0),
			descriptionTitle.topAnchor.constraint(equalTo: topAnchor, constant: 20),
			descriptionTitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			descriptionTitle.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			descriptionText.topAnchor.constraint(equalTo: descriptionTitle.bottomAnchor, constant: 10),
			descriptionText.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			descriptionText.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			descriptionText.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
		])

		descriptionTitle.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 20)
		descriptionTitle.text = "Art Description"

		descriptionText.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 13)
		descriptionText.lineBreakMode = .byWordWrapping
		descriptionText.numberOfLines = 0
		descriptionText.text = desc
	}
}
