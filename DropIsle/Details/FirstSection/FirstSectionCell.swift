//
//  FirstSectionCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-19.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class FirstSectionCell: UITableViewCell, Reusable {

	let title = UILabel()
	let price = UILabel()
	let firstDivider = UIView()
	let size = UILabel()
	let dimension = PaddingLabel()
	let container = UIView()
	let quantity = UILabel()
	let secondDivider = UIView()

	let plus = UIButton()
	let qCount = UILabel()
	let minus = UIButton()

	let addToCart = UIButton()
	let buyNow = UIButton()
	let thirdDivider = UIView()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setup()
		_updateColors()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func setup() {
		title.translatesAutoresizingMaskIntoConstraints = false
		price.translatesAutoresizingMaskIntoConstraints = false
		firstDivider.translatesAutoresizingMaskIntoConstraints = false
		size.translatesAutoresizingMaskIntoConstraints = false
		dimension.translatesAutoresizingMaskIntoConstraints = false
		container.translatesAutoresizingMaskIntoConstraints = false
		quantity.translatesAutoresizingMaskIntoConstraints = false
		plus.translatesAutoresizingMaskIntoConstraints = false
		qCount.translatesAutoresizingMaskIntoConstraints = false
		minus.translatesAutoresizingMaskIntoConstraints = false
		secondDivider.translatesAutoresizingMaskIntoConstraints = false
		addToCart.translatesAutoresizingMaskIntoConstraints = false
		buyNow.translatesAutoresizingMaskIntoConstraints = false
		thirdDivider.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(title)
		contentView.addSubview(price)
		contentView.addSubview(firstDivider)
		contentView.addSubview(size)
		contentView.addSubview(dimension)
		contentView.addSubview(container)
		container.addSubview(quantity)
		container.addSubview(plus)
		container.addSubview(qCount)
		container.addSubview(minus)
		contentView.addSubview(secondDivider)
		contentView.addSubview(addToCart)
		contentView.addSubview(buyNow)
		contentView.addSubview(thirdDivider)

		NSLayoutConstraint.activate([
			title.topAnchor.constraint(equalTo: topAnchor, constant: 20),
			title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			title.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			price.leadingAnchor.constraint(equalTo: title.leadingAnchor),
			price.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 10),

			firstDivider.heightAnchor.constraint(equalToConstant: 0.3),
			firstDivider.topAnchor.constraint(equalTo: price.bottomAnchor, constant: 10),
			firstDivider.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			firstDivider.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			size.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			size.topAnchor.constraint(equalTo: firstDivider.bottomAnchor, constant: 10),

			dimension.heightAnchor.constraint(equalToConstant: 40),
			dimension.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			dimension.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			dimension.topAnchor.constraint(equalTo: size.bottomAnchor, constant: 10),

			container.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			container.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			container.topAnchor.constraint(equalTo: dimension.bottomAnchor, constant: 10),

			quantity.leadingAnchor.constraint(equalTo: container.leadingAnchor),
			quantity.centerYAnchor.constraint(equalTo: container.centerYAnchor),

			plus.heightAnchor.constraint(equalToConstant: 25),
			plus.widthAnchor.constraint(equalToConstant: 25),
			plus.centerYAnchor.constraint(equalTo: container.centerYAnchor),
			plus.trailingAnchor.constraint(equalTo: container.trailingAnchor),

			qCount.heightAnchor.constraint(equalToConstant: 25),
			qCount.widthAnchor.constraint(equalToConstant: 25),
			qCount.centerYAnchor.constraint(equalTo: container.centerYAnchor),
			qCount.trailingAnchor.constraint(equalTo: plus.leadingAnchor, constant: -8),

			minus.heightAnchor.constraint(equalToConstant: 25),
			minus.widthAnchor.constraint(equalToConstant: 25),
			minus.centerYAnchor.constraint(equalTo: container.centerYAnchor),
			minus.trailingAnchor.constraint(equalTo: qCount.leadingAnchor, constant: -8),

			secondDivider.heightAnchor.constraint(equalToConstant: 0.3),
			secondDivider.topAnchor.constraint(equalTo: container.bottomAnchor, constant: 10),
			secondDivider.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			secondDivider.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			addToCart.heightAnchor.constraint(equalToConstant: 55),
			addToCart.topAnchor.constraint(equalTo: secondDivider.bottomAnchor, constant: 15),
			addToCart.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			addToCart.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			buyNow.heightAnchor.constraint(equalToConstant: 55),
			buyNow.topAnchor.constraint(equalTo: addToCart.bottomAnchor, constant: 10),
			buyNow.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			buyNow.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			thirdDivider.heightAnchor.constraint(equalToConstant: 0.3),
			thirdDivider.topAnchor.constraint(equalTo: buyNow.bottomAnchor, constant: 15),
			thirdDivider.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			thirdDivider.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			thirdDivider.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
		])

		title.textColor = .label
		title.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 20)
		title.text = "Painting splash oil by Nadi Bay"
		title.lineBreakMode = .byWordWrapping
		title.numberOfLines = 0

		price.textColor = .label
		price.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 22)
		price.text = "CAD $300"

		firstDivider.backgroundColor = .lightGray

		size.textColor = .label
		size.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 18)
		size.text = "Size"

		dimension.cornerRadius = 5
		dimension.borderWidth = 0.3
		dimension.borderColor = .lightGray
		dimension.textColor = .lightGray
		dimension.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		dimension.text = "39.8 H X 27 W X 0.9 cm"

		quantity.text = "Quantity"
		quantity.textColor = .label
		quantity.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 18)

		qCount.text = "2"
		qCount.textAlignment = .center
		qCount.textColor = .label
		qCount.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 18)

		let plusImage = UIImage(systemName: "plus", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		plus.setImage(plusImage, for: .normal)
		plus.cornerRadius = 12.5
		plus.borderWidth = 2

		let minusImage = UIImage(systemName: "minus", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		minus.setImage(minusImage, for: .normal)
		minus.cornerRadius = 12.5
		minus.borderWidth = 2

		secondDivider.backgroundColor = .lightGray

		addToCart.cornerRadius = 7
		addToCart.borderColor = .secondarySystemGroupedBackground
		addToCart.borderWidth = 2
		addToCart.setTitle("Add to Cart", for: .normal)
		addToCart.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 22)
		addToCart.backgroundColor = UIColor.NavyBlueDark

		buyNow.cornerRadius = 7
		buyNow.borderColor = .secondarySystemGroupedBackground
		buyNow.borderWidth = 2
		buyNow.setTitle("Buy Now", for: .normal)
		buyNow.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 22)
		buyNow.backgroundColor = UIColor.BuyNowColor

		thirdDivider.backgroundColor = .lightGray
	}

	private func _updateColors() {
		plus.layer.borderColor = UIColor.dynamic(light: UIColor.label, dark: UIColor.label).cgColor
		minus.layer.borderColor = UIColor.dynamic(light: UIColor.label, dark: UIColor.label).cgColor
	}

	override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
		super.traitCollectionDidChange(previousTraitCollection)
		self._updateColors()
		self.setNeedsDisplay()
	}
}
