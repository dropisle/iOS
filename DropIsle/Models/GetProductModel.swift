//
//  GetProductModel.swift
//  DropIsle
//
//  Created by CtanLI on 2021-03-13.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import Foundation

struct GetProductModel: Codable {
	let products: [product]
	let nextCursor: String?

	struct product: Codable {
		let description: String?
		let tags: [String]?
		let price: Double?
		let isDeleted: Bool?
		let quantity: Int?
		let sku: String?
		let barcode: String?
		let trackInventory: Bool?
		let productid: String?
		let storeid: String?
		let title: String?
		let condition: String?
		let category: String?
		let type: String?
		let sustainability: String?
		let cost: Double?
		let margin: Double?
		let profit: Double?
		let minPrice: Double?
		let maxPrice: Double?
		let shippingProfiles: [String]?
		let packageProfiles: [String]?
		let __v: Int?
	}
}

struct GetProductImageModel: Codable {
	let isDelete: Bool?
	let size: Int?
	let productid: String?
	let filename: String?
	let createdOn: String?
	let url: String?
	let __v: Int?
	let productimageid: String?
}

//struct CartsModel: Codable {
//	let cart: [Carts]
//}
//
//struct Carts: Codable {
//	let items: [Items]
//	let isCheckedOut: Bool
//	let isDeleted: Bool
//	let subtotal: Int
//	let currency: String
//	let customer: Customer
//	let orders: [Orders]
//	let __v: Int
//	let cartid: String
//}
//
//struct Items: Codable {
//	let storeid: String
//	let productid: String
//	let quantity: Int
//}
//
//struct Customer: Codable {
//	let buyerid: String
//	let name: String
//}
//
//struct Orders: Codable {
//}
