//
//  PaymentIntentModel.swift
//  DropIsle
//
//  Created by CtanLI on 2021-03-31.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import Foundation

struct  PaymentIntentModel: Codable {
	let paymentIntent: PaymentIntent
}

struct PaymentIntent: Codable {
	let id: String?
	let object: String?
	let amount: Int?
	let amount_capturable: Int?
	let amount_received: Int?
	let application: String?
	let application_fee_amount: Int?
	let canceled_at: String?
	let cancellation_reason: String?
	let capture_method: String?
	let charges: Charges?

	let client_secret: String?
	let confirmation_method: String?
	let created: Int?
	let currency: String?
	let customer: String?
	let description: String?
	let invoice: String?
	let last_payment_error: String?
	let livemode: Bool?
	let metadata: MetaData?

	let next_action: String?
	let on_behalf_of: String?
	let payment_method: String?
	let payment_method_options: PaymentMethodOptions?
	let payment_method_types: [String]?

	let receipt_email: String?
	let review: String?
	let setup_future_usage: String?
	let shipping: String?
	let source: String?
	let statement_descriptor: String?
	let statement_descriptor_suffix: String?
	let status: String?
	let transfer_data: String?
	let transfer_group: String?
}

struct Charges: Codable {
	let object: String?
	let data: [DataValues]?
	let has_more: Bool?
	let total_count: Int?
	let url: String?
}

struct DataValues: Codable {
	let id: String?
	let object: String?
	let amount: Int?
	let amount_captured: Int?
	let amount_refunded: Int?
	let application: String?
	let application_fee: String?
	let application_fee_amount: Int?
	let balance_transaction: String?
	let billing_details: BillingDetails?
	let calculated_statement_descriptor: String?
	let captured: Bool?
	let created: Int?
	let currency: String?
	let customer: String?
	let description: String?
	let destination: String?
	let dispute: String?
	let disputed: Bool?
	let failure_code: String?
	let failure_message: String?
	let fraud_details: FraudDetails?
	let invoice: String?
	let livemode: Bool?
	let metadata: MetaData?
	let on_behalf_of: String?
	let order: String?
	let outcome: OutCome?
	let paid: Bool?
	let payment_intent: String?
	let payment_method: String?
	let payment_method_details: PaymentMethodDetails?

	let receipt_email: String?
	let receipt_number: String?
	let receipt_url: String?
	let refunded: Bool?
	let refunds: Refunds?
	let review: String?
	let shipping: String?
	let source: String?
	let source_transfer: String?
	let statement_descriptor: String?
	let statement_descriptor_suffix: String?
	let status: String?
	let transfer_data: String?
	let transfer_group: String?
}

struct BillingDetails: Codable {
	let address: AddressValue?
	let email: String?
	let name: String?
	let phone: String?
}

struct AddressValue: Codable {
	let city: String?
	let country: String?
	let line1: String?
	let line2: String?
	let postal_code: String?
	let state: String?
}

struct FraudDetails: Codable { }

struct MetaData: Codable {
	let cartid: String?
	let buyerid: String?
}

struct OutCome: Codable {
	let network_status: String?
	let reason: String?
	let risk_level: String?
	let risk_score: Int?
	let seller_message: String?
	let type: String?
}

struct PaymentMethodDetails: Codable {
	let card: Card?
	let type: String?
}

struct Card: Codable {
	let brand: String?
	let checks: Checks?
	let country: String?
	let exp_month: Int?
	let exp_year: Int?
	let fingerprint: String?
	let funding: String?
	let installments: String?
	let last4: String?
	let network: String?
	let three_d_secure: String?
	let wallet: String?
}

struct Checks: Codable {
	let address_line1_check: String?
	let address_postal_code_check: String?
	let cvc_check: String?
}

struct Refunds: Codable {
	let object: String?
	let data: [String]?
	let has_more: Bool?
	let total_count: Int?
	let url: String?
}

struct PaymentMethodOptions: Codable {
	let card: CardValue?
}

struct CardValue: Codable {
	let installments: String?
	let network: String?
	let request_three_d_secure: String?
}
