//
//  CartModel.swift
//  DropIsle
//
//  Created by CtanLI on 2021-03-13.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import Foundation

struct CartModel: Codable {
	let carts: [Carts]

	struct Carts: Codable {
		let items: [Items]
		let isCheckedOut: Bool
		let isDeleted: Bool
		let subtotal: Double
		let currency: String
		let customer: Customers
		let orders: [Orders]?
		let __v: Int
		let cartid: String

		let paymentIntentId: String?
		let createdAt: String?
	}

	struct Items: Codable {
		let storeid: String
		let productid: String
		let quantity: Int
		let productvarientid: String?
	}

	struct Customers: Codable {
		let buyerid: String
		let name: String
	}

	struct Orders: Codable {
		let subtotal: Double?
		let isDeleted: Bool
		let fulfilment: String
		let paid: Bool
		let tax: Double
		let shippingFee: Double
		let currency: String
		let refundedAmount: Int
		let paymentCancelled: Bool
		let customer: Customers
		let items: [OrderItems]?
		let storeid: String
		let cartid: String
		let note: String?
		let orderedOn: String
		let confirmationid: String
		let itemFulfilments: [ItemFulfilments]
		let orderNumber: Int
		let __v: Int
		let transferId: String?
		let orderid: String

		let shippingInfo: ShippingInfo?
		let billingInfo: BillingInfo?
		let refundedItems: [RefundedItems]?
		let cancelledItems: [CancelledItems]?
	}

	struct OrderItems: Codable {
		let _id: String
		let productid: String
		let quantity: Int
		let price: Double?
	}

	struct RefundedItems: Codable {
		let _id: String
		let productid: String
		let quantity: Int
		let price: Double?
	}

	struct CancelledItems: Codable {
		let _id: String
		let productid: String
		let quantity: Int
		let price: Double?
	}

	struct ItemFulfilments: Codable {
		let _id: String
		let productid: String
		let fulfilled: Int
		let quantity: Int
	}

	struct ShippingInfo: Codable {
		let name: String
		let address: String
		let email: String?
	}

	struct BillingInfo: Codable {
		let name: String
		let address: String
		let paymentType: String
		let last4Digits: String
		let email: String?
	}
}



