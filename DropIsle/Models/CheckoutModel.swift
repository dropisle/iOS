//
//  CheckouModel.swift
//  DropIsle
//
//  Created by CtanLI on 2021-03-31.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import Foundation

struct CheckoutModel: Codable {
	let carts: [Carts]

	struct Carts: Codable {
		let items: [Items]
		let isCheckedOut: Bool
		let isDeleted: Bool
		let subtotal: Int
		let currency: String
		let customer: Customers
		let orders: [Orders]
		let __v: Int
		let cartid: String
	}

	struct Items: Codable {
		let storeid: String
		let productid: String
		let quantity: Int
	}

	struct Customers: Codable {
		let buyerid: String
		let name: String
	}

	struct Item: Codable {
		let productid: String
		let quantity: Int
	}

	struct ItemFulfilments: Codable {
		let _id: String
		let productid: String
		let fulfilled: Int
		let quantity: Int
	}

	struct Orders: Codable {
		let subtotal: Int
		let isDeleted: Bool
		let fulfilment: String
		let paid: Bool
		let tax: Int
		let shippingFee: Int
		let currency: String
		let refundedAmount: Int
		let paymentCancelled: Bool
		let customer: Customers
		let item: Item
		let storeid: String
		let cartid: String
		let orderedOn: String
		let confirmationid: String
		let itemFulfilments: [ItemFulfilments]
		let orderNumber: Int
		let __v: Int
		let orderid: String
	}
}
