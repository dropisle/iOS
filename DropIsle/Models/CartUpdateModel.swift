//
//  CartUpdateModel.swift
//  DropIsle
//
//  Created by CtanLI on 2021-03-14.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import Foundation

struct CartUpdateModel: Codable {
	let cart: Cart
}

struct Cart: Codable {
	let items: [Item]
	let isCheckedOut: Bool
	let isDeleted: Bool
	let subtotal: Int
	let currency: String
	let customer: Customer
	let orders: [Order]
	let __v: Int
	let cartid: String
}

struct Item: Codable {
	let storeid: String
	let productid: String
	let quantity: Int
}

struct Customer: Codable {
	let buyerid: String
	let name: String
}

struct Order: Codable {
}



