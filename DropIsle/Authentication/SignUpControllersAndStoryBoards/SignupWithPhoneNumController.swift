//
//  SignupWithPhoneNumController.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-10.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit
import CountryPickerView
import DropDown
import CoreLocation
import KeychainAccess
import SVProgressHUD
import IQKeyboardManagerSwift

class SignupWithPhoneNumController: UIViewController, CLLocationManagerDelegate, UpdateLocationDelegate, ModalHandler, CitiesDelegate, RegionDelegate {

	var dropDown = DropDown()
	var errorDropDown = DropDown()
	var appearance = DropDown.appearance()

	var city: String?
	var countryCode: String?
	var country: String?
	var region: String?
	var phoneNumberWithAreaCode: String?

	weak var cpvTextField: CountryPickerView!
	var checkCodeObj = [String: AnyObject]()
	var cities: Countries?

	private var locationManager = CLLocationManager()
	private let location = LocationMonitor.SharedManager
	private var keychain = Keychain()

	static let signInWithPhoneNumController = "signInWithPhoneNumController"
	static let phoneValidationController = "phoneValidationController"
	private var countriesController = "CountriesController"
	private var citiesController = "CitiesController"
	private var regionController = "RegionController"

	@IBOutlet weak var fullName: UITextField!
	@IBOutlet weak var cityField: UITextField!
	@IBOutlet weak var phoneNumber: UITextField!

	override func viewDidLoad() {
		super.viewDidLoad()

		///Location manager delegate
		location.delegate = self

		let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
		cp.delegate = self
		cp.dataSource = self
		phoneNumber.leftView = cp
		phoneNumber.leftViewMode = .always
		self.cpvTextField = cp
		cpvTextField.countryDetailsLabel.font = UIFont(name: "Avenir-Black", size: 14.0)

		cp.translatesAutoresizingMaskIntoConstraints = false
		cp.flagImageView.translatesAutoresizingMaskIntoConstraints = false
		
		cp.flagImageView.heightAnchor.constraint(equalToConstant: 25).isActive = true
		cp.flagImageView.widthAnchor.constraint(equalToConstant: 25).isActive = true

		if let textFieldView = phoneNumber.leftView  {
			cp.leftAnchor.constraint(equalTo: textFieldView.leftAnchor, constant: 0).isActive = true
			cp.topAnchor.constraint(equalTo: textFieldView.topAnchor, constant: 0).isActive = true
			cp.bottomAnchor.constraint(equalTo: textFieldView.bottomAnchor, constant: 0).isActive = true
		}

		// set up drop down.
		self.customizeDropDown()

		//hide keyboard
		self.hideKeyboardWhenTapped()

		//Keyboard applied to all screens
		IQKeyboardManager.shared.enable = true

		// drop down
		dropDown.bottomOffset = CGPoint(x: 0, y: 40)
		dropDown.cellNib = UINib(nibName: "CountryDropDownCell", bundle: nil)

		errorDropDown.bottomOffset = CGPoint(x: 0, y: 40)
		errorDropDown.cellNib = UINib(nibName: "ErrorViewCell", bundle: nil)

		// Set city text field to hide keyboard and perform action
		self.cityField.addTarget(self, action: #selector(selectOrUpdateLocation), for: .touchDown)
		self.cityField.inputView = UIView()
		self.cityField.inputAccessoryView = UIView()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		///Analytics
		LoggerManager.shared.log(event: .phoneSignupScreen)
	}

	func errorMessageCell() {
		self.appearance.textColor = .red
		errorDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
			guard let cell = cell as? ErrorViewCell else { return }
			if index == 0 {
				cell.errorIcon.image = UIImage(named: "errorIcon")
				cell.errorIcon.setImageColor(color: .red)
				cell.layer.borderColor = UIColor.red.cgColor
			}
		}
	}

	func customizeDropDown() {
		appearance.cellHeight = 45
		appearance.backgroundColor = UIColor(white: 1, alpha: 1)
		appearance.selectionBackgroundColor = UIColor(hexString: "#3995F9")
		appearance.separatorColor = UIColor(white: 0.0, alpha: 0.1)
//		appearance.cornerRadius = 5
//		appearance.shadowOpacity = 0.9
//		appearance.shadowRadius = 20
		appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
		appearance.animationduration = 0.30
		if let font = UIFont(name: "Avenir", size: 14.0) {
			appearance.textFont = font
		}
	}

    @IBAction func signUp(_ sender: UIButton) {
			signUpWithPhoneNumber()
	}

	@IBAction func termsAndCondition(_ sender: UIButton) {
	}

	@IBAction func privacyPolicy(_ sender: UIButton) {
	}

	@IBAction func signIn(_ sender: UIButton) {
		self.performSegue(withIdentifier: SignupWithPhoneNumController.signInWithPhoneNumController, sender: self)
	}
}

extension SignupWithPhoneNumController: CountryPickerViewDelegate, CountryPickerViewDataSource {

	func cellImageViewSize(in countryPickerView: CountryPickerView) -> CGSize {
		return CGSize(width: 25, height: 20)
	}

	func cellLabelFont(in countryPickerView: CountryPickerView) -> UIFont {
		return UIFont(name: "Avenir-Black", size: 14.0)!
	}

	func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
		print(cpvTextField.selectedCountry.phoneCode)
		countryPickerView.countryDetailsLabel.font = UIFont(name: "Avenir-Black", size: 14.0)
		countryPickerView.flagImageView.translatesAutoresizingMaskIntoConstraints = false
		countryPickerView.flagImageView.heightAnchor.constraint(equalToConstant: 25).isActive = true
		countryPickerView.flagImageView.widthAnchor.constraint(equalToConstant: 25).isActive = true
	}

	func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
		var countries = [Country]()
		["CA", "GH", "NG", "US"].forEach { code in
			if let country = countryPickerView.getCountryByCode(code) {
				countries.append(country)
			}
		}
		return countries
	}

	func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
		return "Select Country"
	}
}

extension  SignupWithPhoneNumController {

	func modalDismissed(cities: Countries) {
		self.cities = cities
		self.performSegue(withIdentifier: self.citiesController, sender: self)
	}

	func usersCity(country: String?, city: String?, code: String?) {
		self.city = city
		self.countryCode = code
		self.country = country
		cityField.text = "\(city ?? "")" + ", " + "\(country ?? "") " + " " + "\(code ?? "")"
	}

	func setUserCity(city: String?, code: String?, country: String?) {
		self.region = city
		self.countryCode = code
		self.country = country
		self.performSegue(withIdentifier: self.regionController, sender: self)
	}

	///getting region name
	func getRegion(region: String?, code: String?, city: String?, country: String?) {
		self.city = city
		self.region = region
		self.countryCode = code
		self.country = country
		cityField.text =  "\(region ?? "")" + ", " + "\(city ?? "")" + ", " + "\(country ?? "")" + " " + "\(code ?? "")"
	}


	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

		//This is a delegate function to initialize the dismiss Modal view class and call a function
		if let nav = segue.destination as? UINavigationController, let citiesDetails = nav.topViewController as? CitiesController {
			citiesDetails.cities = cities
			citiesDetails.delegate = self
		}

		if let nav = segue.destination as? UINavigationController, let countryVC = nav.topViewController as? CountriesController {
			countryVC.delegate = self
		}

		if let nav = segue.destination as? UINavigationController,
			let regionVC = nav.topViewController as? RegionController, let region = self.region, let countryCode = self.countryCode, let country = self.country {
			regionVC.provinceOrState = region
			regionVC.countryCode = countryCode
			regionVC.country = country
			regionVC.delegate = self
		}

		if segue.identifier == SignInWithPhoneNumController.phoneValidationController {
			let phValidation = segue.destination as? PhoneValidationController
			phValidation?.phoneNumber = phoneNumberWithAreaCode
			phValidation?.city = city
			phValidation?.region = region
			phValidation?.countryCode = countryCode
			phValidation?.country = country
			phValidation?.isFromPhoneSignUpScreen = true
		}
	}
}

extension SignupWithPhoneNumController {

	func updateLocation() {
		_ = ("\(location.currentLocation?.coordinate.latitude ?? 0), \(location.currentLocation?.coordinate.longitude ?? 0)")
		if let _ = location.currentLocation?.coordinate {

		}
		if let currentLocation = location.currentLocation {
			updateLocation(location: currentLocation)
		}
		location.stopMonitoring()
	}

	func openCountryList() {
		self.performSegue(withIdentifier: countriesController, sender: self)
	}

	func updateLocation(location: CLLocation?) {
		if let loc = location {
			let geocoder = CLGeocoder()
			geocoder.reverseGeocodeLocation(loc) { (placemarksArray, _) in
				guard let placeMark = placemarksArray else {
					return
				}
				if (placeMark.count) > 0 {
					let placemark = placemarksArray?.first
					self.city = placemark?.locality
					self.countryCode = placemark?.isoCountryCode
					self.country = placemark?.country
					self.cityField.text = "\(self.city ?? "")" + ", " +  "\(self.country ?? "")" +  " " +  "\(self.countryCode ?? "")"
				}
			}
		}
	}

	@objc func selectOrUpdateLocation(textField: UITextField) {
		switch CLLocationManager.authorizationStatus() {
		case .authorizedWhenInUse:
			print("authorized")
			self.performSegue(withIdentifier: countriesController, sender: self)
		case .denied:
			print("denied")
			self.performSegue(withIdentifier: countriesController, sender: self)
		case .notDetermined:
			location.startMonitoring()
			break
		case .restricted:
			break
		default: break
		}
		locationManager.requestWhenInUseAuthorization()
	}

		///get location if not enabled
	func getLatLng(address: String) {
		let geoCoder = CLGeocoder()
		geoCoder.geocodeAddressString(address) { (placemarks, error) in
			guard let placemarks = placemarks, let location = placemarks.first?.location else { return }
			preferences[.latitude] = "\(location.coordinate.latitude)"
			preferences[.longitude] = "\(location.coordinate.longitude)"
		}
	}
}

extension SignupWithPhoneNumController {

	func signUpWithPhoneNumber() {

		guard let nameText = fullName.text, !nameText.isEmpty  else {
			self.errorDropDown.anchorView = fullName
			self.errorDropDown.dataSource = ["Please enter your full name."]
			self.errorMessageCell()
			self.errorDropDown.show()
			return
		}

		guard let cityText = cityField.text, !cityText.isEmpty  else {
			self.errorDropDown.anchorView = cityField
			self.errorDropDown.dataSource = ["Please select a country."]
			self.errorMessageCell()
			self.errorDropDown.show()
			return
		}

		guard let phText = phoneNumber.text, !phText.isEmpty else {
			self.errorDropDown.anchorView = phoneNumber
			self.errorDropDown.dataSource = ["Please enter your phone number."]
			self.errorMessageCell()
			self.errorDropDown.show()
			return
		}

		///Get User Gender
		SVProgressHUD.show()

		///Get lat/lng from user selected location
		getLatLng(address: cityField.text ?? "")

		let str = fullName.text
		var strArray = str?.components(separatedBy: " ")
		if strArray?[0] == "" {
			strArray?[0] = "unknown"
		}
		let genderUrl = "https://api.genderize.io/?name=\(strArray?[0] ?? "unknown")"
		APIManager.shared.fetchGenericData(method: .get, urlString: genderUrl, params: nil, headers: nil) { (result: Gender) in
			if result.gender != nil {
				self.addUser(gender: result.gender ?? "Unknown")
			} else {
				self.addUser(gender: "Unknown")
			}
		}
	}

	func addUser(gender: String) {
		let areaCode = cpvTextField.countryDetailsLabel.text
		let location: [String: AnyObject] = ["country": country as AnyObject, "countrycode": countryCode as AnyObject, "region": city as AnyObject]
		let params: [String: AnyObject] =
			["name": fullName.text as AnyObject,
			 "gender": gender as AnyObject,
			 "phone": areaCode!.digits + phoneNumber.text! as AnyObject,
			 "contactmethod": "SMS" as AnyObject,
			 "roles": ["Customer"] as AnyObject,
			 "location": location as AnyObject]
		phoneNumberWithAreaCode = areaCode!.digits + phoneNumber.text!
		let addUserUrl = BaseAPIClient.urlFromPath(path: APIEndPoints.signUp)
		APIManager.shared.fetchGenericData(method: .post, urlString: addUserUrl, params: params, headers: nil) { (result: SignUp) in
			if result.code == 200 {
				SVProgressHUD.dismiss(withDelay: 0.5)

				///Switch to validation page to enter 4 digit code and login
				self.performSegue(withIdentifier: SignInWithPhoneNumController.phoneValidationController, sender: self)
			} else {
				SVProgressHUD.dismiss(withDelay: 0.5)
			}
		}
	}
}

extension SignupWithPhoneNumController {
//	func sendVerificationCode() {
//		let url = URL(string: "https://verify.twilio.com/v2/Services/VAaba2fccd85e764cff9499baff3c47db9/Verifications")
//		let params: [String: AnyObject] = ["To": "+2347085952335" as AnyObject, "Channel": "sms" as AnyObject]
//		Alamofire.request(url!, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
//																.authenticate(user: "AC5f7597f049e6160fc3ca7a3ea0efd68a", password: "33d6f4e771b3a5b0c07119e7b2512b6f")
//																.responseJSON { (responseObject) in
//			print(responseObject)
//		}
//	}

//	func verifyCode() {
//		let url = URL(string: "https://verify.twilio.com/v2/Services/VAaba2fccd85e764cff9499baff3c47db9/VerificationCheck")
//		let params: [String: AnyObject] = ["To": "+14435292390" as AnyObject, "Code": "" as AnyObject]
//		Alamofire.request(url!, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
//																.authenticate(user: "AC5f7597f049e6160fc3ca7a3ea0efd68a", password: "33d6f4e771b3a5b0c07119e7b2512b6f")
//																.responseJSON { (responseObject) in
//			print(responseObject)
//		}
//	}
}
