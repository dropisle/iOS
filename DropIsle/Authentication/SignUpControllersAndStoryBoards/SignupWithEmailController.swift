//
//  ViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2018-11-10.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit
import DropDown
import Firebase
import SVProgressHUD
import CoreLocation
import KeychainAccess
import IQKeyboardManagerSwift
import Amplitude_iOS

class SignupWithEmailController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate, UpdateLocationDelegate {

	static let signInWithEmailController = "signInWithEmailController"
	static let enableNotificationController = "enableNotificationController"

	var isPasswordValid = true
	var iconClick = true
	var errorDropDown = DropDown()
	var appearance = DropDown.appearance()

	private var locationManager = CLLocationManager()
	private let location = LocationMonitor.SharedManager
	private var keychain = Keychain()

	//Outlets
	@IBOutlet weak var firstNameField: UITextField!
	@IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var LastNameField: UITextField!
	@IBOutlet weak var passwordField: UITextField!

	@IBOutlet weak var securedText: UILabel! {
		didSet {
			self.securedText.isHidden = false
		}
	}

    @IBOutlet weak var showPasswordButton: UIButton! {
		didSet {
			let origImage = UIImage(systemName: "eye.slash.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 20, weight: .regular, scale: .medium))?.withTintColor(.darkGray, renderingMode: .alwaysOriginal)
			showPasswordButton.setImage(origImage, for: .normal)
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		///Location manager delegate
		location.delegate = self

		self.firstNameField.delegate = self
		self.emailField.delegate = self
		self.passwordField.delegate = self
		self.LastNameField.delegate = self
		self.view.endEditing(true)
		passwordField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

		self.hideKeyboardWhenTapped()

		//Keyboard applied to all screens
		IQKeyboardManager.shared.enable = true

		self.customizeDropDown()
		self.errorDropDown.bottomOffset = CGPoint(x: 0, y: 40)
		self.errorDropDown.cellNib = UINib(nibName: "ErrorViewCell", bundle: nil)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		///Analytics
		LoggerManager.shared.log(event: .emailSignupScreen)
	}

	func errorMessageCell() {
		self.appearance.textColor = .red
		errorDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
			guard let cell = cell as? ErrorViewCell else { return }
			if index == 0 {
				cell.errorIcon.image = UIImage(named: "errorIcon")
				cell.errorIcon.setImageColor(color: .red)
				cell.layer.borderColor = UIColor.red.cgColor
			}
		}

		errorDropDown.cancelAction = { [unowned self] in
			self.firstNameField.layer.borderColor = UIColor.lightGray.cgColor
			self.emailField.layer.borderColor = UIColor.lightGray.cgColor
			self.passwordField.layer.borderColor = UIColor.lightGray.cgColor
		}
	}

	func customizeDropDown() {
		appearance.backgroundColor = UIColor(white: 1, alpha: 1)
		appearance.selectionBackgroundColor = UIColor(hexString: "#3995F9")
		appearance.separatorColor = UIColor(white: 0.0, alpha: 0.1)
//		appearance.cornerRadius = 5
//		appearance.shadowOpacity = 0.9
//		appearance.shadowRadius = 20
		appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
		appearance.animationduration = 0.30
		if let font = UIFont(name: "Avenir", size: 14.0) {
			appearance.textFont = font
		}
	}

	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		return true
	}

    @IBAction func termsAndConditions(_ sender: UIButton) {
    }

    @IBAction func privacyPolicy(_ sender: UIButton) {
    }

    @IBAction func signIn(_ sender: UIButton) {
		self.performSegue(withIdentifier: SignupWithEmailController.signInWithEmailController, sender: self)
    }

    @IBAction func signUp(_ sender: UIButton) {
		DispatchQueue.main.async(execute: {
			self.performSegue(withIdentifier: SignupWithEmailController.enableNotificationController, sender: self)
		})
    }

    @IBAction func showPassword(_ sender: UIButton) {
		if (iconClick == true && passwordField.text != "") {
			let origImage = UIImage(systemName: "eye.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 20, weight: .regular, scale: .medium))?.withTintColor(.systemGray, renderingMode: .alwaysOriginal)
			showPasswordButton.setImage(origImage, for: .normal)
		} else {
			let origImage = UIImage(systemName: "eye.slash.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 20, weight: .regular, scale: .medium))?.withTintColor(.darkGray, renderingMode: .alwaysOriginal)
			showPasswordButton.setImage(origImage, for: .normal)
		}
		iconClick = !iconClick
		passwordField.isSecureTextEntry.toggle()
    }

	@IBAction func closeScreen(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}

	/// Show load indicator
	func showLoadIndicator() {
		SVProgressHUD.show()
	}

	func openCountryList() { }
}

extension SignupWithEmailController {

	@objc func textFieldDidChange(_ textField: UITextField) {
		self.securedText.isHidden = false
		let attrStr = NSMutableAttributedString(
			string: "Password must be at least 8 characters, and contain at least one upper case letter, one lower case letter, and one number.",
			attributes: [
				.font: UIFont.init(name: "Avenir-Medium", size: 12.0) ?? UIFont.systemFont(ofSize: 12.0),
				.foregroundColor: UIColor(hexString: "6A6A6A")
			])

		if let txt = passwordField.text {
			isPasswordValid = true
			attrStr.addAttributes(setupAttributeColor(if: (txt.count >= 8)), range: findRange(in: attrStr.string, for: "at least 8 characters"))
			attrStr.addAttributes(setupAttributeColor(if: (txt.rangeOfCharacter(from: CharacterSet.uppercaseLetters) != nil)), range: findRange(in: attrStr.string, for: "one upper case letter"))
			attrStr.addAttributes(setupAttributeColor(if: (txt.rangeOfCharacter(from: CharacterSet.lowercaseLetters) != nil)), range: findRange(in: attrStr.string, for: "one lower case letter"))
			attrStr.addAttributes(setupAttributeColor(if: (txt.rangeOfCharacter(from: CharacterSet.decimalDigits) != nil)), range: findRange(in: attrStr.string, for: "one number"))
		} else {
			isPasswordValid = false
		}
		securedText.attributedText = attrStr
	}

	// MARK: - In-Place Validation Helpers
	func setupAttributeColor(if isValid: Bool) -> [NSAttributedString.Key: Any] {
		if isValid {
			return [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#828DED")]
		} else {
			isPasswordValid = false
			return [NSAttributedString.Key.foregroundColor: UIColor(hexString: "6A6A6A")]
		}
	}

	func findRange(in baseString: String, for substring: String) -> NSRange {
		if let range = baseString.localizedStandardRange(of: substring) {
			let startIndex = baseString.distance(from: baseString.startIndex, to: range.lowerBound)
			let length = substring.count
			return NSRange(location: startIndex, length: length)
		} else {
			print("Range does not exist in the base string.")
			return NSRange(location: 0, length: 0)
		}
	}

	func validatePassword(password: String?) -> String? {
		var errorMsg = "Password requires at least"

		if let txt = passwordField.text {
			if (txt.rangeOfCharacter(from: CharacterSet.uppercaseLetters) == nil) {
				errorMsg += "one upper case letter"
			}
			if (txt.rangeOfCharacter(from: CharacterSet.lowercaseLetters) == nil) {
				errorMsg += ", one lower case letter"
			}
			if (txt.rangeOfCharacter(from: CharacterSet.decimalDigits) == nil) {
				errorMsg += ", one number"
			}
			if txt.count < 8 {
				errorMsg += ", and eight characters"
			}
		}

		if isPasswordValid {
			return password!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
		} else {
			let alertController = UIAlertController(title: "Password Error", message: errorMsg, preferredStyle: .alert)
			let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
			alertController.addAction(defaultAction)
			self.present(alertController, animated: true, completion: nil)
			return nil
		}
	}
}

extension  SignupWithEmailController {

	func updateLocation() {
		_ = ("\(location.currentLocation?.coordinate.latitude ?? 0), \(location.currentLocation?.coordinate.longitude ?? 0)")
		if let _ = location.currentLocation?.coordinate { }
		location.stopMonitoring()
		if let currentLocation = location.currentLocation {
			updateLocation(location: currentLocation)
		}
	}

	func updateLocation(location: CLLocation?) {
		if let loc = location {
			let geocoder = CLGeocoder()
			geocoder.reverseGeocodeLocation(loc) { (placemarksArray, _) in
				guard let placeMark = placemarksArray else {
					return
				}
				if (placeMark.count) > 0 {
					let _ = placemarksArray?.first

				}
			}
		}
	}

	@objc func selectOrUpdateLocation(textField: UITextField) {
		switch CLLocationManager.authorizationStatus() {
		case .authorizedWhenInUse: break
		case .denied: break
		case .notDetermined:
			location.startMonitoring()
			break
		case .restricted:
			break
		default: break
		}
		locationManager.requestWhenInUseAuthorization()
	}

		///get location if not enabled
	func getLatLng(address: String) {
		let geoCoder = CLGeocoder()
		geoCoder.geocodeAddressString(address) { (placemarks, error) in
			guard let placemarks = placemarks, let location = placemarks.first?.location else { return }
			preferences[.latitude] = "\(location.coordinate.latitude)"
			preferences[.longitude] = "\(location.coordinate.longitude)"
		}
	}
}

// Service call to Add Users
extension  SignupWithEmailController {

	func addUsers() {
		guard let nameText = firstNameField.text, !nameText.isEmpty else {
			self.firstNameField.layer.borderColor = UIColor.red.cgColor
			self.appearance.cellHeight = 40
			self.errorDropDown.anchorView = firstNameField
			self.errorDropDown.dataSource = ["Please enter first name."]
			self.errorMessageCell()
			self.errorDropDown.show()
			return
		}

		guard let cityText = LastNameField.text, !cityText.isEmpty else {
			self.LastNameField.layer.borderColor = UIColor.red.cgColor
			self.appearance.cellHeight = 40
			self.errorDropDown.anchorView = LastNameField
			self.errorDropDown.dataSource = ["Please enter last name"]
			self.errorMessageCell()
			self.errorDropDown.show()
			return
		}


		guard let emailText = emailField.text, !emailText.isEmpty else {
			self.emailField.layer.borderColor = UIColor.red.cgColor
			self.appearance.cellHeight = 40
			self.errorDropDown.anchorView = emailField
			self.errorDropDown.dataSource = ["Please enter your email address."]
			self.errorMessageCell()
			self.errorDropDown.show()
			return
		}

		guard ((emailField.text?.isValidEmail())!) else {
			self.emailField.layer.borderColor = UIColor.red.cgColor
			self.appearance.cellHeight = 40
			self.errorDropDown.anchorView = emailField
			self.errorDropDown.dataSource = ["Please enter a valid email address"]
			self.errorMessageCell()
			self.errorDropDown.show()
			return
		}

		guard let pwText = passwordField.text, !pwText.isEmpty else {
			self.passwordField.layer.borderColor = UIColor.red.cgColor
			self.appearance.cellHeight = 40
			self.errorDropDown.anchorView = passwordField
			self.errorDropDown.dataSource = ["Please enter password."]
			self.errorMessageCell()
			self.errorDropDown.show()
			return
		}

		guard isPasswordValid == true else {
			self.securedText.isHidden = false
			return
		}

		///if validations are successful
		self.securedText.isHidden = false
		self.securedText.text = "Your password is secure"
		self.securedText.textColor =  UIColor(hexString: "#56BD5B")
		self.showLoadIndicator()

		APIManager.shared.fetchGenericData(method: .get, urlString: "", params: nil, headers: nil) { (result: Gender) in
			if result.gender != nil {
				self.addUser(gender: result.gender ?? "Unknown")
			} else {
				self.addUser(gender: "Unknown")
			}
		}
	}

	func addUser(gender: String) {
		let params: [String: AnyObject] =
			["name": firstNameField.text as AnyObject,
			 "email": emailField.text as AnyObject,
			 "password": passwordField.text as AnyObject,
			 "gender": gender as AnyObject,
			 "contactmethod": "EMAIL" as AnyObject,
			 "roles": ["Customer"] as AnyObject]
		let addUserUrl = BaseAPIClient.urlFromPath(path: APIEndPoints.signUp)
		APIManager.shared.fetchGenericData(method: .post, urlString: addUserUrl, params: params, headers: nil) { (result: SignUp) in
			if result.code == 200 {

			} else {
				SVProgressHUD.dismiss(withDelay: 0.2)
			}
		}
	}

	func loginUser(username: String, password: String, countryCode: String, city: String, country: String, region: String) {
		let loginUser = BaseAPIClient.urlFromPath(path: APIEndPoints.login)
		let params: [String: AnyObject] = ["username": username as AnyObject, "password": password as AnyObject]
		APIManager.shared.fetchGenericData(method: .post, urlString: loginUser, params: params, headers: nil) { (result: SignIn) in
			if result.accesstoken == nil {
				SVProgressHUD.showError(withStatus: "Please check your credentials and try again")
			} else {
				SVProgressHUD.dismiss(withDelay: 0.2)
				self.keychain["token"] = result.accesstoken
				self.keychain["refreshtoken"] = result.refreshtoken
				self.keychain["userId"] = result.userid
				self.keychain["countryCode"] = countryCode
				self.keychain["city"] = city
				self.keychain["region"] = region
				self.keychain["country"] = country
				self.keychain["tokenCreationdDate"] = self.getDateString()

				///signUp events
				Amplitude.instance()?.setUserId(result.userid)

				// Get token for push notification
				let FCMToken = try? self.keychain.get("FCMToken")
				self.updateToken(id: CurrentUserInfo.shared.userId(), token: FCMToken ?? "")
			}
		}
	}

	func updateToken(id: String, token: String) {
			_ = APIEndPoints.init() //APIEndPoints.authorizationHeader
		let updateTokenUrl = BaseAPIClient.urlFromPath(path: APIEndPoints.updateToken)
		let header = ["Authorization": "Bearer \(self.keychain["token"] ?? "")"]
		let params: [String: AnyObject] = ["id": id as AnyObject, "token": token  as AnyObject]
		APIManager.shared.fetchGenericData(method: .post, urlString: updateTokenUrl, params: params, headers: header) { (result: UpdateTokenModel) in
			if result.code == 200 {
				DispatchQueue.main.async(execute: {
					self.performSegue(withIdentifier: SignupWithEmailController.enableNotificationController, sender: self)
				})
			} else {
				SVProgressHUD.showError(withStatus: "Oops! something went wrong, please try again")
			}
		}
	}
}
