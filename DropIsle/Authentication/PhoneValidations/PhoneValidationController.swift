//
//  PhoneValidationController.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-11.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit
import SVProgressHUD
import KeychainAccess
import Firebase
import IQKeyboardManagerSwift
import Amplitude_iOS

class PhoneValidationController: UIViewController, UITextFieldDelegate {

	static let enableNotificationController = "enableNotificationController"
	private var keychain = Keychain()
	var phoneNumber: String?
	var city: String?
	var region: String?
	var countryCode: String?
	var country: String?
	var isFromPhoneSignUpScreen = Bool()

	@IBOutlet weak var validationMessage: UILabel!
	@IBOutlet weak var firstCode: UITextField!
	@IBOutlet weak var secodeCode: UITextField!
	@IBOutlet weak var thirdCode: UITextField!
	@IBOutlet weak var fourthCode: UITextField!
	@IBOutlet weak var validateCode: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()

		firstCode.delegate = self
		secodeCode.delegate = self
		thirdCode.delegate = self
		fourthCode.delegate = self

		//hide keyboard
		self.hideKeyboardWhenTapped()

		//Keyboard applied to all screens
		IQKeyboardManager.shared.enable = true

		self.firstCode.becomeFirstResponder()
		firstCode.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
		secodeCode.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
		thirdCode.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
		fourthCode.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)

		if let value = phoneNumber {
			let lastFour = String(value.suffix(4))
			validationMessage.text = "4 digit verification code has been sent to your mobile ******\(lastFour), please enter the code here to complete sign up."
		}
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		///Analytics
		LoggerManager.shared.log(event: .phoneValidationScreen)
	}

	@objc func textFieldDidChange(textField: UITextField) {
		let text = textField.text
		if text?.utf16.count == 1 {
			switch textField {
				case firstCode:
					secodeCode.becomeFirstResponder()
				case secodeCode:
					thirdCode.becomeFirstResponder()
				case thirdCode:
					fourthCode.becomeFirstResponder()
				case fourthCode:
					DispatchQueue.main.async {
						self.validateCode.sendActions(for: .touchUpInside)
				}
				default:break
			}
		}
	}

	@IBAction func validate(_ sender: Any) {
		guard let first = firstCode.text, !first.isEmpty  else {
			firstCode.layer.cornerRadius = 5
			firstCode.layer.borderWidth = 0.3
			firstCode.layer.borderColor = UIColor.red.cgColor
			return
		}
		guard let secode = secodeCode.text, !secode.isEmpty  else {
			secodeCode.layer.cornerRadius = 5
			secodeCode.layer.borderWidth = 0.3
			secodeCode.layer.borderColor = UIColor.red.cgColor
			return
		}
		guard let third = thirdCode.text, !third.isEmpty  else {
			thirdCode.layer.cornerRadius = 5
			thirdCode.layer.borderWidth = 0.3
			thirdCode.layer.borderColor = UIColor.red.cgColor
			return
		}
		guard let fourth = fourthCode.text, !fourth.isEmpty  else {
			fourthCode.layer.cornerRadius = 5
			fourthCode.layer.borderWidth = 0.3
			fourthCode.layer.borderColor = UIColor.red.cgColor
			return
		}

		///Phone Number Login
		loginUser(phoneNumber: phoneNumber ?? "", countryCode: self.countryCode ?? "", city: self.city ?? "", country: self.country ?? "", region: self.region ?? "")
	}

	@IBAction func resendCode(_ sender: UIButton) { }
}

extension PhoneValidationController {
	func loginUser(phoneNumber: String, countryCode: String, city: String, country: String, region: String) {
		SVProgressHUD.show()
		let password = firstCode.text! + secodeCode.text! + thirdCode.text! + fourthCode.text!
		let loginUser = BaseAPIClient.urlFromPath(path: APIEndPoints.login)
		let params: [String: AnyObject] = ["username": phoneNumber as AnyObject, "password": password  as AnyObject]
		APIManager.shared.fetchGenericData(method: .post, urlString: loginUser, params: params, headers: nil) { (result: SignIn) in
			if result.accesstoken == nil {
				SVProgressHUD.showError(withStatus: "Validation failed please check the code and try again!")
			} else {
				SVProgressHUD.dismiss(withDelay: 0.5)
				self.keychain["userId"] = result.userid
				self.keychain["token"] = result.accesstoken
				self.keychain["refreshtoken"] = result.refreshtoken
				self.keychain["tokenCreationdDate"] = self.getDateString()
				if self.isFromPhoneSignUpScreen {
					self.keychain["countryCode"] = countryCode
					self.keychain["city"] = city
					self.keychain["country"] = country
					self.keychain["region"] = region
				}

				///signUp events
				Amplitude.instance()?.setUserId(result.userid)
				let parameter = ["userId": result.userid, "country": self.country, "region" : city, "deviceType": UIDevice.modelName, "appVersion": UIApplication.appVersion, "OSVersion": UIDevice.current.systemVersion, "platform": "iOS"]
				LoggerManager.shared.log(event: .signup, parameters: parameter as [String : Any])

				///Get token for push notification
				let FCMToken = try? self.keychain.get("FCMToken")
				self.updateToken(id: CurrentUserInfo.shared.userId(), token: FCMToken ?? "")
			}
		}
	}

	func updateToken(id: String, token: String) {
		_ = APIEndPoints.init() //APIEndPoints.authorizationHeader
		let updateToknUrl = BaseAPIClient.urlFromPath(path: APIEndPoints.updateToken)
		let header = ["Authorization": "Bearer \(self.keychain["token"] ?? "")"]
		let params: [String: AnyObject] = ["id": id as AnyObject, "token": token  as AnyObject]
		APIManager.shared.fetchGenericData(method: .post, urlString: updateToknUrl, params: params, headers: header) { (result: UpdateTokenModel) in
			if result.code == 200 {
				DispatchQueue.main.async(execute: {
					self.performSegue(withIdentifier: PhoneValidationController.enableNotificationController, sender: self)
				})
			} else {
				SVProgressHUD.showError(withStatus:  "Oops! something went wrong, please try again")
			}
		}
	}
}
