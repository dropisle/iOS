//
//  SignUpOptionController.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-13.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit

class SignUpOptionController: UIViewController {

	var isSignUpOption = false
	var accepted = false
	static let signupWithEmailController = "signupWithEmailController"
	static let signupWithPhoneNumController = "signupWithPhoneNumController"

	static let signInWithEmailController = "signInWithEmailController"
	static let signInWithPhoneNumController = "signInWithPhoneNumController"

    @IBOutlet weak var optionDescription: UILabel!
    @IBOutlet weak var radioButtonOne: UIButton! {
		didSet {
			self.radioButtonOne.setTitle("◉", for: UIControl.State())
			accepted = false
		}
	}

	@IBOutlet weak var radioButtonTwo: UIButton! {
		didSet {
			self.radioButtonTwo.setTitle("◎", for: UIControl.State())
		}
	}

    override func viewDidLoad() {
			super.viewDidLoad()
			if isSignUpOption == true {
				optionDescription.text = "Sign Up Using Email or Phone Number."
			} else {
				optionDescription.text = "Sign In Using Email or Phone Number."
			}
    }

	@IBAction func radioButtonOneAction(_ sender: UIButton) {
		if accepted {
			sender.setTitle("◉", for: .normal)
			self.radioButtonTwo.setTitle("◎", for: .normal)
			sender.titleLabel?.font = UIFont(name: "Avenir", size: 30)
			accepted = false
		}
	}

	@IBAction func radioButtoTwoAction(_ sender: UIButton) {
		if !accepted {
			sender.setTitle("◉", for: .normal)
			self.radioButtonOne.setTitle("◎", for: .normal)
			sender.titleLabel?.font = UIFont(name: "Avenir", size: 30)
			accepted = true
		}
	}

	@IBAction func proceed(_ sender: UIButton) {
		isSignUpOption == true ? goToSignUpOptions() : goToSignInOptions()
	}

	func goToSignUpOptions() {
		if !accepted {
			///Analytics
			LoggerManager.shared.log(event: .phoneCreateAccountSelected)
			DispatchQueue.main.async(execute: {
				self.performSegue(withIdentifier: SignUpOptionController.signupWithPhoneNumController, sender: self)
			})
		} else {
			///Analytics
			LoggerManager.shared.log(event: .emailCreateAccountSelected)
			DispatchQueue.main.async(execute: {
				self.performSegue(withIdentifier: SignUpOptionController.signupWithEmailController, sender: self)
			})
		}
	}

	func goToSignInOptions() {
		///Analytics
		LoggerManager.shared.log(event: .phoneSignInAccountSelected)
		if !accepted {
			DispatchQueue.main.async(execute: {
				self.performSegue(withIdentifier: SignUpOptionController.signInWithPhoneNumController, sender: self)
			})
		} else {
			///Analytics
			LoggerManager.shared.log(event: .emailSignInAccountSelected)
			DispatchQueue.main.async(execute: {
				self.performSegue(withIdentifier: SignUpOptionController.signInWithEmailController, sender: self)
			})
		}
	}

	@IBAction func closeScreen(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}
}
