//
//  SignInWithEmailController.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-12.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit
import DropDown
import SVProgressHUD
import KeychainAccess
import Firebase
import IQKeyboardManagerSwift

class SignInWithEmailController: UIViewController, UITextFieldDelegate {

	static var tabBarController = "TabBarViewController"
	static let signupWithEmailController = "signupWithEmailController"

	private var iconClick = true
	private var isPasswordValid = true
	private var errorDropDown = DropDown()
	private var appearance = DropDown.appearance()
	private var keychain = Keychain()

	@IBOutlet weak var emailField: UITextField!
	@IBOutlet weak var passwordField: UITextField!
	@IBOutlet weak var showHidePassword: UIButton! {
        didSet {
			let origImage = UIImage(systemName: "eye.slash.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 20, weight: .regular, scale: .medium))?.withTintColor(.darkGray, renderingMode: .alwaysOriginal)
			showHidePassword.setImage(origImage, for: .normal)
        }
    }

    override func viewDidLoad() {
			super.viewDidLoad()
			self.passwordField.delegate = self
			self.emailField.delegate = self
			self.customizeDropDown()
			self.errorDropDown.bottomOffset = CGPoint(x: 0, y: 40)
			self.errorDropDown.cellNib = UINib(nibName: "ErrorViewCell", bundle: nil)

			///hide keyboard
			self.hideKeyboardWhenTapped()

			///Keyboard applied to all screens
			IQKeyboardManager.shared.enable = true
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		///Analytics
		LoggerManager.shared.log(event: .emailLoginScreen)
	}

	func errorMessageCell() {
		self.appearance.textColor = .red
		errorDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
			guard let cell = cell as? ErrorViewCell else { return }
			if index == 0 {
				cell.errorIcon.image = UIImage(named: "errorIcon")
				cell.errorIcon.setImageColor(color: .red)
				cell.layer.borderColor = UIColor.red.cgColor
			}
		}
	}

	func customizeDropDown() {
		appearance.backgroundColor = UIColor(white: 1, alpha: 1)
		appearance.selectionBackgroundColor = UIColor(hexString: "#3995F9")
		appearance.separatorColor = UIColor(white: 0.0, alpha: 0.1)
//		appearance.cornerRadius = 5
//		appearance.shadowOpacity = 0.9
//		appearance.shadowRadius = 20
		appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
		appearance.animationduration = 0.30
		if let font = UIFont(name: "Avenir", size: 14.0) {
			appearance.textFont = font
		}
	}

	@IBAction func showHidePasswordAction(_ sender: UIButton) {
		if (iconClick == true && passwordField.text != "") {
			let origImage = UIImage(systemName: "eye.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 20, weight: .regular, scale: .medium))?.withTintColor(.systemGray, renderingMode: .alwaysOriginal)
			showHidePassword.setImage(origImage, for: .normal)
		} else {
			let origImage = UIImage(systemName: "eye.slash.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 20, weight: .regular, scale: .medium))?.withTintColor(.darkGray, renderingMode: .alwaysOriginal)
			showHidePassword.setImage(origImage, for: .normal)
		}
		iconClick = !iconClick
		passwordField.isSecureTextEntry.toggle()
    }

	@IBAction func forgotPassword(_ sender: UIButton) {
		forgotPasswordField()
	}

	func forgotPasswordField() {
		let alertController = UIAlertController(title: "Reset Password", message: "", preferredStyle: UIAlertController.Style.alert)
		alertController.addTextField { textField in
			textField.placeholder = "Enter Email Address."
			textField.font = UIFont(name: "Avenir", size: 18)
			let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
			textField.addConstraint(heightConstraint)
		}
		let saveAction = UIAlertAction(title: "Send", style: UIAlertAction.Style.default, handler: { _ -> Void in
			let emailText = alertController.textFields![0] as UITextField
			if let email = emailText.text {
				self.requestPasswordChange(email: email)
			}
		})
		let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: {
			(_: UIAlertAction!) -> Void in })
		alertController.addAction(saveAction)
		alertController.addAction(cancelAction)
		self.present(alertController, animated: true, completion: nil)
	}

	func requestPasswordChange(email: String) {
		SVProgressHUD.loading(message: "Requesting...", backgroundColor: UIColor(hexString: "#f1f1f1"), textColor: UIColor(hexString: "#FF6347"))
		let params: [String: String] = ["email": email]
		let forgotPW = BaseAPIClient.urlFromPath(path: APIEndPoints.forgotPassword)
		APIManager.shared.fetchGenericData(method: .post, urlString: forgotPW, params: params as [String: AnyObject], headers: nil) { (result: ForgotPasswordModel) in
			if result.code == 200 {
				SVProgressHUD.loading(message: result.message, backgroundColor: .lightGray, textColor: .lightGray)
				DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
					SVProgressHUD.dismiss(withDelay: 0.2)
				}
			} else {
				SVProgressHUD.showError(withStatus: result.message)
			}
		}
	}

	@IBAction func signIn(_ sender: UIButton) {
		guard let emailText = emailField.text, !emailText.isEmpty else {
			self.appearance.cellHeight = 40
			self.errorDropDown.anchorView = emailField
			self.errorDropDown.dataSource = ["Please enter your email address."]
			self.errorMessageCell()
			self.errorDropDown.show()
			return
		}

		guard ((emailField.text?.isValidEmail())!) else {
			self.appearance.cellHeight = 40
			self.errorDropDown.anchorView = emailField
			self.errorDropDown.dataSource = ["Please enter a valid email address"]
			self.errorMessageCell()
			self.errorDropDown.show()
			return
		}

		guard let psText = passwordField.text, !psText.isEmpty else {
			self.appearance.cellHeight = 40
			self.errorDropDown.anchorView = passwordField
			self.errorDropDown.dataSource = ["Please enter your password."]
			self.errorMessageCell()
			self.errorDropDown.show()
			return
		}

		//Make login call
		loginUser(username: emailField.text!, password: passwordField.text!)
	}

	func loginUser(username: String, password: String) {
		showLoadIndicator()
		let loginUser = BaseAPIClient.urlFromPath(path: APIEndPoints.login)
		let params: [String: AnyObject] = ["username": username as AnyObject, "password": password as AnyObject]
		APIManager.shared.fetchGenericData(method: .post, urlString: loginUser, params: params, headers: nil) { (result: SignIn) in
			if result.accesstoken == nil {
				SVProgressHUD.showError(withStatus:  "Please check your credentials and try again OR register to gain access")
			} else {
				SVProgressHUD.dismiss(withDelay: 0.2)
				self.keychain["userId"] = result.userid
				self.keychain["token"] = result.accesstoken
				self.keychain["refreshtoken"] = result.refreshtoken
				self.keychain["tokenCreationdDate"] = self.getDateString()

				///Get token for push notification
				let FCMToken = try? self.keychain.get("FCMToken")
				self.updateToken(id: CurrentUserInfo.shared.userId(), token: FCMToken ?? "")
			}
		}
	}

	func updateToken(id: String, token: String) {
		_ = APIEndPoints.init() //APIEndPoints.authorizationHeader
		let updateToknUrl = BaseAPIClient.urlFromPath(path: APIEndPoints.updateToken)
		let header = ["Authorization": "Bearer \(self.keychain["token"] ?? "")"]
		let params: [String: AnyObject] = ["id": id as AnyObject, "token": token  as AnyObject]
		APIManager.shared.fetchGenericData(method: .post, urlString: updateToknUrl, params: params, headers: header) { (result: UpdateTokenModel) in
			if result.code == 200 {
				DispatchQueue.main.async(execute: {
					self.performSegue(withIdentifier: SignInWithEmailController.tabBarController, sender: self)
				})
			} else {
				SVProgressHUD.showError(withStatus: "Oops! something went wrong, please try again")
			}
		}
	}

	@IBAction func signUp(_ sender: UIButton) {
		DispatchQueue.main.async(execute: {
			self.performSegue(withIdentifier: SignInWithEmailController.signupWithEmailController, sender: self)
		})
	}

	@IBAction func closeScreen(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}

	///Show load indicator
	func showLoadIndicator() {
		SVProgressHUD.show()
	}
}
