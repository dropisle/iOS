//
//  SignInWithPhoneNumController.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-12.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit
import CountryPickerView
import DropDown
import SVProgressHUD
import IQKeyboardManagerSwift

class SignInWithPhoneNumController: UIViewController {

	static let phoneValidationController = "phoneValidationController"
	static let signupWithPhoneNumController = "signupWithPhoneNumController"

	var errorDropDown = DropDown()
	var appearance = DropDown.appearance()
	var isFromPhoneSignUpScreen = Bool()
	weak var cpvTextField: CountryPickerView!
	private var phoneNumberWithAreaCode: String?

	@IBOutlet weak var phoneNumberField: UITextField!

	override func viewDidLoad() {
		super.viewDidLoad()

		let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
		cp.delegate = self
		cp.dataSource = self
		phoneNumberField.leftView = cp
		phoneNumberField.leftViewMode = .always
		self.cpvTextField = cp
		cpvTextField.countryDetailsLabel.font = UIFont(name: "Avenir-Black", size: 14.0)

		cp.translatesAutoresizingMaskIntoConstraints = false
		cp.flagImageView.translatesAutoresizingMaskIntoConstraints = false

		cp.flagImageView.heightAnchor.constraint(equalToConstant: 25).isActive = true
		cp.flagImageView.widthAnchor.constraint(equalToConstant: 25).isActive = true
		
		if let textFieldView = phoneNumberField.leftView  {
			cp.leftAnchor.constraint(equalTo: textFieldView.leftAnchor, constant: 0).isActive = true
			cp.topAnchor.constraint(equalTo: textFieldView.topAnchor, constant: 0).isActive = true
			cp.bottomAnchor.constraint(equalTo: textFieldView.bottomAnchor, constant: 0).isActive = true
		}

		//set up drop down.
		self.customizeDropDown()

		//hide keyboard
		hideKeyboardWhenTapped()

		//Keyboard applied to all screens
		IQKeyboardManager.shared.enable = true

		errorDropDown.bottomOffset = CGPoint(x: 0, y: 40)
		errorDropDown.cellNib = UINib(nibName: "ErrorViewCell", bundle: nil)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		///Analytics
		LoggerManager.shared.log(event: .phoneLoginScreen)
	}

	func errorMessageCell() {
		self.appearance.textColor = .red
		errorDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
			guard let cell = cell as? ErrorViewCell else { return }
			if index == 0 {
				cell.errorIcon.image = UIImage(named: "errorIcon")
				cell.errorIcon.setImageColor(color: .red)
				cell.layer.borderColor = UIColor.red.cgColor
			}
		}
	}

	func customizeDropDown() {
		appearance.cellHeight = 45
		appearance.backgroundColor = UIColor(white: 1, alpha: 1)
		appearance.selectionBackgroundColor = UIColor(hexString: "#3995F9")
		appearance.separatorColor = UIColor(white: 0.0, alpha: 0.1)
//		appearance.cornerRadius = 5
//		appearance.shadowOpacity = 0.9
//		appearance.shadowRadius = 20
		appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
		appearance.animationduration = 0.30
		if let font = UIFont(name: "Avenir", size: 14.0) {
			appearance.textFont = font
		}
	}

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
			if (segue.identifier == "phoneValidationController") {
				let phValidation = segue.destination as? PhoneValidationController
				phValidation?.phoneNumber = phoneNumberWithAreaCode
				phValidation?.isFromPhoneSignUpScreen = false
			}
    }

	@IBAction func signInAction(_ sender: UIButton) {
		guard let phoneNum = phoneNumberField.text, !phoneNum.isEmpty  else {
			self.errorDropDown.anchorView = phoneNumberField
			self.errorDropDown.dataSource = ["Please enter your phone number."]
			self.errorMessageCell()
			self.errorDropDown.show()
			return
		}

		///Phone Logim
		loginUser()
	}

	func loginUser() {
		SVProgressHUD.show()
		guard let areaCode = cpvTextField.countryDetailsLabel.text, let number = phoneNumberField.text else { return }
		let params: [String: AnyObject] = ["phone": areaCode.digits + number as AnyObject]
		phoneNumberWithAreaCode = areaCode.digits + number
		_ = APIEndPoints.init()
		let addUserUrl = BaseAPIClient.urlFromPath(path: APIEndPoints.phoneLogin)
		APIManager.shared.fetchGenericData(method: .post, urlString: addUserUrl, params: params, headers: nil) { (result: PhoneLoginModel) in
			if result.code == 200 {
				SVProgressHUD.dismiss(withDelay: 0.5)

				///Switch to validation page to enter 4 digit code and login
				self.performSegue(withIdentifier: SignInWithPhoneNumController.phoneValidationController, sender: self)
			} else  if result.code == 405 || result.code == 403 {
				SVProgressHUD.showError(withStatus: "User not available \nPlease register to create an account.")
			}
		}
	}

	@IBAction func signUpAction(_ sender: UIButton) {
		DispatchQueue.main.async(execute: {
			self.performSegue(withIdentifier: SignInWithPhoneNumController.signupWithPhoneNumController, sender: self)
		})
	}

	@IBAction func closeScreen(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}
}

extension SignInWithPhoneNumController: CountryPickerViewDelegate, CountryPickerViewDataSource {

	func cellImageViewSize(in countryPickerView: CountryPickerView) -> CGSize {
		return CGSize(width: 25, height: 20)
	}

	func cellLabelFont(in countryPickerView: CountryPickerView) -> UIFont {
		return UIFont(name: "Avenir-Black", size: 14.0)!
	}

	func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
		countryPickerView.countryDetailsLabel.font = UIFont(name: "Avenir-Black", size: 14.0)
		countryPickerView.flagImageView.translatesAutoresizingMaskIntoConstraints = false
		countryPickerView.flagImageView.heightAnchor.constraint(equalToConstant: 25).isActive = true
		countryPickerView.flagImageView.widthAnchor.constraint(equalToConstant: 25).isActive = true
	}

	func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
		var countries = [Country]()
		["CA", "GH", "NG", "US"].forEach { code in
			if let country = countryPickerView.getCountryByCode(code) {
				countries.append(country)
			}
		}
		return countries
	}

	func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
		return "Select Country"
	}
}
