//
//  PriceSummaryCell.swift
//  DropIsle
//
//  Created by CtanLI on 2021-01-19.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import UIKit

class PriceSummaryCell: UITableViewCell, Reusable {

	let priceDetails = UILabel()
	let confirmPriceLabel = UILabel()
	let gstLabel = UILabel()
	let shippingLabel = UILabel()
	let totalLabel = UILabel()

	let confirmPrice = UILabel()
	let gst = UILabel()
	let shipping = UILabel()
	let total = UILabel()
	let divider = UIView()
	let checkout = UIButton()

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		//Initialization code
		self.contentView.backgroundColor = .green
		setUp()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func setUp() {
		priceDetails.translatesAutoresizingMaskIntoConstraints = false
		confirmPrice.translatesAutoresizingMaskIntoConstraints = false
		gst.translatesAutoresizingMaskIntoConstraints = false
		shipping.translatesAutoresizingMaskIntoConstraints = false
		total.translatesAutoresizingMaskIntoConstraints = false
		divider.translatesAutoresizingMaskIntoConstraints = false
		confirmPriceLabel.translatesAutoresizingMaskIntoConstraints = false
		gstLabel.translatesAutoresizingMaskIntoConstraints = false
		shippingLabel.translatesAutoresizingMaskIntoConstraints = false
		totalLabel.translatesAutoresizingMaskIntoConstraints = false
		checkout.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(priceDetails)
		contentView.addSubview(confirmPriceLabel)
		contentView.addSubview(gstLabel)
		contentView.addSubview(shippingLabel)
		contentView.addSubview(totalLabel)
		contentView.addSubview(confirmPrice)
		contentView.addSubview(gst)
		contentView.addSubview(shipping)
		contentView.addSubview(total)
		contentView.addSubview(divider)
		contentView.addSubview(checkout)

		NSLayoutConstraint.activate([
			priceDetails.topAnchor.constraint(equalTo: topAnchor, constant: 5),
			priceDetails.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			priceDetails.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			confirmPriceLabel.topAnchor.constraint(equalTo: priceDetails.bottomAnchor, constant: 10),
			confirmPriceLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),

			confirmPrice.topAnchor.constraint(equalTo: priceDetails.bottomAnchor, constant: 10),
			confirmPrice.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			gstLabel.topAnchor.constraint(equalTo: confirmPriceLabel.bottomAnchor, constant: 10),
			gstLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),

			gst.topAnchor.constraint(equalTo: confirmPrice.bottomAnchor, constant: 10),
			gst.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			shippingLabel.topAnchor.constraint(equalTo: gstLabel.bottomAnchor, constant: 10),
			shippingLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),

			shipping.topAnchor.constraint(equalTo: gst.bottomAnchor, constant: 10),
			shipping.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			totalLabel.topAnchor.constraint(equalTo: shippingLabel.bottomAnchor, constant: 10),
			totalLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),

			total.topAnchor.constraint(equalTo: shipping.bottomAnchor, constant: 10),
			total.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			divider.heightAnchor.constraint(equalToConstant: 0.3),
			divider.topAnchor.constraint(equalTo: totalLabel.bottomAnchor, constant: 10),
			divider.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			divider.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			checkout.heightAnchor.constraint(equalToConstant: 55),
			checkout.topAnchor.constraint(equalTo: divider.bottomAnchor, constant: 15),
			checkout.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			checkout.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
		])

		divider.backgroundColor = .lightGray

		priceDetails.textColor = .label
		priceDetails.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16)
		priceDetails.text = "Price Details"
		priceDetails.lineBreakMode = .byTruncatingTail
		priceDetails.numberOfLines = 1

		confirmPriceLabel.textColor = .label
		confirmPriceLabel.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		confirmPriceLabel.text = "Price"
		confirmPriceLabel.lineBreakMode = .byTruncatingTail
		confirmPriceLabel.numberOfLines = 1

		confirmPrice.textColor = .label
		confirmPrice.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		confirmPrice.text = "$300"
		confirmPrice.lineBreakMode = .byTruncatingTail
		confirmPrice.numberOfLines = 1
		confirmPrice.textAlignment = .right

		gstLabel.textColor = .label
		gstLabel.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		gstLabel.text = "GST"
		gstLabel.lineBreakMode = .byTruncatingTail
		gstLabel.numberOfLines = 1

		gst.textColor = .label
		gst.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		gst.text = "$23.67"
		gst.lineBreakMode = .byTruncatingTail
		gst.numberOfLines = 1
		gst.textAlignment = .right

		shippingLabel.textColor = .label
		shippingLabel.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		shippingLabel.text = "Shipping"
		shippingLabel.lineBreakMode = .byTruncatingTail
		shippingLabel.numberOfLines = 1

		shipping.textColor = .label
		shipping.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		shipping.text = "$7.15"
		shipping.lineBreakMode = .byTruncatingTail
		shipping.numberOfLines = 1
		shipping.textAlignment = .right

		totalLabel.textColor = .label
		totalLabel.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16)
		totalLabel.text = "Total (CAD)"
		totalLabel.lineBreakMode = .byTruncatingTail
		totalLabel.numberOfLines = 1

		total.textColor = .label
		total.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16)
		total.text = "$350.67"
		total.lineBreakMode = .byTruncatingTail
		total.numberOfLines = 1
		total.textAlignment = .right

		checkout.cornerRadius = 7
		checkout.borderColor = .secondarySystemGroupedBackground
		checkout.borderWidth = 2
		checkout.setTitle("Checkout", for: .normal)
		checkout.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 22)
		checkout.backgroundColor = UIColor.NavyBlueDark
	}
}
