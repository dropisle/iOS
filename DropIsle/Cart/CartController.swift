//
//  CartController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-24.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class CartController:  UIViewController, UITableViewDataSource, UITableViewDelegate {

	// MARK: - Properties -
	var tableView: UITableView!
	var carts: CartModel?
	var cart = [CartModel.Items]() {
		didSet {
			DispatchQueue.main.async {
				self.tableView.reloadData()
			}
		}
	}

	var qtyValue = Int()
	var productVarientId = String()

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		title = "Cart"
		setUpTableView()

		//
		getUsersCart()
	}

	func setUpTableView() {
		tableView = UITableView.init(frame: .zero, style: .grouped)
		tableView.dataSource = self
		tableView.delegate = self
		tableView.separatorStyle = .none
		tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.sectionHeaderHeight = .leastNormalMagnitude
		tableView.backgroundColor = .secondarySystemGroupedBackground
		view.addSubview(tableView)

		NSLayoutConstraint.activate([
			tableView.topAnchor.constraint(equalTo: view.topAnchor),
			tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])

		tableView.register(CartCell.self, forCellReuseIdentifier: CartCell.identifier)
		tableView.register(PriceSummaryCell.self, forCellReuseIdentifier: PriceSummaryCell.identifier)
		tableView.register(FooterView.self, forHeaderFooterViewReuseIdentifier: FooterView.identifier)
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return  cart.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: CartCell.identifier ) as! CartCell
		cell.selectionStyle = .none
		cell.delegate = self
		cell.qCount.text = "\(cart[indexPath.row].quantity)"
		getProductById(indexPath: indexPath, cell: cell, productId: cart[indexPath.row].productid)
		return cell
	}

	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		let lastSectionIndex = tableView.numberOfSections - 1
		let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
		if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
		}
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 180
	}

	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return .leastNormalMagnitude
	}

	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		let footerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: FooterView.identifier) as! FooterView
		footerView.delegate = self
		let lastSectionIndex = tableView.numberOfSections - 1
		if  section != lastSectionIndex {
			footerView.checkout.isHidden = true
			footerView.orderSummaryLabel.isHidden = true
			footerView.orderSummary.isHidden = true
		} else {
			footerView.checkout.isHidden = false
			footerView.orderSummaryLabel.isHidden = false
			footerView.orderSummary.isHidden = false
		}
		return footerView
	}

	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		let lastSectionIndex = tableView.numberOfSections - 1
		if  section == lastSectionIndex {
			return 200
		}
		return 0
	}
}

extension CartController: CheckoutActionDelegate, UpdateQuantityDelegate {

	func updateQuantity(cell: CartCell, state: String) {
		if appConstants.minus == state {
			decreaseQuantity(cell: cell)
		}

		if appConstants.plus == state {
			increaseQuantity(cell: cell)
		}
	}

	func getUsersCart() {
		APIManager.shared.requestCallWithUrlParams(method: .get, path: urlPath.getCart.rawValue, params: [:]) { [self] (_ result: Result<CartModel, Error>) in
			switch result {
				case .success(let data):
					let _ = data.carts.map { value in

						//UPDATE THIS FOR THE RIGHT CART TO PULL
						if value.isCheckedOut != true {
							carts = data
							cart.append(contentsOf: value.items)
						}
					}
					tableView.reloadData()
				case .failure(let error):
					self.alert(message: error.localizedDescription)
			}
		}
	}

	func increaseQuantity(cell: CartCell) {

		let productId = cell.products?.productid
		let _ = cart.compactMap({ value in
			if value.productid == productId {
				productVarientId = value.productvarientid ?? ""
			}
		})

		let items = ["productid": cell.products?.productid ?? "", "quantity": 1, "productvarientid": productVarientId] as [String : Any]
		let params = ["cartid": carts?.carts.first?.cartid ?? "", "items": [items]] as [String : Any]
		APIManager.shared.requestCallWithHeaders(method: .post, path: urlPath.addItemToCart.rawValue, params: params) { [self] (_ result: Result<CartUpdateModel, Error>) in
			switch result {
				case .success(let data):
					let _ = data.cart.items.map { value in
						guard let indexPath = tableView.indexPath(for: cell) else { return }
						let cell = tableView.cellForRow(at: indexPath) as? CartCell
						if  cell?.qCount.text == "2" {
							cell?.minus.isEnabled = true
							cell?.minus.layer.borderColor = UIColor.dynamic(light: UIColor.label, dark: UIColor.label).cgColor
						}
						cell?.qCount.text = "\(data.cart.items[indexPath.row].quantity)"
 					}
				case .failure(let error):
					print(error)
			}
		}
	}

	func decreaseQuantity(cell: CartCell) {

		let productId = cell.products?.productid
		let _ = cart.compactMap({ value in
			if value.productid == productId {
				productVarientId = value.productvarientid ?? ""
			}
		})
		
		let items = ["productid": cell.products?.productid ?? "", "quantity": 1, "productvarientid": productVarientId] as [String : Any]
		let params = ["cartid": carts?.carts.first?.cartid ?? "", "items": [items]] as [String : Any]
		APIManager.shared.requestCallWithHeaders(method: .post, path: urlPath.deleteItemInCart.rawValue, params: params) { [self] (_ result: Result<CartUpdateModel, Error>) in
			switch result {
				case .success(let data):
					let _ = data.cart.items.map { value in
						guard let indexPath = tableView.indexPath(for: cell) else { return }
						let cell = tableView.cellForRow(at: indexPath) as? CartCell
						if  cell?.qCount.text == "1" {
							cell?.minus.isEnabled = false
							cell?.minus.layer.borderColor = UIColor.dynamic(light: UIColor.lightGray, dark: UIColor.lightGray).cgColor
						}
						cell?.qCount.text = "\(data.cart.items[indexPath.row].quantity)"
						print("\(value.quantity)")
					}
				case .failure(let error):
					print(error)
			}
		}
	}

	func deleteItemFromCart(cell: CartCell) {

		let productId = cell.products?.productid
		let _ = cart.compactMap({ value in
			if value.productid == productId {
				productVarientId = value.productvarientid ?? ""
			}
		})

		if let value = Int(cell.qCount.text ?? "0") {
			qtyValue = value
		}
		let items = ["productid": cell.products?.productid ?? "", "quantity": qtyValue, "productvarientid": productVarientId] as [String : Any]
		let params = ["cartid": carts?.carts.first?.cartid ?? "", "items": [items]] as [String : Any]
		APIManager.shared.requestCallWithHeaders(method: .post, path: urlPath.deleteItemInCart.rawValue, params: params) { [self] (_ result: Result<CartUpdateModel, Error>) in
			switch result {
				case .success(_):
					guard let indexPath = tableView.indexPath(for: cell) else { return }
					let _ = tableView.cellForRow(at: indexPath) as? CartCell
					cart.remove(at: indexPath.row)
					tableView.performBatchUpdates ({
						tableView.deleteRows(at: [indexPath], with: .fade)
					}) { (_) in
						//tableView.reloadSections(IndexSet(integer: indexPath.section), with: .fade)
					}
				case .failure(let error):
					print(error)
			}
		}
	}

	func getProductById(indexPath: IndexPath, cell: CartCell, productId: String) {
		let  params = [ "productid": productId]
		APIManager.shared.requestCallWithUrlParams(method: .get, path: urlPath.getProduct.rawValue, params: params) { [self] (_ result: Result<GetProductModel, Error>) in
			switch result {
				case .success(let data):
					let _ = data.products.map { value in
						cell.products = value
					}
					//
					downloadProductImage(indexPath: indexPath, cell: cell, productId: productId)
				case .failure(let error):
					print(error)
			}
		}
	}

	func downloadProductImage(indexPath: IndexPath, cell: CartCell, productId: String) {
		let params = [ "productid": productId]
		APIManager.shared.requestCallWithUrlParams(method: .get, path: urlPath.getProductImage.rawValue, params: params as Parameters) {  (_ result: Result<[GetProductImageModel], Error>) in
			let _ = result.map { imageArr in
				weak var _ = self
				cell.photo.sd_imageTransition = SDWebImageTransition.fade
				if let image = imageArr.first?.url {
					cell.photo.sd_setImage(with: URL(string: image)) { (image, error, cache, url) in }
				}
			}
		}
	}

	func checkOutAction() {
		self.performSegue(withIdentifier: segues.addShippingAddress.rawValue, sender: self)
	}
}

//let lastSectionIndex = tableView.numberOfSections - 1
//if  indexPath.section !=  lastSectionIndex {
//	let cell = tableView.dequeueReusableCell(withIdentifier: CartCell.identifier ) as! CartCell
//	cell.selectionStyle = .none
//	return cell
//} else {
//	let cell = tableView.dequeueReusableCell(withIdentifier: PriceSummaryCell.identifier ) as! PriceSummaryCell
//	cell.selectionStyle = .none
//	return cell
//}
