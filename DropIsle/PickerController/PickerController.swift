//
//  PickerController.swift
//  DropIsle
//
//  Created by CtanLI on 2021-03-21.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol PickerViewDelegate: class {
	func pickerOptionSelected(value: String, tag: Int)
}

class PickerController: UIViewController {

	@IBOutlet weak var pickerView: UIPickerView!
	var pickerOptions = [String]()
	var isSelected = Int()
	weak var delegate: PickerViewDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
		pickerView.delegate = self
		pickerView.dataSource = self
		displayPicker()
    }
}

extension PickerController:  UIPickerViewDelegate, UIPickerViewDataSource {
	func displayPicker() {
		switch PickerOption(rawValue: isSelected) {
			case .months:
				pickerOptions.append(contentsOf: Months.indexValues)
			case .years:
				pickerOptions.append(contentsOf: Years.indexValues)
			case .none: break
		}
	}

	// Number of columns of data
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}

	// The number of rows of data
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return pickerOptions.count
	}

	// The data to return for the row and component (column) that's being passed in
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		return pickerOptions[row]
	}

	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		delegate?.pickerOptionSelected(value: String(pickerOptions[row]), tag: isSelected)
	}
}
