//
//  LocationManager.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-12.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import CoreLocation

@objc protocol UpdateLocationDelegate: class {
	func updateLocation()
	func openCountryList()
}

class LocationMonitor: NSObject, CLLocationManagerDelegate {

	//
	// MARK: properties
	//

	// services
	static let SharedManager = LocationMonitor()
	var locationManager = CLLocationManager()
	weak var delegate: UpdateLocationDelegate?

	// data
	var currentLocation: CLLocation?

	//
	// MARK: creation
	//

	override init() {
		super.init()
		if CLLocationManager.locationServicesEnabled() {
			locationManager.delegate = self
			locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
		}
	}

	//
	// MARK: implementations
	//

	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		if let location = locations.first {
			stopMonitoring()
			currentLocation = location
			self.delegate?.updateLocation()
		}
	}

	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		print(error.localizedDescription)
		self.delegate?.openCountryList()
		stopMonitoring()
	}

	func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
		print("Enter")
	}

	//
	// MARK: operations
	//

	func startMonitoring() {
		locationManager.startUpdatingLocation()
	}

	func stopMonitoring() {
		locationManager.stopUpdatingLocation()
	}
}
