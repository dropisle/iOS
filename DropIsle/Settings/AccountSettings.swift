//
//  ProfileController.swift
//  DropIsle
//
//  Created by CtanLI on 2021-02-13.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import UIKit

class AccountSettings: UIViewController {

	let scrollView = UIScrollView()
	let viewContainer = UIView()
	let profileImage = UIImageView()
	let userName = UILabel()
	@objc let showProfile = UIButton()
	let firstDivider = UIView()

	let accountSettings = UILabel()
	let personalInfo = UIButton()
	let personalInfoAvatar = UIImageView()
	let personalInfoDivider = UIView()

	let payments = UIButton()
	let paymentsAvatar = UIImageView()
	let paymentsDivider = UIView()

	let notifications = UIButton()
	let notificationsAvatar = UIImageView()
	let notificationsDivider = UIView()

	let security = UILabel()
	let loginAndPasswords = UIButton()
	let securityInfoAvatar = UIImageView()
	let securityInfoDivider = UIView()

	let referralsAndCredits = UILabel()
	let inviteFriends = UIButton()
	let inviteFriendsAvatar = UIImageView()
	let inviteFriendsDivider = UIView()

	let suppport = UILabel() 
	let hdwSuppport = UIButton()
	let hdwSuppportAvatar = UIImageView()
	let hdwSuppportDivider = UIView()

	let legal = UILabel()
	let toslegal = UIButton()
	let toslegalAvatar = UIImageView()
	let toslegalDivider = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
		personalInfo.addTarget(self, action: #selector(showProfileAction), for: .touchUpInside)
		view.backgroundColor = .secondarySystemGroupedBackground
		setUp()
	}

	@objc func showProfileAction() {
		self.performSegue(withIdentifier: segues.profile.rawValue, sender: self)
	}
}

extension AccountSettings {
	func setUp() {
		scrollView.translatesAutoresizingMaskIntoConstraints = false
		viewContainer.translatesAutoresizingMaskIntoConstraints = false
		profileImage.translatesAutoresizingMaskIntoConstraints = false
		userName.translatesAutoresizingMaskIntoConstraints = false
		showProfile.translatesAutoresizingMaskIntoConstraints = false
		firstDivider.translatesAutoresizingMaskIntoConstraints = false

		accountSettings.translatesAutoresizingMaskIntoConstraints = false
		personalInfo.translatesAutoresizingMaskIntoConstraints = false
		personalInfoAvatar.translatesAutoresizingMaskIntoConstraints = false
		personalInfoDivider.translatesAutoresizingMaskIntoConstraints = false

		payments.translatesAutoresizingMaskIntoConstraints = false
		paymentsAvatar.translatesAutoresizingMaskIntoConstraints = false
		paymentsDivider.translatesAutoresizingMaskIntoConstraints = false

		notifications.translatesAutoresizingMaskIntoConstraints = false
		notificationsAvatar.translatesAutoresizingMaskIntoConstraints = false
		notificationsDivider.translatesAutoresizingMaskIntoConstraints = false

		security.translatesAutoresizingMaskIntoConstraints = false
		loginAndPasswords.translatesAutoresizingMaskIntoConstraints = false
		securityInfoAvatar.translatesAutoresizingMaskIntoConstraints = false
		securityInfoDivider.translatesAutoresizingMaskIntoConstraints = false

		referralsAndCredits.translatesAutoresizingMaskIntoConstraints = false
		inviteFriends.translatesAutoresizingMaskIntoConstraints = false
		inviteFriendsAvatar.translatesAutoresizingMaskIntoConstraints = false
		inviteFriendsDivider.translatesAutoresizingMaskIntoConstraints = false

		suppport.translatesAutoresizingMaskIntoConstraints = false
		hdwSuppport.translatesAutoresizingMaskIntoConstraints = false
		hdwSuppportAvatar.translatesAutoresizingMaskIntoConstraints = false
		hdwSuppportDivider.translatesAutoresizingMaskIntoConstraints = false

		legal.translatesAutoresizingMaskIntoConstraints = false
		toslegal.translatesAutoresizingMaskIntoConstraints = false
		toslegalAvatar.translatesAutoresizingMaskIntoConstraints = false
		toslegalDivider.translatesAutoresizingMaskIntoConstraints = false

		view.addSubview(scrollView)
		scrollView.addSubview(viewContainer)
		viewContainer.addSubview(profileImage)
		viewContainer.addSubview(userName)
		viewContainer.addSubview(showProfile)
		viewContainer.addSubview(firstDivider)

		viewContainer.addSubview(accountSettings)
		viewContainer.addSubview(personalInfo)
		viewContainer.addSubview(personalInfoAvatar)
		viewContainer.addSubview(personalInfoDivider)

		viewContainer.addSubview(payments)
		viewContainer.addSubview(paymentsAvatar)
		viewContainer.addSubview(paymentsDivider)

		viewContainer.addSubview(notifications)
		viewContainer.addSubview(notificationsAvatar)
		viewContainer.addSubview(notificationsDivider)

		viewContainer.addSubview(security)
		viewContainer.addSubview(loginAndPasswords)
		viewContainer.addSubview(securityInfoAvatar)
		viewContainer.addSubview(securityInfoDivider)

		viewContainer.addSubview(referralsAndCredits)
		viewContainer.addSubview(inviteFriends)
		viewContainer.addSubview(inviteFriendsAvatar)
		viewContainer.addSubview(inviteFriendsDivider)

		viewContainer.addSubview(suppport)
		viewContainer.addSubview(hdwSuppport)
		viewContainer.addSubview(hdwSuppportAvatar)
		viewContainer.addSubview(hdwSuppportDivider)

		viewContainer.addSubview(legal)
		viewContainer.addSubview(toslegal)
		viewContainer.addSubview(toslegalAvatar)
		viewContainer.addSubview(toslegalDivider)

		NSLayoutConstraint.activate([
			scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			scrollView.widthAnchor.constraint(equalTo: view.widthAnchor),
			scrollView.topAnchor.constraint(equalTo: view.topAnchor),
			scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

			viewContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
			viewContainer.topAnchor.constraint(equalTo: scrollView.topAnchor),
			viewContainer.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
			viewContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),

			profileImage.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			profileImage.topAnchor.constraint(equalTo: viewContainer.topAnchor, constant: 20),
			profileImage.heightAnchor.constraint(equalToConstant: 80),
			profileImage.widthAnchor.constraint(equalToConstant: 80),

			userName.leadingAnchor.constraint(equalTo: profileImage.trailingAnchor, constant: 10),
			userName.topAnchor.constraint(equalTo: profileImage.topAnchor, constant: 18),

			showProfile.leadingAnchor.constraint(equalTo: profileImage.trailingAnchor, constant: 10),
			showProfile.bottomAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: -18),

			firstDivider.heightAnchor.constraint(equalToConstant: 0.2),
			firstDivider.topAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: 10),
			firstDivider.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor),
			firstDivider.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor),

			accountSettings.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			accountSettings.topAnchor.constraint(equalTo: firstDivider.bottomAnchor, constant: 20),

			personalInfo.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			personalInfo.trailingAnchor.constraint(equalTo: personalInfoAvatar.leadingAnchor, constant: -20),
			personalInfo.topAnchor.constraint(equalTo: accountSettings.bottomAnchor, constant: 25),

			personalInfoAvatar.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),
			personalInfoAvatar.centerYAnchor.constraint(equalTo: personalInfo.centerYAnchor),
			personalInfoAvatar.heightAnchor.constraint(equalToConstant: 20),
			personalInfoAvatar.widthAnchor.constraint(equalToConstant:20),

			personalInfoDivider.heightAnchor.constraint(equalToConstant: 0.5),
			personalInfoDivider.topAnchor.constraint(equalTo: personalInfo.bottomAnchor, constant: 10),
			personalInfoDivider.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			personalInfoDivider.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),

			payments.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			payments.trailingAnchor.constraint(equalTo: paymentsAvatar.leadingAnchor, constant: -20),
			payments.topAnchor.constraint(equalTo: personalInfoDivider.bottomAnchor, constant: 25),

			paymentsAvatar.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),
			paymentsAvatar.centerYAnchor.constraint(equalTo: payments.centerYAnchor),
			paymentsAvatar.heightAnchor.constraint(equalToConstant: 20),
			paymentsAvatar.widthAnchor.constraint(equalToConstant:20),

			paymentsDivider.heightAnchor.constraint(equalToConstant: 0.5),
			paymentsDivider.topAnchor.constraint(equalTo: payments.bottomAnchor, constant: 10),
			paymentsDivider.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			paymentsDivider.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),

			notifications.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			notifications.trailingAnchor.constraint(equalTo: notificationsAvatar.leadingAnchor, constant: -20),
			notifications.topAnchor.constraint(equalTo: paymentsDivider.bottomAnchor, constant: 25),

			notificationsAvatar.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),
			notificationsAvatar.centerYAnchor.constraint(equalTo: notifications.centerYAnchor),
			notificationsAvatar.heightAnchor.constraint(equalToConstant: 20),
			notificationsAvatar.widthAnchor.constraint(equalToConstant:20),

			notificationsDivider.heightAnchor.constraint(equalToConstant: 0.5),
			notificationsDivider.topAnchor.constraint(equalTo: notifications.bottomAnchor, constant: 10),
			notificationsDivider.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			notificationsDivider.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),

			security.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			security.topAnchor.constraint(equalTo: notificationsDivider.bottomAnchor, constant: 20),

			loginAndPasswords.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			loginAndPasswords.trailingAnchor.constraint(equalTo: securityInfoAvatar.leadingAnchor, constant: -20),
			loginAndPasswords.topAnchor.constraint(equalTo: security.bottomAnchor, constant: 25),

			securityInfoAvatar.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),
			securityInfoAvatar.centerYAnchor.constraint(equalTo: loginAndPasswords.centerYAnchor),
			securityInfoAvatar.heightAnchor.constraint(equalToConstant: 20),
			securityInfoAvatar.widthAnchor.constraint(equalToConstant:20),

			securityInfoDivider.heightAnchor.constraint(equalToConstant: 0.5),
			securityInfoDivider.topAnchor.constraint(equalTo: loginAndPasswords.bottomAnchor, constant: 10),
			securityInfoDivider.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			securityInfoDivider.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),

			referralsAndCredits.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			referralsAndCredits.topAnchor.constraint(equalTo: securityInfoDivider.bottomAnchor, constant: 20),

			inviteFriends.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			inviteFriends.trailingAnchor.constraint(equalTo: inviteFriendsAvatar.leadingAnchor, constant: -20),
			inviteFriends.topAnchor.constraint(equalTo: referralsAndCredits.bottomAnchor, constant: 25),

			inviteFriendsAvatar.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),
			inviteFriendsAvatar.centerYAnchor.constraint(equalTo: inviteFriends.centerYAnchor),
			inviteFriendsAvatar.heightAnchor.constraint(equalToConstant: 20),
			inviteFriendsAvatar.widthAnchor.constraint(equalToConstant:20),

			inviteFriendsDivider.heightAnchor.constraint(equalToConstant: 0.5),
			inviteFriendsDivider.topAnchor.constraint(equalTo: inviteFriends.bottomAnchor, constant: 10),
			inviteFriendsDivider.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			inviteFriendsDivider.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),

			suppport.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			suppport.topAnchor.constraint(equalTo: inviteFriendsDivider.bottomAnchor, constant: 20),

			hdwSuppport.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			hdwSuppport.trailingAnchor.constraint(equalTo: hdwSuppportAvatar.leadingAnchor, constant: -20),
			hdwSuppport.topAnchor.constraint(equalTo: suppport.bottomAnchor, constant: 25),

			hdwSuppportAvatar.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),
			hdwSuppportAvatar.centerYAnchor.constraint(equalTo: hdwSuppport.centerYAnchor),
			hdwSuppportAvatar.heightAnchor.constraint(equalToConstant: 20),
			hdwSuppportAvatar.widthAnchor.constraint(equalToConstant:20),

			hdwSuppportDivider.heightAnchor.constraint(equalToConstant: 0.5),
			hdwSuppportDivider.topAnchor.constraint(equalTo: hdwSuppport.bottomAnchor, constant: 10),
			hdwSuppportDivider.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			hdwSuppportDivider.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),

			legal.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			legal.topAnchor.constraint(equalTo: hdwSuppportDivider.bottomAnchor, constant: 20),

			toslegal.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			toslegal.trailingAnchor.constraint(equalTo: toslegalAvatar.leadingAnchor, constant: -20),
			toslegal.topAnchor.constraint(equalTo: legal.bottomAnchor, constant: 25),

			toslegalAvatar.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),
			toslegalAvatar.centerYAnchor.constraint(equalTo: toslegal.centerYAnchor),
			toslegalAvatar.heightAnchor.constraint(equalToConstant: 20),
			toslegalAvatar.widthAnchor.constraint(equalToConstant:20),

			toslegalDivider.heightAnchor.constraint(equalToConstant: 0.5),
			toslegalDivider.topAnchor.constraint(equalTo: toslegal.bottomAnchor, constant: 10),
			toslegalDivider.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 20),
			toslegalDivider.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -20),
			toslegalDivider.bottomAnchor.constraint(equalTo: viewContainer.bottomAnchor, constant: -20)
		])

		profileImage.image = #imageLiteral(resourceName: "nature5")
		profileImage.borderColor = .lightGray
		profileImage.cornerRadius = 40
		profileImage.clipsToBounds = true

		userName.text = "Sandra P."
		userName.textColor = .label
		userName.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 16)

		showProfile.setTitle("show profile", for: .normal)
		showProfile.setTitleColor(.lightGray, for: .normal)
		showProfile.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		firstDivider.backgroundColor = .lightGray

		accountSettings.text = "Account settings"
		accountSettings.textColor = .lightGray
		accountSettings.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		personalInfo.setTitle("Personal information", for: .normal)
		personalInfo.setTitleColor(.label, for: .normal)
		personalInfo.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)
		personalInfo.contentHorizontalAlignment = .left

		let personImage = UIImage(systemName: "person", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		personalInfoAvatar.image = personImage
		personalInfoDivider.backgroundColor = .label

		payments.setTitle("Payments", for: .normal)
		payments.setTitleColor(.label, for: .normal)
		payments.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)
		payments.contentHorizontalAlignment = .left

		let paymentsImage = UIImage(systemName: "creditcard", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		paymentsAvatar.image = paymentsImage
		paymentsDivider.backgroundColor = .label

		notifications.setTitle("Notifications", for: .normal)
		notifications.setTitleColor(.label, for: .normal)
		notifications.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)
		notifications.contentHorizontalAlignment = .left

		let notificationsImage = UIImage(systemName: "bell", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		notificationsAvatar.image = notificationsImage
		notificationsDivider.backgroundColor = .label

		security.text = "Security"
		security.textColor = .lightGray
		security.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		loginAndPasswords.setTitle("Login & Password", for: .normal)
		loginAndPasswords.setTitleColor(.label, for: .normal)
		loginAndPasswords.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)
		loginAndPasswords.contentHorizontalAlignment = .left

		let securityImage = UIImage(systemName: "lock", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		securityInfoAvatar.image = securityImage
		securityInfoDivider.backgroundColor = .label

		referralsAndCredits.text = "Referrals And Credits"
		referralsAndCredits.textColor = .lightGray
		referralsAndCredits.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		inviteFriends.setTitle("Invite Friends", for: .normal)
		inviteFriends.setTitleColor(.label, for: .normal)
		inviteFriends.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)
		inviteFriends.contentHorizontalAlignment = .left

		let inviteFriendsImage = UIImage(systemName: "gift", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		inviteFriendsAvatar.image = inviteFriendsImage
		inviteFriendsDivider.backgroundColor = .label

		suppport.text = "Support"
		suppport.textColor = .lightGray
		suppport.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		hdwSuppport.setTitle("How Dropisle works", for: .normal)
		hdwSuppport.setTitleColor(.label, for: .normal)
		hdwSuppport.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)
		hdwSuppport.contentHorizontalAlignment = .left

		let suppportImage = UIImage(systemName: "network", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		hdwSuppportAvatar.image = suppportImage
		hdwSuppportDivider.backgroundColor = .label

		legal.text = "Legal"
		legal.textColor = .lightGray
		legal.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)

		toslegal.setTitle("Terms of service", for: .normal)
		toslegal.setTitleColor(.label, for: .normal)
		toslegal.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)
		toslegal.contentHorizontalAlignment = .left

		let legalImage = UIImage(systemName: "filemenu.and.cursorarrow.rtl", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		toslegalAvatar.image = legalImage
		toslegalDivider.backgroundColor = .label
	}
}
