//
//  PersistentService.swift
//  DropIsle
//
//  Created by CtanLI on 2019-01-27.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation
import CoreData

class PersistanceService {
	// MARK: - Core Data stack

	private init() {}
	static let shared = PersistanceService()

//	static var context: NSManagedObjectContext {
//		return persistentContainer.viewContext
//	}

	lazy var persistentContainer: NSPersistentContainer = {
		/*
		The persistent container for the application. This implementation
		creates and returns a container, having loaded the store for the
		application to it. This property is optional since there are legitimate
		error conditions that could cause the creation of the store to fail.
		*/
		let container = NSPersistentContainer(name: "DropIsle")
		container.loadPersistentStores(completionHandler: { (_, error) in
			if let error = error as NSError? {
				// Replace this implementation with code to handle the error appropriately.
				// fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

				/*
				Typical reasons for an error here include:
				* The parent directory does not exist, cannot be created, or disallows writing.
				* The persistent store is not accessible, due to permissions or data protection when the device is locked.
				* The device is out of space.
				* The store could not be migrated to the current model version.
				Check the error message to determine what the actual problem was.
				*/
				fatalError("Unresolved error \(error), \(error.userInfo)")
			}
		})
		return container
	}()

	// MARK: - Core Data Saving support
	lazy var context = persistentContainer.viewContext
	func saveContext () {
		//let context = persistentContainer.viewContext
		if context.hasChanges {
			do {
				try context.save()
			} catch {
				// Replace this implementation with code to handle the error appropriately.
				// fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
				let nserror = error as NSError
				fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
			}
		}
	}

	func fetch<T: NSManagedObject>(_ obectType: T.Type) -> [T] {
		let entityName = String(describing: obectType)
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
		do {
			let fetchObject = try context.fetch(fetchRequest) as? [T]
			return fetchObject ?? [T]()
		} catch {
			print(error)
			return [T]()
		}
	}

	func delete(_ object: NSManagedObject) {
		context.delete(object)
		saveContext()
	}
}

//class Currentuser : NSObject {
//	static let sharedInstance = Currentuser()
//	func getCurrentuser() -> AuthUserEntity? {
//		var user = [AuthUserEntity]()
//
//		let fetchrequest: NSFetchRequest<AuthUserEntity> = AuthUserEntity.fetchRequest()
//		do {
//			user = try PersistanceService.shared.context.fetch(fetchrequest) as [AuthUserEntity]
//			print(user)
//			if user.count == 0 { return nil }
//		} catch {
//			fatalError("Failure to fetch user: \(error)")
//		}
//		return user.first!
//	}
//}

//func getCurrentuser() -> AuthUserEntity? {
//	var user = [AuthUserEntity]()
//	let _: NSFetchRequest<AuthUserEntity> = AuthUserEntity.fetchRequest()
//	do {
//		user = PersistanceService.shared.fetch(AuthUserEntity.self)
//		//print(user.first?.guestUserID ?? "")
//		if user.count == 0 { return nil }
//	}
//	return user.first!
//}
