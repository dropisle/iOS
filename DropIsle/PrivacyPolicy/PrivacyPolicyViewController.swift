//
//  PrivacyPolicyViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-03-29.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class PrivacyPolicyViewController: UIViewController {

	let urlString = "https://www.dropisle.com/Privacy"
	var isFromSettingsPage = Bool()
	@IBOutlet weak var webView: WKWebView!

	override func viewDidLoad() {
		addBarButtons()
		super.viewDidLoad()
		self.title = "Privacy Policy"
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		showLoadIndicator()
		DispatchQueue.main.async { [weak self] in
			guard let self = self else { return }
			guard let url = URL(string: self.urlString) else { return }
			let request = URLRequest(url: url)
			self.webView.load(request)
			SVProgressHUD.dismiss(withDelay: 1.0)
		}
	}

	func addBarButtons() {
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton
	}

	func showLoadIndicator() {
		SVProgressHUD.show(withStatus: "Loading...")
	}

	@objc func dismissScreen(sender: AnyObject) {
		if isFromSettingsPage {
			self.navigationController?.popViewController(animated: true)
		} else {
			self.dismiss(animated: true, completion: nil)
		}
	}

	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destination.
	// Pass the selected object to the new view controller.
	}
	*/
}
