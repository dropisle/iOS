//
//  HomeNavController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-12-23.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

class HomeNavController: UINavigationController, UINavigationControllerDelegate {
    override func viewDidLoad() {
			super.viewDidLoad()
			self.setNeedsStatusBarAppearanceUpdate()
			self.modalPresentationCapturesStatusBarAppearance = true
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.delegate = self
	}

	func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
		let te = UINavigationBarAppearance()
		te.configureWithTransparentBackground()
		navigationController.navigationBar.standardAppearance = te
		if (viewController.isKind(of: ProductDetailsController.self)) { }
		viewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
	}
} 

extension HomeNavController {
	open override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	 }
}

