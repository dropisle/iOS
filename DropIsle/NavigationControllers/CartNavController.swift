//
//  ListNavController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-12-23.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

class CartNavController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

extension CartNavController {
	open override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	 }
}
