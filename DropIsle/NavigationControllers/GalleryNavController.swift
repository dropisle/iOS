//
//  ActivityNavController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-12-23.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

class GalleryNavController: UINavigationController, UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.delegate = self
	}

	func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
		let te = UINavigationBarAppearance()
		te.configureWithDefaultBackground()
		viewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
	}
}

extension GalleryNavController {
	open override var preferredStatusBarStyle: UIStatusBarStyle {
	return .lightContent
	 }
}
