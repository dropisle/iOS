//
//  SustainableArtController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-04.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class SustainableArtController: UIViewController {

	private var customConstraints = [NSLayoutConstraint]()

	let containerView = UIView()
	let searchContainer = UIView()
	let searchButton = UIButton()
	let backButton = UIButton()
	let filterButton = UIButton()

	override func viewDidLoad() {
		super.viewDidLoad()
		let te = UINavigationBarAppearance()
		te.configureWithTransparentBackground()
		navigationController?.navigationBar.standardAppearance = te

		navigationItem.setHidesBackButton(true, animated: false)
		navigationController?.setNavigationBarHidden(true, animated: true)
		navigationController?.navigationItem.title = ""
		navigationController?.navigationBar.prefersLargeTitles = false
		navigationItem.largeTitleDisplayMode = .never

		setupCollectionView()
		setUpSearchBar() 
		collectionView.delegate = self
	}

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
	}

	// MARK: - Properties -
	private var presenter = RecommendedPresenter()         // Handles all the data
	var collectionView: UICollectionView!

	// MARK: - Setup methods -
	/// Constructs the UICollectionView and adds it to the view.
	/// Registers all the Cells and Views that the UICollectionView will need
	private func setupCollectionView() {
		// Initialises the collection view with a CollectionViewLayout which we will define
		collectionView = UICollectionView.init(frame: .zero, collectionViewLayout: makeLayout())
		// Assigning data source and background color
		collectionView.dataSource = self
		collectionView.backgroundColor = .secondarySystemGroupedBackground

		// Adding the collection view to the view
		collectionView.contentInsetAdjustmentBehavior = .never
		collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
		view.addSubview(collectionView)

		// This line tells the system we will define our own constraints
		collectionView.translatesAutoresizingMaskIntoConstraints = false
		// Constraining the collection view to the 4 edges of the view
		NSLayoutConstraint.activate([
			collectionView.topAnchor.constraint(equalTo: view.topAnchor),
			collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])

		// Registering all Cells and Classes we will need
		collectionView.register(SustainableArtReusableView.self,
								forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
								withReuseIdentifier: SustainableArtReusableView.identifier)
		collectionView.register(SustainableArtCell.self,
								forCellWithReuseIdentifier: SustainableArtCell.identifier)
	}

	// MARK: - Collection View Helper Methods -
	// In this section you can find all the layout related code

	/// Creates the appropriate UICollectionViewLayout for each section type
	private func makeLayout() -> UICollectionViewLayout {
		// Constructs the UICollectionViewCompositionalLayout
		let layout = UICollectionViewCompositionalLayout { (sectionIndex: Int, layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
			let isWideView = layoutEnvironment.container.effectiveContentSize.width > 500
			switch self.presenter.sectionType(for: sectionIndex) {
				case .singleSectionList:   return self.createRecommendedForYouSection(isWide: isWideView)
			}
		}

		/// Configure the Layout with interSectionSpacing
		let config = UICollectionViewCompositionalLayoutConfiguration()
		config.interSectionSpacing = 20
		layout.configuration = config
		return layout
	}

	/// Creates a layout that shows 3 items per group and scrolls horizontally
	private func createRecommendedForYouSection(isWide: Bool) -> NSCollectionLayoutSection {
		// Defining the size of a single item in this layout
		let itemSize = NSCollectionLayoutSize (
		  widthDimension: .fractionalWidth(1.0),
		  heightDimension: .fractionalHeight(1.0))
		let item = NSCollectionLayoutItem(layoutSize: itemSize)
		item.contentInsets = NSDirectionalEdgeInsets(top: 8, leading: 8, bottom: 8, trailing: 8)

		let groupHeight = NSCollectionLayoutDimension.fractionalWidth(isWide ? 0.55 : 0.85)
		let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: groupHeight)
		let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: isWide ? 2 : 1)
		let section = NSCollectionLayoutSection(group: group)

		let layoutSectionHeader = createSectionHeader(section: 650)
		section.boundarySupplementaryItems = [layoutSectionHeader]

		return section
	}

	//Creates a Layout for the SectionHeader
	private func createSectionHeader(section: Int) -> NSCollectionLayoutBoundarySupplementaryItem {
		// Define size of Section Header
		let layoutSectionHeaderSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(CGFloat(section)))
		// Construct Section Header Layout
		let layoutSectionHeader = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: layoutSectionHeaderSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
		return layoutSectionHeader
	}

	//Animate custom Navbar
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let pan = scrollView.panGestureRecognizer
		let velocity = pan.velocity(in: scrollView).y
		if velocity < -300 {
			animateShowContainerView()
		} else if velocity > 300 {
			animateHideContainerView()
		}
	}
}

// MARK: - UICollectionViewDataSource -

extension SustainableArtController: UICollectionViewDataSource {
	//Tells the UICollectionView how many sections are needed
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return presenter.numberOfSections
	}

	//Tells the UICollectionView how many items the requested sections needs
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return presenter.numberOfItems(for: section)
	}

	//Constructs and configures the item needed for the requested IndexPath
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		// Checks what section type we should use for this indexPath so we use the right cells for that section
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SustainableArtCell.identifier, for: indexPath) as? SustainableArtCell else {
			fatalError("Could not dequeue FeatureCell")
		}
		switch presenter.sectionType(for: indexPath.section) {
			case .singleSectionList:
				presenter.configure(item: cell, for: indexPath)
				return cell
		}
	}

	//Constructs and configures the Supplementary Views for the UICollectionView
	//In this project only used for the Section Headers
	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		if kind == UICollectionView.elementKindSectionHeader {
			guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SustainableArtReusableView.identifier, for: indexPath) as? SustainableArtReusableView else {
				fatalError("Could not dequeue SectionHeader")
			}
			if let title = presenter.title(for: indexPath.section),  let subtitle = presenter.subtitle(for: indexPath.section) {
				headerView.title.text = title
				headerView.subTitle.text = subtitle
			}
			return headerView
		}
		return UICollectionReusableView()
	}
}


extension SustainableArtController: UICollectionViewDelegate {

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		switch presenter.sectionType(for: indexPath.section) {
			case .singleSectionList:
				print(presenter.dataSource[indexPath.section].data[indexPath.row])
		}
	}
}

extension SustainableArtController {

	func setUpSearchBar() {
		containerView.backgroundColor = .clear

		searchContainer.makeCorner(withRadius: 25)
		searchContainer.backgroundColor = .secondarySystemGroupedBackground

		searchButton.backgroundColor = .white
		searchButton.layer.cornerRadius = 25
		searchButton.setTitle("Search by artist, country, style etc.", for: .normal)
		searchButton.setTitleColor(.black, for: .normal)
		searchButton.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14)

		let backButtonImage = UIImage(systemName: "chevron.backward", withConfiguration: UIImage.SymbolConfiguration(pointSize: 15, weight: .semibold, scale: .medium))?.withTintColor(.black, renderingMode: .alwaysOriginal)
		backButton.setImage(backButtonImage, for: .normal)
		backButton.addTarget(self, action: #selector(backToPrevious), for: .touchDown)

		let filterButtonImage = UIImage(systemName: "slider.horizontal.3", withConfiguration: UIImage.SymbolConfiguration(pointSize: 15, weight: .semibold, scale: .medium))?.withTintColor(.black, renderingMode: .alwaysOriginal)
		filterButton.setImage(filterButtonImage, for: .normal)

		view.addSubview(containerView)
		containerView.addSubview(searchContainer)
		containerView.addSubview(searchButton)
		searchButton.addSubview(backButton)
		searchButton.addSubview(filterButton)

		containerView.translatesAutoresizingMaskIntoConstraints = false
		searchContainer.translatesAutoresizingMaskIntoConstraints = false
		searchButton.translatesAutoresizingMaskIntoConstraints = false
		backButton.translatesAutoresizingMaskIntoConstraints = false
		filterButton.translatesAutoresizingMaskIntoConstraints = false

		containerView.pinEdges(to: self.view)
		NSLayoutConstraint.activate([
			containerView.heightAnchor.constraint(equalToConstant: topBarHeight < 88 ?  80 : 110),

			searchContainer.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
			searchContainer.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
			searchContainer.heightAnchor.constraint(equalToConstant: 50),
			searchContainer.topAnchor.constraint(equalTo: containerView.topAnchor, constant: topBarHeight < 88 ?  20 : 50),
			searchContainer.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),

			searchButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
			searchButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),

			searchButton.heightAnchor.constraint(equalToConstant: 50),
			searchButton.topAnchor.constraint(equalTo: view.topAnchor, constant: topBarHeight < 88 ?  20 : 50),
			searchButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),

			backButton.widthAnchor.constraint(equalToConstant: 30),
			backButton.heightAnchor.constraint(equalToConstant: 30),
			backButton.leadingAnchor.constraint(equalTo: searchButton.leadingAnchor, constant: 10),
			backButton.centerYAnchor.constraint(equalTo: searchButton.centerYAnchor),

			filterButton.widthAnchor.constraint(equalToConstant: 30),
			filterButton.heightAnchor.constraint(equalToConstant: 30),
			filterButton.trailingAnchor.constraint(equalTo: searchButton.trailingAnchor, constant: -10),
			filterButton.centerYAnchor.constraint(equalTo: searchButton.centerYAnchor)
		])
	}

	//constraint for hidding containerView
	func setUpContainerView() {
		NSLayoutConstraint.activate([
			searchContainer.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
			searchContainer.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
			searchContainer.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
			searchContainer.topAnchor.constraint(equalTo: containerView.topAnchor, constant: topBarHeight < 88 ?  20 : 50),
			searchContainer.heightAnchor.constraint(equalToConstant: 50)
		])
	}

	@objc func animateHideContainerView() {
		searchButton.backgroundColor = .white
		searchContainer.deactivateAllConstraints()
		searchContainer.makeCorner(withRadius: 25)
		///constraint for hidding containerView
		setUpContainerView()
		UIView.animate(withDuration: 1.0, delay: 0, options: [], animations: {
			self.containerView.layoutIfNeeded()
		}) { (true) in
		}
	}

	@objc func animateShowContainerView() {
		searchButton.backgroundColor = UIColor(hexString: "#f1f1f1")
		searchContainer.deactivateAllConstraints()
		searchContainer.pinEdges(to: self.containerView)
		if UIDevice.init().iPhoneX {
			searchContainer.makeTopCorner(withRadius: 25)
		} else {
			searchContainer.makeTopCorner(withRadius: 0)
		}
		NSLayoutConstraint.activate([
			self.searchContainer.heightAnchor.constraint(equalToConstant: topBarHeight < 88 ?  80 : 110)
		])
			UIView.animate(withDuration: 1.0, delay: 0, options: [], animations: {
				self.containerView.layoutIfNeeded()
			}) { (true) in
		 }
	}

	@objc func backToPrevious(sender: UIButton) {
		navigationController?.popViewController(animated: true)
	}
}

