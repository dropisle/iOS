//
//  Userdefault.swift
//  DropIsle
//
//  Created by CtanLI on 2019-11-12.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

var preferences = UserDefaults.standard

class Defaults {
  fileprivate init() {}
}

class DefaultsKey<ValueType>: Defaults {
  let key: String

  init(_ key: String) {
    self.key = key
  }
}

extension UserDefaults {
	subscript(key: DefaultsKey<String?>) -> String? {
		get { return string(forKey: key.key) }
		set { set(newValue, forKey: key.key) }
	}

	subscript(key: DefaultsKey<Bool>) -> Bool {
		get { return bool(forKey: key.key) }
		set { set(newValue, forKey: key.key) }
	}

	subscript(key: DefaultsKey<[String]>) -> [String] {
		get {
			guard let strings = stringArray(forKey: key.key) else { return [] }
			return strings.compactMap { $0 }
		}
		set { set(newValue.map { $0 }, forKey: key.key) }
	}
}

extension Defaults {
	static let categorySelected = DefaultsKey<String?>("categorySelected")

	static let itemMarkedAsSold = DefaultsKey<[String]>("itemMarkedAsSold")
	static let addedToWishlist = DefaultsKey<[String]>("addedToWishlist")
	static let addedSearchedToWishlist = DefaultsKey<[String]>("addedSearchedToWishlist")
	static let addedOtherItemToWishlist = DefaultsKey<[String]>("addedOtherItemToWishlist")
	static let similarProductsItemToWishlist = DefaultsKey<[String]>("similarProductsItemToWishlist")

	static let latitude = DefaultsKey<String?>("latitude")
	static let longitude = DefaultsKey<String?>("longitude")

	//address values
	static let email = DefaultsKey<String?>("email")
	static let firstName = DefaultsKey<String?>("firstName")
	static let lastName = DefaultsKey<String?>("lastName")
	static let mobile = DefaultsKey<String?>("mobile")
	static let address = DefaultsKey<String?>("address")
	static let city = DefaultsKey<String?>("city")
	static let province = DefaultsKey<String?>("province")
	static let country = DefaultsKey<String?>("country")
	static let postalCode = DefaultsKey<String?>("postalCode")
}
