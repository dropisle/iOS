//
//  Extension.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-11.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import SVProgressHUD
import SystemConfiguration

let imageCache = NSCache<NSString, UIImage>()

protocol Reusable: class {
	static var identifier: String { get }
}

extension Reusable {
	static var identifier: String {
		return String(describing: self)
	}
}

extension UITableView {
	func dequeReusableCell<T: UITableViewCell>(_ index: IndexPath) -> T where T: Reusable {
		guard let cell = self.dequeueReusableCell(withIdentifier: T.identifier, for: index) as? T else {
			fatalError("Failed to dequeue cell.")
		}
		return cell
	}

	func dequeReusableView<T: UITableViewCell>() -> T where T: Reusable {
		guard let cell = self.dequeueReusableCell(withIdentifier: T.identifier) as? T else {
			fatalError("Failed to dequeue cell.")
		}
		return cell
	}
}

extension UICollectionView {
	func dequeReusableCell<T: UICollectionViewCell>(_ index: IndexPath) -> T where T: Reusable {
		guard let cell = self.dequeueReusableCell(withReuseIdentifier: T.identifier, for: index) as? T else {
			fatalError("Failed to dequeue cell.")
		}
		return cell
	}
}

extension String {
	var localized: String {
		return NSLocalizedString(self, comment: self)
	}

	func localizedWithComment(comment: String) -> String {
		return NSLocalizedString(self, comment: comment)
	}
}

extension String {
	func htmlAttributedString() -> NSAttributedString? {
		guard let data = self.data(using: String.Encoding.utf32, allowLossyConversion: false) else { return nil }
		guard let html = try? NSMutableAttributedString(
			data: data,
			options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
			documentAttributes: nil) else { return nil }
		return html
	}
}

extension UIImageView {
	func loadImageUsingCacheWithURLString(urlString: String) {
		let imageString = urlString
		if urlString == "" {
			return
		}
		self.image = nil
		if let cachedImage = imageCache.object(forKey: urlString as NSString) {
			self.image = cachedImage
			return
		}
		let url = URL(string: urlString)!
		URLSession.shared.dataTask(with: url, completionHandler: { (data, _, error) in
			if error != nil {
				print(error ?? "")
				return
			}
			DispatchQueue.main.async {
				if let downloadedImages = UIImage(data: data!) {

					if imageString == urlString {
						self.image = downloadedImages
					}

					imageCache.setObject(downloadedImages, forKey: urlString as NSString)
					self.image = downloadedImages
				}
			}
		}).resume()
	}
}

extension UIColor {
	convenience init(hexString: String) {
		let hexString: NSString = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as NSString
		let scanner = Scanner(string: hexString as String)

		if hexString.hasPrefix("#") {
			scanner.scanLocation = 1
		}

		var color: UInt32 = 0
		scanner.scanHexInt32(&color)

		let mask = 0x000000FF
		let hexRed = Int(color >> 16) & mask
		let hexGreen = Int(color >> 8) & mask
		let hexBlue = Int(color) & mask

		let red   = CGFloat(hexRed) / 255.0
		let green = CGFloat(hexGreen) / 255.0
		let blue  = CGFloat(hexBlue) / 255.0

		self.init(red: red, green: green, blue: blue, alpha: 1)
	}

	func toHexString() -> String {
		var redValue: CGFloat = 0
		var greenValue: CGFloat = 0
		var blueValue: CGFloat = 0
		var alphaValue: CGFloat = 0

		getRed(&redValue, green: &greenValue, blue: &blueValue, alpha: &alphaValue)

		let rgb: Int = (Int)(redValue*255)<<16 | (Int)(greenValue*255)<<8 | (Int)(blueValue*255)<<0

		return NSString(format: "#%06x", rgb) as String
	}

	public class var blueLightTheme: UIColor {
		return UIColor(red: 40/255, green: 169/255, blue: 225/255, alpha: 1.0)
	}

	public class var blueDarkTheme: UIColor {
		return UIColor(red: 0/255, green: 117/255, blue: 168/255, alpha: 1.0)
	}

	public class var orangeTheme: UIColor {
		return UIColor(red: 240/255, green: 87/255, blue: 46/255, alpha: 1.0)
	}

	public class var lightBlueText: UIColor {
		return UIColor(red: 39/255, green: 170/255, blue: 225/255, alpha: 1.0)
	}

	public class var NavyBlueDark: UIColor {
		return UIColor(red: 27/255, green: 41/255, blue: 46/255, alpha: 1.0)
	}

	public class var BuyNowColor: UIColor {
		return UIColor(red: 253/255, green: 99/255, blue: 102/255, alpha: 1.0)
	}

	public class var artDetailScrollColor: UIColor {
		return UIColor(red: 231/255, green: 230/255, blue: 230/255, alpha: 1.0)
	}

	public class var cellExpandBG: UIColor {
		return UIColor(hexString: "eeeeee")
	}

	public class var greenAnnotation: UIColor {
		return UIColor(hexString: "003333")
	}

	public class var grayTheme: UIColor {
		return UIColor(hexString: "999999")
	}
}

extension String {
	func isValidEmail() -> Bool {
		do {
			let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}",
																					options: .caseInsensitive)
			return regex.firstMatch(in: self,
															options: NSRegularExpression.MatchingOptions(rawValue: 0),
															range: NSRange(location: 0, length: self.count)) != nil
		} catch {
			return false
		}
	}

	func isPasswordValid() -> Bool {
		do {
			let regex = try NSRegularExpression(pattern: "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,30}$",
																					options: .caseInsensitive)
			return regex.firstMatch(in: self,
															options: NSRegularExpression.MatchingOptions(rawValue: 0),
															range: NSRange(location: 0, length: self.count)) != nil
		} catch {
			return false
		}
	}
}

extension UIColor {

	func lighter(by percentage: CGFloat = 30.0) -> UIColor? {
		return self.adjust(by: abs(percentage) )
	}

	func darker(by percentage: CGFloat = 30.0) -> UIColor? {
		return self.adjust(by: -1 * abs(percentage) )
	}

	func adjust(by percentage: CGFloat = 30.0) -> UIColor? {
		var redValue: CGFloat=0, greenValue: CGFloat=0, blueValue: CGFloat=0, alphaValue: CGFloat=0
		if self.getRed(&redValue, green: &greenValue, blue: &blueValue, alpha: &alphaValue) {
			return UIColor(red: min(redValue + percentage/100, 1.0),
										 green: min(greenValue + percentage/100, 1.0),
										 blue: min(blueValue + percentage/100, 1.0),
										 alpha: alphaValue)
		} else {
			return nil
		}
	}
}

extension UIColor {
	convenience init(hex: Int, alpha: CGFloat = 1.0) {
		self.init(
			red: CGFloat((hex & 0xFF0000) >> 16) / 255.0,
			green: CGFloat((hex & 0x00FF00) >> 8)  / 255.0,
			blue: CGFloat((hex & 0x0000FF) >> 0)  / 255.0,
			alpha: alpha
		)
	}
}

extension UIView {
	func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
		let animation = CABasicAnimation(keyPath: "transform.rotation")

		animation.toValue = toValue
		animation.duration = duration
		animation.isRemovedOnCompletion = false
		animation.fillMode = CAMediaTimingFillMode.forwards

		self.layer.add(animation, forKey: nil)
	}
}

extension UITextView {

	func resolveHashTags() {

		// turn string in to NSString
		let nsText: NSString = self.text! as NSString

		// this needs to be an array of NSString. String does not work.
		let words: [NSString] = nsText.components(separatedBy: " ") as [NSString]

		// you can't set the font size in the storyboard anymore, since it gets overridden here.
		let attrs = [
			NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12.0)
		]

		// you can staple URLs onto attributed strings
		let attrString = NSMutableAttributedString(string: nsText as String, attributes: attrs)

		// tag each word if it has a hashtag
		for word in words {

			// found a word that is prepended by a hashtag!
			// homework for you: implement @mentions here too.
			if word.hasPrefix("#") {

				// a range is the character position, followed by how many characters are in the word.
				// we need this because we staple the "href" to this range.
				let matchRange: NSRange = nsText.range(of: word as String)

				// convert the word from NSString to String
				// this allows us to call "dropFirst" to remove the hashtag
				var stringifiedWord: String = word as String

				// drop the hashtag
				stringifiedWord = String(stringifiedWord.dropFirst())

				// check to see if the hashtag has numbers.
				// ribl is "#1" shouldn't be considered a hashtag.
				let digits = NSCharacterSet.decimalDigits

				if stringifiedWord.rangeOfCharacter(from: digits) != nil {
					// hashtag contains a number, like "#1"
					// so don't make it clickable
				} else {
					// set a link for when the user clicks on this word.
					// it's not enough to use the word "hash", but you need the url scheme syntax "hash://"
					// note: since it's a URL now, the color is set to the project's tint color
					attrString.addAttribute(NSAttributedString.Key.link, value: "hash:\(stringifiedWord)", range: matchRange)
				}
			}
		}

		// we're used to textView.text
		// but here we use textView.attributedText
		// again, this will also wipe out any fonts and colors from the storyboard,
		// so remember to re-add them in the attrs dictionary above
		self.attributedText = attrString
	}
}

extension UITextView {

	func resolveMentions() {

		// turn string in to NSString
		let nsText: NSString = self.text! as NSString

		// this needs to be an array of NSString. String does not work.
		let words: [NSString] = nsText.components(separatedBy: " ") as [NSString]

		// you can't set the font size in the storyboard anymore, since it gets overridden here.
		let attrs = [
			NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12.0)
		]

		// you can staple URLs onto attributed strings
		let attrString = NSMutableAttributedString(string: nsText as String, attributes: attrs)

		// tag each word if it has a hashtag
		for word in words {

			// found a word that is prepended by a hashtag!
			// homework for you: implement @mentions here too.
			if word.hasPrefix("@") {

				// a range is the character position, followed by how many characters are in the word.
				// we need this because we staple the "href" to this range.
				let matchRange: NSRange = nsText.range(of: word as String)

				// convert the word from NSString to String
				// this allows us to call "dropFirst" to remove the hashtag
				var stringifiedWord: String = word as String

				// drop the hashtag
				stringifiedWord = String(stringifiedWord.dropFirst())

				// check to see if the hashtag has numbers.
				// ribl is "#1" shouldn't be considered a hashtag.
				let digits = NSCharacterSet.decimalDigits

				if stringifiedWord.rangeOfCharacter(from: digits) != nil {
					// hashtag contains a number, like "#1"
					// so don't make it clickable
				} else {
					// set a link for when the user clicks on this word.
					// it's not enough to use the word "hash", but you need the url scheme syntax "hash://"
					// note: since it's a URL now, the color is set to the project's tint color
					attrString.addAttribute(NSAttributedString.Key.link, value: "hash:\(stringifiedWord)", range: matchRange)
				}
			}
		}

		// we're used to textView.text
		// but here we use textView.attributedText
		// again, this will also wipe out any fonts and colors from the storyboard,
		// so remember to re-add them in the attrs dictionary above
		self.attributedText = attrString
	}
}

extension UIView {

	func setCellShadow() {
		self.layer.shadowColor = UIColor.darkGray.cgColor
		self.layer.shadowOffset = CGSize(width: 0, height: 1)
		self.layer.shadowOpacity = 5
		self.layer.shadowRadius = 5.0
		self.layer.masksToBounds = false
		self.clipsToBounds = false
		self.layer.cornerRadius = 9
	}

	func setAnchor(top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?,
								 bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?,
								 paddingTop: CGFloat, paddingLeft: CGFloat, paddingBottom: CGFloat,
								 paddingRight: CGFloat, width: CGFloat = 0, height: CGFloat = 0) {

		self.translatesAutoresizingMaskIntoConstraints = false

		if let top = top {
			self.topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
		}

		if let left = left {
			self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
		}

		if let bottom = bottom {
			self.bottomAnchor.constraint(equalTo: bottom, constant: paddingBottom).isActive = true
		}

		if let right = right {
			self.rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
		}

		if width != 0 {
			self.widthAnchor.constraint(equalToConstant: width).isActive = true
		}

		if height != 0 {
			self.heightAnchor.constraint(equalToConstant: height).isActive = true
		}
	}

	var safeTopAnchor: NSLayoutYAxisAnchor {
		if #available(iOS 11.0, *) {
			return safeAreaLayoutGuide.topAnchor
		}
		return topAnchor
	}

	var safeLeftAnchor: NSLayoutXAxisAnchor {
		if #available(iOS 11.0, *) {
			return safeAreaLayoutGuide.leftAnchor
		}
		return leftAnchor
	}

	var safeBottomAnchor: NSLayoutYAxisAnchor {
		if #available(iOS 11.0, *) {
			return safeAreaLayoutGuide.bottomAnchor
		}
		return bottomAnchor
	}

	var safeRightAnchor: NSLayoutXAxisAnchor {
		if #available(iOS 11.0, *) {
			return safeAreaLayoutGuide.rightAnchor
		}
		return rightAnchor
	}
}

extension UIImageView {
	func filterImage(image: UIImage) {
		let context = CIContext(options: nil)
		if let currentFilter = CIFilter(name: "CISepiaTone") {
			let beginImage = CIImage(image: image)
			currentFilter.setValue(beginImage, forKey: kCIInputImageKey)
			currentFilter.setValue(1.0, forKey: kCIInputIntensityKey)

			if let output = currentFilter.outputImage {
				if let cgimg = context.createCGImage(output, from: output.extent) {
					let processedImage = UIImage(cgImage: cgimg)
					self.image = processedImage
				}
			}
		}
	}
}

extension Array where Element: Equatable {
	func removeDuplicates() -> [Element] {
		var result = [Element]()
		for value in self {
			if result.contains(value) == false {
				result.append(value)
			}
		}
		return result
	}
}

public struct ChunkIterator<I: IteratorProtocol>: IteratorProtocol {
	fileprivate var index: I
	fileprivate let nextIndex: Int
	public mutating func next() -> [I.Element]? {
		guard let head = index.next() else { return nil }
		var build = [head]
		build.reserveCapacity(nextIndex)
		for _ in (1..<nextIndex) {
			guard let xValue = index.next() else { break }
			build.append(xValue)
		}
		return build
	}
}

public struct ChunkSeq<S: Sequence>: Sequence {
	fileprivate let seq: S
	fileprivate let next: Int
	public func makeIterator() -> ChunkIterator<S.Iterator> {
		return ChunkIterator(index: seq.makeIterator(), nextIndex: next)
	}
}

public extension Sequence {
	func chunk(_ nex: Int) -> ChunkSeq<Self> {
		return ChunkSeq(seq: self, next: nex)
	}
}

extension CAGradientLayer {
	convenience init(frame: CGRect, colors: [UIColor]) {
		self.init()
		self.frame = frame
		self.colors = []
		for color in colors {
			self.colors?.append(color.cgColor)
		}
		startPoint = CGPoint(x: 0.3, y: 0.3)
		endPoint = CGPoint(x: 1.3, y: 1.3)
	}

	func creatGradientImage() -> UIImage? {
		var image: UIImage?
		UIGraphicsBeginImageContext(bounds.size)
		if let context = UIGraphicsGetCurrentContext() {
			render(in: context)
			image = UIGraphicsGetImageFromCurrentImageContext()
		}
		UIGraphicsEndImageContext()
		return image
	}
}

extension UIButton {
	func bounceAnimation(value: CGFloat) {
		self.transform = CGAffineTransform(scaleX: value, y: value)
		UIView.animate(withDuration: 2.0,
									 delay: 0,
									 usingSpringWithDamping: 0.2,
									 initialSpringVelocity: 4.0,
									 options: .allowUserInteraction,
									 animations: { [] in
										self.transform = .identity
		}, completion: nil)
	}
}

extension UIApplication {
	var statusBarView: UIView? {
		return value(forKey: "statusBar") as? UIView
	}
}

extension UIDevice {
	var iPhoneX: Bool {
		return UIScreen.main.nativeBounds.height == 2436
	}
}

extension UIScreen {
	enum SizeType: CGFloat {
		case unknown = 0.0
		case iPhone4 = 960.0
		case iPhone5 = 1136.0
		case iPhone6 = 1334.0
		case iPhone6Plus = 1920.0
	}
	var sizeType: SizeType {
		let height = nativeBounds.height
		guard let sizeType = SizeType(rawValue: height) else { return .unknown }
		return sizeType
	}
}

extension UIView {
	func rotate360Degrees(duration: CFTimeInterval = 0.7, completionDelegate: AnyObject? = nil) {
		let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
		rotateAnimation.fromValue = 0.0
		rotateAnimation.toValue = CGFloat(.pi * 2.0)
		rotateAnimation.duration = duration

		if let delegate: AnyObject = completionDelegate {
			rotateAnimation.delegate = delegate as? CAAnimationDelegate
		}
		self.layer.add(rotateAnimation, forKey: nil)
	}
}

extension UIView {
	func reverseRotate360Degrees(duration: CFTimeInterval = 0.7, completionDelegate: AnyObject? = nil) {
		let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
		rotateAnimation.fromValue = CGFloat(.pi * 2.0)
		rotateAnimation.toValue = 0.0
		rotateAnimation.duration = duration

		if let delegate: AnyObject = completionDelegate {
			rotateAnimation.delegate = delegate as? CAAnimationDelegate
		}
		self.layer.add(rotateAnimation, forKey: nil)
	}
}

extension UIView {
	private static let kRotationAnimationKey = "rotationanimationkey"
	func startRotation(duration: Double = 1) {
		if layer.animation(forKey: UIView.kRotationAnimationKey) == nil {
			let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")

			rotationAnimation.fromValue = 0.0
			rotationAnimation.toValue = Float.pi * 2.0
			rotationAnimation.duration = duration
			rotationAnimation.repeatCount = Float.infinity

			layer.add(rotationAnimation, forKey: UIView.kRotationAnimationKey)
		}
	}

	func stopRotation() {
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
			if self.layer.animation(forKey: UIView.kRotationAnimationKey) != nil {
				self.layer.removeAnimation(forKey: UIView.kRotationAnimationKey)
			}
		})
	}
}

extension String {
	func contains(find: String) -> Bool {
		return self.range(of: find) != nil
	}
	func containsIgnoringCase(find: String) -> Bool {
		return self.range(of: find, options: .caseInsensitive) != nil
	}
}

extension UIViewController {
	func hideKeyboardWhenTapped() {
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self,
																														 action: #selector(UIViewController.dismissKeyboard))
		tap.cancelsTouchesInView = false
		view.addGestureRecognizer(tap)
	}

	@objc func dismissKeyboard() {
		view.endEditing(true)
	}
}

extension UIView {
	func animateButtonDown() {
		UIView.animate(withDuration: 0.1, delay: 0.0, options: [.allowUserInteraction, .curveEaseIn], animations: {
			self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
		}, completion: nil)
	}

	func animateButtonUp() {
		UIView.animate(withDuration: 0.1, delay: 0.0, options: [.allowUserInteraction, .curveEaseOut], animations: {
			self.transform = CGAffineTransform.identity
		}, completion: nil)
	}

	func rotate180() {
		UIView.animate(withDuration: 0.3) {
			self.transform = CGAffineTransform(rotationAngle: .pi)
		}
	}

	func rotateBacKtNormalState() {
		UIView.animate(withDuration: 0.3) {
			self.transform = CGAffineTransform.identity
		}
	}
}

extension UIImageView {
	func setImageColor(color: UIColor) {
		let templateImage = self.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
		self.image = templateImage
		self.tintColor = color
	}
}

extension UILabel {
	class func attributedLabel(frame: CGRect) -> UILabel {
		let label = UILabel(frame: frame)
		label.text = "You are yet to list in your area. Start selling your products now."
		label.clipsToBounds = true
		label.numberOfLines = 2
		label.textAlignment = .center
		label.font = UIFont(name: "Avenir-Heavy", size: 15)!
		label.lineBreakMode = .byWordWrapping
		label.layer.cornerRadius = 5
		label.layer.borderWidth = 0.0
		return label
	}

	class func wishAttributedLabel(frame: CGRect) -> UILabel {
		let label = UILabel(frame: frame)
		label.text = "No product in your list. Save items to view and purchase at a later time."
		label.clipsToBounds = true
		label.numberOfLines = 2
		label.textAlignment = .center
		label.font = UIFont(name: "Avenir-Heavy", size: 15)!
		label.lineBreakMode = .byWordWrapping
		label.layer.cornerRadius = 5
		label.layer.borderWidth = 0.0
		return label
	}

	class func emptySearchAttributedLabel(frame: CGRect) -> UILabel {
		let label = UILabel(frame: frame)
		label.text = "Your search did not match any listings. Try another keyword."
		label.clipsToBounds = true
		label.numberOfLines = 2
		label.textAlignment = .center
		label.font = UIFont(name: "Avenir-Heavy", size: 15)!
		label.lineBreakMode = .byWordWrapping
		label.layer.cornerRadius = 5
		label.layer.borderWidth = 0.0
		return label
	}

	class func noNetworkSatetAttributeLabel(frame: CGRect) -> UILabel {
		let label = UILabel(frame: frame)
		label.text = "No product listed in your area. Start selling your products now."
		label.clipsToBounds = true
		label.numberOfLines = 2
		label.textAlignment = .center
		label.font = UIFont(name: "Avenir-Heavy", size: 15)!
		label.lineBreakMode = .byWordWrapping
		label.layer.cornerRadius = 5
		label.layer.borderWidth = 0.0
		return label
	}

	class func noActivityAttributeLabel(frame: CGRect) -> UILabel {
		let label = UILabel(frame: frame)
		label.text = "No activities to display \nEngage by selling and buying products!"
		label.clipsToBounds = true
		label.numberOfLines = 0
		label.textAlignment = .center
		label.font = UIFont(name: "Avenir-Heavy", size: 15)!
		label.lineBreakMode = .byWordWrapping
		return label
	}

	class func noChatAttributeLabel(frame: CGRect) -> UILabel {
		let label = UILabel(frame: frame)
		label.text = "No chat recorded \nEngage by selling and buying products!"
		label.clipsToBounds = true
		label.numberOfLines = 0
		label.textAlignment = .center
		label.font = UIFont(name: "Avenir-Heavy", size: 15)!
		label.lineBreakMode = .byWordWrapping
		return label
	}
}

extension UIButton {
	class func attributedButton(frame: CGRect) -> UIButton {
		let button = UIButton(frame: frame)
		button.clipsToBounds = true
		button.layer.cornerRadius = 5
		button.layer.borderWidth = 0.0
		button.setTitle("Sell Now", for: .normal)
		button.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 15)!
		button.setTitleColor(.white, for: .normal)
		button.backgroundColor = UIColor(red: 0.20, green: 0.58, blue: 0.99, alpha: 1.0)
		return button
	}

	class func wishAttributedButton(frame: CGRect) -> UIButton {
		let button = UIButton(frame: frame)
		button.clipsToBounds = true
		button.layer.cornerRadius = 5
		button.layer.borderWidth = 0.0
		button.setTitle("Browse Products", for: .normal)
		button.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 15)!
		button.setTitleColor(.white, for: .normal)
		button.backgroundColor = UIColor(red: 0.20, green: 0.58, blue: 0.99, alpha: 1.0)
		return button
	}

	class func emptySearchAttributedButton(frame: CGRect) -> UIButton {
		let button = UIButton(frame: frame)
		button.clipsToBounds = true
		button.layer.cornerRadius = 5
		button.layer.borderWidth = 0.0
		button.setTitle("Continue Search", for: .normal)
		button.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 15)!
		button.setTitleColor(.white, for: .normal)
		button.backgroundColor = UIColor(red: 0.20, green: 0.58, blue: 0.99, alpha: 1.0)
		return button
	}
}

extension UIImageView {
	class func wishImageAttribute(frame: CGRect) -> UIImageView {
		let imageView = UIImageView(frame: frame)
		imageView.image = UIImage(named: "wish-bag")
		return imageView
	}

	class func imageAttribute(frame: CGRect) -> UIImageView {
		let imageView = UIImageView(frame: frame)
		imageView.image = UIImage(named: "shopping-bag")
		return imageView
	}

	class func emptySearchImageAttribute(frame: CGRect) -> UIImageView {
		let imageView = UIImageView(frame: frame)
		imageView.image = UIImage(named: "emptySearch")
		imageView.contentMode = .scaleAspectFill
		return imageView
	}

	class func noNetworkSateImage(frame: CGRect) -> UIImageView {
		let imageView = UIImageView(frame: frame)
		imageView.image = UIImage(named: "emptySearch")
		imageView.contentMode = .scaleAspectFill
		return imageView
	}

	class func noActivityImage(frame: CGRect) -> UIImageView {
		let imageView = UIImageView(frame: frame)
		imageView.image = UIImage(named: "emptyActivity")
		imageView.contentMode = .scaleAspectFill
		return imageView
	}

	class func noChatImage(frame: CGRect) -> UIImageView {
		let imageView = UIImageView(frame: frame)
		imageView.image = UIImage(named: "emptyChat")
		imageView.contentMode = .scaleAspectFill
		return imageView
	}
}

//To seprate Int from Strings
extension String {
	var digits: String {
		return components(separatedBy: CharacterSet.decimalDigits.inverted)
			.joined()
	}
}

//Used to resize selected or captured images before upload.
extension UIImage {
	func resized(withPercentage percentage: CGFloat) -> UIImage? {
		let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
		UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
		defer { UIGraphicsEndImageContext() }
		draw(in: CGRect(origin: .zero, size: canvasSize))
		return UIGraphicsGetImageFromCurrentImageContext()
	}

	func resized(toWidth width: CGFloat) -> UIImage? {
		let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
		UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
		defer { UIGraphicsEndImageContext() }
		draw(in: CGRect(origin: .zero, size: canvasSize))
		return UIGraphicsGetImageFromCurrentImageContext()
	}

	func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
		UIGraphicsBeginImageContextWithOptions(size, false, 0)
		color.setFill()
		UIRectFill(CGRect(x: 0, y: size.height - lineWidth, width: size.width, height: lineWidth))
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		return image!
	}

	//
	//Comparing image files
	//
	func isEqualToImage(_ image: UIImage) -> Bool {
		return self.pngData() == image.pngData()
	}
}

extension UIViewController {
	func alert(message: String, title: String = "") {
		let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
		let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
		okAction.setValue(UIColor.black, forKey: "titleTextColor")
		alertController.addAction(okAction)
		self.present(alertController, animated: true, completion: nil)
	}
}

extension UILabel {
	func addTrailing(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor) {
		let readMoreText: String = trailingText + moreText
		if self.visibleTextLength == 0 { return }
		let lengthForVisibleString: Int = self.visibleTextLength
		if let myText = self.text {
			let mutableString = myText
			let trimmedString: String? = (mutableString as NSString).replacingCharacters(in:
				NSRange(location: lengthForVisibleString, length: myText.count - lengthForVisibleString), with: "")
			let readMoreLength: Int = (readMoreText.count)
			guard let safeTrimmedString = trimmedString else { return }
			if safeTrimmedString.count <= readMoreLength { return }
			//"safeTrimmedString.count - readMoreLength" should never be less then the readMoreLength because it'll be a negative value and will crash
			let trimmedForReadMore: String = (safeTrimmedString as NSString).replacingCharacters(in:
				NSRange(location: safeTrimmedString.count - readMoreLength, length: readMoreLength), with: "") + trailingText
			let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore,
																											 attributes: [NSAttributedString.Key.font: self.font!])
			let readMoreAttributed = NSMutableAttributedString(string: moreText,
																												 attributes: [NSAttributedString.Key.font: moreTextFont,
																																			NSAttributedString.Key.foregroundColor: moreTextColor])
			answerAttributed.append(readMoreAttributed)
			self.attributedText = answerAttributed
		}
	}

	var visibleTextLength: Int {
		let font: UIFont = self.font
		let mode: NSLineBreakMode = self.lineBreakMode
		let labelWidth: CGFloat = self.frame.size.width
		let labelHeight: CGFloat = self.frame.size.height
		let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)

		if let myText = self.text {
			let attributes: [AnyHashable: Any] = [NSAttributedString.Key.font: font]
			let attributedText = NSAttributedString(string: myText, attributes: attributes as? [NSAttributedString.Key: Any])
			let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint,
																														 options: .usesLineFragmentOrigin, context: nil)
			if boundingRect.size.height > labelHeight {
				var index: Int = 0
				var prev: Int = 0
				let characterSet = CharacterSet.whitespacesAndNewlines
				repeat {
					prev = index
					if mode == NSLineBreakMode.byCharWrapping {
						index += 1
					} else {
						index = (myText as NSString).rangeOfCharacter(from: characterSet, options: [],
																													range: NSRange(location: index + 1,
																																				 length: myText.count - index - 1)).location
					}
				} while index != NSNotFound && index < myText.count && (myText as NSString).substring(to: index).boundingRect(with:
					sizeConstraint, options: .usesLineFragmentOrigin,
													attributes: attributes as? [NSAttributedString.Key: Any],
													context: nil).size.height <= labelHeight
				return prev
			}
		}
		if self.text == nil {
			return 0
		} else {
			return self.text!.count
		}
	}
}

extension SVProgressHUD {
	class func loading(message: String, backgroundColor: UIColor, textColor: UIColor) {
		SVProgressHUD.show(withStatus: message)
		SVProgressHUD.setFont(UIFont(name: "Avenir", size: 17)!)
		SVProgressHUD.setImageViewSize(CGSize(width: 50, height: 50))
		SVProgressHUD.setShouldTintImages(false)
	}
}

extension UIView {
	func shake(for duration: TimeInterval = 0.5, withTranslation translation: CGFloat = 10) {
		let propertyAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 0.3) {
			self.transform = CGAffineTransform(translationX: translation, y: 0)
		}

		propertyAnimator.addAnimations({
			self.transform = CGAffineTransform(translationX: 0, y: 0)
		}, delayFactor: 0.2)

		propertyAnimator.startAnimation()
	}
}

extension String {
	mutating func replace(_ originalString: String, with newString: String) {
		self = self.replacingOccurrences(of: originalString, with: newString)
	}
}

//
//Show Guest User Login
//
extension UIStoryboard {
	func instantiateVC<T: UIViewController>() -> T? {
		// get a class name and demangle for classes in Swift
		if let name = NSStringFromClass(T.self).components(separatedBy: ".").last {
			return instantiateViewController(withIdentifier: name) as? T
		}
		return nil
	}
}

//
//Return Formatted Currency Value and Code
//
extension String {
	func formattedCurrencyValue(code: String, value: Double) -> String {
		let currencyFormatter = NumberFormatter()
		currencyFormatter.usesGroupingSeparator = true
		currencyFormatter.numberStyle = .currency
		currencyFormatter.currencySymbol = code
		if let priceString = currencyFormatter.string(from: NSNumber(value: value)) {
			return priceString
		}
		return String()
	}
}

extension String {
	// formatting text for currency textField
	func currencyInputFormatting(currencySymbol: String) -> String {
		var number: NSNumber!
		let formatter = NumberFormatter()
		formatter.numberStyle = .currencyAccounting
		formatter.currencySymbol = currencySymbol
		formatter.maximumFractionDigits = 2
		formatter.minimumFractionDigits = 2

		var amountWithPrefix = self

		// remove from String: "$", ".", ","
		let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
		amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")

		let double = (amountWithPrefix as NSString).doubleValue
		number = NSNumber(value: (double / 100))

		// if first number is 0 or all numbers were deleted
		guard number != 0 as NSNumber else {
			return ""
		}
		return formatter.string(from: number)!
	}
}

///Getting currency code from country code
extension Locale {
    static let currency: [String: (code: String?, symbol: String?)] = Locale.isoRegionCodes.reduce(into: [:]) {
        let locale = Locale(identifier: Locale.identifier(fromComponents: [NSLocale.Key.countryCode.rawValue: $1]))
        $0[$1] = (locale.currencyCode, locale.currencySymbol)
    }
}

//
//Detect if collection view index is out of range and skip crash
//
extension Collection where Indices.Iterator.Element == Index {
	subscript (exist index: Index) -> Iterator.Element? {
		return indices.contains(index) ? self[index] : nil
	}
}

// MARK: Set image rounded corners
extension UIImageView {
	func makeRounded() {
		let radius = self.frame.width/2.0
		self.layer.cornerRadius = radius
		self.layer.masksToBounds = true
	}
}

// MARK: Tableview and Collectionview background Message
extension UITableView {
	func setEmptyMessage(_ message: String) {
		let messageLabel = UILabel(frame: CGRect(x: 0, y: 0,
																						 width: self.bounds.size.width,
																						 height: self.bounds.size.height))
		messageLabel.text = message
		messageLabel.textColor = .lightGray
		messageLabel.numberOfLines = 0
		messageLabel.lineBreakMode = .byWordWrapping
		messageLabel.textAlignment = .center
		messageLabel.font = UIFont(name: "Avenir-Heavy", size: 18)
		messageLabel.sizeToFit()
		self.backgroundView = messageLabel
		self.separatorStyle = .none
	}
	func restore() {
		self.backgroundView = nil
		self.separatorStyle = .singleLine
	}
}

extension UICollectionView {
	func setEmptyMessage(_ message: String) {
		let messageLabel = UILabel(frame: CGRect(x: 0, y: 0,
																						 width: self.bounds.size.width,
																						 height: self.bounds.size.height))
		messageLabel.text = message
		messageLabel.textColor = .lightGray
		messageLabel.numberOfLines = 0
		messageLabel.lineBreakMode = .byWordWrapping
		messageLabel.textAlignment = .center
		messageLabel.font = UIFont(name: "Avenir-Heavy", size: 18)
		messageLabel.sizeToFit()
		self.backgroundView = messageLabel
	}
	func restore() {
		self.backgroundView = nil
	}
}

extension Date {
	var millisecondsSince1970: Int64 {
		return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
	}

	init(milliseconds: Int64) {
		self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
	}
}

// MARK: Scroll table to very top of view safe gaurd area
extension UIScrollView {
	func scrollToTop(_ animated: Bool) {
		var topContentOffset: CGPoint
		if #available(iOS 11.0, *) {
			topContentOffset = CGPoint(x: -safeAreaInsets.left, y: -safeAreaInsets.top)
		} else {
			topContentOffset = CGPoint(x: -contentInset.left, y: -contentInset.top)
		}
		setContentOffset(topContentOffset, animated: animated)
	}
}

// MARK: Capitalize first letter only
extension String {
	func capitalized() -> String {
		return prefix(1).uppercased() + self.lowercased().dropFirst()
	}

	mutating func capitalizeFirstLetter() {
		self = self.capitalized()
	}
}

// MARK: Attributed text with bold effect
extension NSMutableAttributedString {
	@discardableResult func bold(_ text: String) -> NSMutableAttributedString {
		let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: "Avenir-Black", size: 16)!]
		let boldString = NSMutableAttributedString(string: text, attributes: attrs)
		append(boldString)
		return self
	}

	@discardableResult func normal(_ text: String) -> NSMutableAttributedString {
		let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: "Avenir-Heavy", size: 13)!]
		let normal = NSAttributedString(string: text, attributes: attrs)
		append(normal)
		return self
	}
}

extension UIViewController {
	func getDateString() -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
		let stringDate = dateFormatter.string(from: Date())
		return stringDate
	}

	func getDate(stringDate: String) -> Date? {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
		let date = dateFormatter.date(from: stringDate)
		return date
	}
}

//MARK: Compress images before uploads
extension UIViewController {
	func compressImage(image: UIImage) -> Data? {
		// Reducing file size to a 10th
		var actualHeight: CGFloat = image.size.height
		var actualWidth: CGFloat = image.size.width
		let maxHeight: CGFloat = 1136.0
		let maxWidth: CGFloat = 640.0
		var imgRatio: CGFloat = actualWidth/actualHeight
		let maxRatio: CGFloat = maxWidth/maxHeight
		var compressionQuality: CGFloat = 0.5

		if actualHeight > maxHeight || actualWidth > maxWidth {
			if imgRatio < maxRatio {
				//adjust width according to maxHeight
				imgRatio = maxHeight / actualHeight
				actualWidth = imgRatio * actualWidth
				actualHeight = maxHeight
			} else if imgRatio > maxRatio {
				//adjust height according to maxWidth
				imgRatio = maxWidth / actualWidth
				actualHeight = imgRatio * actualHeight
				actualWidth = maxWidth
			} else {
				actualHeight = maxHeight
				actualWidth = maxWidth
				compressionQuality = 1
			}
		}
		let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
		UIGraphicsBeginImageContext(rect.size)
		image.draw(in: rect)
		guard let img = UIGraphicsGetImageFromCurrentImageContext() else {
			return nil
		}
		UIGraphicsEndImageContext()
		guard let imageData = img.jpegData(compressionQuality: compressionQuality) else {
			return nil
		}
		return imageData
	}
}

extension UIView {
	func setUpMargins() {
		self.layer.backgroundColor = UIColor.clear.cgColor
		self.layer.shadowColor = UIColor.lightGray.cgColor
		self.layer.shadowOffset = CGSize(width: 0, height: 0)
		self.layer.shadowRadius = 0.0
		self.layer.cornerRadius = 10
		self.layer.shadowOpacity = 0.0
		self.layer.masksToBounds = true
		//cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
	}
}

extension UIDevice {

	static let modelName: String = {
		var systemInfo = utsname()
		uname(&systemInfo)
		let machineMirror = Mirror(reflecting: systemInfo.machine)
		let identifier = machineMirror.children.reduce("") { identifier, element in
			guard let value = element.value as? Int8, value != 0 else { return identifier }
			return identifier + String(UnicodeScalar(UInt8(value)))
		}

		func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
			#if os(iOS)
			switch identifier {
				case "iPod5,1":                                 										return "iPod touch (5th generation)"
				case "iPod7,1":                                 										return "iPod touch (6th generation)"
				case "iPod9,1":                                 										return "iPod touch (7th generation)"
				case "iPhone3,1", "iPhone3,2", "iPhone3,3":     		return "iPhone 4"
				case "iPhone4,1":                               									return "iPhone 4s"
				case "iPhone5,1", "iPhone5,2":                  						return "iPhone 5"
				case "iPhone5,3", "iPhone5,4":                  					return "iPhone 5c"
				case "iPhone6,1", "iPhone6,2":                  						return "iPhone 5s"
				case "iPhone7,2":                               									return "iPhone 6"
				case "iPhone7,1":                               									return "iPhone 6 Plus"
				case "iPhone8,1":                               									return "iPhone 6s"
				case "iPhone8,2":                               									return "iPhone 6s Plus"
				case "iPhone9,1", "iPhone9,3":                  						return "iPhone 7"
				case "iPhone9,2", "iPhone9,4":                  					return "iPhone 7 Plus"
				case "iPhone8,4":                               									return "iPhone SE"
				case "iPhone10,1", "iPhone10,4":                					return "iPhone 8"
				case "iPhone10,2", "iPhone10,5":                					return "iPhone 8 Plus"
				case "iPhone10,3", "iPhone10,6":                					return "iPhone X"
				case "iPhone11,2":                              									return "iPhone XS"
				case "iPhone11,4", "iPhone11,6":                					return "iPhone XS Max"
				case "iPhone11,8":                              									return "iPhone XR"
				case "iPhone12,1":                              									return "iPhone 11"
				case "iPhone12,3":                              								return "iPhone 11 Pro"
				case "iPhone12,5":                              								return "iPhone 11 Pro Max"
				case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":		return "iPad 2"
				case "iPad3,1", "iPad3,2", "iPad3,3":           					return "iPad (3rd generation)"
				case "iPad3,4", "iPad3,5", "iPad3,6":           				return "iPad (4th generation)"
				case "iPad6,11", "iPad6,12":                    							return "iPad (5th generation)"
				case "iPad7,5", "iPad7,6":                      							return "iPad (6th generation)"
				case "iPad7,11", "iPad7,12":                    							return "iPad (7th generation)"
				case "iPad4,1", "iPad4,2", "iPad4,3":           					return "iPad Air"
				case "iPad5,3", "iPad5,4":                      							return "iPad Air 2"
				case "iPad11,4", "iPad11,5":                    							return "iPad Air (3rd generation)"
				case "iPad2,5", "iPad2,6", "iPad2,7":           				return "iPad mini"
				case "iPad4,4", "iPad4,5", "iPad4,6":          		 			return "iPad mini 2"
				case "iPad4,7", "iPad4,8", "iPad4,9":           				return "iPad mini 3"
				case "iPad5,1", "iPad5,2":                      							return "iPad mini 4"
				case "iPad11,1", "iPad11,2":                    							return "iPad mini (5th generation)"
				case "iPad6,3", "iPad6,4":                    						 		return "iPad Pro (9.7-inch)"
				case "iPad6,7", "iPad6,8":                      							return "iPad Pro (12.9-inch)"
				case "iPad7,1", "iPad7,2":                     								return "iPad Pro (12.9-inch) (2nd generation)"
				case "iPad7,3", "iPad7,4":                      							return "iPad Pro (10.5-inch)"

				case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4": 		return "iPad Pro (11-inch)"
				case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8": 	return "iPad Pro (12.9-inch) (3rd generation)"

				case "AppleTV5,3":                              								return "Apple TV"
				case "AppleTV6,2":                              								return "Apple TV 4K"
				case "AudioAccessory1,1":                       						return "HomePod"
				case "i386", "x86_64":                         						  return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
				default:                                        											return identifier
			}
			#elseif os(tvOS)
			switch identifier {
				case "AppleTV5,3": return "Apple TV 4"
				case "AppleTV6,2": return "Apple TV 4K"
				case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
				default: return identifier
			}
			#endif
		}
		return mapToDevice(identifier: identifier)
	}()
}

extension UIApplication {
	static var appVersion: String {
		if let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") {
			return "\(appVersion)"
		} else {
			return ""
		}
	}

	static var build: String {
		if let buildVersion = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) {
			return "\(buildVersion)"
		} else {
			return ""
		}
	}

	static var versionBuildNumber: String {
		let version = UIApplication.appVersion
		let build = UIApplication.build
		var versionAndBuild = "v\(version)"
		if version != build {
			versionAndBuild = "Dropisle v\(version) (\(build))"
		}
		return versionAndBuild
	}
}

extension UIViewController {
	@objc func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
		let touchPoint = sender.location(in: view?.window)
		var initialTouchPoint = CGPoint.zero

		switch sender.state {
			case .began:
				initialTouchPoint = touchPoint
			case .changed:
				if touchPoint.y > initialTouchPoint.y {
					view.frame.origin.y = touchPoint.y - initialTouchPoint.y
			}
			case .ended, .cancelled:
				if touchPoint.y - initialTouchPoint.y > 200 {
					dismiss(animated: true, completion: nil)
				} else {
					UIView.animate(withDuration: 0.2, animations: {
						self.view.frame = CGRect(x: 0,
																		 y: 0,
																		 width: self.view.frame.size.width,
																		 height: self.view.frame.size.height)
					})
			}
			case .failed, .possible:
				break
			@unknown default:
				break
		}
	}
}

//MARK: Version 2

extension UITextField {
	func setLeftPaddingPoints(_ amount: CGFloat) {
		let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
		self.leftView = paddingView
		self.leftViewMode = .always
	}
}

// MARK: UIColor mode color for orders
extension UIColor {
	static func orderStatusModeColor() -> UIColor {
		if #available(iOS 13, *) {
			return UIColor.init { (trait) -> UIColor in
				// the color can be from your own color config struct as well.
				return trait.userInterfaceStyle == .dark ? .black : .white
			}
		}
		else { return UIColor.clear }
	}

	static func modeColor() -> UIColor {
		if #available(iOS 13, *) {
			return UIColor.init { (trait) -> UIColor in
				// the color can be from your own color config struct as well.
				return trait.userInterfaceStyle == .dark ? .white : .black
			}
		}
		else { return UIColor.clear }
	}

	static func switchColor() -> UIColor {
		if #available(iOS 13, *) {
			return UIColor.init { (trait) -> UIColor in
				// the color can be from your own color config struct as well.
				return trait.userInterfaceStyle == .dark ? .black : UIColor(hexString: "#DFDFDF")
			}
		}
		else { return UIColor.clear }
	}

	static func stepperColor() -> UIColor {
		if #available(iOS 13, *) {
			return UIColor.init { (trait) -> UIColor in
				// the color can be from your own color config struct as well.
				return trait.userInterfaceStyle == .dark ? .darkGray : .white
			}
		}
		else { return UIColor.clear }
	}

	static func imageColor() -> UIColor {
		if #available(iOS 13, *) {
			return UIColor.init { (trait) -> UIColor in
				return trait.userInterfaceStyle == .dark ? .white : .white
			}
		}
		else { return UIColor.clear }
	}

	static func tabBarTintColor() -> UIColor {
		if #available(iOS 13, *) {
			return UIColor.init { (trait) -> UIColor in
				return trait.userInterfaceStyle == .dark ? UIColor.secondarySystemGroupedBackground : UIColor.white
			}
		} else { return UIColor.clear }
	}

	static func tabBarItemColor() -> UIColor {
		if #available(iOS 13, *) {
			return UIColor.init { (trait) -> UIColor in
				return trait.userInterfaceStyle == .dark ? UIColor.white : UIColor.black
			}
		} else { return UIColor.clear }
	}

	//cgColor dark and light mode
	static func dynamic(light: UIColor, dark: UIColor) -> UIColor {
		if #available(iOS 13.0, *) {
			return UIColor(dynamicProvider: {
				switch $0.userInterfaceStyle {
					case .dark:
						return dark
					case .light, .unspecified:
						return light
					@unknown default:
						assertionFailure("Unknown userInterfaceStyle: \($0.userInterfaceStyle)")
						return light
				}
			})
		}
		// iOS 12 and earlier
		return light
	}
}

@IBDesignable extension UIView {

	@IBInspectable var borderWidth: CGFloat {
		set {
			layer.borderWidth = newValue
		}
		get {
			return layer.borderWidth
		}
	}

	@IBInspectable var cornerRadius: CGFloat {
		set {
			layer.cornerRadius = newValue
		}
		get {
			return layer.cornerRadius
		}
	}

	@IBInspectable var borderColor: UIColor? {
		set {
			guard let uiColor = newValue else { return }
			layer.borderColor = uiColor.cgColor
		}
		get {
			guard let color = layer.borderColor else { return nil }
			return UIColor(cgColor: color)
		}
	}

	@IBInspectable var shadowColor: UIColor? {
		set {
			guard let uiColor = newValue else { return }
			layer.shadowColor = uiColor.cgColor
		}
		get{
			guard let color = layer.shadowColor else { return nil }
			return UIColor(cgColor: color)
		}
	}

	@IBInspectable var shadowOpacity: Float{
		set {
			layer.shadowOpacity = newValue
		}
		get{
			return layer.shadowOpacity
		}
	}

	@IBInspectable var shadowOffset: CGSize{
		set {
			layer.shadowOffset = newValue
		}
		get{
			return layer.shadowOffset
		}
	}

	@IBInspectable var shadowRadius: CGFloat{
		set {
			layer.shadowRadius = newValue
		}
		get{
			return layer.shadowRadius
		}
	}
}

@IBDesignable extension UIStackView {
	@IBInspectable var customBackgroundColor: UIColor? {
		get {
			return backgroundColor
		}
		set{
			backgroundColor = newValue
			let subview = UIView(frame: bounds)
			subview.backgroundColor = newValue
			subview.autoresizingMask = [.flexibleWidth, .flexibleHeight]
			insertSubview(subview, at: 0)
		}
	}
}

@IBDesignable class PaddingLabel: UILabel {
	@IBInspectable var topInset: CGFloat = 5.0
	@IBInspectable var bottomInset: CGFloat = -5.0
	@IBInspectable var leftInset: CGFloat = 16.0
	@IBInspectable var rightInset: CGFloat = 16.0

	override func drawText(in rect: CGRect) {
		let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
		super.drawText(in: rect.inset(by: insets))
	}

	override var intrinsicContentSize: CGSize {
		let size = super.intrinsicContentSize
		return CGSize(width: size.width + leftInset + rightInset,  height: size.height + topInset + bottomInset)
	}
}

extension UIView {
	func pinEdges(to other: UIView) {
		NSLayoutConstraint.activate([
		leadingAnchor.constraint(equalTo: other.leadingAnchor),
		trailingAnchor.constraint(equalTo: other.trailingAnchor),
		topAnchor.constraint(equalTo: other.topAnchor)
		])
	}
}

extension UIView {
	func removeConstraints() { removeConstraints(constraints) }
	func deactivateAllConstraints() { NSLayoutConstraint.deactivate(getAllConstraints()) }
	func getAllSubviews() -> [UIView] { return UIView.getAllSubviews(view: self) }
	func getAllConstraints() -> [NSLayoutConstraint] {
		var subviewsConstraints = getAllSubviews().flatMap { $0.constraints }
		if let superview = self.superview {
			subviewsConstraints += superview.constraints.compactMap { (constraint) -> NSLayoutConstraint? in
				if let view = constraint.firstItem as? UIView, view == self { return constraint }
				return nil
			}
		}
		return subviewsConstraints + constraints
	}
	class func getAllSubviews(view: UIView) -> [UIView] {
		return view.subviews.flatMap { [$0] + getAllSubviews(view: $0) }
	}
}

extension UIView {
	func makeTopCorner(withRadius radius: CGFloat) {
		self.layer.cornerRadius = radius
		self.layer.masksToBounds = true
		self.layer.isOpaque = false
		self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
	}
}

extension UIView {
	func makeCorner(withRadius radius: CGFloat) {
		self.layer.cornerRadius = radius
		self.layer.masksToBounds = true
		self.layer.isOpaque = false
		self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMaxXMaxYCorner]
	}
}

extension UILabel {
	func textHeight(withWidth width: CGFloat, fontSize: Int) -> CGFloat {
		guard let text = text else {
			return 0
		}
		return text.height(withWidth: width, font: UIFont(name: "AppleSDGothicNeo-Regular", size: CGFloat(fontSize))!)
	}
}

extension String {
	func height(withWidth width: CGFloat, font: UIFont) -> CGFloat {
		let maxSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
		let actualSize = self.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], attributes: [.font : font], context: nil)
		return actualSize.height
	}
}

extension UIViewController {
	var topBarHeight: CGFloat {
		var top = self.navigationController?.navigationBar.frame.height ?? 0.0
		if #available(iOS 13.0, *) {
			top += UIApplication.shared.windows.first?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
		} else {
			top += UIApplication.shared.statusBarFrame.height
		}
		return top
	}
}

//Favoured Button Draw
@IBDesignable class HeartButton: UIButton {
	@IBInspectable var filled: Bool = true
	@IBInspectable var strokeWidth: CGFloat = 3
	@IBInspectable var strokeColor: UIColor?
	@IBInspectable var fillColor: UIColor?

	override func draw(_ rect: CGRect) {
		let bezierPath = UIBezierPath(heartIn: self.bounds)

		if self.strokeColor != nil {
			self.strokeColor!.setStroke()
		} else {
			self.tintColor.setStroke()
		}

		bezierPath.lineWidth = self.strokeWidth
		bezierPath.stroke()

		if self.filled {
			self.fillColor?.setFill()
			bezierPath.fill()
		}
	}
}

extension Int {
	var degreesToRadians: CGFloat { return CGFloat(self) * .pi / 180 }
}

extension UIBezierPath {
	convenience init(heartIn rect: CGRect) {
		self.init()

		//Calculate Radius of Arcs using Pythagoras
		let sideOne = rect.width * 0.4
		let sideTwo = rect.height * 0.3
		let arcRadius = sqrt(sideOne*sideOne + sideTwo*sideTwo)/2

		//Left Hand Curve
		self.addArc(withCenter: CGPoint(x: rect.width * 0.3, y: rect.height * 0.35), radius: arcRadius, startAngle: 135.degreesToRadians, endAngle: 315.degreesToRadians, clockwise: true)

		//Top Centre Dip
		self.addLine(to: CGPoint(x: rect.width/2, y: rect.height * 0.2))

		//Right Hand Curve
		self.addArc(withCenter: CGPoint(x: rect.width * 0.7, y: rect.height * 0.35), radius: arcRadius, startAngle: 225.degreesToRadians, endAngle: 45.degreesToRadians, clockwise: true)

		//Right Bottom Line
		self.addLine(to: CGPoint(x: rect.width * 0.5, y: rect.height * 0.95))

		//Left Bottom Line
		self.close()
	}
}

// MARK: UITextField text spacing formatting
extension UITextField {
	public func setText(to newText: String, preservingCursor: Bool) {
		if preservingCursor {
			let cursorPosition = offset(from: beginningOfDocument, to: selectedTextRange!.start) + newText.count - (text?.count ?? 0)
			text = newText
			if let newPosition = self.position(from: beginningOfDocument, offset: cursorPosition) {
				selectedTextRange = textRange(from: newPosition, to: newPosition)
			}
		} else {
			text = newText
		}
	}
}

extension String {
	func grouping(every groupSize: String.IndexDistance, with separator: Character) -> String {
		let cleanedUpCopy = replacingOccurrences(of: String(separator), with: "")
		return String(cleanedUpCopy.enumerated().map() {
			$0.offset % groupSize == 0 ? [separator, $0.element] : [$0.element]
		}.joined().dropFirst())
	}
}

extension UITableView {
	func isLast(for indexPath: IndexPath) -> Bool {
		let indexOfLastSection = numberOfSections > 0 ? numberOfSections - 1 : 0
		let indexOfLastRowInLastSection = numberOfRows(inSection: indexOfLastSection) - 1
		return indexPath.section == indexOfLastSection && indexPath.row == indexOfLastRowInLastSection
	}
}
