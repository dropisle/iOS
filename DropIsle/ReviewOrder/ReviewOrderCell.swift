//
//  ReviewOrderCell.swift
//  DropIsle
//
//  Created by CtanLI on 2021-01-18.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol UpdateReviewQuantityDelegate {
	func updateQuantity(cell: ReviewOrderCell, state: String)
	func deleteItemFromCart(cell: ReviewOrderCell)
}

class ReviewOrderCell: UITableViewCell, Reusable, AppConfigurable {

	let title = UILabel()
	let price = UILabel()
	let deleteItem = UIButton()
	let country = UILabel()
	let plus = UIButton()
	let qCount = UILabel()
	let minus = UIButton()
	let photo = UIImageView()
	let container = UIView()
	let divider = UIView()

	var delegate: UpdateReviewQuantityDelegate?
	var deletItem = Bool()

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}

	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		// Configure the view for the selected state
	}

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		//Initialization code
		minus.addTarget(self, action: #selector(reduceQuantity), for: .touchUpInside)
		plus.addTarget(self, action: #selector(increaseQuantity), for: .touchUpInside)
		deleteItem.addTarget(self, action: #selector(deleteCartItem), for: .touchUpInside)

		setup()
		_updateColors()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	var products: GetProductModel.product? {
		didSet {
			title.text  = products?.title
			price.text = "CAD \(products?.price ?? 0)"
		}
	}

	@objc func reduceQuantity() {
		delegate?.updateQuantity(cell: self, state: appConstants.minus)
	}

	@objc func increaseQuantity() {
		delegate?.updateQuantity(cell: self, state: appConstants.plus)
	}

	@objc func deleteCartItem() {
		deletItem = true
		delegate?.deleteItemFromCart(cell: self)
	}

	func setup() {
		title.translatesAutoresizingMaskIntoConstraints = false
		price.translatesAutoresizingMaskIntoConstraints = false
		deleteItem.translatesAutoresizingMaskIntoConstraints = false
		plus.translatesAutoresizingMaskIntoConstraints = false
		qCount.translatesAutoresizingMaskIntoConstraints = false
		minus.translatesAutoresizingMaskIntoConstraints = false
		photo.translatesAutoresizingMaskIntoConstraints = false
		country.translatesAutoresizingMaskIntoConstraints = false
		container.translatesAutoresizingMaskIntoConstraints = false
		divider.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(title)
		contentView.addSubview(price)
		contentView.addSubview(photo)
		contentView.addSubview(country)
		contentView.addSubview(container)
		contentView.addSubview(divider)
		container.addSubview(plus)
		container.addSubview(qCount)
		container.addSubview(minus)
		container.addSubview(deleteItem)

		NSLayoutConstraint.activate([
			title.topAnchor.constraint(equalTo: topAnchor, constant: 10),
			title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			title.trailingAnchor.constraint(equalTo: photo.leadingAnchor, constant: -10),

			photo.topAnchor.constraint(equalTo: topAnchor, constant: 10),
			photo.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			photo.heightAnchor.constraint(equalToConstant: 100),
			photo.widthAnchor.constraint(equalToConstant: 135),

			country.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 10),
			country.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			country.trailingAnchor.constraint(equalTo: photo.leadingAnchor, constant: -10),

			price.topAnchor.constraint(equalTo: country.bottomAnchor, constant: 10),
			price.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),

			deleteItem.leadingAnchor.constraint(equalTo: container.leadingAnchor),
			deleteItem.centerYAnchor.constraint(equalTo: container.centerYAnchor),

			plus.heightAnchor.constraint(equalToConstant: 25),
			plus.widthAnchor.constraint(equalToConstant: 25),
			plus.centerYAnchor.constraint(equalTo: container.centerYAnchor),
			plus.trailingAnchor.constraint(equalTo: container.trailingAnchor),

			qCount.heightAnchor.constraint(equalToConstant: 25),
			qCount.widthAnchor.constraint(equalToConstant: 25),
			qCount.centerYAnchor.constraint(equalTo: container.centerYAnchor),
			qCount.trailingAnchor.constraint(equalTo: plus.leadingAnchor, constant: -8),

			minus.heightAnchor.constraint(equalToConstant: 25),
			minus.widthAnchor.constraint(equalToConstant: 25),
			minus.centerYAnchor.constraint(equalTo: container.centerYAnchor),
			minus.trailingAnchor.constraint(equalTo: qCount.leadingAnchor, constant: -8),

			container.topAnchor.constraint(equalTo: photo.bottomAnchor, constant: 10),
			container.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			container.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			container.heightAnchor.constraint(equalToConstant: 30),

			divider.heightAnchor.constraint(equalToConstant: 0.3),
			divider.topAnchor.constraint(equalTo: container.bottomAnchor, constant: 10),
			divider.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			divider.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			divider.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
		])

		title.textColor = .label
		title.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16)
		title.text = "Painting splash oil by Nadi Bay mmm Painting splash oil by Nadi Bay"
		title.lineBreakMode = .byTruncatingTail
		title.numberOfLines = 2

		country.textColor = .label
		country.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		country.text = "Canada"

		price.textColor = .label
		price.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 15)
		price.text = "CAD $300"

		photo.image = UIImage(named: "nature1")
		photo.contentMode = .scaleAspectFill
		photo.clipsToBounds = true
		photo.cornerRadius = 10

		divider.backgroundColor = .lightGray

		let trashmage = UIImage(systemName: "trash", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		deleteItem.setImage(trashmage, for: .normal)

		qCount.textAlignment = .center
		qCount.textColor = .label
		qCount.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 15)

		let plusImage = UIImage(systemName: "plus", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		plus.setImage(plusImage, for: .normal)
		plus.cornerRadius = 12.5
		plus.borderWidth = 2

		let minusImage = UIImage(systemName: "minus", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .semibold, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		minus.setImage(minusImage, for: .normal)
		minus.cornerRadius = 12.5
		minus.borderWidth = 2
	}

	private func _updateColors() {
		plus.layer.borderColor = UIColor.dynamic(light: UIColor.label, dark: UIColor.label).cgColor
		minus.layer.borderColor = UIColor.dynamic(light: UIColor.label, dark: UIColor.label).cgColor
	}

	override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
		super.traitCollectionDidChange(previousTraitCollection)
		self._updateColors()
		self.setNeedsDisplay()
	}

	func configure(with app: App) {

	}
}
