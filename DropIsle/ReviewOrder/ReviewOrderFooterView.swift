//
//  ReviewOrderFooterView.swift
//  DropIsle
//
//  Created by CtanLI on 2021-01-18.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol PurchaseActionDelegate {
	func checkOutAction()
}

class ReviewOrderFooterView: UITableViewHeaderFooterView, Reusable {
	
	let shippingDetails = UILabel()
	let confirmPriceLabel = UILabel()
	let shippingLabel = UILabel()
	let estimatedDeliveryDateLabel = UILabel()
	
	let confirmPrice = UILabel()
	let shipping = UILabel()
	let estimatedDeliveryDate = UILabel()
	let divider = UIView()
	let checkout = UIButton()
	
	let orderSummaryLabel = UILabel()
	let orderSummary = UILabel()
	
	var delegate: PurchaseActionDelegate?
	
	override init(reuseIdentifier: String?) {
		super.init(reuseIdentifier: reuseIdentifier)
		self.contentView.backgroundColor = .secondarySystemGroupedBackground
		setUp()
		checkout.addTarget(self, action: #selector(checkOut), for: .touchUpInside)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	@objc func checkOut() {
		delegate?.checkOutAction()
	}
	
	func setUp() {
		shippingDetails.translatesAutoresizingMaskIntoConstraints = false
		shipping.translatesAutoresizingMaskIntoConstraints = false
		estimatedDeliveryDateLabel.translatesAutoresizingMaskIntoConstraints = false
		estimatedDeliveryDate.translatesAutoresizingMaskIntoConstraints = false
		shippingLabel.translatesAutoresizingMaskIntoConstraints = false
		orderSummaryLabel.translatesAutoresizingMaskIntoConstraints = false
		orderSummary.translatesAutoresizingMaskIntoConstraints = false
		checkout.translatesAutoresizingMaskIntoConstraints = false
		
		contentView.addSubview(shippingDetails)
		contentView.addSubview(shippingLabel)
		contentView.addSubview(shipping)
		contentView.addSubview(estimatedDeliveryDateLabel)
		contentView.addSubview(estimatedDeliveryDate)
		contentView.addSubview(orderSummaryLabel)
		contentView.addSubview(orderSummary)
		contentView.addSubview(checkout)
		
		NSLayoutConstraint.activate([
			shippingDetails.topAnchor.constraint(equalTo: topAnchor, constant: 5),
			shippingDetails.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			shippingDetails.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			
			shippingLabel.topAnchor.constraint(equalTo: shippingDetails.bottomAnchor, constant: 10),
			shippingLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			
			shipping.centerYAnchor.constraint(equalTo: shippingLabel.centerYAnchor),
			shipping.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			
			estimatedDeliveryDateLabel.topAnchor.constraint(equalTo: shippingLabel.bottomAnchor, constant: 7),
			estimatedDeliveryDateLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			
			estimatedDeliveryDate.centerYAnchor.constraint(equalTo: estimatedDeliveryDateLabel.centerYAnchor),
			estimatedDeliveryDate.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			
			orderSummaryLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			orderSummaryLabel.topAnchor.constraint(equalTo: estimatedDeliveryDate.bottomAnchor, constant: 20),
			
			orderSummary.centerYAnchor.constraint(equalTo: orderSummaryLabel.centerYAnchor),
			orderSummary.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			
			checkout.heightAnchor.constraint(equalToConstant: 55),
			checkout.topAnchor.constraint(equalTo: orderSummaryLabel.bottomAnchor, constant: 15),
			checkout.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			checkout.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
		])
		
		divider.backgroundColor = .lightGray
		
		shippingDetails.textColor = .label
		shippingDetails.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 14)
		shippingDetails.text = "Shipping Details"
		shippingDetails.lineBreakMode = .byTruncatingTail
		shippingDetails.numberOfLines = 1
		
		shippingLabel.textColor = .label
		shippingLabel.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 12)
		shippingLabel.text = "Shipping (Standard)"
		shippingLabel.lineBreakMode = .byTruncatingTail
		shippingLabel.numberOfLines = 1
		
		shipping.textColor = .label
		shipping.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 12)
		shipping.text = "$7.15"
		shipping.lineBreakMode = .byTruncatingTail
		shipping.numberOfLines = 1
		shipping.textAlignment = .right
		
		estimatedDeliveryDateLabel.textColor = .label
		estimatedDeliveryDateLabel.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 12)
		estimatedDeliveryDateLabel.text = "Estimated Delivery"
		estimatedDeliveryDateLabel.lineBreakMode = .byTruncatingTail
		estimatedDeliveryDateLabel.numberOfLines = 1
		
		estimatedDeliveryDate.textColor = .label
		estimatedDeliveryDate.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 12)
		estimatedDeliveryDate.text = "3 - 15 days"
		estimatedDeliveryDate.lineBreakMode = .byTruncatingTail
		estimatedDeliveryDate.numberOfLines = 1
		estimatedDeliveryDate.textAlignment = .right
		
		orderSummaryLabel.textColor = .label
		orderSummaryLabel.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 14)
		orderSummaryLabel.text = "Order Summary (4 items)"
		orderSummaryLabel.lineBreakMode = .byTruncatingTail
		orderSummaryLabel.numberOfLines = 1
		
		orderSummary.textColor = .label
		orderSummary.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14)
		orderSummary.text = "CAD $560"
		orderSummary.lineBreakMode = .byTruncatingTail
		orderSummary.numberOfLines = 1
		orderSummary.textAlignment = .right
		
		checkout.cornerRadius = 7
		checkout.borderColor = .secondarySystemGroupedBackground
		checkout.borderWidth = 2
		checkout.setTitle("Place your order", for: .normal)
		checkout.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 22)
		checkout.backgroundColor = UIColor.NavyBlueDark
	}
}
