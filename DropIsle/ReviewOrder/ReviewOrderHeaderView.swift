//
//  ReviewOrderHeaderView.swift
//  DropIsle
//
//  Created by CtanLI on 2021-02-04.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol UpdateHeaderPropertiseDelegate {
	func updateAddressElements(cell: ReviewOrderHeaderView)
}

class ReviewOrderHeaderView: UITableViewHeaderFooterView, Reusable {

	let addressContainer = UIView()
	let addressText = UIButton()
	let addressValues = UILabel()

	let paymentContainer = UIView()
	let paymentCard = UIButton()
	let paymentInfo = UILabel()
	let paymentCardImage = UIImageView()

	var delegate: UpdateHeaderPropertiseDelegate?

	override init(reuseIdentifier: String?) {
		super.init(reuseIdentifier: reuseIdentifier)
		contentView.backgroundColor = .secondarySystemGroupedBackground
		addressText.addTarget(self, action: #selector(addAddressAction), for: .touchUpInside)

		paymentInfo.isHidden = true
		paymentCardImage.isHidden = true
		setUp()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	@objc func addAddressAction() {
		delegate?.updateAddressElements(cell: self)
	}

	func setUp() {
		addressContainer.translatesAutoresizingMaskIntoConstraints = false
		addressText.translatesAutoresizingMaskIntoConstraints = false
		addressValues.translatesAutoresizingMaskIntoConstraints = false
		paymentContainer.translatesAutoresizingMaskIntoConstraints = false
		paymentCard.translatesAutoresizingMaskIntoConstraints = false
		paymentInfo.translatesAutoresizingMaskIntoConstraints = false
		paymentCardImage.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(addressContainer)
		contentView.addSubview(paymentContainer)
		addressContainer.addSubview(addressText)
		addressContainer.addSubview(addressValues)
		paymentContainer.addSubview(paymentCard)
		paymentContainer.addSubview(paymentInfo)
		paymentContainer.addSubview(paymentCardImage)

		NSLayoutConstraint.activate([
			addressContainer.topAnchor.constraint(equalTo: topAnchor, constant: 25),
			addressContainer.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
			addressContainer.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),

			addressText.topAnchor.constraint(equalTo: addressContainer.topAnchor, constant: 5),
			addressText.leadingAnchor.constraint(equalTo: addressContainer.leadingAnchor, constant: 20),
			addressText.trailingAnchor.constraint(equalTo: addressContainer.trailingAnchor, constant: -20),

			addressValues.topAnchor.constraint(equalTo: addressText.bottomAnchor, constant: 5),
			addressValues.bottomAnchor.constraint(equalTo: addressContainer.bottomAnchor, constant: -5),
			addressValues.leadingAnchor.constraint(equalTo: addressContainer.leadingAnchor, constant: 20),
			addressValues.trailingAnchor.constraint(equalTo: addressContainer.trailingAnchor, constant: -20),

			paymentContainer.heightAnchor.constraint(equalToConstant: 45),
			paymentContainer.topAnchor.constraint(equalTo: addressContainer.bottomAnchor, constant: 15),
			paymentContainer.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
			paymentContainer.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),

			paymentCard.topAnchor.constraint(equalTo: paymentContainer.topAnchor, constant: 5),
			paymentCard.leadingAnchor.constraint(equalTo: paymentContainer.leadingAnchor, constant: 20),
			paymentCard.trailingAnchor.constraint(equalTo: paymentContainer.trailingAnchor, constant: -20),

			paymentCardImage.topAnchor.constraint(equalTo: paymentCard.bottomAnchor, constant: 8),
			paymentCardImage.leadingAnchor.constraint(equalTo: paymentContainer.leadingAnchor, constant: 20),

			paymentCardImage.heightAnchor.constraint(equalToConstant: 30),
			paymentCardImage.widthAnchor.constraint(equalToConstant: 55),
			paymentInfo.centerYAnchor.constraint(equalTo: paymentCardImage.centerYAnchor),
			paymentInfo.leadingAnchor.constraint(equalTo: paymentCardImage.trailingAnchor, constant: 10),
		])

		addressContainer.backgroundColor = .systemRed
		addressText.backgroundColor = .systemGreen
		addressValues.backgroundColor = .systemYellow
		paymentContainer.backgroundColor = .systemBlue
		paymentCard.backgroundColor = .systemOrange
		paymentInfo.backgroundColor = .systemTeal

		addressText.setTitle("Shipping Address", for: .normal)
		addressText.setTitleColor(.label, for: .normal)
		addressText.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 16)
		addressText.contentHorizontalAlignment = .left

		addressValues.lineBreakMode = .byClipping
		addressValues.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14)
		addressValues.numberOfLines = 0
		addressValues.baselineAdjustment = .alignCenters

		paymentCard.setTitle("Payment Method", for: .normal)
		paymentCard.setTitleColor(.label, for: .normal)
		paymentCard.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 16)
		paymentCard.contentHorizontalAlignment = .left

		paymentCardImage.image = 	#imageLiteral(resourceName: "visaIcon")

		paymentInfo.lineBreakMode = .byClipping
		paymentInfo.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14)
		paymentInfo.numberOfLines = 0
		paymentInfo.baselineAdjustment = .alignCenters
		paymentInfo.text = "ending with 2344"

	}
}
