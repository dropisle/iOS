//
//  ReviewOrderController.swift
//  DropIsle
//
//  Created by CtanLI on 2021-01-18.
//  Copyright © 2021 DropIsle, Inc. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import Stripe

class ReviewOrderController: UIViewController, UITableViewDataSource, UITableViewDelegate {

	// MARK: - Properties -
	var tableView: UITableView!
	var carts: CartModel?
	var cart = [CartModel.Items]() {
		didSet {
			DispatchQueue.main.async {
				self.tableView.reloadData()
			}
		}
	}

	var qtyValue = Int()
	var cardParams = STPPaymentMethodCardParams()
	var billingAddress = STPPaymentMethodBillingDetails()
	var shippingAddressDetails: Address?
	var paymentIntentClientSecret: String?

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		title = "Review order"
		setUpTableView()

		//
		getUsersCart()
		//pay(paymentIntentClientSecret: "")
	}

	func setUpTableView() {
		tableView = UITableView.init(frame: .zero, style: .grouped)
		tableView.dataSource = self
		tableView.delegate = self
		tableView.separatorStyle = .none
		tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.sectionHeaderHeight = .leastNormalMagnitude
		tableView.backgroundColor = .secondarySystemGroupedBackground
		view.addSubview(tableView)

		NSLayoutConstraint.activate([
			tableView.topAnchor.constraint(equalTo: view.topAnchor),
			tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])

		tableView.register(ReviewOrderCell.self, forCellReuseIdentifier: ReviewOrderCell.identifier)
		tableView.register(ReviewOrderFooterView.self, forHeaderFooterViewReuseIdentifier: ReviewOrderFooterView.identifier)
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return  cart.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: ReviewOrderCell.identifier ) as! ReviewOrderCell
		cell.selectionStyle = .none
		cell.delegate = self
		cell.qCount.text = "\(cart[indexPath.row].quantity)"
		getProductById(indexPath: indexPath, cell: cell, productId: cart[indexPath.row].productid)
		return cell
	}

	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		let lastSectionIndex = tableView.numberOfSections - 1
		let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
		if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
		}
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 180
	}

	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return .leastNormalMagnitude
	}

	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		let footerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: ReviewOrderFooterView.identifier) as! ReviewOrderFooterView
		footerView.delegate = self
		let lastSectionIndex = tableView.numberOfSections - 1
		if  section != lastSectionIndex {
			footerView.checkout.isHidden = true
			footerView.orderSummaryLabel.isHidden = true
			footerView.orderSummary.isHidden = true
		} else {
			footerView.checkout.isHidden = false
			footerView.orderSummaryLabel.isHidden = false
			footerView.orderSummary.isHidden = false
		}
		return footerView
	}

	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		let lastSectionIndex = tableView.numberOfSections - 1
		if  section == lastSectionIndex {
			return 200
		}
		return 0
	}
}

extension ReviewOrderController: PurchaseActionDelegate, UpdateReviewQuantityDelegate {

	func updateQuantity(cell: ReviewOrderCell, state: String) {
		if appConstants.minus == state {
			decreaseQuantity(cell: cell)
		}

		if appConstants.plus == state {
			increaseQuantity(cell: cell)
		}
	}

	func increaseQuantity(cell: ReviewOrderCell) {
		let items = ["productid": cell.products?.productid ?? "", "quantity": 1] as [String : Any]
		let params = ["cartid": carts?.carts.first?.cartid ?? "", "items": [items]] as [String : Any]
		APIManager.shared.requestCallWithHeaders(method: .post, path: urlPath.addItemToCart.rawValue, params: params) { [self] (_ result: Result<CartUpdateModel, Error>) in
			switch result {
				case .success(let data):
					let _ = data.cart.items.map { value in
						guard let indexPath = tableView.indexPath(for: cell) else { return }
						let cell = tableView.cellForRow(at: indexPath) as? CartCell
						if  cell?.qCount.text == "2" {
							cell?.minus.isEnabled = true
							cell?.minus.layer.borderColor = UIColor.dynamic(light: UIColor.label, dark: UIColor.label).cgColor
						}
						cell?.qCount.text = "\(data.cart.items[indexPath.row].quantity)"
					}
				case .failure(let error):
					print(error)
			}
		}
	}

	func decreaseQuantity(cell: ReviewOrderCell) {
		let items = ["productid": cell.products?.productid ?? "", "quantity": 1] as [String : Any]
		let params = ["cartid": carts?.carts.first?.cartid ?? "", "items": [items]] as [String : Any]
		APIManager.shared.requestCallWithHeaders(method: .post, path: urlPath.deleteItemInCart.rawValue, params: params) { [self] (_ result: Result<CartUpdateModel, Error>) in
			switch result {
				case .success(let data):
					let _ = data.cart.items.map { value in
						guard let indexPath = tableView.indexPath(for: cell) else { return }
						let cell = tableView.cellForRow(at: indexPath) as? CartCell
						if  cell?.qCount.text == "1" {
							cell?.minus.isEnabled = false
							cell?.minus.layer.borderColor = UIColor.dynamic(light: UIColor.lightGray, dark: UIColor.lightGray).cgColor
						}
						cell?.qCount.text = "\(data.cart.items[indexPath.row].quantity)"
						print("\(value.quantity)")
					}
				case .failure(let error):
					print(error)
			}
		}
	}

	func deleteItemFromCart(cell: ReviewOrderCell) {
		if let value = Int(cell.qCount.text ?? "0") {
			qtyValue = value
		}
		let items = ["productid": cell.products?.productid ?? "", "quantity": qtyValue] as [String : Any]
		let params = ["cartid": carts?.carts.first?.cartid ?? "", "items": [items]] as [String : Any]
		APIManager.shared.requestCallWithHeaders(method: .post, path: urlPath.deleteItemInCart.rawValue, params: params) { [self] (_ result: Result<CartUpdateModel, Error>) in
			switch result {
				case .success(_):
					guard let indexPath = tableView.indexPath(for: cell) else { return }
					let _ = tableView.cellForRow(at: indexPath) as? CartCell
					cart.remove(at: indexPath.row)
					tableView.performBatchUpdates ({
						tableView.deleteRows(at: [indexPath], with: .fade)
					}) { (_) in
						//tableView.reloadSections(IndexSet(integer: indexPath.section), with: .fade)
					}
				case .failure(let error):
					print(error)
			}
		}
	}

	func getUsersCart() {
		APIManager.shared.requestCallWithUrlParams(method: .get, path: urlPath.getCart.rawValue, params: [:]) { [self] (_ result: Result<CartModel, Error>) in
			switch result {
				case .success(let data):

					//UPDATE THIS FOR THE RIGHT CART TO PULL
					carts = data
					let _ = data.carts.map { value in
						cart.append(contentsOf: value.items)
					}
					tableView.reloadData()
				case .failure(let error):
					print(error)
			}
		}
	}

	func getProductById(indexPath: IndexPath, cell: ReviewOrderCell, productId: String) {
		let  params = [ "productid": productId]
		APIManager.shared.requestCallWithUrlParams(method: .get, path: urlPath.getProduct.rawValue, params: params) { [self] (_ result: Result<GetProductModel, Error>) in
			switch result {
				case .success(let data):
					let _ = data.products.map { value in
						cell.products = value
					}
					//
					downloadProductImage(indexPath: indexPath, cell: cell, productId: productId)
				case .failure(let error):
					print(error)
			}
		}
	}

	func downloadProductImage(indexPath: IndexPath, cell: ReviewOrderCell, productId: String) {
		let params = [ "productid": productId]
		APIManager.shared.requestCallWithUrlParams(method: .get, path: urlPath.getProductImage.rawValue, params: params as Parameters) {  (_ result: Result<[GetProductImageModel], Error>) in
			let _ = result.map { imageArr in
				weak var _ = self
				cell.photo.sd_imageTransition = SDWebImageTransition.fade
				if let image = imageArr.first?.url {
					cell.photo.sd_setImage(with: URL(string: image)) { (image, error, cache, url) in }
				}
			}
		}
	}

	func checkOutAction() {
		//
		checkOutCart()
	}
}

extension ReviewOrderController: STPAuthenticationContext {

	func checkOutCart() {
		let fname = shippingAddressDetails?.firstName ?? ""
		let lname = shippingAddressDetails?.lastName ?? ""
		let shippingEmail = shippingAddressDetails?.email ?? ""
		let shippingCity = shippingAddressDetails?.city ?? ""
		let shippingProvince = shippingAddressDetails?.province ?? ""
		let shippingPostalCode = shippingAddressDetails?.postalCode ?? ""
		let shippingCountry = shippingAddressDetails?.country ?? ""
		let subAddress = "\(shippingCity) " + "\(shippingProvince), " + "\(shippingPostalCode)"
		let shippingAddress = "\(shippingAddressDetails?.address ?? "") \n\(subAddress) \n\(shippingCountry)"
		let shippingInfo = ["name": fname + " " + lname, "address": shippingAddress, "email": shippingEmail]

		let name = billingAddress.name ?? ""
		let billingEmail = billingAddress.email ?? ""
		let billingAdd = billingAddress.address?.line1 ?? ""
		let billingCity = billingAddress.address?.city ?? ""
		let billingPostalCode = billingAddress.address?.postalCode ?? ""
		let billingProvinceState = billingAddress.address?.state ?? ""
		let billingCountry = billingAddress.address?.country ?? ""
		let subBillingAddress = "\(billingCity) " + "\(billingProvinceState), " + "\(billingPostalCode)"
		let billingAddress = "\(billingAdd) \n\(subBillingAddress) \n\(billingCountry)"
		let billingInfo = ["name": name, "address": billingAddress, "email": billingEmail]


		let params = ["cartid": carts?.carts.first?.cartid ?? "", "shippingInfo": shippingInfo, "billingInfo":  billingInfo] as [String : Any]
		APIManager.shared.requestCallWithHeaders(method: .post, path: urlPath.checkout.rawValue, params: params) { (_ result: Result<CheckoutModel, Error>) in
			switch result {
				case .success(let data):
					print(data)
					let _ = data.carts.map { value in
						if value.isCheckedOut == true {
							//
							self.getPaymentIntent()
						}
					}
				case .failure(let error):
					print(error)
			}
		}
	}

	func getPaymentIntent() {
		let params = ["cartid": carts?.carts.first?.cartid ?? ""] as [String : Any]
		APIManager.shared.requestCallWithHeaders(method: .post, path: urlPath.paymentintent.rawValue, params: params) { [self] (_ result: Result<PaymentIntentModel, Error>) in
			switch result {
				case .success(let data):
					if let clientSecret = data.paymentIntent.client_secret {
						pay(paymentIntentClientSecret: clientSecret)
					}
				case .failure(let error):
					print(error)
			}
		}
	}

	//
	//
	//
	//
	@objc func pay(paymentIntentClientSecret: String) {
		// Collect card details
		let paymentMethodParams = STPPaymentMethodParams(card: cardParams, billingDetails: billingAddress, metadata: nil)
		let paymentIntentParams = STPPaymentIntentParams(clientSecret: "pi_1IdNPqLoD6WUdDINTE1vaZVq_secret_CAoYMTpInR5sdJCLj9IFSCPuV")
		paymentIntentParams.paymentMethodParams = paymentMethodParams
		// Submit the payment
		let paymentHandler = STPPaymentHandler.shared()
		paymentHandler.confirmPayment(paymentIntentParams, with: self) { (status, paymentIntent, error) in
			switch (status) {
				case .failed:
					self.displayAlert(title: "Payment failed", message: error?.localizedDescription ?? "")
					break
				case .canceled:
					self.displayAlert(title: "Payment canceled", message: error?.localizedDescription ?? "")
					break
				case .succeeded:
					self.displayAlert(title: "Payment succeeded", message: paymentIntent?.description ?? "")
					break
				@unknown default:
					fatalError()
					break
			}
		}
	}

	func authenticationPresentingViewController() -> UIViewController {
		return self
	}

	func displayAlert(title: String, message: String, restartDemo: Bool = false) {
		DispatchQueue.main.async {
			let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
			alert.addAction(UIAlertAction(title: "OK", style: .cancel))
			self.present(alert, animated: true, completion: nil)
		}
	}
}
