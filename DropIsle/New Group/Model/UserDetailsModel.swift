//
//  UserDetailsModel.swift
//  DropIsle
//
//  Created by CtanLI on 2019-10-17.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

struct UserDetailsModel: Decodable {
	let userid: String
	let photo: String
	let firstname: String
	let lastname: String
	let email: String?
	let phone: String?
	let gender: String
	let status: Int?
	let created: String
	let country: country
}

struct country: Decodable {
	let region: String
	let country: String
	let countrycode: String
}
