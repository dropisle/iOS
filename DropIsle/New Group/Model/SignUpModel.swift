//
//  SignUpModel.swift
//  DropIsle
//
//  Created by CtanLI on 2019-01-25.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

struct SignUp: Decodable {
	let code: Int?
	let message: String?
	let userid: String?
}
