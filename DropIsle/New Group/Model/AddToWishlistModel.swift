//
//  AddToWishlistModel.swift
//  DropIsle
//
//  Created by CtanLI on 2019-09-23.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

struct AddToWishlistModel: Decodable {
		let code: Int?
		let message: String?
		let status: String?
}
