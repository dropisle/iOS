//
//  SignInModel.swift
//  DropIsle
//
//  Created by CtanLI on 2019-01-25.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

struct SignIn: Decodable {
	let accesstoken: String?
	let refreshtoken: String?
	let userid: String?
	let fcmtoken: String?
	let code: Int?
	let message: String?
}
