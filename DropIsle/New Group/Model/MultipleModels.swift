//
//  RatingModel.swift
//  DropIsle
//
//  Created by CtanLI on 2019-09-20.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

struct RatingModel: Decodable {
	let code: Int
	let message: String
}

struct RemoveItemModel: Decodable {
	let code: Int
	let message: String
}

struct UpdateTokenModel: Decodable {
	let code: Int?
	let message: String?
	let status: String?
}

struct TermsOfService: Decodable {
	let conditions: String
}

struct EditProductModel: Decodable {
	let code: Int
	let message: String
}

struct ReportProductAndMessage: Decodable {
	let code: Int
	let message: String
	let status: String
}

struct BlockUserModel: Decodable {
	let code: Int
	let message: String
	let status: String
}

struct UpdateProfileImageModel: Decodable {
	let code: Int
	let message: String
	let status: String
}

struct UpdateUserModel: Decodable {
	let code: Int
	let message: String
}

struct ChangePasswordModel: Decodable {
	let code: Int
	let message: String
	let status: String?
}

struct DeleteMessage: Decodable {
	let code: Int?
	let message: String?
	let status: String?
}

struct DeactivateAccount: Decodable {
	let code: Int
	let message: String
	let status: String
}

struct TokenRefresh: Decodable {
	let code: Int
	let accesstoken: String
}

struct PhoneSMSVerificationModel: Decodable {
	let sid: String
	let service_sid: String
	let to: String
	let channel: String
	let status: String
	let valid: Bool
	let date_created: String
	let date_updated: String
	let amount: String?
	let payee: String?
	let url: String?
	let lookup: lookup
}

struct lookup: Decodable {
	let carrier: carrier
}

struct carrier: Decodable {
	let error_code: String?
	let name: String
	let mobile_country_code: String
	let mobile_network_code: String
	let type: String
}

struct FAQ: Decodable {
	let title: String
	let description: String
}
