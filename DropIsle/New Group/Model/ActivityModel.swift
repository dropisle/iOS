//
//  SearchModel.swift
//  DropIsle
//
//  Created by CtanLI on 2019-09-21.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

struct ActivityModel: Decodable {
	let id: String
	let description: String
	let type: String
	let logtime: String
	let image: String
}
