//
//  ProductDetailsModel.swift
//  DropIsle
//
//  Created by CtanLI on 2019-08-19.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

struct ProductDetailsModel: Decodable {

	//
	// Product Object Model
	//
		let condition: String
		let images: [String]
		let price: price
		let location: location?
		let name: String
		let rating: String
		let description: String
		let attributes: [attributes]?
		let id: String
		let status: String
		let photo: photo
		let category: String
		let quantity: Int
		let user: user
		let like: String
		let watch: String
		let block: String
		let comments: [String]?

	//
	// main image Model
	//
	struct photo: Decodable {
		let url: String
		let width: String
		let height: String
	}

	//
	// price Category Model
	//
	struct price: Decodable {
		let price: Double
		let currency: String
		let negotiable: String
	}

	//
	// target Country Model
	//
	struct location: Decodable {
		let country: String?
		let city: String?
		let countrycode: String?
		let region: String?
	}

	struct attributes: Decodable {
		let value: String
		let key: String
	}

	struct user: Decodable {
		let firstname: String?
		let userid: String?
		let lastname: String?
		let photo: String?
	}
}
