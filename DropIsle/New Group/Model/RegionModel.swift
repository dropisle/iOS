//
//  RegionModel.swift
//  DropIsle
//
//  Created by CtanLI on 2020-05-06.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import Foundation

struct RegionModel: Decodable {
	let name: String
	let state: String
	let lng: Double
	let lat: Double
	let country: String
	let population: Int
}
