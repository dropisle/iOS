//
//  BlockedUserListModel.swift
//  DropIsle
//
//  Created by CtanLI on 2019-12-13.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

struct BlockedUserListModel: Decodable {

	let content: [content]
	let pageable: pageable
	let totalPages: Int
	let totalElements: Int
	let last: Bool
	let first: Bool
	let sort: sort
	let numberOfElements: Int
	let size: Int
	let number: Int

	struct pageable: Decodable {
		let sort: sort
		let pageSize: Int
		let pageNumber: Int
		let offset: Int
		let unpaged: Bool
		let paged: Bool
	}

	struct sort: Decodable {
		let sorted: Bool
		let unsorted: Bool
	}

	struct content: Decodable {
		let name: String
		let userid: String
	}
}
