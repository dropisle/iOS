//
//  Gender.swift
//  DropIsle
//
//  Created by CtanLI on 2019-07-26.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

struct Gender: Decodable {
	let name: String?
	let gender: String?
	let probability: Double?
	let count: Int?
}
