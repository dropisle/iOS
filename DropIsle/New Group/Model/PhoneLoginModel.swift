//
//  PhoneVerification.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-11.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import Foundation

struct PhoneLoginModel: Decodable {
	let code: Int
	let message: String
}

struct ForgotPasswordModel: Decodable {
	let code: Int
	let message: String
}
