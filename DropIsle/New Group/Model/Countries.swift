//
//  Countries.swift
//  DropIsle
//
//  Created by CtanLI on 2019-07-08.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

struct Countries: Decodable {
	let name: String
	let code: String
	let regions: [String]
}
