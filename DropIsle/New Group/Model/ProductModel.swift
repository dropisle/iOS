//
//  Category.swift
//  DropIsle
//
//  Created by CtanLI on 2019-03-02.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

struct ProductModel: Decodable {

	// Product Object Model
	let content: [content]
	let pageable: pageable
	let totalPages: Int
	let totalElements: Int
	let last: Bool
	let first: Bool
	let sort: sort
	let numberOfElements: Int
	let size: Int
	let number: Int

	struct pageable: Decodable {
		let sort: sort
		let pageSize: Int
		let pageNumber: Int
		let offset: Int
		let unpaged: Bool
		let paged: Bool
	}

	struct sort: Decodable {
		let sorted: Bool
		let unsorted: Bool
	}

	struct content: Decodable {
		let condition: String
		let images: [String]
		let price: price
		let location: location?
		let name: String
		let description: String
		let attributes: [attributes]?
		let id: String
		let user: user
		let photo: photo
		let category: String
		let quantity: Int
		let status: String
		let like: String?
		let watch: String?
		let block: String?

		let rating: String?
		let comments: [String]?
		let distance: Double?
		///let rating: rating?
	}

	//
	// main image Model
	//
	struct photo: Decodable {
		let url: String
		let width: String
		let height: String
	}

	//
	// price Category Model
	//
	struct price: Decodable {
		let price: Double
		let currency: String
		let negotiable: String
	}

	//
	// target Country Model
	//
	struct location: Decodable {
		let country: String?
		let city: String?
		let countrycode: String?
		let region: String?
	}

	struct attributes: Decodable {
		let value: String
		let key: String
	}

	struct user: Decodable {
		let firstname: String?
		let userid: String?
		let lastname: String?
		let photo: String?
	}

	struct rating: Decodable {
		let id: String
		let name: String
		let rating: String
		let comments: [String]?
	}
}
