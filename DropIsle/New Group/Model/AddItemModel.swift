//
//  AddItemModel.swift
//  DropIsle
//
//  Created by CtanLI on 2019-07-27.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

struct AddItemModel: Decodable {
	let code: Int?
	let message: String?
}
