//
//  EditProfileViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-16.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import YPImagePicker
import IQKeyboardManagerSwift
import KeychainAccess
import DropDown
import SVProgressHUD
import CoreLocation
import SDWebImage

class EditProfileViewController: UIViewController, UITextFieldDelegate, UIViewControllerTransitioningDelegate, ModalHandler, CitiesDelegate, RegionDelegate {

	var config = YPImagePickerConfiguration()
	var imageArrayElements: [UIImage]? = [UIImage]()
	var imageArrayToUpload: [Data]? = [Data]()
	var accepted = false
	var genderText: String?

	var dropDown = DropDown()
	var appearance = DropDown.appearance()

	private var keychain = Keychain()
	private var locationManager = CLLocationManager()
	private let location = LocationMonitor.SharedManager

	var city: String?
	var countryCode: String?
	var countryText: String?
	var region: String?
	var cities: Countries?

	private var countriesController = "CountriesController"
	private var citiesController = "CitiesController"
	private var regionController = "RegionController"

	@IBOutlet weak var openPhotoButton: CircularButton!
	@IBOutlet weak var userImageView: CircularImageView!
	@IBOutlet weak var userFullName: UITextField!
	@IBOutlet weak var regionValue: UILabelEx!
	@IBOutlet weak var phoneNumber: UITextField!
	@IBOutlet weak var email: UITextField!
	@IBOutlet weak var genderMale: UIButton! {
		didSet {
			genderMale.setTitle("◉", for: .normal)
			genderText = "Male"
			accepted = false
		}
	}

	@IBOutlet weak var genderFemale: UIButton! {
		didSet {
			genderFemale.setTitle("◎", for: .normal)
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		title = "Edit Profile"
		tabBarController?.tabBar.isHidden = true
		navigationController?.navigationBar.titleTextAttributes =
			[NSAttributedString.Key.foregroundColor: UIColor.white,
			 NSAttributedString.Key.font: UIFont(name: "Avenir-Black", size: 20)!]
		view.endEditing(true)

		///get user profile by id
		getUserDetails(userId: CurrentUserInfo.shared.userId())

		///Keyboard applied to all screens
		IQKeyboardManager.shared.enable = true

		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton

		userImageView.layer.borderWidth = 5
		userImageView.layer.borderColor = UIColor.white.cgColor

		let origImage = UIImage(named: "Add")
		let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
		openPhotoButton.setImage(tintedImage, for: .normal)
		openPhotoButton.tintColor = .white

		let tap = UITapGestureRecognizer(target: self, action: #selector(selectOrUpdateLocation))
		regionValue.isUserInteractionEnabled = true
		regionValue.addGestureRecognizer(tap)

		let profileImgTap = UITapGestureRecognizer(target: self, action: #selector(showPicker))
		userImageView.isUserInteractionEnabled = true
		userImageView.addGestureRecognizer(profileImgTap)

		///Drop down
		dropDown.bottomOffset = CGPoint(x: 0, y: 40)
		dropDown.cellNib = UINib(nibName: "CountryDropDownCell", bundle: nil)

		///Set up image picker
		setUpImagePicker()

		if let tabBarController = self.tabBarController as? TabBarViewController {
		}
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		///Analytics
		LoggerManager.shared.log(event: .editProfile)
	}

	deinit {
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "userUpdated"), object: nil)
	}

	func setUpImagePicker() {
		config.isScrollToChangeModesEnabled = true
		config.onlySquareImagesFromCamera = true
		config.usesFrontCamera = true
		config.showsPhotoFilters = true
		config.shouldSaveNewPicturesToAlbum = true
		config.albumName = "DefaultYPImagePickerAlbumName"
		config.startOnScreen = .library
		config.screens = [.library, .photo]
		config.showsCrop = .none
		config.targetImageSize = YPImageSize.original
		config.overlayView = UIView()
		config.hidesStatusBar = true
		config.hidesBottomBar = false
		config.preferredStatusBarStyle = UIStatusBarStyle.default
		config.bottomMenuItemSelectedTextColour = UIColor(red: 0.20, green: 0.58, blue: 0.99, alpha: 1.0)
		config.bottomMenuItemUnSelectedTextColour = .lightGray
		//config.filters = [DefaultYPFilters...]

		//Library
		config.library.options = nil
		config.library.onlySquare = false
		config.library.minWidthForItem = nil
		config.library.mediaType = YPlibraryMediaType.photo
		config.library.maxNumberOfItems = 1
		config.library.minNumberOfItems = 1
		config.library.numberOfItemsInRow = 4
		config.library.spacingBetweenItems = 1.0
		config.library.skipSelectionsGallery = false
	}

	@objc func dismissScreen(sender: AnyObject) {
		_ = navigationController?.popViewController(animated: true)
	}

	@IBAction func changeImageAction(_ sender: UIButton) {
		self.showPicker()
	}

	@IBAction func genderMaleAction(_ sender: UIButton) {
		if accepted {
			sender.setTitle("◉", for: .normal)
			self.genderFemale.setTitle("◎", for: .normal)
			sender.titleLabel?.font = UIFont(name: "Avenir", size: 30)
			genderText = "Male"
			accepted = false
		}
	}

	@IBAction func genderFemaleAction(_ sender: UIButton) {
		if !accepted {
			sender.setTitle("◉", for: .normal)
			self.genderMale.setTitle("◎", for: .normal)
			sender.titleLabel?.font = UIFont(name: "Avenir", size: 30)
			genderText = "Female"
			accepted = true
		}
	}

	@IBAction func makeChanges(_ sender: UIButton) {
		updateUserDetails()
	}

	@objc func showPicker() {
		let picker = YPImagePicker(configuration: config)
		picker.didFinishPicking { [unowned picker] items, _ in
			for item in items {
				switch item {
					case .photo(let photo):
						print(photo.image)
						let image = photo.image
						self.userImageView.image = image
						self.imageArrayElements?.append(image)
						self.getParameters()
					default: break
				}
			}
			picker.dismiss(animated: true, completion: nil)
		}
		if #available(iOS 13.0, *) {
			if UIDevice.current.userInterfaceIdiom == .pad {
				picker.modalPresentationStyle = .overFullScreen
			} else {
				picker.modalPresentationStyle = .automatic
			}
		} else {
			// Fallback on earlier versions
			picker.modalPresentationStyle = .overFullScreen
		}
		picker.modalTransitionStyle = .coverVertical
		picker.transitioningDelegate = self
		present(picker, animated: true, completion: nil)
	}

	func getParameters() {
		let myGroup = DispatchGroup()
		myGroup.enter()
		DispatchQueue.global(qos: .background).async {
			for image in self.imageArrayElements! {
				let data = self.compressImage(image: image)
				self.imageArrayToUpload?.append(data!)
			}
			myGroup.leave()
		}

		myGroup.notify(queue: .global()) {
			self.updateUserProfileImage()
			self.imageArrayToUpload?.removeAll()
		}
	}

	func updateUserDetails() {
		var jsonObj = [String: Any]()
		let userId = CurrentUserInfo.shared.userId()
		let name = userFullName.text ?? ""
		let locationObj: [String: String] = ["countrycode": countryCode ?? "", "country": countryText ?? "", "region": city ?? ""]
		let param: [String: Any] = ["userid": userId, "name": name, "gender": genderText ?? "", "location": locationObj]
		if let theJSONData = try?  JSONSerialization.data(withJSONObject: param, options: .prettyPrinted) {
			if let jsonObject = try? JSONSerialization.jsonObject(with: theJSONData, options: []) as? [String: Any] {
				jsonObj = jsonObject
			}
		}
		_ = APIEndPoints.init()
		let updateUserUrl = BaseAPIClient.urlFromPath(path: APIEndPoints.updateUser)
		APIManager.shared.fetchGenericData(method: .post,
																			 urlString: updateUserUrl,
																			 params: jsonObj as [String : AnyObject],
																			 headers: APIEndPoints.authorizationHeader) { (result: UpdateUserModel) in
																				if result.code == 200 {
																					self.alert(message: result.message)

																					///Post a notification
																					NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userUpdated"), object: nil, userInfo: nil)
																					self.keychain["countryCode"] = self.countryCode
																					self.keychain["city"] = self.city
																					self.keychain["country"] = self.countryText
																					self.keychain["region"] = self.region
																					self.keychain["userName"] = name

																					///Get lat/lng from user selected location
																					self.getLatLng(address: self.regionValue.text ?? "")
																				} else {
																					SVProgressHUD.showError(withStatus: "Oops! an error occurred updating user info, try again later")
																				}
		}
	}

	func updateUserProfileImage() {
		let params = ["userid": CurrentUserInfo.shared.userId()]
		let imageKey = "photo"
		_ = APIEndPoints.init()
		let addUserImageUrl = BaseAPIClient.urlFromPath(path: APIEndPoints.addUserImage)
		APIManager.shared.uploadProductToServer(method: .post,
																						urlString: addUserImageUrl,
																						mainImageKey: imageKey,
																						mainData: imageArrayToUpload?.first,
																						imageArrayKey: nil,
																						imageArrData: nil,
																						params: params as [String: AnyObject],
																						headers: APIEndPoints.authorizationHeader) { (result: UpdateProfileImageModel) in
																							if result.code == 200 {
																								self.alert(message: result.message)
																								// Post a notification
																								NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userUpdated"), object: nil, userInfo: nil)
																							} else {
																								SVProgressHUD.showError(withStatus: "Oops! an error occurred updating user image, try again later")
																							}
		}
	}
}

extension EditProfileViewController {
	func getUserDetails(userId: String) {
		ProfileService.sharedInstance.getUserDetails( userId: userId, completion: {[weak self] (response) in
			guard let self = self else { return }
			if response.status == 500 {
				SVProgressHUD.showError(withStatus: "Oops! an error occurred getting user details")
			} else {
				DispatchQueue.main.async {
					///Analytics
					let parameter = ["region": "\(response.country.country.capitalized), \(response.country.region) \(response.country.countrycode)"] as [String : Any]
					LoggerManager.shared.log(event: .updatedRegion, parameters: parameter)

					response.lastname.isEmpty == true ? (self.userFullName.text = response.firstname) : (self.userFullName.text = response.firstname + " " + response.lastname)
					self.phoneNumber.text = response.phone
					self.email.text = response.email
					self.regionValue.text = "\(response.country.country.capitalized), \(response.country.region) \(response.country.countrycode)"
					self.city = response.country.region
					self.countryCode = response.country.countrycode
					self.countryText = response.country.country

					self.getProfilePhoto(photoImage: response.photo, completion: { (imageData) in
						self.userImageView?.sd_imageTransition = SDWebImageTransition.fade
						self.userImageView.image = UIImage(data: imageData)
					})
					if response.gender == "Male" {
						self.genderMale.setTitle("◉", for: .normal)
						self.genderFemale.setTitle("◎", for: .normal)
						self.accepted = false
					} else {
						self.genderFemale.setTitle("◉", for: .normal)
						self.genderMale.setTitle("◎", for: .normal)
						self.accepted = true
					}
				}
			}
		})
	}

	func getProfilePhoto(photoImage: String, completion: @escaping (Data) -> Void) {
		APIManager.shared.getUserProfileImage(photoImage: photoImage) { (imageData) in
			completion(imageData)
		}
	}
}

extension EditProfileViewController {
	func updateLocation() {
		_ = ("\(location.currentLocation?.coordinate.latitude ?? 0), \(location.currentLocation?.coordinate.longitude ?? 0)")
		_ = location.currentLocation?.coordinate
		if let currentLocation = location.currentLocation {
			updateLocation(location: currentLocation)
		}
		location.stopMonitoring()
	}

	func openCountryList() {
		self.performSegue(withIdentifier: countriesController, sender: self)
	}

	func updateLocation(location: CLLocation?) {
		if let loc = location {
			let geocoder = CLGeocoder()
			geocoder.reverseGeocodeLocation(loc) { (placemarksArray, _) in
				guard let placeMark = placemarksArray else {
					return
				}
				if (placeMark.count) > 0 {
					_ = placemarksArray?.first
				}
			}
		}
	}

	@objc func selectOrUpdateLocation(_ textField: UITextField) {
		switch CLLocationManager.authorizationStatus() {
			case .authorizedWhenInUse:
				print("authorized")
				self.performSegue(withIdentifier: countriesController, sender: self)
			case .denied:
				self.performSegue(withIdentifier: countriesController, sender: self)
			case .notDetermined:
				location.startMonitoring()
			case .restricted:
				break
			default:
				break
		}
		locationManager.requestWhenInUseAuthorization()
	}

	///get location if not enabled
	func getLatLng(address: String) {
		let geoCoder = CLGeocoder()
		geoCoder.geocodeAddressString(address) { (placemarks, error) in
			guard let placemarks = placemarks, let location = placemarks.first?.location else { return }
			preferences[.latitude] = "\(location.coordinate.latitude)"
			preferences[.longitude] = "\(location.coordinate.longitude)"
		}
	}
}

extension  EditProfileViewController {

	func modalDismissed(cities: Countries) {
		self.cities = cities
		self.performSegue(withIdentifier: self.citiesController, sender: self)
	}

	func usersCity(country: String?, city: String?, code: String?) {
		self.city = city
		self.countryCode = code
		self.countryText = country
		regionValue.text = "\(city ?? "")" + ", " + "\(country ?? "") " + " " + "\(code ?? "")" 
	}

	///setting province ot state name
	func setUserCity(city: String?, code: String?, country: String?) {
		self.region = city
		self.countryCode = code
		self.countryText = country
		self.performSegue(withIdentifier: self.regionController, sender: self)
	}

	///getting region name
	func getRegion(region: String?, code: String?, city: String?, country: String?) {
		self.city = city
		self.region = region
		self.countryCode = code
		self.countryText = country
		regionValue.text =  "\(region ?? "")" + ", " + "\(city ?? "")" + ", " + "\(country ?? "")" + " " + "\(code ?? "")" 
	}

	///In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let nav = segue.destination as? UINavigationController,
			let citiesDetails = nav.topViewController as? CitiesController {
			citiesDetails.cities = cities
			citiesDetails.delegate = self
		}

		///This is a delegate function to initialize the dismiss Modal view class and call a function
		if let nav = segue.destination as? UINavigationController,
			let countryVC = nav.topViewController as? CountriesController {
			countryVC.delegate = self
		}

		if let nav = segue.destination as? UINavigationController, let regionVC = nav.topViewController as? RegionController, let region = self.region, let countryCode = self.countryCode, let country = self.countryText {
			regionVC.provinceOrState = region
			regionVC.countryCode = countryCode
			regionVC.country = country
			regionVC.delegate = self
		}
	}
}

///Used to add pading to regionValue label
class UILabelEx: UILabel {
	var padding: UIEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
	override func drawText(in rect: CGRect) {
		super.drawText(in: rect.inset(by: padding))
	}
	override func sizeThatFits(_ size: CGSize) -> CGSize {
		var adjSize = super.sizeThatFits(size)
		adjSize.width += padding.left + padding.right
		adjSize.height += padding.top + padding.bottom
		return adjSize
	}
}

class CircularImageView: UIImageView {
	override func layoutSubviews() {
		super.layoutSubviews()
		clipsToBounds = true
		layer.cornerRadius = min(frame.width, frame.height) / 2
		layer.borderWidth = 1
		layer.borderColor = UIColor(hexString: "#F0F0F0").cgColor
	}
}

class CircularButton: UIButton {
	override func layoutSubviews() {
		super.layoutSubviews()
		clipsToBounds = true
		layer.cornerRadius = min(frame.width, frame.height) / 2
	}
}
