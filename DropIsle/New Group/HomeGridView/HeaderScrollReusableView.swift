//
//  HeaderScrollReusableView.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-03.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

class HeaderScrollReusableView: UICollectionReusableView {
	@IBOutlet weak var scrollView: UIScrollView!
}
