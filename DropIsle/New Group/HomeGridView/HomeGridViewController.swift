//
//  HomeGridViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-01-25.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SVProgressHUD
import CoreLocation
import SDWebImage
import FirebaseAnalytics
import FSPagerView

class HomeGridViewController: UIViewController,
	UIViewControllerTransitioningDelegate,
	UICollectionViewDataSourcePrefetching,
	UIViewControllerPreviewingDelegate,
	UICollectionViewDelegateFlowLayout,
	AddToWishListDelegate,
	SelectedViewControllerDelegate,
	Reusable,
	ContactSellerDelegate,
	CategoryActionDelegate {

	var indexForCategory = String()
	var peekedData: ProductModel.content?
	var wishlistState = String()
	var wishlistAddedOrRemoved = false
	var isButtonSetupComplete = false
	let headerId = "headerId"
	var previousController: UIViewController?
	var scView: UIScrollView!
	let buttonPadding: CGFloat = 10
	var xOffset: CGFloat = 10
	var vStack: UIStackView?
	var hStack: UIStackView?
	var pageControl = FSPageControl()
	var emptyBackgroundView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
	var chatItemBar = BadgedButtonItem(with: UIImage(named: "Send")?.withRenderingMode(.alwaysTemplate))
	var pagerView = FSPagerView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIDevice.current.userInterfaceIdiom == .pad ? 350 : 222))
	var footerView: CustomFooterView?
	var homeHeaderView: CustomHomeHeaderView?
	let footerViewReuseIdentifier = "refreshFooterView"
	let headerViewReuseIdentifier = "headViewId"

	@IBOutlet weak var homeGridCollectionView: UICollectionView!

	var product = [ProductModel]()
	var content = [ProductModel.content]() {
		didSet {
			DispatchQueue.main.async {
				self.homeGridCollectionView.reloadData()
			}
		}
	}

	lazy var refreshControl: UIRefreshControl = {
		let refresh = UIRefreshControl()
		refresh.tintColor = .black
		refresh.addTarget(self, action: #selector(pullToRefreshTrendingProducts), for: .valueChanged)
		return refresh
	}()

	override func loadView() {
		super.loadView()
		let _ = HomeGridViewController()
		fetchTrendingProducts(page: "\(self.product.first?.pageable.pageNumber ?? 1)")
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		if traitCollection.forceTouchCapability == UIForceTouchCapability.available {
			registerForPreviewing(with: self, sourceView: homeGridCollectionView)
		}

		///setting up background view for empty collection
		setBackgroundView()

		///Setting up right bar button
		addRightBarButtonItems()

		let longTitleLabel = UILabel()
		longTitleLabel.text = "Discover Deals"
		longTitleLabel.font = UIFont(name: "Avenir-Black", size: 20)
		longTitleLabel.sizeToFit()
		longTitleLabel.textColor = UIColor.white
		let leftItem = UIBarButtonItem(customView: longTitleLabel)
		navigationItem.leftBarButtonItem = leftItem
		navigationController?.navigationBar.barTintColor = UIColor(hexString: "#3293FC")

		DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
			self.chatItemBar.badgeAnimation = true
		}
		chatItemBar.tapAction = { [weak self] in
			self?.openChat()
			DispatchQueue.main.async {
				self?.chatItemBar.setBadge(with: 0)
			}
		}

		homeGridCollectionView?.backgroundColor = UIColor(hexString: "#f6f6f6")
		if UIDevice.current.userInterfaceIdiom == .pad {
			homeGridCollectionView?.contentInset = UIEdgeInsets(top: 90, left: 10, bottom: 10, right: 10)
		} else {
			homeGridCollectionView?.contentInset = UIEdgeInsets(top: 90, left: 10, bottom: 10, right: 10)
		}
		homeGridCollectionView.showsVerticalScrollIndicator = false
		homeGridCollectionView.register(CarouselViewCell.self, forCellWithReuseIdentifier: CarouselViewCell.identifier)

		///Set refreshControl on collectionview header
		homeGridCollectionView.refreshControl = refreshControl
		refreshControl.translatesAutoresizingMaskIntoConstraints = false
		refreshControl.topAnchor.constraint(equalTo: homeGridCollectionView.topAnchor, constant: -320).isActive = true
		refreshControl.centerXAnchor.constraint(equalTo: homeGridCollectionView.centerXAnchor).isActive = true

		///Set the CustomLayout delegate
		if let layout = homeGridCollectionView?.collectionViewLayout as? CustomCollectionView {
			layout.delegate = self
		}
		homeGridCollectionView.delegate = self
		homeGridCollectionView.dataSource = self
		homeGridCollectionView.prefetchDataSource = self
		///(self.tabBarController as? TabBarViewController)?.viewControllerdelegate = self
		homeGridCollectionView.register(UINib(nibName: "CustomFooterView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerViewReuseIdentifier)
		homeGridCollectionView.register(UINib(nibName: "CustomHomeHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerViewReuseIdentifier)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.tabBarController?.tabBar.isHidden = false
		if let tabBarController = self.tabBarController as? TabBarViewController {

		}

		///Open Product Detail if the Guest User Signs up from Product Detail Page.
		if let product = Global.productOnRegister, let productDetails = Global.productIfOpenWithDynamicLink  {
			///Detail Page Viewing Action events
			LoggerManager.shared.log(event: .guestUserSignUpOrDynamicLinkClicked)

			showDetailScreen(productObj: product, productDetailObj: productDetails)
			Global.productOnRegister = nil
			Global.productIfOpenWithDynamicLink = nil
		}

		///signUp events
		LoggerManager.shared.log(event: .home)
	}

	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		checkInternetState()
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "notifyWishlistScreen"), object: nil)
	}

	@objc func fetchTrendingProducts(page: String) {
		CurrentUserInfo.shared.userId().isEmpty ? fetchTrendingProductsForGuest(page: page) : fetchTrendingProductsForRegisteredUsers(page: page)
	}

	private func fetchTrendingProductsByCategories(category: String, page: String) {
		CurrentUserInfo.shared.userId().isEmpty ? fetchProductByCategories(category: category, page: page) : fetchProductByCategoryAndUserId(category: category, page: page)
	}

	///Setting up right bar button
	func addRightBarButtonItems() 	{
		let chatItem = UIButton.init(type: .custom)
		chatItem.setImage(UIImage(named: "Send")?.withRenderingMode(.alwaysTemplate), for: .normal)
		chatItem.tintColor = UIColor.white
		chatItem.addTarget(self, action: #selector(openChat), for: .touchUpInside)

		let filterItem = UIButton.init(type: .custom)
		filterItem.setImage(UIImage(named: "Filter")?.withRenderingMode(.alwaysTemplate), for: .normal)
		filterItem.tintColor = UIColor.white
		filterItem.addTarget(self, action: #selector(filterSearch), for: .touchUpInside)

		let stackview = UIStackView.init(arrangedSubviews: [filterItem, chatItem])
		stackview.distribution = .fillEqually
		stackview.axis = .horizontal
		stackview.alignment = .center
		stackview.spacing = 20

		let rightBarButton = UIBarButtonItem(customView: stackview)
		self.navigationItem.rightBarButtonItem = rightBarButton
	}

	// MARK: Pull to refresh
	@objc func pullToRefreshTrendingProducts() {
		self.refreshControl.endRefreshing()
		content.removeAll()
		if var storedCategory = preferences[.categorySelected] {
			storedCategory.replace("&", with: "and")
			storedCategory == "Picks for you" ? fetchTrendingProducts(page: "\(1)") : fetchTrendingProductsByCategories(category: storedCategory, page: "\(1)")
		}
	}

	private func fetchProductByCategories(category: String, page: String) {
		HomeService.sharedInstance.getProductByCategory(category: category, page: page) { [weak self] (products) in
			guard let self = self else { return }
			DispatchQueue.main.async(execute: {
				self.refreshControl.endRefreshing()
				self.product.removeAll()
				let prod = products.content.filter { $0.status != APIEndPoints.soldSate }
				self.product.append(products)
				self.content.append(contentsOf: prod)
			})
		}
	}

	private func fetchProductByCategoryAndUserId(category: String, page: String) {
		HomeService.sharedInstance.getProductByCategoryAndUserId(category: category, page: page) { [weak self] (products) in
			guard let self = self else { return }
			DispatchQueue.main.async(execute: {
				self.refreshControl.endRefreshing()
				self.product.removeAll()
				let prod = products.content.filter { $0.status != APIEndPoints.soldSate }
				self.product.append(products)
				self.content.append(contentsOf: prod)
			})
		}
	}

	private func fetchTrendingProductsForGuest(page: String) {
		HomeService.sharedInstance.getProductForGuestUsers(page: page) { [weak self] (products) in
			guard let self = self else { return }
			DispatchQueue.main.async(execute: {
				self.refreshControl.endRefreshing()
				self.product.removeAll()
				let prod = products.content.filter { $0.status != APIEndPoints.soldSate }
				self.product.append(products)
				self.content.append(contentsOf: prod)
			})
		}
	}

	private func fetchTrendingProductsForRegisteredUsers(page: String) {
		HomeService.sharedInstance.getProductForRegisteredUsers(page: page) { [weak self] (products) in
			guard let self = self else { return }
			DispatchQueue.main.async(execute: {
				self.refreshControl.endRefreshing()
				self.product.removeAll()
				let prod = products.content.filter { $0.status != APIEndPoints.soldSate }
				self.product.append(products)
				self.content.append(contentsOf: prod)
			})
		}
	}

	///Open filter screen
	@objc func filterSearch() {
		///Analytics
		LoggerManager.shared.log(event: .showFilterScreen)

		let storyBoard: UIStoryboard = UIStoryboard(name: "FilterStoryboard", bundle: nil)
		if let newViewController = storyBoard.instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController {
			self.navigationController?.pushViewController(newViewController, animated: true)
		}
	}

	///Open chat screen
	@objc func openChat() {
		///Analytics
		LoggerManager.shared.log(event: .showChatScreen)

		let storyBoard: UIStoryboard = UIStoryboard(name: "Chats", bundle: nil)
		if let newViewController = storyBoard.instantiateViewController(withIdentifier: "ChatsController") as? ChatsController {
			self.navigationController?.pushViewController(newViewController, animated: true)
		}
	}

	///Open Product detail when user logs in from details page
	func showDetailScreen(productObj: ProductModel.content, productDetailObj: ProductDetailsModel?) {
		let storyBoard: UIStoryboard = UIStoryboard(name: "ProductDetailPage", bundle: nil)
		if let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProductDetailPageController") as? ProductDetailPageController {
			newViewController.productObject = productObj
			newViewController.productDetailsObject = productDetailObj
			navigationController?.pushViewController(newViewController, animated: true)
		}
	}
}

extension HomeGridViewController: UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if (self.content.count == 0) {
			DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
				guard self.content.count != 0 else {
					self.homeGridCollectionView.backgroundView = self.emptyBackgroundView
					return
				}
			}
			SVProgressHUD.show()
		} else {
			homeGridCollectionView.backgroundView = nil
		}
		SVProgressHUD.dismiss(withDelay: 1)
		return content.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeGridViewCell.identifier, for: indexPath) as? HomeGridViewCell else {
			return UICollectionViewCell()
		}
		cell.setUpMargins()
		cell.delegate = self
		if !content.isEmpty {
			cell.product = content[indexPath.row]
		}

		///Check if the product added to wishlist id's are in storage and update the button when scrolling.
		if let productId = cell.product?.id {
			if preferences[.addedToWishlist].contains(productId) {
				cell.setAddToWishList(wishListImage: UIImage(named: "addToWishListSolid"))
			}
		}
		return cell
	}

	func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
		///Begin asynchronously fetching data for the requested index paths.
		for indexPath in indexPaths {
			guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeGridViewCell.identifier, for: indexPath) as? HomeGridViewCell else { return }
			if !content.isEmpty {
				let model = content[indexPath.row]
				cell.product = model
			}
		}
	}

	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		if indexPath.row == content.count - 1 {
			self.footerView?.animateFinal()
			self.footerView?.startAnimate()
			self.footerView?.isHidden = false
			guard product.first?.numberOfElements == 10 else { return }
			if var pageNumber = self.product.first?.pageable.pageNumber {
				pageNumber += 2
				if let storedCategory = preferences[.categorySelected] {
					storedCategory == "Picks for you" ? fetchTrendingProducts(page: "\(pageNumber)") : fetchTrendingProductsByCategories(category: storedCategory, page: "\(pageNumber)")
				}
			}
		}
	}

	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		if kind == UICollectionView.elementKindSectionFooter {
			let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! CustomFooterView
			self.footerView = aFooterView
			self.footerView?.backgroundColor = UIColor.clear
			return aFooterView
		}

		if kind == UICollectionView.elementKindSectionHeader {
			let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerViewReuseIdentifier, for: indexPath) as! CustomHomeHeaderView
			headerView.delegate = self
			homeHeaderView = headerView
			homeHeaderView?.backgroundColor = UIColor.clear
			return headerView
		}
		return UICollectionReusableView()
	}

	func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
		if elementKind == UICollectionView.elementKindSectionHeader {
			self.footerView?.isHidden = true
			self.footerView?.prepareInitialAnimation()
		}
	}

	func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
		if elementKind == UICollectionView.elementKindSectionHeader {
			self.footerView?.stopAnimate()
			self.footerView?.isHidden = false
		}
	}

	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		self.footerView?.stopAnimate()
		self.footerView?.isHidden = true
	}
}

extension HomeGridViewController: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let cell = collectionView.cellForItem(at: indexPath) as? HomeGridViewCell

		///Analytics
		let parameter = ["productName": cell?.product?.name, "productCategory": cell?.product?.category]
		LoggerManager.shared.log(event: .showDetailScreen, parameters: parameter as [String : Any])

		if let product = cell?.product {
			showDetailScreen(productObj: product, productDetailObj: nil)
		}
	}
}

extension HomeGridViewController: HomeScreenLayoutDelegate {
	func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
		if UIDevice.current.userInterfaceIdiom == .pad {
			guard let height = content[exist: indexPath.item]  else { return 550 }
			guard let imageHeight = NumberFormatter().number(from: height.photo.height) else { return 0 }
			let expectedHeight = CGFloat(truncating: imageHeight)
			if (expectedHeight) > 550 {
				return 550
			} else {
				return 550
			}
		}

		guard let height = content[exist: indexPath.item]  else { return 250 }
		guard let imageHeight = NumberFormatter().number(from: height.photo.height) else { return 0 }
		let expectedHeight = CGFloat(truncating: imageHeight)
		if (expectedHeight) > 250 {
			return 250
		} else {
			return 250
		}
	}

	func collectionView(_ collectionView: UICollectionView, widthForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
		return 300
	}
}

extension HomeGridViewController {
	func addToWishList(cell: HomeGridViewCell) {
		CurrentUserInfo.shared.userId().isEmpty ? CheckIfUserIsRegistered() : AddAndRemoveFromWishList(cell: cell)
	}

	func AddAndRemoveFromWishList(cell: HomeGridViewCell) {
		///Animating button on click to verify action
		cell.addToWishListButton.bounceAnimation(value: 0.1)

		guard let indexPath = self.homeGridCollectionView.indexPath(for: cell) else {
			return
		}
		var dict = [String: String]()
		var jsonObj = [String: Any]()
		let cell = homeGridCollectionView.cellForItem(at: indexPath) as? HomeGridViewCell
		let userId = CurrentUserInfo.shared.userId()
		guard let itemCode = cell?.product?.id else { return }

		if preferences[.addedToWishlist].contains(itemCode) {
			self.wishlistState = "remove"
		}
		if !preferences[.addedToWishlist].contains(itemCode) {
			self.wishlistState = "add"
		}

		dict.updateValue(itemCode, forKey: "itemcode")
		dict.updateValue(userId, forKey: "userid")
		dict.updateValue(wishlistState, forKey: "type")
		if let theJSONData = try?  JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
			if let jsonObject = try? JSONSerialization.jsonObject(with: theJSONData, options: []) as? [String: Any] {
				jsonObj = jsonObject
			}
		}
		HomeService.sharedInstance.addAndRemoveProductFromWishlist(params: jsonObj as [String: AnyObject]) { (result) in
			if result.code == 200 {
				SVProgressHUD.dismiss(withDelay: 0.5)

				///Check if wishlist already added by user
				if result.status == "added" {
					///Analytics
					let parameter = ["userId": CurrentUserInfo.shared.userId(), "productName": cell?.product?.name ?? "", "productCategory": cell?.product?.category ?? ""] as [String : Any]
					LoggerManager.shared.log(event: .addToWishlist, parameters: parameter)

					cell?.setAddToWishList(wishListImage: UIImage(named: "addToWishListSolid"))
					///Post a notification
					NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyWishlistScreen"), object: nil, userInfo: nil)

					///Update Model but don't reload collection when wishlist button is clicked
					if let productId = cell?.product?.id {
						preferences[.addedToWishlist].append(productId)
					}
				} else {
					///Analytics
					let parameter = ["userId": CurrentUserInfo.shared.userId(), "productName": cell?.product?.name ?? "", "productCategory": cell?.product?.category ?? ""] as [String : Any]
					LoggerManager.shared.log(event: .removeFromWishlist, parameters: parameter)

					cell?.setAddToWishList(wishListImage: UIImage(named: "addToWishListEmpty"))
					if let productId = cell?.product?.id {
						let storedWishlistProductId = preferences[.addedToWishlist]
						let availableProductId = storedWishlistProductId.filter { $0 != productId }
						preferences.removeObject(forKey: "addedToWishlist")
						preferences[.addedToWishlist].append(contentsOf: availableProductId)
					}
				}
			} else {
				SVProgressHUD.dismiss(withDelay: 0.5)
			}
		}
	}
}

extension HomeGridViewController: UITabBarControllerDelegate {

	///protocol to fire tabbarController delegate and scroll collection view
	func SelectedViewController(viewController: UIViewController) {
		if previousController == viewController || previousController == nil {
			if let navigationController = viewController as? UINavigationController, let vc = navigationController.viewControllers.first as? HomeGridViewController {
				if vc.isViewLoaded && (vc.view.window != nil) {
					let visibleIndex = vc.homeGridCollectionView.indexPathsForVisibleItems
					if visibleIndex.count != 0 {
						vc.homeGridCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
					}}}}
		previousController = viewController
	}

	func HomeProductReload() {
		guard  preferences[.categorySelected] != "Picks for you" else { return }

		if let homeHeaderView = homeHeaderView {
			homeHeaderView.scView .scrollToTop(true)
			///setting first category when home is clicked
			homeHeaderView.setPicksForYouAsInitialCategory()
		}

		content.removeAll()
		preferences[.categorySelected] = "Picks for you"
		fetchTrendingProducts(page: "\(1)")
	}
}

extension HomeGridViewController {
	func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
		guard let selectedIndexPath = homeGridCollectionView.indexPathForItem(at: location) else { return UIViewController() }
		guard let cell = homeGridCollectionView.cellForItem(at: selectedIndexPath) as? HomeGridViewCell else{ return UIViewController() }
		let storyboard = UIStoryboard.init(name: "Preview", bundle: nil)
		let destVC = storyboard.instantiateViewController(withIdentifier: "PreviewController")
		destVC.preferredContentSize = CGSize.init(width: 0.0, height: 500)
		if let data = cell.product {
			peekedData = data
		}
		(destVC as? PreviewController)?.data = peekedData
		(destVC as? PreviewController)?.delegate = self
		cell.setUpMargins()
		previewingContext.sourceRect = cell.frame
		return destVC
	}

	func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
		let storyBoard: UIStoryboard = UIStoryboard(name: "ProductDetailPage", bundle: nil)
		if let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProductDetailPageController") as? ProductDetailPageController {
			newViewController.productObject = peekedData
			show(newViewController, sender: self)
		}
	}

	func conteactSeller(product: ProductDetailsModel) {
		CurrentUserInfo.shared.userId().isEmpty ? CheckIfUserIsRegistered() : showConversationViewController(productDetails: product)
	}

	func showConversationViewController(productDetails: ProductDetailsModel) {
//		let newViewController = ConversationViewController()
//		newViewController.productDetails = productDetails
//		navigationController?.pushViewController(newViewController, animated: true)
	}
}

extension HomeGridViewController {
	func setBackgroundView() {
		let stackView = UIStackView()
		emptyBackgroundView.backgroundColor = UIColor(hexString: "#f6f6f6")
		let view1 = UIImageView.imageAttribute(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
		view1.heightAnchor.constraint(equalToConstant: 100).isActive = true
		view1.widthAnchor.constraint(equalToConstant: 100).isActive = true

		let view2 = UILabel.attributedLabel(frame: CGRect(x: 0, y: 0, width: 278, height: 48))
		view2.heightAnchor.constraint(equalToConstant: 48).isActive = true
		view2.widthAnchor.constraint(equalToConstant: 278).isActive = true

		let view3 = UIButton.attributedButton(frame: CGRect(x: 0, y: 0, width: 108, height: 29))
		view3.addTarget(self, action: #selector(goToListingScreen), for: .touchDown)
		view3.heightAnchor.constraint(equalToConstant: 29).isActive = true
		view3.widthAnchor.constraint(equalToConstant: 108).isActive = true

		stackView.axis = .vertical
		stackView.distribution = .equalSpacing
		stackView.alignment = .center
		stackView.spacing = 8

		stackView.addArrangedSubview(view1)
		stackView.addArrangedSubview(view2)
		///stackView.addArrangedSubview(view3)

		stackView.translatesAutoresizingMaskIntoConstraints = false
		emptyBackgroundView.addSubview(stackView)
		stackView.topAnchor.constraint(equalTo: emptyBackgroundView.topAnchor, constant: UIDevice.current.userInterfaceIdiom == .pad ? 350 : 200).isActive = true
		stackView.centerXAnchor.constraint(equalTo: emptyBackgroundView.centerXAnchor).isActive = true
	}

	@objc func goToListingScreen() {
		if CurrentUserInfo.shared.userId().isEmpty {
			let storyboard = UIStoryboard(name: "GuestUserSignUpStoryboard", bundle: nil)
			if let pvc = storyboard.instantiateVC() as? GuestSignUpPanelViewController {
				pvc.modalPresentationStyle = UIModalPresentationStyle.custom
				pvc.modalTransitionStyle = .crossDissolve
				pvc.transitioningDelegate = self
				self.present(pvc, animated: true, completion: nil)
			}
		} else {
		}
	}
}

extension HomeGridViewController {

	///Headerview Actions
	func gotoDropcodeController() {
		let storyBoard: UIStoryboard = UIStoryboard(name: "SettingsStoryboard", bundle: nil)
		if let newViewController = storyBoard.instantiateViewController(withIdentifier: "DropcodeController") as? DropcodeController {
			self.navigationController?.pushViewController(newViewController, animated: true)
		}
	}

	func gotoListItemController() {
	}

	//MARK: Home page categories clicked
	func fetchProductCategory(sender: UIButton) {
		var category = APIEndPoints.categories[sender.tag]
		preferences[.categorySelected] = category.0
		if let homeHeaderView = homeHeaderView {
			for subview in homeHeaderView.scView.subviews {
				if let hStack = subview as? UIStackView {
					for subview in hStack.arrangedSubviews {
						if let vStack = subview as? UIStackView {
							if let item = vStack.arrangedSubviews.last as? UIButton {
								if sender.titleLabel?.text == item.titleLabel?.text {
									item.setTitleColor(.black, for: .normal)
									item.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 11)
								} else {
									item.setTitleColor(.lightGray, for: .normal)
									item.titleLabel!.font = UIFont(name: "Avenir-Medium", size: 10)
								}
							}
						}
					}
				}
			}
		}

		//MARK: Called when categories are clicked
		content.removeAll()
		category.0.replace("&", with: "and")
		indexForCategory = category.0
		sender.tag == 0 ?  fetchTrendingProducts(page: "\(1)") : fetchTrendingProductsByCategories(category: category.0, page: "\(1)")
	}


	func CheckIfUserIsRegistered() {
		///Analytics
		LoggerManager.shared.log(event: .guestUserCreateOrSignInScreen)

		let storyboard = UIStoryboard(name: "GuestUserSignUpStoryboard", bundle: nil)
		if let pvc = storyboard.instantiateVC() as? GuestSignUpPanelViewController {
			pvc.modalPresentationStyle = UIModalPresentationStyle.custom
			pvc.modalTransitionStyle = .crossDissolve
			pvc.transitioningDelegate = self
			self.present(pvc, animated: true, completion: nil)
		}
	}
}

extension HomeGridViewController {
	///Displaying cutom bottom view for internet state.
	@objc func checkInternetState() {
		if !NetworkState().isConnected {
			NetWorkStateView(state: false)

			///Show Guest User Screen
			LoggerManager.shared.log(event: .networkStateOff)
		} else {
			NetWorkStateView(state: true)
		}
	}

	func NetWorkStateView(state: Bool) {
		let frame = CGRect(x: 0, y: UIScreen.main.bounds.height - (view.safeAreaInsets.bottom + 44), width: UIScreen.main.bounds.width, height: 44)
		let button = UILabel(frame: frame)
		if let indeedTabBarController = self.tabBarController {
			button.font = UIFont(name: "Avenir-Heavy", size: 15)
			button.numberOfLines = 2
			button.lineBreakMode = .byWordWrapping
			button.textAlignment = .center
			let textString = "😥 Offline... Click to Retry"
			let range = (textString as NSString).range(of: "Click to Retry")
			let attributedString = NSMutableAttributedString(string: textString, attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
			attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(hexString: "#3293FC"), range: range)
			button.attributedText = attributedString
			let tap = UITapGestureRecognizer(target: self, action: #selector(checkInternetState))
			button.isUserInteractionEnabled = true
			button.addGestureRecognizer(tap)
			indeedTabBarController.view.addSubview(button)
			if state {
				_ = indeedTabBarController.view.subviews.filter { $0 is UILabel }.map { $0.isHidden = true }
			}
		}
	}
}
