//
//  PreviewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-12-14.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

protocol ContactSellerDelegate: class {
	func conteactSeller(product: ProductDetailsModel)
}

class PreviewController: UIViewController, UIViewControllerTransitioningDelegate {
	
	weak var delegate: ContactSellerDelegate?
	var data: ProductModel.content?
	var details: ProductDetailsModel?
	var likeState = String()

	//MARK:- IBOutlets
	@IBOutlet weak var productName: UILabel!
	@IBOutlet weak var productPrice: UILabel!
	@IBOutlet weak var imgView: UIImageView!

    override func viewDidLoad() {
			super.viewDidLoad()
			if let obj = data {
				productName.text = obj.name
				let price = "\(obj.price.price)".dropLast(2)
				productPrice.text = "\(price)".currencyInputFormatting(currencySymbol: obj.price.currency)
				imgView.sd_setImage(with: URL(string: obj.photo.url)) { (image, error, cache, url) in }
				fetchProductDetailForRegisteredUsers(productId: obj.id, userId: CurrentUserInfo.shared.userId())
			}
    }

	override var previewActionItems: [UIPreviewActionItem] {
		return previewActions()
	}

	func previewActions() -> [UIPreviewActionItem] {
		data?.like == "NO" ? (self.likeState = "like") : (self.likeState = "unlike")
		let previewAction1 = UIPreviewAction.init(title: likeState, style: .default) { [weak self] (action, controller) in
			self?.likeUserAndUnlikeUser()
		}

		let previewAction2 = UIPreviewAction.init(title: "Contact Seller", style: .default) {  [weak self] (action, controller) in
			if let details = self?.details {
				self?.delegate?.conteactSeller(product: details)
			}
		}
		return [previewAction1, previewAction2]
	}

	func likeUserAndUnlikeUser() {
		let userId = CurrentUserInfo.shared.userId()
		var dict = [String: String]()
		var params = [String: AnyObject]()
		dict.updateValue(data?.id ?? "", forKey: "itemcode")
		dict.updateValue(userId, forKey: "userid")
		dict.updateValue(likeState, forKey: "type")
		if let theJSONData = try?  JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
			if let jsonObject = try? JSONSerialization.jsonObject(with: theJSONData, options: []) as? [String: AnyObject] {
				params = jsonObject
			}
		}
		ProductDetailService.sharedInstance.likeProduct(params: params) { (response) in
			if response.code == 200 {
				SVProgressHUD.showSuccess(withStatus: "Done")
			} else {
				SVProgressHUD.showError(withStatus: "Please try again \nSomething went wrong")
			}
		}
	}

	func fetchProductDetailForRegisteredUsers(productId: String, userId: String) {
		ProductDetailService.sharedInstance.getProductByIdAndUserId(productId: productId, userId: userId) { [weak self] (result: ProductDetailsModel) in
			self?.details = result
		}
	}
}

