//
//  HomeService.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-01.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

protocol NetworkService {
	func loadFeed(page: String, completion: @escaping (ProductModel) -> Void)
}

class HomeTService {
	let networkService: NetworkService
	init(networkService: NetworkService) {
		self.networkService = networkService
	}

	func load(page: String) {
		networkService.loadFeed(page: "") { (result) in
		}
	}
}

class HomeService {
	private init() { }
	static let sharedInstance = HomeService()
	var homeService = APIManager.shared

	func getProductForGuestUsers(page: String, completion: @escaping (ProductModel) -> Void) {
		guard let lat = preferences[.latitude], let lng = preferences[.longitude] else { return }
		let urlString = String(format: APIEndPoints.getProductForGuestUsers, lat, lng, page)
		let getProductUrl = BaseAPIClient.urlFromPath(path: urlString)
		homeService.fetchGenericData(method: .get, urlString: getProductUrl, params: nil, headers: nil) { (response: ProductModel) in
			completion(response)
		}
	}

	func getProductForRegisteredUsers(page: String, completion: @escaping (ProductModel) -> Void) {
		guard let lat = preferences[.latitude], let lng = preferences[.longitude] else { return }
		let urlString = String(format: APIEndPoints.getProductForRegisteredUsers, lat, lng, CurrentUserInfo.shared.userId(), page)
		let getProductUrl = BaseAPIClient.urlFromPath(path: urlString)
		homeService.fetchGenericData(method: .get, urlString: getProductUrl, params: nil, headers: nil) { (response: ProductModel) in
			completion(response)
		}
	}

	func getProductByCategory(category: String, page: String, completion: @escaping (ProductModel) -> Void) {
		guard let lat = preferences[.latitude], let lng = preferences[.longitude] else { return }
		let urlString = String(format: APIEndPoints.getProductByCategory, category, lat, lng, page)
		let getProductUrl = BaseAPIClient.urlFromPath(path: urlString)
		homeService.fetchGenericData(method: .get, urlString: getProductUrl, params: nil, headers: nil) { (response: ProductModel) in
			completion(response)
		}
	}

	func getProductByCategoryAndUserId(category: String, page: String, completion: @escaping (ProductModel) -> Void) {
		guard let lat = preferences[.latitude], let lng = preferences[.longitude] else { return }
		let urlString = String(format: APIEndPoints.getProductByCategoryAndUserId, category, lat, lng, CurrentUserInfo.shared.userId(), page)
		let getProductUrl = BaseAPIClient.urlFromPath(path: urlString)
		homeService.fetchGenericData(method: .get, urlString: getProductUrl, params: nil, headers: nil) { (response: ProductModel) in
			completion(response)
		}
	}

	func addAndRemoveProductFromWishlist(params: [String: AnyObject], completion: @escaping (AddToWishlistModel) -> Void) {
		let urlString = String(format: APIEndPoints.addProductToWishlit)
		let addProductToWishlistUrl = BaseAPIClient.urlFromPath(path: urlString)
		_ = APIEndPoints.init()
		homeService.fetchGenericData(method: .post,
																 urlString: addProductToWishlistUrl, params:
		params, headers: APIEndPoints.authorizationHeader) { (response: AddToWishlistModel) in
			if response.code == 200 {
				completion(response)
			} else {
				completion(response)
			}
		}
	}
}

