//
//  HomeGridCustomLayout.swift
//  DropIsle
//
//  Created by CtanLI on 2019-01-25.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation
import UIKit

protocol HomeScreenLayoutDelegate: class {
	//1. Method to ask the delegate for the height of the image
	func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat
	func collectionView(_ collectionView: UICollectionView, widthForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat
}

class CustomCollectionView: UICollectionViewLayout {
	//1. Pinterest Layout Delegate
	weak var delegate: HomeScreenLayoutDelegate!

	//2. Configurable properties
	fileprivate var numberOfColumns = 2
	fileprivate var cellPadding: CGFloat = 6

	//3. Array to keep a cache of attributes.
	fileprivate var cache = [UICollectionViewLayoutAttributes]()

	//4. Content height and size
	fileprivate var contentHeight: CGFloat = 0

	fileprivate var contentWidth: CGFloat {
		guard let collectionView = collectionView else {
			return 0
		}
		let insets = collectionView.contentInset
		return collectionView.bounds.width - (insets.left + insets.right)
	}

	override var collectionViewContentSize: CGSize {
		return CGSize(width: contentWidth, height: contentHeight)
	}

	override func prepare() {
		cache.removeAll()
		// 1. Only calculate once
		guard cache.isEmpty == true, let collectionView = collectionView else {
			return
		}

		//Reset content height when reloading collectionview
		contentHeight = 0

		// 2. Pre-Calculates the X Offset for every column and adds an array to increment the currently max Y Offset for each column
		let columnWidth = contentWidth / CGFloat(numberOfColumns)
		var xOffset = [CGFloat]()
		for column in 0 ..< numberOfColumns {
			xOffset.append(CGFloat(column) * columnWidth)
		}

		var column = 0
		var yOffset = [CGFloat](repeating: 0, count: numberOfColumns)

		///Set header
		if collectionView.tag == 0 {
			// Add Attributes for section footer
			let indexPath = IndexPath(item: 0, section: 0)
			let headerAtrributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, with: indexPath)
			let _ = UIDevice.modelName
			if UIDevice.current.userInterfaceIdiom == .pad {
				headerAtrributes.frame = CGRect(x: 0, y: -80, width: columnWidth * 2, height: 80)
			}
			if UIDevice.current.userInterfaceIdiom == .phone {
				headerAtrributes.frame = CGRect(x: 0, y: -80, width: columnWidth * 2, height: 80)
			}
			cache.append(headerAtrributes)
		}

			// 3. Iterates through the list of items in the first section
			for item in 0 ..< collectionView.numberOfItems(inSection: 0) {
				let indexPath = IndexPath(item: item, section: 0)
				// 4. Asks the delegate for the height of the picture and the annotation and calculates the cell frame.
				let photoHeight = delegate.collectionView(collectionView, heightForPhotoAtIndexPath: indexPath)
				let photoWidth = delegate.collectionView(collectionView, widthForPhotoAtIndexPath: indexPath)
				_ = photoHeight / photoWidth

				let height = cellPadding * 2 + photoHeight
				let frame = CGRect(x: xOffset[column], y: yOffset[column], width: columnWidth, height: height)
				let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)

				// 5. Creates an UICollectionViewLayoutItem with the frame and add it to the cache
				let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
				attributes.frame = insetFrame
				cache.append(attributes)

				if item + 1 == collectionView.numberOfItems(inSection: 0)  {
					// Add Attributes for section footer
					let headerAtrributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, with: indexPath)
					let _ = UIDevice.modelName
					if UIDevice.current.userInterfaceIdiom == .pad {
						headerAtrributes.frame = CGRect(x: 0, y: -80, width: columnWidth * 2, height: 80)
					}
					if UIDevice.current.userInterfaceIdiom == .phone {
						headerAtrributes.frame = CGRect(x: 0, y: -80, width: columnWidth * 2, height: 80)
					}
					//cache.append(headerAtrributes)
				}

				if item + 1 == collectionView.numberOfItems(inSection: 0)  {
					// Add Attributes for section footer
					let footerAtrributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, with: indexPath)
					let currentBounds = self.collectionView?.bounds
					let Offset = currentBounds!.origin.y + currentBounds!.size.height - attributes.size.height/5
					footerAtrributes.frame = CGRect(x: 0, y: Offset, width: columnWidth * 2, height: 40)
 					cache.append(footerAtrributes)
				}

				// 6. Updates the collection view content height
				contentHeight = max(contentHeight, frame.maxY)
				yOffset[column] = yOffset[column] + height
				column = column < (numberOfColumns - 1) ? (column + 1) : 0
		}
	}

	override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
		var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
		// Loop through the cache and look for items in the rect
		for attributes in cache {
			if attributes.frame.intersects(rect) {
				visibleLayoutAttributes.append(attributes)
			}
		}
		return visibleLayoutAttributes
	}

	override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
		return true
	}

	override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
		return cache[indexPath.item]
	}
}
