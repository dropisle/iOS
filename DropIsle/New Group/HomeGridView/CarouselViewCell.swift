//
//  CarouselViewCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-01-31.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

class CarouselViewCell: UICollectionViewCell, Reusable, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

		let collectionView1: UICollectionView = {
			let layout = UICollectionViewFlowLayout()
			layout.minimumLineSpacing = 8
			layout.scrollDirection = .horizontal

			let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
			cv.backgroundColor = .blue
			return cv
		}()

		let cellId = "cellId"
		override init(frame: CGRect) {
			super.init(frame: frame)
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
				self.setup()
			})
		}

		required init?(coder aDecoder: NSCoder) {
			fatalError("init(coder:) has not been implemented")
		}

		func setup() {
			addSubview(collectionView1)
			collectionView1.setAnchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
			collectionView1.backgroundColor = UIColor.yellow

			collectionView1.delegate = self
			collectionView1.dataSource = self

			collectionView1.register(IconsCell.self, forCellWithReuseIdentifier: cellId)
			collectionView1.showsHorizontalScrollIndicator = false
		}

		func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
			return 10
		}

		func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! IconsCell
			return cell
		}

		func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

		}

//		func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//			return CGSize(width: frame.width/2, height: 50)
//		}
//	
//		func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//			return UIEdgeInsets(top: 25, left: 14, bottom: 25, right: 14)
//		}

		private class IconsCell: UICollectionViewCell {
			@IBOutlet weak var name: UILabel!

			var textView: UITextView = {
				let tv = UITextView()
				tv.font = UIFont(name: "Helvetica", size: 15)
				tv.font = UIFont.boldSystemFont(ofSize: 15)
				tv.textAlignment = .center
				tv.layer.cornerRadius = 10.0
				return tv
			}()

			override init(frame: CGRect) {
				super.init(frame: frame)
				setupIconCell()
			}

			func setupIconCell() {
				addSubview(textView)
				textView.setAnchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 2, paddingLeft: 5, paddingBottom: 5, paddingRight: 5)
			}

			override func awakeFromNib() {
				super.awakeFromNib()
			}

			required init?(coder aDecoder: NSCoder) {
				fatalError("init(coder:) has not been implemented")
			}
	}
}
