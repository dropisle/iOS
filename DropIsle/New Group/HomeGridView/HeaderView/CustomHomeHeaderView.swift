//
//  CustomHeaderView.swift
//  DropIsle
//
//  Created by CtanLI on 2020-03-22.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit
import FSPagerView

protocol CategoryActionDelegate: class {
	func fetchProductCategory(sender: UIButton)
	func gotoDropcodeController()
	func gotoListItemController()
}

class CustomHomeHeaderView: UICollectionReusableView {

	var vStack: UIStackView?
	var hStack: UIStackView?
	var scView: UIScrollView!
	let buttonPadding: CGFloat = 10
	var xOffset: CGFloat = 10
	var pageControl = FSPageControl()
	var pagerView = FSPagerView()
	weak var delegate: CategoryActionDelegate?

	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var categoryContainer: UIView!
	@IBOutlet weak var bannerView: UIView!
	@IBOutlet weak var bannerContainerView: UIView!
	@IBOutlet weak var pageController: UIView!

	override func awakeFromNib() {
		super.awakeFromNib()
		let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 80)
		scView = UIScrollView(frame: frame)
		scView.backgroundColor = UIColor(hexString: "#f6f6f6")
		scView.translatesAutoresizingMaskIntoConstraints = false
		categoryContainer.addSubview(scView)
		bannerContainerView.backgroundColor = UIColor(hexString: "#f6f6f6")

		scView.showsHorizontalScrollIndicator = false
		scView?.leftAnchor.constraint(equalTo: categoryContainer.leftAnchor, constant: 0).isActive = true
		scView?.topAnchor.constraint(equalTo: categoryContainer.topAnchor, constant: 0).isActive = true
		scView?.rightAnchor.constraint(equalTo: categoryContainer.rightAnchor, constant: 0).isActive = true
		scView?.bottomAnchor.constraint(equalTo: categoryContainer.bottomAnchor, constant: 0).isActive = true

		setUpButtonArray()
		setUpImageContainer()

		///set first index to mimic selected index picks for you
		setPicksForYouAsInitialCategory()
	}

	func setUpButtonArray() {
		hStack = UIStackView()
		hStack?.translatesAutoresizingMaskIntoConstraints = false
		hStack?.axis = .horizontal
		hStack?.distribution = .fillProportionally
		hStack?.alignment = .center
		hStack?.spacing = 8

		scView.addSubview(hStack ?? UIStackView())
		hStack?.leftAnchor.constraint(equalTo: scView.leftAnchor, constant: 0).isActive = true
		hStack?.topAnchor.constraint(equalTo: scView.topAnchor, constant: 5).isActive = true
		hStack?.rightAnchor.constraint(equalTo: scView.rightAnchor, constant: -0).isActive = true
		hStack?.bottomAnchor.constraint(equalTo: scView.bottomAnchor, constant: 0).isActive = true

		if UIDevice.current.userInterfaceIdiom == .pad {
			hStack?.widthAnchor.constraint(equalTo: scView.widthAnchor, multiplier: 1.0, constant: 0).isActive = true
		}

		for (index, value) in APIEndPoints.categories.enumerated() {
			let actionWithText = UIButton()
			actionWithText.translatesAutoresizingMaskIntoConstraints = false
			actionWithText.backgroundColor = .clear
			actionWithText.setTitle(value.0, for: .normal)
			actionWithText.frame = CGRect(x: xOffset, y: CGFloat(buttonPadding), width: 0, height: 0)
			actionWithText.titleLabel!.font = UIFont(name: "Avenir-Medium", size: 10)
			actionWithText.setTitleColor(.lightGray, for: .normal)
			actionWithText.titleLabel?.lineBreakMode = .byTruncatingTail
			actionWithText.titleLabel?.numberOfLines = 2
			actionWithText.addTarget(self, action: #selector(categoryAction), for: .touchUpInside)
			actionWithText.tag = index

			let actionWithImage = UIButton()
			actionWithImage.translatesAutoresizingMaskIntoConstraints = false
			actionWithImage.layer.cornerRadius = 0
			actionWithImage.backgroundColor = .clear
			actionWithImage.frame = CGRect(x: xOffset, y: CGFloat(buttonPadding), width: 0, height: 0)
			actionWithImage.setTitleColor(.clear, for: .normal)
			actionWithImage.setImage(value.1, for: .normal)
			actionWithImage.setTitle(value.0, for: .normal)
			actionWithImage.titleEdgeInsets = UIEdgeInsets(top: 0, left: -0, bottom: 0, right: 50)
			actionWithImage.addTarget(self, action: #selector(categoryAction), for: .touchUpInside)
			actionWithImage.tag = index

			xOffset = xOffset + CGFloat(buttonPadding) + actionWithImage.frame.size.width

			vStack = UIStackView()
			vStack?.axis = .vertical
			vStack?.distribution = .fill
			vStack?.alignment = .center
			vStack?.spacing = 4

			vStack?.addArrangedSubview(actionWithImage)
			vStack?.addArrangedSubview(actionWithText)
			actionWithText.heightAnchor.constraint(equalToConstant: 30).isActive = true
			actionWithText.widthAnchor.constraint(equalToConstant: 70).isActive = true
			actionWithImage.heightAnchor.constraint(equalToConstant: 25).isActive = true
			actionWithImage.widthAnchor.constraint(equalToConstant: 25).isActive = true
			hStack?.addArrangedSubview(vStack ?? UIStackView())
		}
	}

	@objc func categoryAction(sender: UIButton) {
		delegate?.fetchProductCategory(sender: sender)
	}

	func setPicksForYouAsInitialCategory() {
		for subview in scView.subviews  {
			if let hStack = subview as? UIStackView {
				for subview in hStack.arrangedSubviews {
					if let vStack = subview as? UIStackView {
						if let item = vStack.arrangedSubviews.last as? UIButton {
							item.setTitleColor(.lightGray, for: .normal)
							item.titleLabel!.font = UIFont(name: "Avenir-Medium", size: 10)
							if item.titleLabel?.text  == "Picks for you" {
								item.setTitleColor(.black, for: .normal)
								item.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 11)
							}}}}}}
	}
}


extension CustomHomeHeaderView: FSPagerViewDataSource, FSPagerViewDelegate  {
	func setUpImageContainer() {
		///Mark pageviewer
		pagerView.dataSource = self
		pagerView.delegate = self
		pagerView.itemSize = FSPagerView.automaticSize
		pagerView.interitemSpacing = 1
		pagerView.transformer = FSPagerViewTransformer(type: .crossFading)
		pagerView.isInfinite = false
		pagerView.automaticSlidingInterval = 0
		pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")

		/// Create a page control
		pageControl.contentHorizontalAlignment = .center
		pageControl.numberOfPages = 1
		pageControl.hidesForSinglePage = true
		pageControl.backgroundColor = .clear
		pageControl.setStrokeColor(.clear, for: .normal)
		pageControl.setStrokeColor(.clear, for: .selected)
		pageControl.setFillColor(UIColor.lightGray, for: .normal)
		pageControl.setFillColor(UIColor(red: 0.20, green: 0.58, blue: 0.99, alpha: 1.0), for: .selected)

		bannerView.addSubview(pagerView)
		pagerView.translatesAutoresizingMaskIntoConstraints = false

		pagerView.leftAnchor.constraint(equalTo: bannerView.leftAnchor, constant: 0).isActive = true
		pagerView.topAnchor.constraint(equalTo: bannerView.topAnchor, constant: 0).isActive = true
		pagerView.rightAnchor.constraint(equalTo: bannerView.rightAnchor, constant: 0).isActive = true
		pagerView.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor, constant: -15).isActive = true

		pageController.addSubview(pageControl)
		pageControl.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
        pageControl.centerXAnchor.constraint(equalTo: pageController.centerXAnchor),
        pageControl.centerYAnchor.constraint(equalTo: pageController.centerYAnchor),
        pageControl.heightAnchor.constraint(equalToConstant: 10),
				pageControl.widthAnchor.constraint(equalToConstant: pageControl.frame.size.width)
    ])
	}
	
	func numberOfItems(in pagerView: FSPagerView) -> Int {
		return 1
	}

	///ADD Analytics
	func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
		let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
		let banner: [UIImage] = [UIImage(named: "i2")!]
		cell.imageView?.contentMode = .scaleAspectFill
		cell.imageView?.clipsToBounds = true
		cell.imageView?.layer.masksToBounds = true
		cell.imageView?.image = banner[index]
		return cell
	}

	func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
		pagerView.deselectItem(at: index, animated: true)
		pagerView.scrollToItem(at: index, animated: true)
		if index == 0 {
			delegate?.gotoDropcodeController()
		} 
	}

	func pagerView(_ pagerView: FSPagerView, willDisplay cell: FSPagerViewCell, forItemAt index: Int) {
		self.pageControl.currentPage = index
	}

	func pagerView(_ pagerView: FSPagerView, didEndDisplaying cell: FSPagerViewCell, forItemAt index: Int) {
		self.pageControl.currentPage = index
	}
}

