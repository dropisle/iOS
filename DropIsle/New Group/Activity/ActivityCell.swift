//
//  ActivityCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-10-13.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import SDWebImage

class ActivityCell: UITableViewCell {

	lazy var formatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateStyle = .medium
		return formatter
	}()

	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var timeStamp: UILabel!
	@IBOutlet weak var activityImage: UIImageView!

	var activity: ActivityModel? {
		didSet {
			guard let activity = activity else {
				return 
			}
			weak var weakSelf = self
			activityImage?.sd_imageTransition = SDWebImageTransition.fade
			weakSelf?.activityImage.sd_setImage(with: URL(string: activity.image),
																					placeholderImage: UIImage(named: "placeholder"),
																					options: .fromCacheOnly) { (image, error, cacheType, url) in
																						self.activityImage.image = image
			}

			var desc = activity.description
			desc.replace(":", with: " ")
			if desc.contains(find: "removed") {
				self.descriptionLabel.attributedText = checkRange(desc: desc, range: "removed", color: UIColor(hexString: "#800020"))
			} else if desc.contains(find: "added") {
				self.descriptionLabel.attributedText = checkRange(desc: desc, range: "added", color: UIColor(hexString: "#197319"))
			} else if desc.contains(find: "unliked") {
				self.descriptionLabel.attributedText = checkRange(desc: desc, range: "unliked", color: UIColor(hexString: "#800020"))
			} else if desc.contains(find: "liked") {
				self.descriptionLabel.attributedText = checkRange(desc: desc, range: "liked", color: UIColor(hexString: "#197319"))
			} else {
				self.descriptionLabel.text = activity.description
			}

			let stringormat = activity.logtime
			let dateToConvert = Int64(stringormat)
			let conver = Date(milliseconds: dateToConvert ?? 0)
			let format = formatter.string(from: conver)
			self.timeStamp.text = "\(format)"
		}
	}

	func checkRange(desc: String, range: String, color: UIColor) -> NSAttributedString {
		let range = (desc as NSString).range(of: range)
		let attributedString = NSMutableAttributedString(string: desc,
																										 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
		attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
		return attributedString
	}

	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
