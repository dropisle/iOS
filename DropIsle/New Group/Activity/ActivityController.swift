//
//  ActivityController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-10-13.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import SVProgressHUD

class ActivityController: UIViewController, UITableViewDelegate, UITableViewDataSource {

	private var emptyBackgroundView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))

	@IBOutlet weak var activityTableView: UITableView!

	var activity = [ActivityModel]() {
		didSet {
			DispatchQueue.main.async {
				self.activityTableView.reloadData()
			}
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
			navigationItem.title = "Activity"
			navigationController?.navigationBar.barTintColor = UIColor(hexString: "#3293FC")
			navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Avenir-Black", size: 20)!]

			setBackgroundView()
			activityTableView.separatorColor = .clear
			activityTableView.backgroundColor = UIColor(hexString: "#f6f6f6")

			//get user activities
			getActivities()
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Avenir-Black", size: 20)!]
	}

	private func getActivities() {
		_ = APIEndPoints.init()
		let urlString = String(format: APIEndPoints.getActivities, CurrentUserInfo.shared.userId())
		let activityUrl = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .get,
																			 urlString: activityUrl,
																			 params: nil,
																			 headers: APIEndPoints.authorizationHeader) { (result: [ActivityModel]) in
			if !result.isEmpty {
				self.activity.append(contentsOf: result)
			} else {
				print("No activities to display \nEngage by selling and buying products!")
			}
		}
	}

   	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
			if activity.count == 0 {
				DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
					guard self.activity.count != 0 else {
						self.activityTableView.backgroundView = self.emptyBackgroundView
						return
					}
				}
			SVProgressHUD.show()
			}
			SVProgressHUD.dismiss(withDelay: 0.5)
		return activity.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
			guard let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityCell") as? ActivityCell else  {
				return UITableViewCell()
			}
			cell.activity = activity[indexPath.row]
			return cell
	}
}

extension ActivityController {
	private func setBackgroundView() {
		let stackView = UIStackView()
		emptyBackgroundView.backgroundColor = UIColor.white
		let view1 = UIImageView.noActivityImage(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
		view1.heightAnchor.constraint(equalToConstant: 200).isActive = true
		view1.widthAnchor.constraint(equalToConstant: 200).isActive = true

		let view2 = UILabel.noActivityAttributeLabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 48))
		view2.heightAnchor.constraint(equalToConstant: 48).isActive = true
		view2.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true

		stackView.axis = .vertical
		stackView.distribution = .equalSpacing
		stackView.alignment = .center
		stackView.spacing = 8

		stackView.addArrangedSubview(view1)
		stackView.addArrangedSubview(view2)

		stackView.translatesAutoresizingMaskIntoConstraints = false
		emptyBackgroundView.addSubview(stackView)
		stackView.centerXAnchor.constraint(equalTo: emptyBackgroundView.centerXAnchor).isActive = true
		stackView.centerYAnchor.constraint(equalTo: emptyBackgroundView.centerYAnchor).isActive = true
	}
}
