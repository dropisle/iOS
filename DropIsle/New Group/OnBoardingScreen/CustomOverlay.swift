//
//  CustomOverlay.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-09.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit
import SwiftyOnboard

class CustomOverlay: SwiftyOnboardOverlay {

    @IBOutlet weak var contentControl: UIPageControl!

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CustomOverlay", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
