//
//  OnBoardingController.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-09.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit
import SwiftyOnboard
import KeychainAccess
import SVProgressHUD
import CoreLocation

class OnBoardingController: UIViewController, UpdateLocationDelegate {

	var isSignUpOption = false
	var guestButtonClicked = false

	static let tabBarController = "TabBarViewController"
	static let signupWithEmailController = "signupWithEmailController"
	static let signInWithEmailController = "signInWithEmailController"

	private var keychain = Keychain()
	let numberOfPages = 3
	var timer = RepeatingTimer(timeInterval: 3)
	var locationManager = CLLocationManager()
	let location = LocationMonitor.SharedManager

	@IBOutlet weak var onBoardView: UIView!
	@IBOutlet weak var guestButton: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()
		//onBoarding
		///Location manager delegate
		location.delegate = self

		/*
		onBoardView.style = .light
		onBoardView.fadePages = true
		onBoardView.delegate = self
		onBoardView.dataSource = self
		onBoardView.backgroundColor = .white

		timer.eventHandler = {
			self.autoScrollOnBoarding()
		}
		timer.resume()
		SVProgressHUD.dismiss()
		*/
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		///Analytics
		LoggerManager.shared.log(event: .onBoarding)
	}

	@objc func autoScrollOnBoarding() {
		/*
		DispatchQueue.main.async {
			let currentPage = self.onBoardView.currentPage
			if currentPage >= self.numberOfPages {
				self.onBoardView.goToPage(index: 0, animated: false)
			} else {
				self.onBoardView.goToPage(index: currentPage + 1, animated: true)
			}
		}
		*/
	}

	@IBAction func guestUser(_ sender: UIButton) {
		///Analytics
		LoggerManager.shared.log(event: .guestUser)

		///guestButtonClicked
		guestButtonClicked = true

		self.performSegue(withIdentifier: OnBoardingController.tabBarController, sender: self)

		DispatchQueue.main.async(execute: {
			self.timer.cancel()
		})
	}

	func randomString(length: Int) -> String {
		let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
		return String((0...length-1).map { _ in letters.randomElement()! })
	}

	@IBAction func createAccount(_ sender: UIButton) {
		///Analytics
		LoggerManager.shared.log(event: .createAccount)

		DispatchQueue.main.async(execute: {
			self.timer.cancel()
			self.isSignUpOption = true
			self.performSegue(withIdentifier: OnBoardingController.signupWithEmailController, sender: self)
		})
	}

	@IBAction func signInOptionScreen(_ sender: UIButton) {
		///Analytics
		LoggerManager.shared.log(event: .signInAccount)

		DispatchQueue.main.async(execute: {
			self.timer.cancel()
			self.isSignUpOption = false
			self.performSegue(withIdentifier: OnBoardingController.signInWithEmailController, sender: self)
		})
	}

	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	}
}

extension OnBoardingController: SwiftyOnboardDelegate, SwiftyOnboardDataSource {

	func swiftyOnboardNumberOfPages(_ swiftyOnboard: SwiftyOnboard) -> Int {
		return 4
	}

	func swiftyOnboardPageForIndex(_ swiftyOnboard: SwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
		let view = CustomPageView.instanceFromNib() as? CustomPageView
		if index == 0 {
			//On the first page, change the text in the labels to say the following:
			view?.imageSet.image = UIImage(named: "onBoardImage4")
			//view?.textSet.text = "A quality marketplace app \nwasn’t available for Africa, \nSo, we built it."
		} else if index == 1 {
			//On the second page, change the text in the labels to say the following:
			view?.imageSet.image = UIImage(named: "onBoardImage2")
			//view?.textSet.text = "Dropisle is a modern and simpler way for you to buy things or sell your own stuff."
		} else if index == 2 {
			//On the thrid page, change the text in the labels to say the following:
			view?.imageSet.image = UIImage(named: "onBoardImage3")
			//view?.textSet.text = "Join a marketplace connecting millions across Africa for free."
		} else {
			//On the thrid page, change the text in the labels to say the following:
			view?.imageSet.image = UIImage(named: "onBoardImage1")
			//view?.textSet.text = "Save money, live well, empower your community."
		}
		return view
	}

	func swiftyOnboardViewForOverlay(_ swiftyOnboard: SwiftyOnboard) -> SwiftyOnboardOverlay? {
		let overlay = CustomOverlay.instanceFromNib() as? CustomOverlay
		return overlay
	}

	func swiftyOnboardOverlayForPosition(_ swiftyOnboard: SwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
		let overlay = overlay as? CustomOverlay
		let currentPage = round(position)
		overlay?.contentControl.currentPage = Int(currentPage)
	}
}

extension OnBoardingController {
	///Check if location enabled and append users current location
	func checkForUserLocation() {
		switch CLLocationManager.authorizationStatus() {
			case .authorizedWhenInUse: location.startMonitoring()
				break
			case .denied:
				DispatchQueue.main.async {
					self.performSegue(withIdentifier: OnBoardingController.tabBarController, sender: self)
				}
				break
			case .notDetermined: location.startMonitoring()
				break
			case .restricted:
				DispatchQueue.main.async {
					self.performSegue(withIdentifier: OnBoardingController.tabBarController, sender: self)
				}
				break
			default: break
		}
		locationManager.requestWhenInUseAuthorization()
	}

	///get location if enabled
	func updateLocation() {
		_ = ("\(location.currentLocation?.coordinate.latitude ?? 0), \(location.currentLocation?.coordinate.longitude ?? 0)")
		if let _ = location.currentLocation?.coordinate { }
		if let currentLocation = location.currentLocation {
			preferences[.latitude] = "\(currentLocation.coordinate.latitude)"
			preferences[.longitude] = "\(currentLocation.coordinate.longitude)"
		}
		if guestButtonClicked {
			DispatchQueue.main.async {
				self.performSegue(withIdentifier: OnBoardingController.tabBarController, sender: self)
			}
		}
		location.stopMonitoring()
	}

	func openCountryList() {
		///Go to home page is location permission is denied
		DispatchQueue.main.async {
			self.performSegue(withIdentifier: OnBoardingController.tabBarController, sender: self)
		}
	}
}
