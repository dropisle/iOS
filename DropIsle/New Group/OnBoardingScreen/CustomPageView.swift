//
//  CustomPageView.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-09.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit
import SwiftyOnboard

class CustomPageView: SwiftyOnboardPage {

    @IBOutlet weak var imageSet: UIImageView!
    @IBOutlet weak var textSet: UILabel!

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CustomPageView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
