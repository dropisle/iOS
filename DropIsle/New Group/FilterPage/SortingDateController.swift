//
//  SortingDateController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-03-06.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol SortingDateDelegate: class {
	func sortingDate(sortingDate: String)
}

class SortingDateController: UIViewController {

	weak var delegate: SortingDateDelegate?
	
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
	@IBAction func newAction(_ sender: UIButton) {
		delegate?.sortingDate(sortingDate: "new")
		self.dismiss(animated: true, completion: nil)
	}

	@IBAction func closeScreen(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}
}
