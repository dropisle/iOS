//
//  SortingViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-03-06.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol SortPriceDelegate: class {
	func sortPrice(sortPrice: String)
}

class SortingPriceController: UIViewController {

	weak var delegate: SortPriceDelegate?
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
    
	@IBAction func lowToHighAction(_ sender: UIButton) {
		self.delegate?.sortPrice(sortPrice: "low")
		self.dismiss(animated: true, completion: nil)
	}

	@IBAction func highToLowAction(_ sender: UIButton) {
		self.delegate?.sortPrice(sortPrice: "high")
		self.dismiss(animated: true, completion: nil)
	}

	@IBAction func closeScreen(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}
	/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
