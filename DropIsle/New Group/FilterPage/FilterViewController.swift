//
//  FilterViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-17.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import SwiftMessages
import CoreLocation

class FilterViewController: UIViewController,
	CategoriesDelegate,
	ModalHandler,
	CitiesDelegate,
	ItemConditionDelegate,
	SortPriceDelegate,
	SortingDateDelegate,
	PassLocationSearchDelegate {

	var cities: Countries?
	var newItemAction = false
	var usedItemAction = false
	var recentAction = false
	var priceLowToHighAction = false
	var priceHighToLowAction = false
	var sort = [String: String]()
	var distanceObj = [String: String]()

	let coutntriesController = "CountriesController"
	let citiesController = "CitiesController"
	let panelViewController = "PanelViewController"
	let itemConditionController = "ItemConditionController"
	let sortingPriceController = "SortingPriceController"
	let sortingDateController = "SortingDateController"
	let locationFilterController = "LocationFilterController"

	///filter Variables
	var category = String()
	var location = String()
	var sortBy = String()
	var sortByDate = String()
	var condition = String()
	var filterObjects = [String: Any]()

	@IBOutlet weak var maxValue: UITextField!
	@IBOutlet weak var minValue: UITextField!
	@IBOutlet weak var minCurrencyCode: UILabel!
	@IBOutlet weak var maxCurrencyCode: UILabel!
	@IBOutlet weak var categoryTypes: UIButton!
	@IBOutlet weak var sortByPriceButton: UIButton!
	@IBOutlet weak var itemConditionButton: UIButton!
	@IBOutlet weak var sortByDateButton: UIButton!
	@IBOutlet weak var locationAddress: UIButton!
	@IBOutlet weak var currentDistance: UILabel!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = "Filter"
		self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Avenir-Black", size: 20)!]
		self.tabBarController?.tabBar.isHidden = true

		if let currencySymbol = Locale.current.currencySymbol {
			minCurrencyCode.text = currencySymbol
			maxCurrencyCode.text = currencySymbol
		}

		addDoneButtonOnKeyboard()
		if let tabBarController = self.tabBarController as? TabBarViewController {
		}

		maxValue.tintColor = .darkGray
		minValue.tintColor = .darkGray
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		///Ananlytics
		LoggerManager.shared.log(event: .filter)
	}

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		self.tabBarController?.tabBar.isHidden = false
	}

	@IBAction func dismissScreen(_ sender: UIBarButtonItem) {
		self.navigationController?.popViewController(animated: true)
	}

	///Item Condition
	func itemCondition(itemCondition: String) {
		itemConditionButton.setTitleColor(.black, for: .normal)
		itemConditionButton.setTitle(itemCondition, for: .normal)
		condition = itemCondition
	}

	///Sorting by price
	func sortPrice(sortPrice: String) {
		sortByPriceButton.setTitleColor(.black, for: .normal)
		sortByPriceButton.setTitle(sortPrice.capitalized, for: .normal)
		sortBy = sortPrice
	}

	func sortingDate(sortingDate: String) {
		sortByDateButton.setTitleColor(.black, for: .normal)
		sortByDateButton.setTitle(sortingDate.capitalized, for: .normal)
		sortByDate = sortingDate
	}

	@IBAction func openContryList(_ sender: UIButton) {
		self.performSegue(withIdentifier: coutntriesController, sender: self)
	}

	// MARK: Dismiss KeyBoard
	func addDoneButtonOnKeyboard() {
		let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 30))
		doneToolbar.barStyle = .default

		let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
		let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneButtonAction))
		done.tintColor = .black
		let items = [flexSpace, done]
		doneToolbar.items = items
		doneToolbar.sizeToFit()

		maxValue.inputAccessoryView = doneToolbar
		minValue.inputAccessoryView = doneToolbar
	}

	@objc func doneButtonAction() {
		self.view.endEditing(true)
	}

	@IBAction func resetFilter(_ sender: UIBarButtonItem) {
		clearFilterFields()
	}

	func clearFilterFields() {
		locationAddress.setTitle("Set Location", for: .normal)
		locationAddress.setTitleColor(.lightGray, for: .normal)
		categoryTypes.setTitle("All Categories", for: .normal)
		categoryTypes.setTitleColor(.lightGray, for: .normal)
		minValue.text = String()
		maxValue.text = String()
		minValue.placeholder = "Min"
		maxValue.placeholder = "Max"
		sortByPriceButton.setTitle("Select Range", for: .normal)
		sortByPriceButton.setTitleColor(.lightGray, for: .normal)
		itemConditionButton.setTitle("Select Condition", for: .normal)
		itemConditionButton.setTitleColor(.lightGray, for: .normal)
		sortByDateButton.setTitle("Select Period", for: .normal)
		sortByDateButton.setTitleColor(.lightGray, for: .normal)

		category = String()
		location = String()
		condition = String()

		///Remove filters
		filterObjects.removeAll()
	}

	@IBAction func filterAction(_ sender: UIButton) {
		checkFilteredFields()
	}

	@IBAction func openLocation(_ sender: UIButton) {
		self.performSegue(withIdentifier: locationFilterController, sender: self)
	}

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == panelViewController) {
			let viewController = segue.destination as? PanelViewController
			viewController?.delegate = self
		}

		if (segue.identifier == itemConditionController) {
			let viewController = segue.destination as? ItemConditionController
			viewController?.delegate = self
		}

		if (segue.identifier == sortingPriceController) {
			let viewController = segue.destination as? SortingPriceController
			viewController?.delegate = self
		}

		if (segue.identifier == sortingDateController) {
			let viewController = segue.destination as? SortingDateController
			viewController?.delegate = self
		}

		if (segue.identifier == locationFilterController) {
			let viewController = segue.destination as? LocationFilterController
			viewController?.delegate = self
		}

		if let nav = segue.destination as? UINavigationController, let citiesDetails = nav.topViewController as? CitiesController {
			citiesDetails.cities = cities
			citiesDetails.delegate = self
		}

		///This is a delegate function to initialize the dismiss Modal view class and call a function
		if let nav = segue.destination as? UINavigationController, let countryVC = nav.topViewController as? CountriesController {
			countryVC.delegate = self
		}
	}
}

extension FilterViewController {
	func checkFilteredFields() {

		///setting string param
		if !CurrentUserInfo.shared.userId().isEmpty {
			filterObjects.updateValue(CurrentUserInfo.shared.userId(), forKey: "userid")
		}
		if !category.isEmpty {
			category.replace("&", with: "and")
			filterObjects.updateValue(category, forKey: "category") //["category"] = category
		}
		if let min = minValue.text, !min.isEmpty, let max = maxValue.text, !max.isEmpty {
			filterObjects.updateValue(min, forKey: "min") //["min"] = min
			filterObjects.updateValue(max + "1", forKey: "max") //["max"] = max
		}
		if !condition.isEmpty {
			filterObjects.updateValue(condition, forKey: "condition")  //["condition"] = condition
		}

		///Setting Sort object param
		if sortByDate == "new" {
			sort.updateValue(sortByDate, forKey: "date")
		}
		if sortBy == "high" {
			sort.updateValue(sortBy, forKey: "price")
		}
		if sortBy == "low" {
			sort.updateValue(sortBy, forKey: "price")
		}

		///Setting Distance object param
		if let theJSONData = try?  JSONSerialization.data(withJSONObject: sort, options: .prettyPrinted) {
			if let jsonObject = try? JSONSerialization.jsonObject(with: theJSONData, options: []) as? [String: Any] {
				if !jsonObject.isEmpty {
					filterObjects["sort"] = jsonObject
				}
			}
		}

		///Filter Search
		showSearchResultScreen(filterDict: filterObjects)

		filterObjects.removeAll()
		sort.removeAll()
		distanceObj.removeAll()
	}

	func showSearchResultScreen(filterDict: [String: Any]) {
		let myStoryBoard = UIStoryboard(name: "SearchResultStoryboard", bundle: Bundle.main)
		guard let popup = myStoryBoard.instantiateViewController(withIdentifier: "SearchResultViewController") as? SearchResultViewController else {
			return
		}
		popup.filterObjects = filterDict
		let navigationController = UINavigationController(rootViewController: popup)
		navigationController.navigationBar.isHidden = true
		if #available(iOS 13.0, *) {
			navigationController.modalPresentationStyle = .overFullScreen
		} else {
			// Fallback on earlier versions
			navigationController.modalPresentationStyle = .overFullScreen
		}
		navigationController.modalTransitionStyle = .crossDissolve
		self.present(navigationController, animated: true, completion: nil)
	}
}

///Disabled for now
extension FilterViewController {
	func categories(categoryName: String) {
		categoryTypes.setTitleColor(.black, for: .normal)
		categoryTypes.setTitle(categoryName, for: .normal)
		category = categoryName
	}

	func modalDismissed(cities: Countries) {
		self.cities = cities
		self.performSegue(withIdentifier: self.citiesController, sender: self)
	}

	func usersCity(country: String?, city: String?, code: String?) { }
	func setUserCity(city: String?, code: String?, country: String?) { }
}

extension FilterViewController {

	func passingData(address: String, location: CLLocation, distance: Int) {

		let countryLocale = NSLocale.current
		guard let countryCode = countryLocale.regionCode else { return }
		print("add \(address)", "Loc \(location)", "Dist \(distance)", "Code \(countryCode)")
		locationAddress.setTitle(address, for: .normal)
		locationAddress.setTitleColor(.black, for: .normal)
		currentDistance.text = "Current \(distance) km"

		///Check is user select country wide search on filter screen
		if distance == 10000 {
			filterObjects.updateValue(countryCode, forKey: "countrycode")
			currentDistance.text = "Country wide"
			locationAddress.setTitle(CurrentUserInfo.shared.country(), for: .normal)
		}

		///Append filter values from map / search table
		distanceObj.updateValue("\(distance)", forKey: "distance")
		distanceObj.updateValue("\(location.coordinate.latitude)", forKey: "lat")
		distanceObj.updateValue("\(location.coordinate.longitude)", forKey: "lng")

		if let theJSONData = try?  JSONSerialization.data(withJSONObject: distanceObj, options: .prettyPrinted) {
			if let jsonObject = try? JSONSerialization.jsonObject(with: theJSONData, options: []) as? [String: Any] {
				if !jsonObject.isEmpty {
					filterObjects["distance"] = jsonObject
				}
			}
		}
	}
}
