//
//  LocationFilterController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-05-27.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

protocol PassLocationSearchDelegate {
	func passingData(address: String, location: CLLocation, distance: Int)
}

class LocationFilterController: UIViewController, MKMapViewDelegate, UpdateLocationDelegate, SearchLocationControllerDelegate {

	let searchLocationController = "SearchLocationController"
	let location = LocationMonitor.SharedManager
	var searchedAddress = String()
	var distanceInKm = Int()
	var cllocation = CLLocation()
	var locationCoordinate2D = CLLocationCoordinate2D()
	var locationManager = CLLocationManager()
	var delegate: PassLocationSearchDelegate?

	@IBOutlet weak var mapView: MKMapView!
	@IBOutlet weak var distanceValue: UILabel!
	@IBOutlet weak var slider: UISlider!
	@IBOutlet weak var searchLocation: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = "Set Location"
		self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Avenir-Black", size: 20)!]

		//Location manager delegate
		location.delegate = self

		mapView.delegate = self
		mapView.showsScale = false
		mapView.showsCompass = false

		mapView.pointOfInterestFilter = .some(MKPointOfInterestFilter(including: [.airport, .atm]))

		mapView.showsUserLocation = true
		mapView.userTrackingMode = .followWithHeading

		let gestureRecognizer = UITapGestureRecognizer( target: self, action: #selector(triggerTouchAction))
		mapView.addGestureRecognizer(gestureRecognizer)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		checkForUserLocation()
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		self.tabBarController?.tabBar.isHidden = true
	}

	///Update location on map and remover overlay for new updates
	func updateLocation() {
		_ = ("\(location.currentLocation?.coordinate.latitude ?? 0), \(location.currentLocation?.coordinate.longitude ?? 0)")
		guard let coord = location.currentLocation?.coordinate else { return }

		///get coordinate for address convertion on screen load
		cllocation =  CLLocation(latitude: coord.latitude, longitude: coord.longitude)
		getAddress(location: cllocation)

		locationCoordinate2D = coord
		if let lat = location.currentLocation?.coordinate.latitude , let lng = location.currentLocation?.coordinate.longitude {
			let center =  CLLocationCoordinate2DMake(lat, lng)
			let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
			let region = MKCoordinateRegion(center: center, span : span)
			mapView.setRegion(region, animated: true)
			for annotation in self.mapView.annotations {
				mapView.removeAnnotation(annotation)
			}
			for overlay in mapView.overlays {
				mapView.removeOverlay(overlay)
			}

			///Defualt state of slider and text when search value is applied to map
			distanceValue.text = "Browse products within \(50)km"
			distanceInKm = 50
			slider.value = 6

			showCircle(coordinate: coord, radius: Double(50) * 1000, mapView: mapView)
			location.stopMonitoring()
		}
	}

	///Show circular overlay for radius regions
	func showCircle(coordinate: CLLocationCoordinate2D, radius: CLLocationDistance, mapView: MKMapView) {
		let circle = MKCircle(center: coordinate, radius: radius)
		mapView.addOverlay(circle)
		mapView.setVisibleMapRect(circle.boundingMapRect, edgePadding: UIEdgeInsets(top: 80, left: 10, bottom: 0, right: 10), animated: true)
	}

	func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
		if let circleOverlay = overlay as? MKCircle {
			let circleRenderer = MKCircleRenderer(overlay: circleOverlay)
			circleRenderer.fillColor = UIColor(hexString: "#3293FC")
			circleRenderer.alpha = 0.2
			return circleRenderer
		}
		return MKOverlayRenderer()
	}

	func openCountryList() { }

	@IBAction func slideDistance(_ sender: UISlider) {
		sender.value = roundf(sender.value)
		switch roundf(sender.value) {
			case 1:
				distanceValue.text = "Browse products within \(1)km"
				distanceInKm = 1
			case 2:
				distanceValue.text = "Browse products within \(3)km"
				distanceInKm = 3
			case 3:
				distanceValue.text = "Browse products within \(5)km"
				distanceInKm = 5
			case 4:
				distanceValue.text = "Browse products within \(10)km"
				distanceInKm = 10
			case 5:
				distanceValue.text = "Browse products within \(25)km"
				distanceInKm = 25
			case 6:
				distanceValue.text = "Browse products within \(50)km"
				distanceInKm = 50
			case 7:
				distanceValue.text = "Browse products within \(100)km"
				distanceInKm = 100
			case 8:
				distanceValue.text = "Browse products within \(200)km"
				distanceInKm = 200
			case 9:
				distanceValue.text = "Browse products within \(500)km"
				distanceInKm = 500
			case 10:
				distanceValue.text = "Browsing \(CurrentUserInfo.shared.country()) Wide"
				distanceInKm = 10000
			default:
				break
		}

		for overlay in mapView.overlays {
			mapView.removeOverlay(overlay)
			let circle = MKCircle(center: locationCoordinate2D, radius: (Double(distanceInKm) * 1000))
			mapView.addOverlay(circle)
			mapView.setVisibleMapRect(circle.boundingMapRect, edgePadding: UIEdgeInsets(top: 80, left: 10, bottom: 0, right: 10), animated: true)
		}
	}

	@objc func triggerTouchAction(_ gestureReconizer: UITapGestureRecognizer) {
		let locationInView = gestureReconizer.location(in: mapView)
		let locationOnMap = mapView.convert(locationInView, toCoordinateFrom: mapView)

		//get coordinate for address convertion
		cllocation =  CLLocation(latitude: locationOnMap.latitude, longitude: locationOnMap.longitude)
    print("Tapped at lat: \(locationOnMap.latitude) long: \(locationOnMap.longitude)")
		getAddress(location: cllocation)

		//Adding annotation on map when clicked
		addAnnotationOnLocation(pointAtCoordinate: locationOnMap)

		//Defualt state of slider and text when search value is applied to map
		distanceValue.text = "Browse products within \(50)km"
		distanceInKm = 50
		slider.value = 6
	}

	func addAnnotationOnLocation(pointAtCoordinate: CLLocationCoordinate2D) {
		let annotation = MKPointAnnotation()
		annotation.coordinate = pointAtCoordinate
		locationCoordinate2D = pointAtCoordinate

		for annotation in self.mapView.annotations {
			mapView.removeAnnotation(annotation)
		}

		for overlay in mapView.overlays {
			mapView.removeOverlay(overlay)
			mapView.addAnnotation(annotation)
			showCircle(coordinate: locationCoordinate2D, radius: Double(50) * 1000, mapView: mapView)
		}
	}

	//Check if location enabled and append users current location
	func checkForUserLocation() {
		switch CLLocationManager.authorizationStatus() {
			case .authorizedWhenInUse: location.startMonitoring()
				break
			case .denied: showLocationAlert()
				break
			case .notDetermined: showLocationAlert()
				break
			case .restricted: showLocationAlert()
				break
			default: break
		}
	}

	func showLocationAlert() {
    let alertController = UIAlertController(title: "Enable Location with Dropisle", message: "Allow access to your Location and enable Dropisle browse products near you.", preferredStyle: .alert)
    let action = UIAlertAction(title: "Enable Location Service", style: .default) { (_) -> Void in
			guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
         }
    }
    let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
		action.setValue(UIColor.black, forKey: "titleTextColor")
		cancel.setValue(UIColor.black, forKey: "titleTextColor")
		alertController.addAction(action)
		alertController.addAction(cancel)
		self.present(alertController, animated: true, completion: nil)
	}

	@IBAction func resetUserLocation(_ sender: UIButton) {
		location.startMonitoring()
	}

	@IBAction func searchLocationAction(_ sender: UIButton) {
		self.performSegue(withIdentifier: searchLocationController, sender: self)
	}

	@IBAction func applyLocation(_ sender: UIButton) {
		self.navigationController?.popViewController(animated: true)
		delegate?.passingData(address: searchLocation.titleLabel?.text ?? "", location: cllocation, distance: distanceInKm)
	}

	@IBAction func dismissScreen(_ sender: UIBarButtonItem) {
		self.navigationController?.popViewController(animated: true)
	}

	// MARK: - Navigation
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == searchLocationController {
			let option = segue.destination as? SearchLocationController
			option?.regionCoord = mapView.region
			option?.coordinate = locationCoordinate2D
			option?.delegate = self
		}
	}
}

extension LocationFilterController {
	//Location passed from search table on click
	func newLocation(_ result: String) {
		searchLocation.setTitle(result, for: .normal)
		searchLocation.setTitleColor(.darkGray, for: .normal)
		let geoCoder = CLGeocoder()
		geoCoder.geocodeAddressString(result) { (placemarks, error) in
			guard let placemarks = placemarks,	let location = placemarks.first?.location?.coordinate else { return }

			//get coordinate for address convertion on screen load
			self.cllocation =  CLLocation(latitude: location.latitude, longitude: location.longitude)

			self.locationCoordinate2D = location
			self.addAnnotationOnLocation(pointAtCoordinate: self.locationCoordinate2D)
			for overlay in self.mapView.overlays {
				self.mapView.removeOverlay(overlay)
				let circle = MKCircle(center: self.locationCoordinate2D, radius: Double(50) * 1000)
				self.mapView.addOverlay(circle)
				self.mapView.setVisibleMapRect(circle.boundingMapRect, edgePadding: UIEdgeInsets(top: 80, left: 10, bottom: 0, right: 10), animated: true)

				//Defualt state of slider and text when search value is applied to map
				self.distanceValue.text = "Browse products within \(50)km"
				self.distanceInKm = 50
				self.slider.value = 6
			}
		}
	}
}

extension LocationFilterController {
//Current selected location on map Location
	func getAddress(location: CLLocation?) {
		if let loc = location {
			let geocoder = CLGeocoder()
			geocoder.reverseGeocodeLocation(loc) { (placemarksArray, _) in
				guard let placeMark = placemarksArray else {
					return
				}
				if (placeMark.count) > 0 {
					let placemark = placemarksArray?.first
					let loc = "\(placemark?.name ?? "")" + ", " +   "\(placemark?.administrativeArea ?? "")" + ", " +  "\(placemark?.country ?? "")" +  " " +  "\(placemark?.isoCountryCode ?? "")"
					self.searchLocation.setTitle(loc, for: .normal)
					self.searchLocation.setTitleColor(.black, for: .normal)
				}
			}
		}
	}
}
