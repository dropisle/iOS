//
//  SearchLocationCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-05-29.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class SearchLocationCell: UITableViewCell, Reusable {

	@IBOutlet weak var searchValue: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}

	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
	}
}
