//
//  SearchLocationController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-05-28.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit
import MapKit
import GooglePlaces

protocol SearchLocationControllerDelegate: class {
	func newLocation(_ result: String)
}

class SearchLocationController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

	var matchingItems: [MKMapItem] = []
	var regionCoord = MKCoordinateRegion()
	var coordinate = CLLocationCoordinate2D()
	weak var delegate: SearchLocationControllerDelegate?

	@IBOutlet weak var navigationBar: UINavigationBar!
	@IBOutlet weak var searchBar: UITextField!
	@IBOutlet weak var searchLocationTable: UITableView!

	var array = [GMSAutocompletePrediction]()
	lazy var filters: GMSAutocompleteFilter = {
		var filters = GMSAutocompleteFilter()
		filters.type = .noFilter
		return filters
	}()

	override func viewDidLoad() {
		super.viewDidLoad()
		navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Avenir-Black", size: 20)!]
		navigationBar.isTranslucent = true
		navigationBar.setBackgroundImage(UIImage(), for: .default)
		navigationBar.backgroundColor = .clear
		navigationBar.barTintColor = .clear
		searchBar.delegate = self
		searchBar.setLeftPaddingPoints(40)
		searchBar.tintColor = .lightGray
		searchBar.becomeFirstResponder()

		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeKeyboard))
		tap.cancelsTouchesInView = false
		view.addGestureRecognizer(tap)
	}

	func getLocation (_ searchValue: String) {
		let searchBarText = searchValue
		let request = MKLocalSearch.Request()
		request.naturalLanguageQuery = searchBarText
		request.region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 180, longitudinalMeters: 360) //regionCoord
		let search = MKLocalSearch(request: request)
		search.start { response, _ in
			guard let response = response else { return }
			print(response.mapItems)
			for i in response.mapItems {
				if !self.matchingItems.contains(i) {
					let prod = self.matchingItems.filter { $0.name == i.name }
					self.matchingItems.append(contentsOf: prod)
					print(self.matchingItems.count)
				}
			}
			self.matchingItems.append(contentsOf: response.mapItems)
			self.searchLocationTable.reloadData()
		}
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return matchingItems.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchLocationCell") as? SearchLocationCell else { return UITableViewCell() }
		let selectedItem = matchingItems[indexPath.row].placemark
		cell.searchValue?.text = selectedItem.name
		//cell.searchValue?.text = "\(selectedItem.name ?? ""), \(selectedItem.thoroughfare ?? ""), \(selectedItem.locality ?? ""), \(selectedItem.subLocality ?? ""), \(selectedItem.administrativeArea ?? ""), \(selectedItem.postalCode ?? ""), \(selectedItem.country ?? "")"
		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let cell = tableView.cellForRow(at: indexPath) as? SearchLocationCell
		if let value = cell?.searchValue.text {
			delegate?.newLocation(value)
			self.dismiss(animated: true, completion: nil)
		}
	}

	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		return true
	}

	@objc func closeKeyboard() {
		view.endEditing(true)
	}

	@IBAction func textFieldDidChange(_ sender: UITextField) {
		if let text = sender.text {
			getLocation(text)

//			GMSPlacesClient.shared().autocompleteQuery(text, bounds: nil, filter: filters) { (result, error) in
//				if error == nil && result != nil {
//					self.array = result!
//					print(self.array)
//				}
//			}

		}
	}

	@IBAction func closeScreen(_ sender: UIBarButtonItem) {
		self.dismiss(animated: true, completion: nil)
	}
}
