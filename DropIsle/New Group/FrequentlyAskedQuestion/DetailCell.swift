//
//  DetailCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-01-20.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class DetailCell: FoldingCell {

@IBOutlet var closeNumberLabel: UILabel!
@IBOutlet var openNumberLabel: UILabel!

	var frequentlyAskedQuestion: FAQ? {
		didSet {
			closeNumberLabel.text = frequentlyAskedQuestion?.title
			if let labelTextFormatted = frequentlyAskedQuestion?.description {
				let attributedString = labelTextFormatted.htmlToAttributedString
				openNumberLabel.attributedText = attributedString
			}
		}
	}

		override func awakeFromNib() {
			foregroundView.layer.cornerRadius = 5
			foregroundView.layer.masksToBounds = true

			containerView.layer.cornerRadius = 5
			containerView.layer.borderWidth = 0.7
			containerView.layer.borderColor = UIColor(hexString: "#6F75E8").cgColor
			containerView.layer.masksToBounds = true
			super.awakeFromNib()
		}

		override func animationDuration(_ itemIndex: NSInteger, type _: FoldingCell.AnimationType) -> TimeInterval {
				let durations = [0.26, 0.2, 0.2]
				return durations[itemIndex]
		}
	}

	// MARK: - Actions ⚡️
extension String {
    var htmlToAttributedString: NSAttributedString? {
				let modifiedString = "<style>body{font-family: 'Avenir'; font-size: 15px;}</style>\(self)"
			guard let data = modifiedString.data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
