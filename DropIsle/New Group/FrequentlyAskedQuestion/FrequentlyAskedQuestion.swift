//
//  FrequentlyAskedQuestion.swift
//  DropIsle
//
//  Created by CtanLI on 2020-01-20.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class FrequentlyAskedQuestion: UITableViewController {

  enum Const {
		static let closeCellHeight: CGFloat = 60
	}

	var cellHeights: [CGFloat] = []
	var frequentlyAskedQuestion = [FAQ]() {
		didSet {
				DispatchQueue.main.async {
				self.tableView.reloadData()
			}
		}
	}

	// MARK: Life Cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "Frequently Asked Question"
		self.tabBarController?.tabBar.isHidden = true
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton

		loadFAQjson()
		setup()
	}

	// MARK: Helpers
	private func setup() {
		cellHeights = (0..<frequentlyAskedQuestion.count).map { _ in Const.closeCellHeight }
		tableView.estimatedRowHeight = Const.closeCellHeight
		tableView.rowHeight = UITableView.automaticDimension
		if #available(iOS 10.0, *) {
		tableView.refreshControl = UIRefreshControl()
		tableView.refreshControl?.addTarget(self, action: #selector(refreshHandler), for: .valueChanged)
		}
	}

	func loadFAQjson() {
		guard let url = Bundle.main.url(forResource:"FrequentlyAskedQuestion", withExtension: "json") else { return }
		guard let jsonData = try? Data(contentsOf: url) else { return }
    guard let object  = try? JSONDecoder().decode([FAQ].self, from: jsonData) else { return }
		frequentlyAskedQuestion.append(contentsOf: object)
	}

	// MARK: Actions
	
	@objc func refreshHandler() {
		let deadlineTime = DispatchTime.now() + .seconds(1)
		DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: { [weak self] in
			if #available(iOS 10.0, *) {
				self?.tableView.refreshControl?.endRefreshing()
			}
			self?.tableView.reloadData()
		})
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
		}
	}

		// MARK: - TableView

	extension FrequentlyAskedQuestion {
	override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
		return frequentlyAskedQuestion.count
	}

	override func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
	guard case let cell as DetailCell = cell else { return }

	if cellHeights[indexPath.row] == Const.closeCellHeight {
		cell.unfold(false, animated: false, completion: nil)
	} else {
		cell.unfold(true, animated: false, completion: nil)
	}
		cell.frequentlyAskedQuestion = frequentlyAskedQuestion[indexPath.row]
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "FoldingCell", for: indexPath) as? FoldingCell else { return UITableViewCell() }
		let durations: [TimeInterval] = [0.26, 0.2, 0.2]
		cell.durationsForExpandedState = durations
		cell.durationsForCollapsedState = durations
		return cell
	}

	override func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return cellHeights[indexPath.row]
	}

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
	guard let cell = tableView.cellForRow(at: indexPath) as? FoldingCell else { return }

	if cell.isAnimating() { return }
	guard case let ncell as DetailCell = cell else { return }

	var duration = 0.0
	let cellIsCollapsed = cellHeights[indexPath.row] == Const.closeCellHeight

	if cellIsCollapsed {
		ncell.openNumberLabel.sizeToFit()
		cellHeights[indexPath.row] = ncell.openNumberLabel.bounds.size.height + 40.0
		cell.unfold(true, animated: true, completion: nil)
		duration = 0.5
	} else {
		cellHeights[indexPath.row] = Const.closeCellHeight
		cell.unfold(false, animated: true, completion: nil)
		duration = 0.8
	}

	UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
	tableView.beginUpdates()
	tableView.endUpdates()

	if cell.frame.maxY > tableView.frame.maxY {
			tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.top, animated: true)
	}
		}, completion: nil)
	}
}
