//
//  SearchResultViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-12.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import SVProgressHUD
import CoreLocation

class SearchResultViewController: PannableViewController, UICollectionViewDataSourcePrefetching, Reusable, AddSearchToWishListDelegate {

	var searchValue: String?
	var coordinates = CLLocationCoordinate2D()
	var filterObjects = [String: Any]()
	var wishlistState = String()
	var valueToSearch = [String: String]()
	var emptyBackgroundView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))

	var products = [ProductModel]()
	var content = [ProductModel.content]() {
		didSet {
			DispatchQueue.main.async {
				self.searchResultCollectionView.reloadData()
			}
		}
	}

	@IBOutlet weak var customViewToDrag: UIView!
	@IBOutlet weak var searchResultCollectionView: UICollectionView!
	@IBOutlet weak var searchResultTitle: UILabel!
	@IBOutlet weak var closeButton: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()
			setBackgroundView()
			searchResultTitle.text = searchValue?.uppercased()
			searchResultTitle.font = UIFont(name: "Avenir-Heavy", size: 15)
			customViewToDrag.clipsToBounds = true
			customViewToDrag.layer.cornerRadius = 20
			customViewToDrag.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]

			//Set the CustomLayout delegate
			searchResultCollectionView.prefetchDataSource = self
			searchResultCollectionView.showsVerticalScrollIndicator = false
			searchResultCollectionView.backgroundColor = UIColor(hexString: "#f6f6f6")
			searchResultCollectionView?.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
			if let layout = searchResultCollectionView?.collectionViewLayout as? CustomCollectionView {
				layout.delegate = self
			}

		setCloseButtonImageColor()

			//Start Search or Filter Search
			filterObjects.isEmpty ? searchByText(page: "\(1)") : filterByParams(filterDict: filterObjects, page: "\(1)")
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		///Analytics
		LoggerManager.shared.log(event: .searchResult)
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
			self.content.removeAll()
	}

	deinit {
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "notifyWishlistScreen"), object: nil)
	}

	func setCloseButtonImageColor() {
		let origImage = UIImage(named: "Close")
		let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
		closeButton.setImage(tintedImage, for: .normal)
		closeButton.tintColor = .darkGray
	}

	@IBAction func closeView(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}

	@objc func searchByText(page: String) {
		if CurrentUserInfo.shared.userId().isEmpty {
			let countryLocale = NSLocale.current
			valueToSearch.updateValue(searchValue ?? "", forKey: "text")
			valueToSearch.updateValue(countryLocale.regionCode ?? "", forKey: "countrycode")
		} else {
			valueToSearch.updateValue(searchValue ?? "", forKey: "text")
		}
		guard let lat = preferences[.latitude], let lng = preferences[.longitude] else { return }
		let guestUrlString = String(format: APIEndPoints.guestUserSearch, lat, lng, page)
		let userUrlString = String(format: APIEndPoints.registeredUserSearch, lat, lng, CurrentUserInfo.shared.userId(), page)
		CurrentUserInfo.shared.userId().isEmpty ? searchProducts(url: guestUrlString, page: page) : searchProducts(url: userUrlString, page: page)
	}

	///Filtering products based on selected options from filter screen
	@objc func filterByParams(filterDict: [String: Any], page: String) {
		let fGuestUrl = String(format: APIEndPoints.filterProductsUsers, page)
		let fUserUrl = String(format: APIEndPoints.filterProductsUsers, page)
		CurrentUserInfo.shared.userId().isEmpty ? filteredProducts(url: fGuestUrl, filterDict: filterDict, page: page) : filteredProducts(url: fUserUrl, filterDict: filterDict, page: page)
	}

	func searchProducts(url: String, page: String) {
		let searchUrl = BaseAPIClient.urlFromPath(path: url)
		APIManager.shared.fetchGenericData(method: .post, urlString: searchUrl, params: valueToSearch as [String: AnyObject], headers: nil) { (products: ProductModel) in
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
				self.products.removeAll()
				let prod = products.content.filter { $0.status != APIEndPoints.soldSate }
				self.products.append(products)
				self.content.append(contentsOf: prod)
			})
		}
	}

	func filteredProducts(url: String, filterDict: [String: Any], page: String) {
		searchResultTitle.text = "Filtered Search"
		searchResultTitle.font = UIFont(name: "Avenir-Heavy", size: 15)
		searchResultTitle.textColor = .darkGray
		let filterUrl = BaseAPIClient.urlFromPath(path: url)
		APIManager.shared.fetchGenericData(method: .post, urlString: filterUrl, params: filterDict as [String: AnyObject], headers: nil) { (products: ProductModel) in
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
				self.products.removeAll()
				let prod = products.content.filter { $0.status != APIEndPoints.soldSate }
				self.products.append(products)
				self.content.append(contentsOf: prod)
			})
		}
	}

	func addToWishList(cell: SearchResultViewCell) {
		CurrentUserInfo.shared.userId().isEmpty ? CheckIfUserIsRegistered() : AddAndRemoveFromWishList(cell: cell)
	}

	func AddAndRemoveFromWishList(cell: SearchResultViewCell) {

		//Animating button on click to verify action
		cell.addToWishList.bounceAnimation(value: 0.1)

		var dict = [String: String]()
		var jsonObj = [String: Any]()
		guard let indexPath = self.searchResultCollectionView.indexPath(for: cell) else {
			return
		}
		let cell = searchResultCollectionView.cellForItem(at: indexPath) as? SearchResultViewCell
		let userId = CurrentUserInfo.shared.userId()
		guard let itemCode = cell?.searchedProducts?.id else {
			return
		}

		//Check if product id currently exist remove else add
		if preferences[.addedSearchedToWishlist].contains(itemCode) {
			self.wishlistState = "remove"
		}

		if !preferences[.addedSearchedToWishlist].contains(itemCode) {
			self.wishlistState = "add"
		}

		dict.updateValue(itemCode, forKey: "itemcode")
		dict.updateValue(userId, forKey: "userid")
		dict.updateValue(wishlistState, forKey: "type")
		if let theJSONData = try?  JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
			if let jsonObject = try? JSONSerialization.jsonObject(with: theJSONData, options: []) as? [String: Any] {
				jsonObj = jsonObject
			}
		}

		HomeService.sharedInstance.addAndRemoveProductFromWishlist(params: jsonObj as [String: AnyObject]) { (result) in
			if result.code == 200 {
				SVProgressHUD.dismiss(withDelay: 0.5)
				
				//Check if wishlist already added by user
				if result.status == "added" {
					///Show Detail Screen
					LoggerManager.shared.log(event: .addToWishlist)

					cell?.setAddToWishList(wishListImage: UIImage(named: "addToWishListSolid"))
					// Post a notification
					NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyWishlistScreen"), object: nil, userInfo: nil)

					//Update Model but dont reload collection when wishlist button is clicked
					if let productId = cell?.searchedProducts?.id {
						preferences[.addedSearchedToWishlist].append(productId)
					}
				} else {
					///Show Detail Screen
					LoggerManager.shared.log(event: .removeFromWishlist)

					cell?.setAddToWishList(wishListImage: UIImage(named: "addToWishListEmpty"))
					if let productId = cell?.searchedProducts?.id {
						let storedWishlistProductId = preferences[.addedSearchedToWishlist]
						let availableProductId = storedWishlistProductId.filter { $0 != productId }
						preferences.removeObject(forKey: "addedSearchedToWishlist")
						preferences[.addedSearchedToWishlist].append(contentsOf: availableProductId)
					}
				}
			} else {
				SVProgressHUD.dismiss(withDelay: 0.5)
			}
		}
	}
}

extension SearchResultViewController: UICollectionViewDataSource {

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if (self.content.count == 0) {
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
			guard self.content.count != 0 else {
				self.searchResultCollectionView.backgroundView = self.emptyBackgroundView
				return
				}
			}
		} else {
			searchResultCollectionView.backgroundView = nil
		}
		SVProgressHUD.dismiss()
		return content.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchResultViewCell.identifier, for: indexPath) as? SearchResultViewCell else {
			return UICollectionViewCell()
		}
		cell.setUpMargins()
		cell.delegate = self
		cell.searchedProducts = content[indexPath.row]

		//Check if the product added to wishlist id's are in storage and update the button when scrolling.
		if let productId = cell.searchedProducts?.id {
			if preferences[.addedSearchedToWishlist].contains(productId) {
				cell.setAddToWishList(wishListImage: UIImage(named: "addToWishListSolid"))
			}
		}
		return cell
	}

	func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
			//Begin asynchronously fetching data for the requested index paths.
			for indexPath in indexPaths {
				guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchResultViewCell.identifier, for: indexPath) as? SearchResultViewCell else {
					return
				}
				let model = content[indexPath.row]
				cell.searchedProducts = model
			}
	}

	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		if indexPath.row == content.count - 1 {
			guard products.first?.numberOfElements == 10 else {
				return
			}
			if var pageNumber = self.products.first?.pageable.pageNumber {
				pageNumber += 2

				//Start Search or Filter Search
				filterObjects.isEmpty ? searchByText(page: "\(pageNumber)") : filterByParams(filterDict: filterObjects, page: "\(pageNumber)")
			}
		}
	}
}

extension SearchResultViewController: UICollectionViewDelegate, UIViewControllerTransitioningDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		///Show Detail Screen
		LoggerManager.shared.log(event: .showDetailScreen)

		let cell = collectionView.cellForItem(at: indexPath) as? SearchResultViewCell
		let storyBoard: UIStoryboard = UIStoryboard(name: "ProductDetailPage", bundle: nil)
		if let pd = storyBoard.instantiateViewController(withIdentifier: "ProductDetailPageController") as? ProductDetailPageController {
			pd.isFromSearchController = true
			pd.productObject = cell?.searchedProducts
			let nav = UINavigationController(rootViewController: pd)
			nav.modalPresentationStyle = UIModalPresentationStyle.custom
			nav.modalTransitionStyle = .crossDissolve
			nav.transitioningDelegate = self
			self.present(nav, animated: true, completion: nil)
		}
	}
}

extension SearchResultViewController: HomeScreenLayoutDelegate {
	func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
			if UIDevice.current.userInterfaceIdiom == .pad {
				guard let height = content[exist: indexPath.item]  else { return 550 }
					guard let imageHeight = NumberFormatter().number(from: height.photo.height) else { return 0 }
					let expectedHeight = CGFloat(truncating: imageHeight)
					if (expectedHeight) > 550 {
						return 550
					} else {
						return 550
					}
			}

			guard let height = content[exist: indexPath.item]  else { return 250 }
			guard let imageHeight = NumberFormatter().number(from: height.photo.height) else { return 0 }
			let expectedHeight = CGFloat(truncating: imageHeight)
			if (expectedHeight) > 250 {
				return 250
			} else {
				return 250
			}
	}

	func collectionView(_ collectionView: UICollectionView, widthForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
		return 300
	}
}

extension SearchResultViewController {

	private func setBackgroundView() {
		let stackView = UIStackView()
		emptyBackgroundView.backgroundColor = UIColor.white
		let view1 = UIImageView.emptySearchImageAttribute(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
		view1.heightAnchor.constraint(equalToConstant: 100).isActive = true
		view1.widthAnchor.constraint(equalToConstant: 100).isActive = true

		let view2 = UILabel.emptySearchAttributedLabel(frame: CGRect(x: 0, y: 0, width: 278, height: 48))
		view2.heightAnchor.constraint(equalToConstant: 48).isActive = true
		view2.widthAnchor.constraint(equalToConstant: 278).isActive = true

		let view3 = UIButton.emptySearchAttributedButton(frame: CGRect(x: 0, y: 0, width: 135, height: 35))
		view3.addTarget(self, action: #selector(dismissScreen), for: .touchDown)
		view3.heightAnchor.constraint(equalToConstant: 35).isActive = true
		view3.widthAnchor.constraint(equalToConstant: 135).isActive = true

		stackView.axis = .vertical
		stackView.distribution = .equalSpacing
		stackView.alignment = .center
		stackView.spacing = 8

		stackView.addArrangedSubview(view1)
		stackView.addArrangedSubview(view2)
		stackView.addArrangedSubview(view3)

		stackView.translatesAutoresizingMaskIntoConstraints = false
		emptyBackgroundView.addSubview(stackView)
		stackView.centerXAnchor.constraint(equalTo: emptyBackgroundView.centerXAnchor).isActive = true
		stackView.centerYAnchor.constraint(equalTo: emptyBackgroundView.centerYAnchor).isActive = true
	}

	@objc func dismissScreen() {
		self.dismiss(animated: true, completion: nil)
	}
}

extension SearchResultViewController {
	private func CheckIfUserIsRegistered() {
		///Show Guest User Screen
		LoggerManager.shared.log(event: .guestUserCreateOrSignInScreen)

		let storyboard = UIStoryboard(name: "GuestUserSignUpStoryboard", bundle: nil)
		let pvc = storyboard.instantiateVC() as? GuestSignUpPanelViewController
		if let pvc = pvc {
			pvc.modalPresentationStyle = UIModalPresentationStyle.custom
			pvc.modalTransitionStyle = .crossDissolve
			pvc.transitioningDelegate = self
			self.present(pvc, animated: true, completion: nil)
		}
	}
}
