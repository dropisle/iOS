//
//  SearchResultViewCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-12.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import SDWebImage

protocol AddSearchToWishListDelegate: class {
	func addToWishList(cell: SearchResultViewCell)
}

class SearchResultViewCell: UICollectionViewCell, Reusable {

	weak var delegate: AddSearchToWishListDelegate?

	@IBOutlet weak var productImage: UIImageView!
	@IBOutlet weak var productName: UILabel!
	@IBOutlet weak var productPrice: UILabel!
	@IBOutlet weak var addToWishList: UIButton!

	var searchedProducts: ProductModel.content? {
		didSet {
			guard let searchedProducts = searchedProducts else {
				return
			}
			weak var weakSelf = self
			productImage.sd_imageTransition = SDWebImageTransition.fade
			weakSelf?.productImage.sd_setImage(with: URL(string: searchedProducts.photo.url), placeholderImage: UIImage(named: "placeholder"), options: .fromCacheOnly) { (image, error, cacheType, url) in }
			self.productName.text = searchedProducts.name

			let price = "\(searchedProducts.price.price)".dropLast(2)
			self.productPrice.text = "\(price)".currencyInputFormatting(currencySymbol: searchedProducts.price.currency)
			self.searchedProducts?.watch == "YES" ? setAddToWishList(wishListImage: UIImage(named: "addToWishListSolid")) : setAddToWishList(wishListImage: UIImage(named: "addToWishListEmpty"))
		}
	}

	@IBAction func addToWishList(_ sender: UIButton) {
		self.delegate?.addToWishList(cell: self)
	}

	func setAddToWishList(wishListImage: UIImage?) {
		let tintedImage = wishListImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
		addToWishList.setImage(tintedImage, for: .normal)
		addToWishList.tintColor = .lightGray
	}

	override func awakeFromNib() {
		super.awakeFromNib()
	}
}
