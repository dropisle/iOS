//
//  SearchViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-11.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import CoreLocation

class SearchViewController: UIViewController, UISearchControllerDelegate, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, Reusable, UpdateLocationDelegate {

	@IBOutlet weak var searchTableView: UITableView!

	var clear = UIButton()
	var address = String()
	var coordinates = CLLocationCoordinate2D()
	var recentSearches: [String] = []
	var recentlySearchedItems: [String] = []
	var storedRecentSearches: [String] = []
	var defaults = UserDefaults.standard
	var userLocationCordinate: CLLocationCoordinate2D?
	var searchController = UISearchController(searchResultsController: nil)
	var locationManager = CLLocationManager()
	let location = LocationMonitor.SharedManager

	lazy var searchTextField: UITextField? = { [unowned self] in
		var textField: UITextField?
		searchController.searchBar.subviews.forEach({ view in
			view.subviews.forEach({ view in
				if let view  = view as? UITextField {
					textField = view
				}
			})
		})
		return textField
		}()

	override func viewDidLoad() {
		super.viewDidLoad()

		///Location manager delegate
		location.delegate = self

		searchTableView.keyboardDismissMode = .onDrag
		searchTableView.showsVerticalScrollIndicator = false
		searchController.obscuresBackgroundDuringPresentation = false
		searchController.searchBar.placeholder = "Search Dropisle"
		navigationItem.searchController = searchController
		navigationController!.navigationBar.isTranslucent = false
		navigationController!.navigationBar.barTintColor = UIColor(red: 0.20, green: 0.58, blue: 0.99, alpha: 1.0)
		searchController.searchBar.barTintColor = UIColor(red: 0.20, green: 0.58, blue: 0.99, alpha: 1.0)
		searchController.searchBar.searchBarStyle = .default
		searchController.searchBar.isTranslucent = false
		searchController.searchBar.tintColor = UIColor.white
		definesPresentationContext = true
		navigationItem.hidesSearchBarWhenScrolling = false
		searchController.delegate = self
		searchController.searchBar.delegate = self
		let iconView = searchController.searchBar.customTextfield?.leftView as? UIImageView
		iconView?.image = iconView?.image?.withRenderingMode(.alwaysTemplate)
		iconView?.tintColor = .lightGray

		if #available(iOS 13.0, *) {
			searchController.searchBar.customTextfield?.font = UIFont(name: "Avenir-Book", size: 15)
			searchController.searchBar.customTextfield?.backgroundColor = .white
			searchController.searchBar.customTextfield?.layer.cornerRadius = 5
			searchController.searchBar.customTextfield?.clipsToBounds = true
			searchController.searchBar.customTextfield?.layer.bounds.size.height = 20

			UITextField.appearance(whenContainedInInstancesOf: [type(of: searchController.searchBar)]).tintColor = .lightGray
			UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.darkGray]
			UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = NSAttributedString(string: "Search Dropisle", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
			UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Book", size: 15)!, NSAttributedString.Key.foregroundColor: UIColor.black]
		} else {
			if let textfield = self.searchTextField?.subviews.first {
				searchTextField!.font = UIFont(name: "Avenir-Book", size: 15)
				textfield.backgroundColor = .white
				textfield.layer.cornerRadius = 5
				textfield.clipsToBounds = true
				textfield.layer.bounds.size.height = 20
				UITextField.appearance(whenContainedInInstancesOf: [type(of: searchController.searchBar)]).tintColor = .lightGray
				UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.darkGray]
				UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = NSAttributedString(string: "Search Dropisle", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
				UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Book", size: 15)!, NSAttributedString.Key.foregroundColor: UIColor.black]
			}
		}

		searchTableView.delegate = self
		searchTableView.dataSource = self

		let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "Filter")?.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(filterSearch))
		rightBarButtonItem.tintColor = UIColor.white
		self.navigationItem.rightBarButtonItem = rightBarButtonItem

		guard let navigationController = navigationController else { return }
		navigationController.view.backgroundColor = .white

		///Update recent search table
		updatedRecentlySearchedTable()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.tabBarController?.tabBar.isHidden = false
		navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Avenir-Black", size: 20)!]

		if let tabBarController = self.tabBarController as? TabBarViewController {
		}

		///Analytics
		LoggerManager.shared.log(event: .search)

		//MARK: Disabling location check because i am getting location from Launch Screen and OnBoarding Screen
		///Check for user current location OR Use selected stored location
		///checkForUserLocation()
	}

	@objc func filterSearch() {
		///Analytics
		LoggerManager.shared.log(event: .showFilterScreen)

		let storyBoard: UIStoryboard = UIStoryboard(name: "FilterStoryboard", bundle: nil)
		if let newViewController = storyBoard.instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController {
			self.navigationController?.pushViewController(newViewController, animated: true)
		}
	}

	///Ignore delegate
	func openCountryList() { }

	///Check if location enabled and append users current location
	func checkForUserLocation() {
		switch CLLocationManager.authorizationStatus() {
			case .authorizedWhenInUse: location.startMonitoring()
				break
			case .denied: getLatLng()
				break
			case .notDetermined: getLatLng()
				break
			case .restricted: getLatLng()
				break
			default: break
		}
	}

	///get location if enabled
	func updateLocation() {
		_ = ("\(location.currentLocation?.coordinate.latitude ?? 0), \(location.currentLocation?.coordinate.longitude ?? 0)")
		if let coord = location.currentLocation?.coordinate {
			self.coordinates = coord
			location.stopMonitoring()
		}
	}

	///get location if not enabled
	func getLatLng() {
		if CurrentUserInfo.shared.userId().isEmpty {
			let countryLocale = NSLocale.current
			address = countryLocale.regionCode ?? ""
		} else {
			address = CurrentUserInfo.shared.city() + ", " + CurrentUserInfo.shared.country() + " " + CurrentUserInfo.shared.countryCode()
		}

		let geoCoder = CLGeocoder()
		geoCoder.geocodeAddressString(address) { (placemarks, error) in
			guard let placemarks = placemarks, let location = placemarks.first?.location else { return }
			print(location.coordinate)
			let coord = location.coordinate
			self.coordinates = coord
		}
	}

	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		showSearchResultScreen(value: searchBar.text ?? "", coordinates: coordinates)
		if let searchItem = searchController.searchBar.text {
			if defaults.object(forKey: "recentlySearchedItems") != nil {
				if let recentSearches = defaults.array(forKey: "recentlySearchedItems") as? [String] {
					recentlySearchedItems.append(contentsOf: recentSearches)
					self.recentSearches = recentlySearchedItems.removeDuplicates()
				}
			}
			recentSearches.append(searchItem)
		}
		searchController.searchBar.text = String()
		searchController.searchBar.placeholder = "Search Dropisle"

		///Store array of recently searched items
		defaults.set(recentSearches, forKey: "recentlySearchedItems")

		///Empty Array
		recentlySearchedItems.removeAll()
		recentSearches.removeAll()

		///Update recent search table
		updatedRecentlySearchedTable()
	}

	func updatedRecentlySearchedTable() {
		if let recentSearches = defaults.array(forKey: "recentlySearchedItems") as? [String] {
			storedRecentSearches.removeAll()
			storedRecentSearches.append(contentsOf: recentSearches)
			storedRecentSearches.reverse()
			searchTableView.reloadData()
		}
	}

	@objc func clearSearchHistory() {
		defaults.removeObject(forKey: "recentlySearchedItems")
		storedRecentSearches.removeAll()
		searchTableView.reloadData()
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return storedRecentSearches.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchViewCell") as? SearchViewCell else { return UITableViewCell() }
		let backgroundView = UIView()
		backgroundView.backgroundColor = .white
		cell.selectedBackgroundView = backgroundView
		cell.productName.text = storedRecentSearches[indexPath.row]
		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let cell = tableView.cellForRow(at: indexPath) as? SearchViewCell
		showSearchResultScreen(value: cell?.productName.text ?? "", coordinates: coordinates)
	}

	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let rect = CGRect(x: 15, y: 0, width: tableView.frame.size.width, height: 44)
		let title = UILabel()
		title.frame = rect
		title.font = UIFont(name: "Avenir-Heavy", size: 14)
		title.text = "Recently Searched"

		let headerView = UIView(frame: rect)
		headerView.addSubview(title)
		headerView.backgroundColor = .white
		return headerView
	}

	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 44
	}

	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		let rect = CGRect(x: 15, y: 0, width: tableView.frame.size.width, height: 44)
		clear.frame = rect
		clear.setTitle("Clear search history", for: .normal)
		clear.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 14)
		clear.setTitleColor(.lightGray, for: .normal)
		clear.addTarget(self, action: #selector(clearSearchHistory), for: .touchUpInside)

		let headerView = UIView(frame: rect)
		headerView.addSubview(clear)
		clear.center = headerView.center
		headerView.backgroundColor = .white
		return headerView
	}

	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 44
	}

	func showSearchResultScreen(value: String, coordinates: CLLocationCoordinate2D) {
		///Analytics
		LoggerManager.shared.log(event: .searchValues, parameters: ["userId": CurrentUserInfo.shared.userId(), "searchText": value])

		let myStoryBoard = UIStoryboard(name: "SearchResultStoryboard", bundle: Bundle.main)
		guard let popup = myStoryBoard.instantiateViewController(withIdentifier: "SearchResultViewController") as? SearchResultViewController else {
			return
		}
		popup.searchValue = value
		popup.coordinates = coordinates
		let navigationController = UINavigationController(rootViewController: popup)
		navigationController.navigationBar.isHidden = true
		if #available(iOS 13.0, *) {
			navigationController.modalPresentationStyle = .overFullScreen
		} else {
			// Fallback on earlier versions
			navigationController.modalPresentationStyle = .overFullScreen
		}
		navigationController.modalTransitionStyle = .crossDissolve
		self.present(navigationController, animated: true, completion: nil)
	}
}

extension UISearchBar {
	var customTextfield: UITextField? {
		if #available(iOS 13.0, *) {
			return self.searchTextField
		} else {
			// Fallback on earlier versions
			for view: UIView in (self.subviews[0]).subviews {
				if let textField = view as? UITextField {
					return textField
				}}}
		return nil
	}
}
