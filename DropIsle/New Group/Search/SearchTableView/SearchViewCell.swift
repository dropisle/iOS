//
//  SearchViewCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-12.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

class SearchViewCell: UITableViewCell, Reusable {

	@IBOutlet weak var productName: UILabel!
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		// selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
