//
//  CoutntriesController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-07-08.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol ModalHandler: class {
	func modalDismissed(cities: Countries)
}

class CountriesController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

	var filteredData = [Countries]()
	let cellId = "CountryCell"
	weak var delegate: ModalHandler?
	private var citiesController = "CitiesController"

	var countries = [Countries]() {
		didSet {
			DispatchQueue.main.async {
				self.filteredData = self.countries
				self.countryTable.reloadData()
			}
		}
	}

	@IBOutlet weak var countryTable: UITableView!
	@IBOutlet weak var searchBar: UISearchBar!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = "Countries"
		navigationController?.navigationBar.barTintColor = UIColor(hexString: "#3293FC")
		UINavigationBar.appearance().tintColor = UIColor.white
		UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

		searchBar.delegate = self
		searchBar.placeholder = "Search Country"
		searchBar.tintColor = .black
		countryTable.keyboardDismissMode = .onDrag

		fetchCountries()
	}

	func fetchCountries() {
		let countriesUrl = BaseAPIClient.urlFromPath(path: APIEndPoints.contries)
		APIManager.shared.fetchGenericData(method: .get,
																			 urlString: countriesUrl,
																			 params: nil,
																			 headers: nil) { (result: [Countries]) in
																				if result.count != 0 {
																					let sortedCountries = result.sorted(by: { $0.name < $1.name })
																					_ = sortedCountries.map { values in
																						self.countries.append(values)
																					}
																				} else {
																					self.alert(message: "Error getting list, please try again later.")
																				}
		}
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return filteredData.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? CountryCell

		let flag = emojiFlag(countryCode: filteredData[indexPath.row].code)
		cell?.countryNames.text = filteredData[indexPath.row].name.capitalized
		cell?.isoCodeFlag.text = flag
		return cell!
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		self.dismiss(animated: true) {
			self.delegate?.modalDismissed(cities: self.filteredData[indexPath.row])
		}
	}

	@IBAction func closeScreen(_ sender: UIBarButtonItem) {
		self.dismiss(animated: true, completion: nil)
	}
}

extension CountriesController {
	func emojiFlag(countryCode: String) -> String {
		var string = String()
		for uS in countryCode.unicodeScalars {
			string.unicodeScalars.append(UnicodeScalar(127397 + uS.value)!)
		}
		return string
	}

	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		if searchText == String() {
			self.filteredData = self.countries
		} else {
			filteredData = countries.filter({( country : Countries) -> Bool in
				guard let searchValue = searchBar.text else { return Bool() }
				return country.name.lowercased().contains(searchValue.lowercased())
			})
		}
		self.countryTable.reloadData()
	}

	func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
		self.searchBar.showsCancelButton = true
	}

	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		searchBar.showsCancelButton = false
		searchBar.text = ""
		searchBar.resignFirstResponder()
		self.filteredData = self.countries
		countryTable.reloadData()
	}

	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		searchBar.resignFirstResponder()
	}
}
