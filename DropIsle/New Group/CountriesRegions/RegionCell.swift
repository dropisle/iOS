//
//  RegionCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-05-06.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class RegionCell: UITableViewCell {

	@IBOutlet weak var regionNames: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
