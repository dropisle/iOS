//
//  CitiesController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-07-08.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol CitiesDelegate: class {
	func usersCity(country: String?, city: String?, code: String?)
	func setUserCity(city: String?, code: String?, country: String?)
}

class CitiesController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

	let cellId = "CitiesCell"
	var cities: Countries!
	var sortedCities = [String]()
	var filteredData = [String]()
	weak var delegate: CitiesDelegate?

	@IBOutlet weak var citiesTable: UITableView!
	@IBOutlet weak var searchBar: UISearchBar!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = "Region"
		navigationController?.navigationBar.barTintColor = UIColor(hexString: "#3293FC")
		UINavigationBar.appearance().tintColor = UIColor.white
		UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = .white
		navigationItem.leftBarButtonItem = backButton

		searchBar.delegate = self
		searchBar.placeholder = "Search Region"
		searchBar.tintColor = .black
		citiesTable.keyboardDismissMode = .onDrag

		///
		filteredData = cities.regions
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.dismiss(animated: true, completion: nil)
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return filteredData.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? CitiesCell
		sortedCities = filteredData.sorted(by: { $0 < $1 })
		cell?.citiesName.text = sortedCities[indexPath.row]
		return cell!
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let cell = tableView.cellForRow(at: indexPath) as? CitiesCell
		self.dismiss(animated: true) {
			if self.cities.code == "US" || self.cities.code == "CA" {
				self.delegate?.setUserCity(city: cell?.citiesName.text, code: self.cities.code, country: self.cities.name.capitalized)
			} else {
				self.delegate?.usersCity(country: self.cities.name.capitalized, city: self.sortedCities[indexPath.row], code: self.cities.code)
			}
		}
	}
}

extension CitiesController {
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		if searchText == String() {
			self.filteredData = self.cities.regions
			searchBar.resignFirstResponder()
		} else {
			filteredData = cities.regions.filter({( states : String) -> Bool in
				guard let searchValue = searchBar.text else { return Bool() }
				return states.lowercased().contains(searchValue.lowercased())
			})
		}
		self.citiesTable.reloadData()
	}

	func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
		self.searchBar.showsCancelButton = true
	}

	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		searchBar.showsCancelButton = false
		searchBar.text = ""
		searchBar.resignFirstResponder()
		self.filteredData = self.cities.regions
		citiesTable.reloadData()
	}

	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		searchBar.resignFirstResponder()
	}
}

