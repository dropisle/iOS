//
//  RegionController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-05-06.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol RegionDelegate: class {
	func getRegion(region: String?, code: String?, city: String?, country: String?)
}

class RegionController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

	let cellId = "RegionCell"
	weak var delegate: RegionDelegate?
	var provinceOrState = String()
	var countryCode = String()
	var country = String()
	var url = String()

	var filteredData = [RegionModel]()
	var regions = [RegionModel]() {
		didSet {
			DispatchQueue.main.async {
				self.filteredData = self.regions
				self.regionTable.reloadData()
			}
		}
	}

	@IBOutlet weak var regionTable: UITableView!
	@IBOutlet weak var searchBar: UISearchBar!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		super.viewDidLoad()
		self.title = "Cities"
		navigationController?.navigationBar.barTintColor = UIColor(hexString: "#3293FC")
		UINavigationBar.appearance().tintColor = UIColor.white
		UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = .white
		navigationItem.leftBarButtonItem = backButton

		searchBar.delegate = self
		searchBar.placeholder = "Search City"
		searchBar.tintColor = .black
		regionTable.keyboardDismissMode = .onDrag
		
		///get cities
		getRegion()
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.dismiss(animated: true, completion: nil)
	}

	func getRegion() {
		let urlString = String(format: APIEndPoints.region, countryCode, provinceOrState)
		let regionUrl = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .get, urlString: regionUrl, params: nil, headers: nil) { (result: [RegionModel]) in
			if result.count != 0 {
				let sortedCountries = result.sorted(by: { $0.name < $1.name })
				_ = sortedCountries.map { values in
					self.regions.append(values)
				}
			} else {
				self.alert(message: "Error getting list, please try again later.")
			}
		}
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return filteredData.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? RegionCell
		cell?.regionNames.text = filteredData[indexPath.row].name
		return cell!
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		self.dismiss(animated: true) {
			self.delegate?.getRegion(region: self.filteredData[indexPath.row].state, code: self.filteredData[indexPath.row].country, city: self.filteredData[indexPath.row].name, country: self.country)
		}
	}
}

extension RegionController {

	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		if searchText == String() {
			self.filteredData = self.regions
		} else {
			filteredData = regions.filter({( region: RegionModel) -> Bool in
				guard let searchValue = searchBar.text else { return Bool() }
				return region.name.lowercased().contains(searchValue.lowercased())
			})
		}
		self.regionTable.reloadData()
	}

	func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
		self.searchBar.showsCancelButton = true
	}

	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		searchBar.showsCancelButton = false
		searchBar.text = ""
		searchBar.resignFirstResponder()
		self.filteredData = self.regions
		regionTable.reloadData()
	}

	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		searchBar.resignFirstResponder()
	}
}

