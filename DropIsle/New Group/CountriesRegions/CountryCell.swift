//
//  CountryCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-07-08.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {

	@IBOutlet weak var countryNames: UILabel!
	@IBOutlet weak var isoCodeFlag: UILabel!

	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
