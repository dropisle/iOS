//
//  ProfileMenuCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-06-11.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class ProfileMenuCell: UITableViewCell {

	@IBOutlet weak var imgView: UIImageView!
	@IBOutlet weak var optionTitle: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
