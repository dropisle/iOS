//
//  PofileMenuController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-06-11.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

struct Options {
    let title: String
    let image: UIImage
}

protocol MenuActionDelegate: class {
	func openSettingsPage()
	func openDropCodePage()
	func openStatisticsPage()
}

class ProfileMenuController: UIViewController {

	weak var delegate: MenuActionDelegate?

	@IBOutlet weak var profileMenuTable: UITableView!
	@IBOutlet weak var customViewHeader: UIView!

	var tableOptions = [Options(title: "Setting", image: UIImage(named: "Settings")!), Options(title: "DropCode", image: UIImage(named: "qrCodeImage")!), Options(title: "My Statistics", image: UIImage(named: "stats")!)]

	static func instantiate() -> ProfileMenuController {
		return (UIStoryboard(name: "ProfilePage", bundle: nil).instantiateViewController(withIdentifier: "ProfileMenuController") as? ProfileMenuController)!
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		profileMenuTable.layer.backgroundColor = UIColor.clear.cgColor
		profileMenuTable.backgroundColor = .clear
		view.backgroundColor = UIColor.white.withAlphaComponent(1.10)

		customViewHeader.clipsToBounds = true
		customViewHeader.layer.cornerRadius = 20
		customViewHeader.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
		view.layer.cornerRadius = 20
		view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
	}
}

extension ProfileMenuController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return tableOptions.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileMenuCell" , for: indexPath) as? ProfileMenuCell else { return UITableViewCell() }
		cell.imgView.image = tableOptions[indexPath.row].image
		cell.optionTitle.text = tableOptions[indexPath.row].title
		cell.layer.backgroundColor = UIColor.clear.cgColor
		cell.backgroundColor = .clear
		cell.contentView.backgroundColor = UIColor.clear
    return cell
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if indexPath.row == 0 {
			delegate?.openSettingsPage()
			self.dismiss(animated: true, completion: nil)
		} else if  indexPath.row == 1 {
			delegate?.openDropCodePage()
			self.dismiss(animated: true, completion: nil)
		} else {
			delegate?.openStatisticsPage()
			self.dismiss(animated: true, completion: nil)
		}
  }
}
