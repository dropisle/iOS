//
//  OtherUserOnSaleViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-03-09.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SVProgressHUD

class OtherUserOnSaleViewController: UIViewController,
																															UICollectionViewDataSourcePrefetching,
																															UIViewControllerTransitioningDelegate,
																															IndicatorInfoProvider,
																															Reusable,
																															AddOthersListToWishListDelegate {

	var profileView: UIView?
	var viewConst = NSLayoutConstraint()
	var headerView = UIView()
	var userIdentifier = String()
	private var wishlistState = String()

	@IBOutlet weak var OtherUserOnSaleCollectionView: UICollectionView!

	var product = [ProductModel]()
	var content = [ProductModel.content]() {
		didSet {
			DispatchQueue.main.async {
				self.OtherUserOnSaleCollectionView.reloadData()
			}
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		//Set the CustomLayout delegate
		OtherUserOnSaleCollectionView.showsVerticalScrollIndicator = false
		OtherUserOnSaleCollectionView.backgroundColor = UIColor(hexString: "#f6f6f6")
		OtherUserOnSaleCollectionView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)

		if let layout = OtherUserOnSaleCollectionView?.collectionViewLayout as? CustomCollectionView {
			layout.delegate = self
		}

		OtherUserOnSaleCollectionView.delegate = self
		OtherUserOnSaleCollectionView.dataSource = self
		OtherUserOnSaleCollectionView.prefetchDataSource = self

		//get product by user id
		fetchProductsByOwner()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		///signUp events
		LoggerManager.shared.log(event: .otherUsersOnSale)
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "notifyWishlistScreen"), object: nil)
	}

	func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
		return IndicatorInfo(title: "Listings")
	}

	private func fetchProductsByOwner() {
		CurrentUserInfo.shared.userId().isEmpty ? getProductByOwnerId(page: "\(1)") : getProductByOwnerAndUserId(page: "\(1)")
	}

	private func getProductByOwnerId(page: String) {
		ProfileService.sharedInstance.getProductByOwnerId(userId: userIdentifier, page: page) {[weak self] (products) in
			guard let self = self else { return }
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
				self.product.removeAll()
				_ = products.content.filter { $0.status != APIEndPoints.soldSate }
				self.product.append(products)
				self.content.append(contentsOf: products.content)
			})
		}
	}

	private func getProductByOwnerAndUserId(page: String) {
		ProfileService.sharedInstance.getProductByOwnerAndUserId(userId: userIdentifier, page: page) {[weak self] (products) in
			guard let self = self else { return }
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
				self.product.removeAll()
				_ = products.content.filter { $0.status != APIEndPoints.soldSate}
				self.product.append(products)
				self.content.append(contentsOf: products.content)
			})
		}
	}
}

extension OtherUserOnSaleViewController {
	func addToWishList(cell: OtherUserOnSaleViewCell) {
		CurrentUserInfo.shared.userId().isEmpty ? CheckIfUserIsRegistered() : AddAndRemoveFromWishList(cell: cell)
	}

	func AddAndRemoveFromWishList(cell: OtherUserOnSaleViewCell) {

		//Animating button on click to verify action
		cell.addToWishListButton.bounceAnimation(value: 0.1)

		guard let indexPath = self.OtherUserOnSaleCollectionView.indexPath(for: cell) else {
			return
		}
		var dict = [String: String]()
		var jsonObj = [String: Any]()
		let cell = OtherUserOnSaleCollectionView.cellForItem(at: indexPath) as? OtherUserOnSaleViewCell
		let userId = CurrentUserInfo.shared.userId()
		guard let itemCode = cell?.product?.id else {
			return
		}

		if preferences[.addedOtherItemToWishlist].contains(itemCode) {
			self.wishlistState = "remove"
		}
		if !preferences[.addedOtherItemToWishlist].contains(itemCode) {
			self.wishlistState = "add"
		}

		dict.updateValue(itemCode, forKey: "itemcode")
		dict.updateValue(userId, forKey: "userid")
		dict.updateValue(wishlistState, forKey: "type")
		if let theJSONData = try?  JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
			if let jsonObject = try? JSONSerialization.jsonObject(with: theJSONData, options: []) as? [String: Any] {
				jsonObj = jsonObject
			}
		}
		HomeService.sharedInstance.addAndRemoveProductFromWishlist(params: jsonObj as [String: AnyObject]) { (result) in
			if result.code == 200 {
				
				//Check if wishlist already added by user
				if result.status == "added" {
					///Analytics
					let parameter = ["userId": CurrentUserInfo.shared.userId(), "productName": cell?.product?.name ?? "", "productCategory": cell?.product?.category ?? ""] as [String : Any]
					LoggerManager.shared.log(event: .addToWishlist, parameters: parameter)
					
					cell?.setAddToWishList(wishListImage: UIImage(named: "addToWishListSolid"))
					// Post a notification
					NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyWishlistScreen"), object: nil, userInfo: nil)

					if let productId = cell?.product?.id {
						preferences[.addedOtherItemToWishlist].append(productId)
					}
				} else {
					///Analytics
					let parameter = ["userId": CurrentUserInfo.shared.userId(), "productName": cell?.product?.name ?? "", "productCategory": cell?.product?.category ?? ""] as [String : Any]
					LoggerManager.shared.log(event: .removeFromWishlist, parameters: parameter)

					cell?.setAddToWishList(wishListImage: UIImage(named: "addToWishListEmpty"))
					if let productId = cell?.product?.id {
						let storedWishlistProductId = preferences[.addedOtherItemToWishlist]
						let availableProductId = storedWishlistProductId.filter { $0 != productId }
						preferences.removeObject(forKey: "addedOtherItemToWishlist")
						preferences[.addedToWishlist].append(contentsOf: availableProductId)
					}
				}
			} else {
				SVProgressHUD.dismiss(withDelay: 0.5)
			}
		}
	}
}

extension OtherUserOnSaleViewController: UICollectionViewDataSource {

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return content.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OtherUserOnSaleViewCell.identifier, for: indexPath) as? OtherUserOnSaleViewCell else {
			return UICollectionViewCell()
		}
		cell.setUpMargins()
		cell.delegate = self
		cell.product = content[indexPath.row]

		//Check if the product added to wishlist id's are in storage and update the button when scrolling.
		if let productId = cell.product?.id {
			if preferences[.addedOtherItemToWishlist].contains(productId) {
				cell.setAddToWishList(wishListImage: UIImage(named: "addToWishListSolid"))
			} 
		}
		return cell
	}

	func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
			//Begin asynchronously fetching data for the requested index paths.
			for indexPath in indexPaths {
				guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OtherUserOnSaleViewCell.identifier, for: indexPath) as? OtherUserOnSaleViewCell else {
					return
				}
				let model = content[indexPath.row]
				cell.product = model
			}
	}

	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		if indexPath.row == content.count - 1 {
			guard product.first?.numberOfElements == 10 else {
				return
			}
			if var pageNumber = self.product.first?.pageable.pageNumber {
				pageNumber += 2
					CurrentUserInfo.shared.userId().isEmpty ? getProductByOwnerId(page: "\(pageNumber)") : getProductByOwnerAndUserId(page: "\(pageNumber)")
			}
		}
	}

	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		if scrollView == self.OtherUserOnSaleCollectionView {
			viewConst.constant = 0
			UIView.animate(withDuration: 0.5, animations: { () -> Void in
				self.view.layoutIfNeeded()
			})
		}
	}

	func scrollViewDidScroll(_ scrollView: UIScrollView) {
	 let offsetY = scrollView.contentOffset.y
		if offsetY == 0.0 || offsetY < 0.0 {
			if scrollView == self.OtherUserOnSaleCollectionView {
				viewConst.constant = 103
				UIView.animate(withDuration: 0.5, animations: { () -> Void in
					self.view.layoutIfNeeded()
				})
			}
		}
	}
}

extension OtherUserOnSaleViewController: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let cell = collectionView.cellForItem(at: indexPath) as? OtherUserOnSaleViewCell

		///Analytics
		let parameter = ["productName": cell?.product?.name, "productCategory": cell?.product?.category]
		LoggerManager.shared.log(event: .showDetailScreen, parameters: parameter as [String : Any])
		
		let storyBoard: UIStoryboard = UIStoryboard(name: "ProductDetailPage", bundle: nil)
		let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProductDetailPageController") as! ProductDetailPageController
		newViewController.productObject = cell?.product
		navigationController?.pushViewController(newViewController, animated: true)
	}
}

extension OtherUserOnSaleViewController: HomeScreenLayoutDelegate {
	func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
		if UIDevice.current.userInterfaceIdiom == .pad {
				guard let height = content[exist: indexPath.item]  else { return 550 }
					guard let imageHeight = NumberFormatter().number(from: height.photo.height) else { return 0 }
					let expectedHeight = CGFloat(truncating: imageHeight)
					if (expectedHeight) > 550 {
						return 550
					} else {
						return 550
					}
			}

			guard let height = content[exist: indexPath.item]  else { return 250 }
			guard let imageHeight = NumberFormatter().number(from: height.photo.height) else { return 0 }
			let expectedHeight = CGFloat(truncating: imageHeight)
			if (expectedHeight) > 250 {
				return 250
			} else {
				return 250
			}
	}

	func collectionView(_ collectionView: UICollectionView, widthForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
		return 300
	}
}

extension OtherUserOnSaleViewController {
	private func CheckIfUserIsRegistered() {
		///Show Guest User Screen
		LoggerManager.shared.log(event: .guestUserCreateOrSignInScreen)

		let storyboard = UIStoryboard(name: "GuestUserSignUpStoryboard", bundle: nil)
		if let pvc = storyboard.instantiateVC() as? GuestSignUpPanelViewController {
			pvc.modalPresentationStyle = UIModalPresentationStyle.custom
			pvc.modalTransitionStyle = .crossDissolve
			pvc.transitioningDelegate = self
			self.present(pvc, animated: true, completion: nil)
		}
	}
}
