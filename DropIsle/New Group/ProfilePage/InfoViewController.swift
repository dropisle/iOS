//
//  InfoViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-06.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class InfoViewController: UIViewController, IndicatorInfoProvider {

	var otherUserInfo = Bool()
	var memberSinceDateTime = String()

	@IBOutlet weak var conversationView: UIView!
	@IBOutlet weak var memberSince: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()
		if otherUserInfo {
			conversationView.isHidden = true
		}
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		let date = "\(GlobalProfileInfo.memberSince)"
		let memberSinceDate = date.toDate()

		let dateFormatter = DateFormatter()
		dateFormatter.dateStyle = .medium
		dateFormatter.timeStyle = .none
		let value = dateFormatter.string(from: memberSinceDate)
		memberSince.setTitle("Member since \(value)", for: .normal)
	}

	func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
		return IndicatorInfo(title: "Info")
	}
}

extension String {
  func toDate(withFormat format: String = "yyyyMMdd HH:mm:ss") -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    guard let date = dateFormatter.date(from: self) else {
			return Date()
    }
    return date
  }
}
