//
//  ProfilePanelViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-04-27.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol DeleteItemDelegate: class {
	func deleteItem(onSaleCell: OnSaleViewCell, wishlistCell: WishlistCell)
}

class ProfilePanelViewController: UIViewController {

	@IBOutlet weak var noButton: UIButton!

	weak var delegate: DeleteItemDelegate!
	var onSaleCell = OnSaleViewCell()
	var wishlistCell = WishlistCell()

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		noButton.layer.cornerRadius = 5
		noButton.layer.borderWidth = 1
		noButton.layer.borderColor = UIColor.lightGray.cgColor
	}

	@IBAction func yesAction(_ sender: UIButton) {
		self.delegate.deleteItem(onSaleCell: onSaleCell, wishlistCell: wishlistCell)
		self.dismiss(animated: true, completion: nil)
	}

	@IBAction func noAction(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}

	@IBAction func closeAction(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		self.dismiss(animated: true, completion: nil)
	}
}
