//
//  OnSaleViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-06.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import DropDown
import SVProgressHUD

class OnSaleViewController: UIViewController, UICollectionViewDataSourcePrefetching, IndicatorInfoProvider, Reusable, MoreActionDelegate, DeleteItemDelegate {

	var viewConst = NSLayoutConstraint()
	var profileView: UIView?
	var userIdentifier = String()
	private var emptyBackgroundView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
	private var dropDown = DropDown()
	private var appearance = DropDown.appearance()

	@IBOutlet weak var OnSaleCollectionView: UICollectionView!

	var product = [ProductModel]()
	var content = [ProductModel.content]() {
		didSet {
			DispatchQueue.main.async {
				self.OnSaleCollectionView.reloadData()
			}
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		OnSaleCollectionView.showsVerticalScrollIndicator = false
		OnSaleCollectionView.backgroundColor = UIColor(hexString: "#f6f6f6")
		OnSaleCollectionView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
		if let layout = OnSaleCollectionView?.collectionViewLayout as? CustomCollectionView {
			layout.delegate = self
		}

		OnSaleCollectionView.delegate = self
		OnSaleCollectionView.dataSource = self
		OnSaleCollectionView.prefetchDataSource = self

		//Drop down apperance
		customizeDropDown()

		//get product by user id if coming from detail page or nav bar click
		userIdentifier.isEmpty ? getProducts(userId: CurrentUserInfo.shared.userId(), page: "\(1)") : getProducts(userId: userIdentifier, page: "\(1)")

		// Set up background view if table is empty
		self.setBackgroundView()
	}

	func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
		return IndicatorInfo(title: "Listings")
	}

	private func getProducts(userId: String, page: String) {
		ProfileService.sharedInstance.getProductByOwnerId(userId: userId, page: page) {[weak self] (products) in
			guard let self = self else { return }
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
				self.product.removeAll()
				self.product.append(products)
				self.content.append(contentsOf: products.content)
			})
		}
	}
}

extension OnSaleViewController {
	func customizeDropDown() {
		appearance.cellHeight = 45
		appearance.backgroundColor = UIColor(white: 1, alpha: 1)
		appearance.selectionBackgroundColor = .clear
		appearance.separatorColor = UIColor(white: 0.0, alpha: 0.1)
//		appearance.cornerRadius = 0.0
//		appearance.shadowOpacity = 0.9
//		appearance.shadowRadius = 20
		appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
		appearance.animationduration = 0.30
		if let font = UIFont(name: "Avenir", size: 14.0) {
			appearance.textFont = font
		}
	}

	func setupDropDownForAvailableState(cell: OnSaleViewCell) {
		self.appearance.textColor = .darkGray
		dropDown.bottomOffset = CGPoint(x: -75, y: 0)
		dropDown.anchorView = cell.moreButton
		dropDown.width = 100
		dropDown.dataSource = ["Edit", "Delete", "Sold"]
		dropDown.cellNib = UINib(nibName: "CountryDropDownCell", bundle: nil)
		dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
			guard let cell = cell as? CountryDropDownCell else { return }
			if index == 0 {
				cell.countryFlagImage.image = UIImage(named: "Edit")?.withRenderingMode(.alwaysTemplate)
				cell.countryFlagImage.tintColor = .lightGray
			} else if index == 1 {
				cell.countryFlagImage.image = UIImage(named: "Delete")?.withRenderingMode(.alwaysTemplate)
				cell.countryFlagImage.tintColor = .lightGray
			} else {
				cell.countryFlagImage.image = UIImage(named: "Sold")?.withRenderingMode(.alwaysTemplate)
				cell.countryFlagImage.tintColor = .lightGray
			}
		}
		// Action triggered on selection
		dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
			guard let indexPath = self.OnSaleCollectionView.indexPath(for: cell) else {
				return
			}
			guard let cell = self.OnSaleCollectionView.cellForItem(at: indexPath) as? OnSaleViewCell else {
				return
			}
			if index == 0 {
				//Edit product
				//_ = self.tabBarController?.selectedIndex = TabBarViewController.TabBarItem.sellItem.rawValue

				let storyBoard: UIStoryboard = UIStoryboard(name: "ListItemStoryboard", bundle: nil)
				if let addProduct = storyBoard.instantiateViewController(withIdentifier: "ListItemController") as? ListItemController {
					addProduct.productToEdit = cell.product
					let nav = UINavigationController(rootViewController: addProduct)
					nav.modalPresentationStyle = UIModalPresentationStyle.custom
					nav.modalTransitionStyle = .coverVertical
					nav.transitioningDelegate = self
					self.present(nav, animated: true, completion: nil)
				}


//				let navController = self.tabBarController?.viewControllers?[2] as! UINavigationController
//				let sellItems = navController.topViewController as? ListItemController
//				sellItems?.productToEdit = cell.product
			} else if index == 1 {
				let storyboard = UIStoryboard(name: "ProfilePanelStoryboard", bundle: nil)
				if let pvc = storyboard.instantiateViewController(withIdentifier: "ProfilePanelViewController") as? ProfilePanelViewController {
					pvc.delegate = self
					pvc.onSaleCell = cell
					pvc.modalPresentationStyle = UIModalPresentationStyle.custom
					pvc.modalTransitionStyle = .crossDissolve
					pvc.transitioningDelegate = self
					self.present(pvc, animated: true, completion: nil)
				}
			} else {
				guard let indexPath = self.OnSaleCollectionView.indexPath(for: cell) else {
					return
				}
				guard let cell = self.OnSaleCollectionView.cellForItem(at: indexPath) as? OnSaleViewCell else {
					return
				}
				let alertController = UIAlertController(title: "Confirm item is Sold", message: "You will no longer be able to edit or receive messages regarding this item.", preferredStyle: .alert)
				let oKAction = UIAlertAction(title: "Mark Sold", style: .default) { (_: UIAlertAction) in
					self.markAsSold(cell: cell)
				}
				let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
				alertController.addAction(oKAction)
				alertController.addAction(cancelAction)
				self.present(alertController, animated: true, completion: nil)
			}
		}
	}

	func setupDropDownForUnavailableState(cell: OnSaleViewCell) {
		self.appearance.textColor = .darkGray
		dropDown.bottomOffset = CGPoint(x: -75, y: 20)
		dropDown.anchorView = cell.moreButton
		dropDown.width = 100
		dropDown.dataSource = ["Delete"]
		dropDown.cellNib = UINib(nibName: "CountryDropDownCell", bundle: nil)
		dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
			guard let cell = cell as? CountryDropDownCell else { return }
			if index == 0 {
				cell.countryFlagImage.image = UIImage(named: "Delete")?.withRenderingMode(.alwaysTemplate)
				cell.countryFlagImage.tintColor = .lightGray
			}
		}
		// Action triggered on selection
		dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
			guard let indexPath = self.OnSaleCollectionView.indexPath(for: cell) else {
				return
			}
			let cell = self.OnSaleCollectionView.cellForItem(at: indexPath) as! OnSaleViewCell
			if index == 0 {
				let storyboard = UIStoryboard(name: "ProfilePanelStoryboard", bundle: nil)
				if let pvc = storyboard.instantiateViewController(withIdentifier: "ProfilePanelViewController") as? ProfilePanelViewController {
					pvc.delegate = self
					pvc.onSaleCell = cell
					pvc.modalPresentationStyle = UIModalPresentationStyle.custom
					pvc.modalTransitionStyle = .crossDissolve
					pvc.transitioningDelegate = self
					self.present(pvc, animated: true, completion: nil)
				}
			}
		}
	}

	func showMoreOptions(cell: OnSaleViewCell) {
		cell.product?.status == APIEndPoints.soldSate ? setupDropDownForUnavailableState(cell: cell) : setupDropDownForAvailableState(cell: cell)
		dropDown.show()
	}

	///Delegate to delete item from onSale class
	func deleteItem(onSaleCell: OnSaleViewCell, wishlistCell: WishlistCell) {
		if let indexpath = OnSaleCollectionView.indexPath(for: onSaleCell) {
			let params: [String: AnyObject] = ["itemid": onSaleCell.product?.id as AnyObject, "userid": CurrentUserInfo.shared.userId() as AnyObject]
			ProfileService.sharedInstance.removeUserUploadedItem(params: params) { (result) in
				if result.code == 200 {
					let indexPath = IndexPath(item: indexpath.item, section: 0)
					self.content.remove(at: indexPath.item)
					self.OnSaleCollectionView.performBatchUpdates({
						self.OnSaleCollectionView.deleteItems(at: [indexPath])
					}) { (_) in
						self.OnSaleCollectionView.reloadItems(at: self.OnSaleCollectionView.indexPathsForVisibleItems)
					}
				} else {
					SVProgressHUD.showError(withStatus: "Please try again \nSomething went wrong")
				}
			}
		}
	}
	
	private func markAsSold(cell: OnSaleViewCell) {
		let userId = CurrentUserInfo.shared.userId()
		guard let itemid =  cell.product?.id else {
			return
		}
		let params: [String: String] = ["status": APIEndPoints.soldSate, "userid": userId, "itemid": itemid]
		ProfileService.sharedInstance.editUploadedItem(params: params as [String: AnyObject]) { (result) in
			if result.code == 200 {
				cell.onSaleBackgroundView.isHidden = false
				preferences[.itemMarkedAsSold].append(cell.product?.id ?? "")
			} else {
				SVProgressHUD.showError(withStatus: "Please try again \nSomething went wrong")
			}
		}
	}
}

extension OnSaleViewController: UIViewControllerTransitioningDelegate {
	func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
		return FullSizePresentationController(presentedViewController: presented, presenting: presenting)
	}
}

extension OnSaleViewController: UICollectionViewDataSource {

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if (self.content.count == 0) {
			DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
				guard self.content.count != 0 else {
					self.OnSaleCollectionView.backgroundView = self.emptyBackgroundView
					return
				}
			}
		} else {
			OnSaleCollectionView.backgroundView = nil
		}
		return content.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OnSaleViewCell.identifier, for: indexPath) as? OnSaleViewCell else {
			return UICollectionViewCell()
		}
		cell.setUpMargins()
		cell.delegate = self
		cell.product = content[indexPath.row]

		if let productId = cell.product?.id {
			if preferences[.itemMarkedAsSold].contains(productId) {
				cell.onSaleBackgroundView.isHidden = false
			}
		}
		return cell
	}

	func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
		///Begin asynchronously fetching data for the requested index paths.
		for indexPath in indexPaths {
			guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OnSaleViewCell.identifier, for: indexPath) as? OnSaleViewCell else {
				return
			}
			let model = content[indexPath.row]
			cell.product = model
		}
	}

	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		if indexPath.row == content.count - 1 {
			guard product.first?.numberOfElements == 10 else {
				return
			}
			if var pageNumber = self.product.first?.pageable.pageNumber {
				pageNumber += 2
				userIdentifier.isEmpty ? getProducts(userId: CurrentUserInfo.shared.userId(), page: "\(pageNumber)") : getProducts(userId: userIdentifier, page: "\(pageNumber)")
			}
		}
	}

	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		if scrollView == self.OnSaleCollectionView {
			viewConst.constant = 0
			UIView.animate(withDuration: 0.5, animations: { () -> Void in
				self.view.layoutIfNeeded()
			})
		}
	}

	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let offsetY = scrollView.contentOffset.y
		if offsetY == 0.0 || offsetY < 0.0 {
			if scrollView == self.OnSaleCollectionView {
				viewConst.constant = 103
				UIView.animate(withDuration: 0.5, animations: { () -> Void in
					self.view.layoutIfNeeded()
				})
			}
		}
	}
}

extension OnSaleViewController: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let cell = collectionView.cellForItem(at: indexPath) as! OnSaleViewCell
		let storyBoard: UIStoryboard = UIStoryboard(name: "ProductDetailPage", bundle: nil)
		if let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProductDetailPageController") as? ProductDetailPageController {
			newViewController.productObject = cell.product
			self.navigationController?.pushViewController(newViewController, animated: true)
		}
	}
}

extension OnSaleViewController: HomeScreenLayoutDelegate {
	func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
		if UIDevice.current.userInterfaceIdiom == .pad {
			guard let height = content[exist: indexPath.item]   else { return 550 }
			guard let imageHeight = NumberFormatter().number(from: height.photo.height) else { return 0 }
			let expectedHeight = CGFloat(truncating: imageHeight)
			if (expectedHeight) > 550 {
				return 550
			} else {
				return 550
			}
		}

		guard let height = content[exist: indexPath.item]   else { return 250 }
		guard let imageHeight = NumberFormatter().number(from: height.photo.height) else { return 0 }
		let expectedHeight = CGFloat(truncating: imageHeight)
		if (expectedHeight) > 250 {
			return 250
		} else {
			return 250
		}
	}

	func collectionView(_ collectionView: UICollectionView, widthForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
		return 300
	}
}

extension OnSaleViewController {
	private func setBackgroundView() {
		let stackView = UIStackView()
		emptyBackgroundView.backgroundColor = UIColor.white
		let view1 = UIImageView.imageAttribute(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
		view1.heightAnchor.constraint(equalToConstant: 100).isActive = true
		view1.widthAnchor.constraint(equalToConstant: 100).isActive = true

		let view2 = UILabel.attributedLabel(frame: CGRect(x: 0, y: 0, width: 278, height: 48))
		view2.heightAnchor.constraint(equalToConstant: 48).isActive = true
		view2.widthAnchor.constraint(equalToConstant: 278).isActive = true

		let view3 = UIButton.attributedButton(frame: CGRect(x: 0, y: 0, width: 108, height: 29))
		view3.addTarget(self, action: #selector(goToListingScreen), for: .touchDown)
		view3.heightAnchor.constraint(equalToConstant: 29).isActive = true
		view3.widthAnchor.constraint(equalToConstant: 108).isActive = true

		stackView.axis = .vertical
		stackView.distribution = .equalSpacing
		stackView.alignment = .center
		stackView.spacing = 8

		stackView.addArrangedSubview(view1)
		stackView.addArrangedSubview(view2)
		///stackView.addArrangedSubview(view3)

		stackView.translatesAutoresizingMaskIntoConstraints = false
		emptyBackgroundView.addSubview(stackView)
		stackView.centerXAnchor.constraint(equalTo: emptyBackgroundView.centerXAnchor).isActive = true
		stackView.centerYAnchor.constraint(equalTo: emptyBackgroundView.centerYAnchor).isActive = true
	}

	@objc func goToListingScreen() {
	}
}
