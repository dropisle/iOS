//
//  ProfileService.swift
//  DropIsle
//
//  Created by CtanLI on 2019-09-24.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

class ProfileService {
	static let sharedInstance = ProfileService()
	func getProductByOwnerId(userId: String, page: String, completion: @escaping (ProductModel) -> Void) {
		_ = APIEndPoints.init()
		let urlString = String(format: APIEndPoints.getProductByOwnerId, userId, page)
		let getProductByUserId = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .get, urlString: getProductByUserId, params: nil, headers: nil) { (response: ProductModel ) in
			completion(response)
		}
	}

	func getProductByOwnerAndUserId(userId: String, page: String, completion: @escaping (ProductModel) -> Void) {
		_ = APIEndPoints.init()
		let urlString = String(format: APIEndPoints.getProductByOwnerAndUserId, userId, CurrentUserInfo.shared.userId(), page)
		let getProductByUserId = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .get, urlString: getProductByUserId, params: nil, headers: nil) { (response: ProductModel ) in
			completion(response)
		}
	}

	func getUserWislist(userId: String, page: String, completion: @escaping (ProductModel) -> Void) {
		_ = APIEndPoints.init()
		let urlString = String(format: APIEndPoints.getUserWishlist, userId, page)
		let getUserWislist = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .get, urlString: getUserWislist, params: nil, headers: APIEndPoints.authorizationHeader) { (response: ProductModel) in
			completion(response)
		}
	}

	func removeUserUploadedItem(params: [String: AnyObject], completion: @escaping (RemoveItemModel) -> Void) {
		_ = APIEndPoints.init()
		let urlString = String(format: APIEndPoints.removeUserUploadedItem)
		let removeUserUploadedItem = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .post, urlString: removeUserUploadedItem, params: params, headers: APIEndPoints.authorizationHeader) { (response: RemoveItemModel) in
				completion(response)
		}
	}

	func getUserDetails(userId: String, completion: @escaping (UserDetailsModel) -> Void) {
		_ = APIEndPoints.init()
		let urlString = String(format: APIEndPoints.getUserDetails, userId)
		let userDetails = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .get, urlString: userDetails, params: nil, headers: nil) { (response: UserDetailsModel) in
				completion(response)
		}
	}

	func editUploadedItem(params: [String: AnyObject], completion: @escaping (EditProductModel) -> Void) {
		_ = APIEndPoints.init()
		let urlString = String(format: APIEndPoints.editProduct)
		let editProduct = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .post, urlString: editProduct, params: params, headers: APIEndPoints.authorizationHeader) { (response: EditProductModel) in
				completion(response)
		}
	}
}
