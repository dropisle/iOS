//
//  WishListViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-06.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SVProgressHUD

class WishListViewController: UIViewController, UICollectionViewDataSourcePrefetching, IndicatorInfoProvider, DeleteFromWishlistDelegate, DeleteItemDelegate, UIViewControllerTransitioningDelegate {

	var viewConst = NSLayoutConstraint()
	var profileView: UIView?
	var userIdentifier = String()
	var emptyBackgroundView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))

	@IBOutlet weak var wishlistCollectionView: UICollectionView!

	var product = [ProductModel]()
	var content = [ProductModel.content]() {
		didSet {
			DispatchQueue.main.async {
				self.wishlistCollectionView.reloadData()
			}
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
			NotificationCenter.default.addObserver(self, selector: #selector(upDateScreen), name: NSNotification.Name(rawValue: "notifyWishlistScreen"), object: nil)
			setBackgroundView()

			//Set the CustomLayout delegate
			wishlistCollectionView.showsVerticalScrollIndicator = false
			wishlistCollectionView.backgroundColor = UIColor(hexString: "#f6f6f6")
			wishlistCollectionView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
			if let layout = wishlistCollectionView?.collectionViewLayout as? CustomCollectionView {
				layout.delegate = self
			}

			wishlistCollectionView.delegate = self
			wishlistCollectionView.dataSource = self
			wishlistCollectionView.prefetchDataSource = self

			// get wishlist
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
				self.getUserWishlist(page: "\(1)")
			})
			//headerView.translatesAutoresizingMaskIntoConstraints = false
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		///signUp events
		LoggerManager.shared.log(event: .wishlist)
	}

	@objc func upDateScreen() {
		getUserWishlist(page: "\(1)")
	}

	func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
		return IndicatorInfo(title: "Wishlist")
	}

	private func getUserWishlist(page: String) {
		let userId = CurrentUserInfo.shared.userId()
		ProfileService.sharedInstance.getUserWislist(userId: userId, page: page) {[weak self] (products) in
			guard let self = self else { return }
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
				self.product.removeAll()
				 _ = products.content.filter { $0.status != APIEndPoints.soldSate}
				self.content.append(contentsOf: products.content)
				self.product.append(products)
			})
		}
	}
}

extension WishListViewController: UICollectionViewDataSource {

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if (self.content.count == 0) {
			DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
			guard self.content.count != 0 else {
				self.wishlistCollectionView.backgroundView = self.emptyBackgroundView
				return
				}
			}
		} else {
			self.wishlistCollectionView.backgroundView = nil
		}
		return content.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WishlistCell.identifier, for: indexPath) as? WishlistCell else {
			return UICollectionViewCell()
		}
		cell.setUpMargins()
		cell.product = content[indexPath.row]
		cell.delegate = self
		return cell
	}

	func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
			//Begin asynchronously fetching data for the requested index paths.
			for indexPath in indexPaths {
				guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WishlistCell.identifier, for: indexPath) as? WishlistCell else {
					return
				}
				let model = content[indexPath.row]
				cell.product = model
			}
	}

	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		if indexPath.row == content.count - 1 {
			guard product.first?.numberOfElements == 10 else {
				return
			}
			if var pageNumber = self.product.first?.pageable.pageNumber {
				pageNumber += 2

				///load more wishlist
				getUserWishlist(page: "\(pageNumber)")
			}
		}
	}

//	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//		if scrollView == self.wishlistCollectionView {
//			viewConst.constant = 0
//			UIView.animate(withDuration: 0, animations: { () -> Void in
//				self.view.layoutIfNeeded()
//			})
//		}
//	}

	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let offsetY = scrollView.contentOffset.y
		if offsetY == 0.0 || offsetY < 0.0 {
			if scrollView == self.wishlistCollectionView {
				viewConst.constant = 103
				UIView.animate(withDuration: 0, animations: { () -> Void in
					self.view.layoutIfNeeded()
				})
			}
		} else {
			viewConst.constant = 0
			UIView.animate(withDuration: 0, animations: { () -> Void in
				self.view.layoutIfNeeded()
			})
		}
	}

	///Delete item from wishlist
	func deleteFromWishlist(cell: WishlistCell) {
		let storyboard = UIStoryboard(name: "ProfilePanelStoryboard", bundle: nil)
		if let pvc = storyboard.instantiateViewController(withIdentifier: "ProfilePanelViewController") as? ProfilePanelViewController {
			pvc.delegate = self
			pvc.wishlistCell = cell
			pvc.modalPresentationStyle = UIModalPresentationStyle.custom
			pvc.modalTransitionStyle = .crossDissolve
			pvc.transitioningDelegate = self
			self.present(pvc, animated: true, completion: nil)
		}
	}

	func deleteItem(onSaleCell: OnSaleViewCell, wishlistCell: WishlistCell) {
		guard let indexPath = self.wishlistCollectionView.indexPath(for: wishlistCell) else {
			return
		}
		var params = [String: String]()
		let cell = wishlistCollectionView.cellForItem(at: indexPath) as? WishlistCell
		let userId = CurrentUserInfo.shared.userId()
		guard let itemCode = cell?.product?.id else {
			return
		}
		params.updateValue(itemCode, forKey: "itemcode")
		params.updateValue(userId, forKey: "userid")
		params.updateValue("removed", forKey: "type")
		SVProgressHUD.show()
		HomeService.sharedInstance.addAndRemoveProductFromWishlist(params: params as [String: AnyObject]) { (result) in
			if result.code == 200 {
				SVProgressHUD.dismiss(withDelay: 0.2)
				let indexPath = IndexPath(item: indexPath.item, section: 0)
				self.content.remove(at: indexPath.item)
				self.wishlistCollectionView.performBatchUpdates({
					self.wishlistCollectionView.deleteItems(at: [indexPath])
			   }) { (_) in
				   self.wishlistCollectionView.reloadItems(at: self.wishlistCollectionView.indexPathsForVisibleItems)
			   }

				///Remove product id from wishlist arrays
				if let productId = cell?.product?.id {
					let storedWishlistProductId = preferences[.addedToWishlist]
					if storedWishlistProductId.contains(productId) {
						let availableProductId = storedWishlistProductId.filter { $0 != productId }
						preferences.removeObject(forKey: "addedToWishlist")
						preferences[.addedToWishlist].append(contentsOf: availableProductId)
					}

					let storedSearchWishlistProductId = preferences[.addedSearchedToWishlist]
					if storedSearchWishlistProductId.contains(productId) {
						let availableProductId = storedSearchWishlistProductId.filter { $0 != productId }
						preferences.removeObject(forKey: "addedSearchedToWishlist")
						preferences[.addedSearchedToWishlist].append(contentsOf: availableProductId)
					}

					let storedOtherWishlistProductId = preferences[.addedOtherItemToWishlist]
					if storedOtherWishlistProductId.contains(productId) {
						let availableProductId = storedOtherWishlistProductId.filter { $0 != productId }
						preferences.removeObject(forKey: "addedOtherItemToWishlist")
						preferences[.addedOtherItemToWishlist].append(contentsOf: availableProductId)
					}

					let storedSimilarWishlistProductId = preferences[.similarProductsItemToWishlist]
					if storedSimilarWishlistProductId.contains(productId) {
						let availableProductId = storedSimilarWishlistProductId.filter { $0 != productId }
						preferences.removeObject(forKey: "similarProductsItemToWishlist")
						preferences[.similarProductsItemToWishlist].append(contentsOf: availableProductId)
					}
				}
			} else {
				SVProgressHUD.dismiss(withDelay: 0.2)
			}
		}
	}
}

extension WishListViewController: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let cell = collectionView.cellForItem(at: indexPath) as? WishlistCell

		///Analytics
		let parameter = ["productName": cell?.product?.name, "productCategory": cell?.product?.category]
		LoggerManager.shared.log(event: .showDetailScreen, parameters: parameter as [String : Any])

		let storyBoard: UIStoryboard = UIStoryboard(name: "ProductDetailPage", bundle: nil)
		if let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProductDetailPageController") as? ProductDetailPageController {
			newViewController.productObject = cell?.product
			self.navigationController?.pushViewController(newViewController, animated: true)
		}
	}
}

extension WishListViewController: HomeScreenLayoutDelegate {
	func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
		if UIDevice.current.userInterfaceIdiom == .pad {
				guard let height = content[exist: indexPath.item]  else { return 550 }
					guard let imageHeight = NumberFormatter().number(from: height.photo.height) else { return 0 }
					let expectedHeight = CGFloat(truncating: imageHeight)
					if (expectedHeight) > 550 {
						return 550
					} else {
						return 550
					}
			}

			guard let height = content[exist: indexPath.item]  else { return 250 }
			guard let imageHeight = NumberFormatter().number(from: height.photo.height) else { return 0 }
			let expectedHeight = CGFloat(truncating: imageHeight)
			if (expectedHeight) > 250 {
				return 250
			} else {
				return 250
			}
	}

	func collectionView(_ collectionView: UICollectionView, widthForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
		return 300
	}
}

extension WishListViewController {
	private func setBackgroundView() {
		let stackView = UIStackView()
		emptyBackgroundView.backgroundColor = UIColor.white
		let view1 = UIImageView.wishImageAttribute(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
		view1.heightAnchor.constraint(equalToConstant: 100).isActive = true
		view1.widthAnchor.constraint(equalToConstant: 100).isActive = true

		let view2 = UILabel.wishAttributedLabel(frame: CGRect(x: 0, y: 0, width: 278, height: 48))
		view2.heightAnchor.constraint(equalToConstant: 48).isActive = true
		view2.widthAnchor.constraint(equalToConstant: 278).isActive = true

		let view3 = UIButton.wishAttributedButton(frame: CGRect(x: 0, y: 0, width: 130, height: 29))
		view3.addTarget(self, action: #selector(goToHomeScreen), for: .touchDown)
		view3.heightAnchor.constraint(equalToConstant: 29).isActive = true
		view3.widthAnchor.constraint(equalToConstant: 130).isActive = true

		stackView.axis = .vertical
		stackView.distribution = .equalSpacing
		stackView.alignment = .center
		stackView.spacing = 8

		stackView.addArrangedSubview(view1)
		stackView.addArrangedSubview(view2)
		stackView.addArrangedSubview(view3)

		stackView.translatesAutoresizingMaskIntoConstraints = false
		emptyBackgroundView.addSubview(stackView)
		stackView.centerXAnchor.constraint(equalTo: emptyBackgroundView.centerXAnchor).isActive = true
		stackView.centerYAnchor.constraint(equalTo: emptyBackgroundView.centerYAnchor).isActive = true
	}

	@objc func goToHomeScreen() {
	}
}
