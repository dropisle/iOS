//
//  ProfileViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-06.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SVProgressHUD
import Alamofire
import SDWebImage
import SwiftMessages

struct GlobalProfileInfo {
	static var memberSince = String()
	static var userNameForShareLink = String()
}

class ProfileViewController: ButtonBarPagerTabStripViewController, MenuActionDelegate {

	var userIdentifier = String()
	var memberSinceDateTime = String()

	@IBOutlet weak var userName: UILabel!
	@IBOutlet weak var userProfileImage: UIImageView!
	@IBOutlet weak var containerViewConstraint: NSLayoutConstraint!
	@IBOutlet weak var headerView: UIView!
	@IBOutlet weak var userLocation: UILabel!

	override func viewDidLoad() {
		navigationItem.title = "Profile"		
		NotificationCenter.default.addObserver(self, selector: #selector(updateProfileInfo), name: NSNotification.Name(rawValue: "userUpdated"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(reloadOnSaleOnNewUpload), name: NSNotification.Name(rawValue: "newListingAdded"), object: nil)

		settings.style.selectedBarBackgroundColor = UIColor(hexString: "#4c4c4c")
		settings.style.buttonBarItemBackgroundColor = .white
		settings.style.buttonBarItemFont = UIFont(name: "Avenir-Medium", size: 18)!
		settings.style.selectedBarHeight = 1.5
		settings.style.buttonBarMinimumLineSpacing = 0
		settings.style.buttonBarItemsShouldFillAvailableWidth = true
		settings.style.buttonBarLeftContentInset = 0
		settings.style.buttonBarRightContentInset = 0
		super.viewDidLoad()

		self.userProfileImage.layer.borderWidth = 5
		self.userProfileImage.layer.borderColor = UIColor.white.cgColor

		changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
			guard changeCurrentIndex == true else { return }
			oldCell?.label.textColor = .gray
			newCell?.label.textColor = UIColor(hexString: "#4c4c4c")

			if animated {
				UIView.animate(withDuration: 0.1, animations: { () -> Void in
					newCell?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
					oldCell?.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
				})
			} else {
				newCell?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
				oldCell?.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
			}
		}

		///set up right bar button
		if userIdentifier == CurrentUserInfo.shared.userId() || userIdentifier.isEmpty {

				///get current user detatils
				getUserDetails(userId: CurrentUserInfo.shared.userId())

				///Setting up right bar button
				addRightBarButtonItems()

				//Check if tabbar is clicked and hide back button
			} else {

				///get current user detatils
				getUserDetails(userId: userIdentifier)
				addBarButtons(value: true)
				self.navigationItem.rightBarButtonItem = nil
			}
			headerView.translatesAutoresizingMaskIntoConstraints = false
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.tabBarController?.tabBar.isHidden = false
		navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Avenir-Black", size: 20)!]

		if let tabBarController = self.tabBarController as? TabBarViewController {
		}

		///signUp events
		LoggerManager.shared.log(event: .profile)
	}

	///Setting up right bar button
	func addRightBarButtonItems() 	{
		let qrCodeBarButtonItem = UIButton.init(type: .custom)
		qrCodeBarButtonItem.setImage(UIImage(named: "qrCodeImage")?.withRenderingMode(.alwaysTemplate), for: .normal)
		qrCodeBarButtonItem.tintColor = UIColor.white
		qrCodeBarButtonItem.addTarget(self, action: #selector(qrCodeScreenAction), for: .touchUpInside)

		let settingsBarButtonItem = UIButton.init(type: .custom)
		settingsBarButtonItem.setImage(UIImage(named: "Menu")?.withRenderingMode(.alwaysTemplate), for: .normal)
		settingsBarButtonItem.tintColor = UIColor.white
		settingsBarButtonItem.addTarget(self, action: #selector(showProfileMenu), for: .touchUpInside)

		let stackview = UIStackView.init(arrangedSubviews: [qrCodeBarButtonItem, settingsBarButtonItem])
		stackview.distribution = .fillEqually
		stackview.axis = .horizontal
		stackview.alignment = .center
		stackview.spacing = 20

		let rightBarButton = UIBarButtonItem(customView: stackview)
		self.navigationItem.rightBarButtonItem = rightBarButton
	}
	
	///Called if user uploads a new listing to reload the onSaleTable
	@objc func reloadOnSaleOnNewUpload() {
		reloadPagerTabStripView()
		DispatchQueue.main.async {
			self.moveToViewController(at: 0, animated: false)
		}
	}

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
	}

	///Update profile after user update is made
	@objc func updateProfileInfo() {
		getUserDetails(userId: CurrentUserInfo.shared.userId())
	}

	///Back button setup
	func addBarButtons(value: Bool) {
		if value {
			let back = UIImage(named: "ArrowLeft")!
			let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
			backButton.tintColor = UIColor.white
			navigationItem.leftBarButtonItem = backButton
		}
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
	}

	override public func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
		if userIdentifier == CurrentUserInfo.shared.userId() || userIdentifier.isEmpty {
				guard let child_1 = UIStoryboard(name: "OnSaleStoryboard", bundle: nil).instantiateViewController(withIdentifier: "OnSaleViewController") as? OnSaleViewController else {
					return []
				}
					child_1.userIdentifier = userIdentifier
					child_1.viewConst = containerViewConstraint
				guard let child_2 = UIStoryboard(name: "WishListStoryboard", bundle: nil).instantiateViewController(withIdentifier: "WishListViewController") as? WishListViewController else {
					return []
				}
				child_2.userIdentifier = userIdentifier
				child_2.viewConst = containerViewConstraint
				let child_3 = UIStoryboard(name: "InfoStoryboard", bundle: nil).instantiateViewController(withIdentifier: "InfoViewController")
				return [child_1, child_2, child_3]
		 } else {
				guard let child_1 = UIStoryboard(name: "OtherUserOnSaleStoryboard", bundle: nil).instantiateViewController(withIdentifier: "OtherUserOnSaleViewController") as? OtherUserOnSaleViewController else {
					return []
				}
					child_1.userIdentifier = userIdentifier
					child_1.viewConst = containerViewConstraint
					child_1.headerView = headerView
				guard let child_2 = UIStoryboard(name: "InfoStoryboard", bundle: nil).instantiateViewController(withIdentifier: "InfoViewController") as? InfoViewController else {
					return []
				}
				child_2.otherUserInfo = true
				return [child_1, child_2]
			}
		}

	@objc func qrCodeScreenAction() {
		///signUp events
		LoggerManager.shared.log(event: .dropCode)

		let storyBoard: UIStoryboard = UIStoryboard(name: "SettingsStoryboard", bundle: nil)
		if let newViewController = storyBoard.instantiateViewController(withIdentifier: "DropcodeController") as? DropcodeController {
			navigationController?.pushViewController(newViewController, animated: true)
		}
	}
}

extension ProfileViewController {
	func getUserDetails(userId: String) {
		ProfileService.sharedInstance.getUserDetails( userId: userId, completion: {[weak self] (response) in
			guard let self = self else { return }
			if response.status == 500 {
				SVProgressHUD.showError(withStatus: "Oops! an error occurred getting user details")
			} else {
				DispatchQueue.main.async {
					let name = response.firstname + " " + response.lastname
					let photo = response.photo

					self.userName.text = name
					self.userLocation.text = response.country.region
					///user name to share profile with
					GlobalProfileInfo.userNameForShareLink = name
					GlobalProfileInfo.memberSince = response.created

					self.getProfilePhoto(photoImage: photo, fullName: name) { (imageData) in
						self.userProfileImage?.sd_imageTransition = SDWebImageTransition.fade
						self.userProfileImage.image = UIImage(data: imageData)
					}
				}
			}
		})
	}

	func getProfilePhoto(photoImage: String, fullName: String, completion: @escaping (Data) -> Void) {
		APIManager.shared.getUserProfileImage(photoImage: photoImage) { (imageData) in
			completion(imageData)
		}
	}
}

extension ProfileViewController {
	@objc func showProfileMenu() {
		let destinationVC = ProfileMenuController.instantiate()
		destinationVC.delegate = self
		let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: destinationVC)
		segue.configure(layout: .bottomMessage)
		segue.messageView.backgroundHeight = 460
		segue.keyboardTrackingView = KeyboardTrackingView()
		segue.perform()
	}

	func openSettingsPage() {
		///signUp events
		LoggerManager.shared.log(event: .settings)
		let storyBoard: UIStoryboard = UIStoryboard(name: "SettingsStoryboard", bundle: nil)
		if let newViewController = storyBoard.instantiateViewController(withIdentifier: "SettingsController") as? SettingsController {
			navigationController?.pushViewController(newViewController, animated: true)
		}
	}

	func openDropCodePage() {
		let storyBoard: UIStoryboard = UIStoryboard(name: "SettingsStoryboard", bundle: nil)
		if let newViewController = storyBoard.instantiateViewController(withIdentifier: "DropcodeController") as? DropcodeController {
			navigationController?.pushViewController(newViewController, animated: true)
		}
	}

	func openStatisticsPage() {
		let storyBoard: UIStoryboard = UIStoryboard(name: "SettingsStoryboard", bundle: nil)
		if let newViewController = storyBoard.instantiateViewController(withIdentifier: "MyStatsController") as? MyStatsController {
			navigationController?.pushViewController(newViewController, animated: true)
		}
	}
}
