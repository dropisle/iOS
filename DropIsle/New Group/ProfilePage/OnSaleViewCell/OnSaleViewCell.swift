//
//  OnSaleViewCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-03-03.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import SDWebImage

protocol MoreActionDelegate: class {
	func showMoreOptions(cell: OnSaleViewCell)
}

class OnSaleViewCell: UICollectionViewCell, Reusable {

	weak var delegate: MoreActionDelegate?

	@IBOutlet weak var mainImageView: UIImageView!
	@IBOutlet weak var productNameLabel: UILabel!
	@IBOutlet weak var productPriceLabel: UILabel!
	@IBOutlet weak var moreButton: UIButton!
	@IBOutlet weak var onSaleBackgroundView: UIView!

	var product: ProductModel.content? {
		didSet {
			guard let product = product else { return }
			weak var weakSelf = self
			mainImageView.sd_imageTransition = SDWebImageTransition.fade
			weakSelf?.mainImageView.sd_setImage(with: URL(string: product.photo.url), placeholderImage: UIImage(named: "placeholder"), options: .fromCacheOnly) { (image, error, cacheType, url) in }
			self.productNameLabel.text = product.name
			let price = "\(product.price.price)".dropLast(2)
			self.productPriceLabel.text = "\(price)".currencyInputFormatting(currencySymbol: product.price.currency)
			product.status == APIEndPoints.soldSate ? (onSaleBackgroundView.isHidden = false) : (onSaleBackgroundView.isHidden = true)
		}
	}

	@IBAction func moreButtonAction(_ sender: UIButton) {
		self.delegate?.showMoreOptions(cell: self)
	}

	func setMoreButtonImageColor() {
		let origImage = UIImage(named: "More")
		let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
		moreButton.setImage(tintedImage, for: .normal)
		moreButton.tintColor = UIColor(hexString: "#AAAAAA")
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		self.setMoreButtonImageColor()
	}
}
