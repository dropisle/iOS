//
//  OtherUserOnSaleViewCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-03-09.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import SDWebImage

protocol AddOthersListToWishListDelegate: class {
	func addToWishList(cell: OtherUserOnSaleViewCell)
}

class OtherUserOnSaleViewCell: UICollectionViewCell, Reusable {

	weak var delegate: AddOthersListToWishListDelegate?

	@IBOutlet weak var mainImageView: UIImageView!
	@IBOutlet weak var productNameLabel: UILabel!
	@IBOutlet weak var productPriceLabel: UILabel!
	@IBOutlet weak var addToWishListButton: UIButton!
	@IBOutlet weak var soldBackgroundView: UIView!

	var product: ProductModel.content? {
		didSet {
			if let imageUrl = product?.photo.url, let productName = product?.name, let currency = product?.price.currency, let price =  product?.price.price {
				weak var weakSelf = self
				mainImageView.sd_imageTransition = SDWebImageTransition.fade
				weakSelf?.mainImageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"), options: .fromCacheOnly) { (image, error, cacheType, url) in }
				self.productNameLabel.text = productName
				let price = "\(price)".dropLast(2)
				self.productPriceLabel.text = "\(price)".currencyInputFormatting(currencySymbol: currency)
			}
			self.product?.watch == "YES" ? setAddToWishList(wishListImage: UIImage(named: "addToWishListSolid")) : setAddToWishList(wishListImage: UIImage(named: "addToWishListEmpty"))
			product?.status == APIEndPoints.soldSate ? (soldBackgroundView.isHidden = false) : (soldBackgroundView.isHidden = true)
		}
	}

	@IBAction func addToWishListAction(_ sender: UIButton) {
		self.delegate?.addToWishList(cell: self)
	}

	func setAddToWishList(wishListImage: UIImage?) {
		let tintedImage = wishListImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
		addToWishListButton.setImage(tintedImage, for: .normal)
		addToWishListButton.tintColor = .lightGray
	}

	override func awakeFromNib() {
		super.awakeFromNib()
	}
}
