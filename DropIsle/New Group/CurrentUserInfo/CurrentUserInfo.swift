//
//  CurrentUser.swift
//  DropIsle
//
//  Created by CtanLI on 2019-01-28.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation
import KeychainAccess

class CurrentUserInfo: NSObject {
	static let shared = CurrentUserInfo()
	let keychain = Keychain()

	func userId() -> String {
		var uid = String()
		if let id = try? keychain.get("userId") {
			uid = id
		}
		return uid
	}

	func refreshToken() -> String {
		var rtn = String()
		if let token = try? keychain.get("refreshtoken") {
			rtn = token
		}
		return rtn
	}

	func userName() -> String {
		var userName = String()
		if let name = try? keychain.get("userName") {
			userName = name
		}
		return userName
	}

	func countryCode() -> String {
		var country = String()
		if let cty = try? keychain.get("countryCode") {
			country = cty
		}
		return country
	}

	func city() -> String {
		var city = String()
		if let cty = try? keychain.get("city") {
			city = cty
		}
		return city
	}

	func country() -> String {
		var country = String()
		if let ctry = try? keychain.get("country") {
			country = ctry
		}
		return country
	}

	func region() -> String {
		var region = String()
		if let rgn = try? keychain.get("region") {
			region = rgn
		}
		return region
	}
}
