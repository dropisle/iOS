//
//  PushNotificationSettingsController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-02-02.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class PushNotificationSettingsController: UITableViewController {

	var application = UIApplication.shared

	@IBOutlet weak var notificationSwitcher: UISwitch!

	override func viewDidLoad() {
		super.viewDidLoad()
			navigationItem.title = "Notification"
			self.tabBarController?.tabBar.isHidden = true
			let back = UIImage(named: "ArrowLeft")!
			let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
			backButton.tintColor = UIColor.white
			navigationItem.leftBarButtonItem = backButton
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		checkIfNotificationIsTurnedOnOrOff()
	}

	func checkIfNotificationIsTurnedOnOrOff() {
		UNUserNotificationCenter.current().getNotificationSettings { (settings) in
		   if settings.authorizationStatus == .authorized {
			DispatchQueue.main.async {
				self.notificationSwitcher.isOn = true
			}
		} else {
			DispatchQueue.main.async {
				self.notificationSwitcher.isOn = false
					UIApplication.shared.registerForRemoteNotifications()
				}
			}
		}
	}
	
	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
	}

	override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 40
	}

	override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
			let header = view as? UITableViewHeaderFooterView
		  view.tintColor = UIColor(hexString: "#f9f9f9")
			header?.textLabel?.textColor = .lightGray
			header?.textLabel?.font = UIFont(name: "Avenir", size: 12)
	}
	
	@IBAction func notificationSwitcherAction(_ sender: UISwitch) {
		let isRegisteredForRemoteNotifications = UIApplication.shared.isRegisteredForRemoteNotifications
		if !isRegisteredForRemoteNotifications {
			 // User is registered for notification
			if #available(iOS 10, *) {
				UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .alert, .badge]) { (granted, _) in
					if granted {
						DispatchQueue.main.async {
							self.application.registerForRemoteNotifications()
						}
					} else {
						DispatchQueue.main.async {
							if self.notificationSwitcher.isOn == false  || self.notificationSwitcher.isOn == true {
								UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!,
																					options: [:], completionHandler: nil)
							}
						}
					}
				}
			} else {
				application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert],
																																								categories: nil))
				application.registerForRemoteNotifications()
			}
			application.registerForRemoteNotifications()
		} else {
			 // Show alert user is not registered for notification
			if notificationSwitcher.isOn == false  || notificationSwitcher.isOn == true {
				UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
			}
		}
	}
}

extension PushNotificationSettingsController {}
