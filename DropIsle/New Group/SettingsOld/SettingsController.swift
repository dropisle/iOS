//
//  DropisleSettingsController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-02-02.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

struct SettingsModel {
    let title: String
    let image: UIImage
}

class SettingsController: UITableViewController {

	let accountController = "AccountController"
	let securityController = "SecurityController"
	let notificationController = "NotificationController"
	let aboutController = "AboutController"
	let helpAndSupportController = "HelpAndSupportController"
	let loginsController = "LoginsController"
	
	@IBOutlet weak var varsionText: UILabel!

	var tableOptions = [SettingsModel(title: "Account", image: UIImage(named: "profile")!), SettingsModel(title: "Security", image: UIImage(named: "Locked")!), SettingsModel(title: "Notifications", image: UIImage(named: "Notification")!), SettingsModel(title: "About", image: UIImage(named: "Announcement")!), SettingsModel(title: "Help & Support", image: UIImage(named: "Earphones")!), SettingsModel(title: "Logins", image: UIImage(named: "Fingerprint")!)]

	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "Settings"
		self.tabBarController?.tabBar.isHidden = true
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton

		if let tabBarController = self.tabBarController as? TabBarViewController {
		}
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
	}

	override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
		let header = view as? UITableViewHeaderFooterView
		view.tintColor = UIColor(hexString: "#f9f9f9")
		header?.textLabel?.textColor = .lightGray
		header?.textLabel?.font = UIFont(name: "Avenir", size: 12)
	}
}

extension SettingsController {
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return tableOptions.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell" , for: indexPath) as? SettingsCell else { return UITableViewCell() }
		cell.settingsTitle.text = tableOptions[indexPath.row].title
		cell.settingsImage.image = tableOptions[indexPath.row].image
		return cell
	}

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

		if indexPath.row == 0 {
			self.performSegue(withIdentifier: accountController, sender: self)
		}

		if indexPath.row == 1 {
			self.performSegue(withIdentifier: securityController, sender: self)
		}

		if indexPath.row == 2 {
			self.performSegue(withIdentifier: notificationController, sender: self)
		}

		if indexPath.row == 3 {
			self.performSegue(withIdentifier: aboutController, sender: self)
		}

		if indexPath.row == 4 {
			self.performSegue(withIdentifier: helpAndSupportController, sender: self)
		}

		if indexPath.row == 5 {
			self.performSegue(withIdentifier: loginsController, sender: self)
		}
	}
}

