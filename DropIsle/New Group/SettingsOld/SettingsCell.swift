//
//  SettingsCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-06-12.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {

	@IBOutlet weak var settingsTitle: UILabel!
	@IBOutlet weak var settingsImage: UIImageView!

	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
