//
//  MyStatsController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-07-28.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class MyStatsController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		
		navigationItem.title = "Statistics"
		self.tabBarController?.tabBar.isHidden = true
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if let tabBarController = self.tabBarController as? TabBarViewController {
		}
	}

	/*
	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destination.
	// Pass the selected object to the new view controller.
	}
	*/

}
