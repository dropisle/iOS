//
//  AboutController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-06-13.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class AboutController: UIViewController {
	
	var aboutData = ["Privacy Policy", "Condition of Use", "Version"]
	var versionNumber = UIApplication.versionBuildNumber
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "About"
		self.tabBarController?.tabBar.isHidden = true
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton
	}
	
	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
	}
}

extension AboutController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return aboutData.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "AboutCell" , for: indexPath) as? AboutCell else { return UITableViewCell() }
		cell.aboutTitles.text = aboutData[indexPath.row]
		cell.versionInfo.text = versionNumber
		if indexPath.row == 2 {
			cell.versionInfo.isHidden = false
			cell.arrowIndicator.isHidden = true
		}
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if indexPath.row == 0 {
			let storyBoard: UIStoryboard = UIStoryboard(name: "PrivacyPolicyStoryboard", bundle: nil)
			if let newViewController = storyBoard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as? PrivacyPolicyViewController {
				newViewController.isFromSettingsPage = true
				navigationController?.pushViewController(newViewController, animated: true)
			}
		}
		
		if  indexPath.row == 1 {
			let storyBoard: UIStoryboard = UIStoryboard(name: "TermsOfServiceStoryboard", bundle: nil)
			if let newViewController = storyBoard.instantiateViewController(withIdentifier: "TermsOfServiceViewController") as? TermsOfServiceViewController {
				newViewController.isFromSettingsPage = true
				navigationController?.pushViewController(newViewController, animated: true)
			}
		}
	}
}
