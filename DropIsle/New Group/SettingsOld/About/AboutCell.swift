//
//  AboutCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-06-13.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class AboutCell: UITableViewCell {

	@IBOutlet weak var aboutTitles: UILabel!
	@IBOutlet weak var versionInfo: UILabel!
	@IBOutlet weak var arrowIndicator: UIImageView!

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
		versionInfo.isHidden = true
	}

	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		// Configure the view for the selected state
	}
}
