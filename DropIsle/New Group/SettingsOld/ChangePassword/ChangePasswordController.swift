//
//  ChangePasswordController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-02-02.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit
import SVProgressHUD
import IQKeyboardManagerSwift

class ChangePasswordController: UIViewController {

	var oldPasswordState = true
	var newPasswordState = true

	@IBOutlet weak var passwordChangeLogo: UIImageView!
	@IBOutlet weak var enterEmail: UITextField!
	@IBOutlet weak var enterOldPassword: UITextField!
	@IBOutlet weak var enterNewPassword: UITextField!
	@IBOutlet weak var oldPassword: UIButton! {
		 didSet {
			 let origImage = UIImage(named: "eye")
			 let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
			 oldPassword.setImage(tintedImage, for: .normal)
			 oldPassword.tintColor = UIColor(hexString: "#727272")
		 }
	 }

	@IBOutlet weak var newPassword: UIButton! {
		didSet {
			let origImage = UIImage(named: "eye")
			let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
			newPassword.setImage(tintedImage, for: .normal)
			newPassword.tintColor = UIColor(hexString: "#727272")
		}
	}

	var isPasswordValid = true

	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "Change Password"
		self.tabBarController?.tabBar.isHidden = true
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton

		///Keyboard disabled for current screen
		IQKeyboardManager.shared.enable = true
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
	}

	func getCredentials() {
		let newPasswordValidText = self.validatePassword(password: enterNewPassword.text)
			if !newPasswordValidText.isEmpty {
				self.changePassword(email: enterEmail.text!, oldPassword: enterOldPassword.text!, newPassword: newPasswordValidText)
			}
	}

	func changePassword(email: String, oldPassword: String, newPassword: String) {
		let params = ["email": email, "oldpassword": oldPassword, "newpassword": newPassword]
		_ = APIEndPoints.init()
		let changePasswordUrl = BaseAPIClient.urlFromPath(path: APIEndPoints.changePassword)
		APIManager.shared.fetchGenericData(method: .post,
																			 urlString: changePasswordUrl,
																			 params: params as [String: AnyObject],
																			 headers: APIEndPoints.authorizationHeader) { (result: ChangePasswordModel) in
			if result.code == 200 {
				self.alert(message: result.message)
			} else if result.code == 205 {
				self.alert(message: result.message)
			} else {
				SVProgressHUD.showError(withStatus: "Error occurred changing password, try again later")
			}
		}
	}

	@IBAction func showOldPassWord(_ sender: UIButton) {
		if (oldPasswordState == true && enterOldPassword.text != "") {
			oldPassword.tintColor = UIColor(hexString: "#828DED")
		} else {
			oldPassword.tintColor = UIColor(hexString: "#727272")
		}
		oldPasswordState = !oldPasswordState
		enterOldPassword.isSecureTextEntry.toggle()
	}

	@IBAction func showNewPassWord(_ sender: UIButton) {
		if (newPasswordState == true && enterNewPassword.text != "") {
			newPassword.tintColor = UIColor(hexString: "#828DED")
		} else {
			newPassword.tintColor = UIColor(hexString: "#727272")
		}
		newPasswordState = !newPasswordState
		enterNewPassword.isSecureTextEntry.toggle()
	}

	@IBAction func changePasswordAction(_ sender: UIButton) {
		getCredentials()
	}
}

extension ChangePasswordController {
	func validatePassword(password: String?) -> String {
		var errorMsg = "Password requires at least"

		if let txt = password {
			if txt.rangeOfCharacter(from: CharacterSet.uppercaseLetters) == nil {
				errorMsg += " one upper case letter"
				isPasswordValid = false
			}
			if txt.rangeOfCharacter(from: CharacterSet.lowercaseLetters) == nil {
				errorMsg += ", one lower case letter"
				isPasswordValid = false
			}
			if txt.rangeOfCharacter(from: CharacterSet.decimalDigits) == nil {
				errorMsg += ", one number"
				isPasswordValid = false
			}
			if txt.count < 8 {
				errorMsg += " and eight characters"
				isPasswordValid = false
			}
		}

		if isPasswordValid {
			return password!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
		} else {
			isPasswordValid = true
			let alertController = UIAlertController(title: "Password Error", message: errorMsg, preferredStyle: .alert)
			let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
			alertController.addAction(defaultAction)
			self.present(alertController, animated: true, completion: nil)
			return String()
		}
	}
}
