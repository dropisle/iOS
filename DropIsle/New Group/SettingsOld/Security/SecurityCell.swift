//
//  SecurityCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-06-12.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class SecurityCell: UITableViewCell {

	@IBOutlet weak var securityTitles: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
