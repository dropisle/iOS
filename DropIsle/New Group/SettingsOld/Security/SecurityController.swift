//
//  SecurityController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-06-12.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class SecurityController: UIViewController {

	var securityData = ["Change Password"]
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "Security"
		self.tabBarController?.tabBar.isHidden = true
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
	}
}

extension SecurityController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return securityData.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "SecurityCell" , for: indexPath) as? SecurityCell else { return UITableViewCell() }
		cell.securityTitles.text = securityData[indexPath.row]
		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if indexPath.row == 0 {
			let storyBoard: UIStoryboard = UIStoryboard(name: "SettingsStoryboard", bundle: nil)
			if let newViewController = storyBoard.instantiateViewController(withIdentifier: "ChangePasswordController") as? ChangePasswordController {
				navigationController?.pushViewController(newViewController, animated: true)
			}
		}
	}
}
