//
//  AccountController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-06-12.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class AccountController: UIViewController {

	var accountData = ["Edit Profile"]
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "Account"
		self.tabBarController?.tabBar.isHidden = true
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
	}
}

extension AccountController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return accountData.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCell" , for: indexPath) as? AccountCell else { return UITableViewCell() }
		cell.accountTitles.text = accountData[indexPath.row]
		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if indexPath.row == 0 {
			let storyBoard: UIStoryboard = UIStoryboard(name: "EditProfileStoryboard", bundle: nil)
			if let newViewController = storyBoard.instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController {
				navigationController?.pushViewController(newViewController, animated: true)
			}
		}
	}
}
