//
//  LoginsController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-06-13.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit
import KeychainAccess
import SVProgressHUD

class LoginsController: UIViewController {

	var loginsData = [String]()
	var keychain = Keychain()
	let launchScreenController = "launchScreenController"

	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "Logins"
		self.tabBarController?.tabBar.isHidden = true
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton

		loginsData = ["Log Out \(CurrentUserInfo.shared.userName())", "Disable Account"]
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
	}
}

extension LoginsController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return loginsData.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "LoginsCell" , for: indexPath) as? LoginsCell else { return UITableViewCell() }
		cell.loginsTitle.text = loginsData[indexPath.row]

		if indexPath.row == 0 {
			cell.loginsTitle.font = UIFont(name: "Avenir-Medium", size: 14.0)
		}

		if indexPath.row == 1 {
			cell.loginsTitle.textColor = UIColor(hexString: "#D0011B")
			cell.loginsTitle.font = UIFont(name: "Avenir-Medium", size: 14.0)
		}
		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if indexPath.row == 0 {
			logOutAction()
		}

		if  indexPath.row == 1 {
			disableAccount()
		}
	}
}

extension LoginsController {
	func logOutAction() {
		let alertController = UIAlertController(title: "Log out from Dropisle",
																						message: "Are you sure?",
																						preferredStyle: .alert)
		let oKAction = UIAlertAction(title: "Log out", style: .default) { (_: UIAlertAction) in
			///Clear local storage before user logout.
			_ = try? self.keychain.remove("userId")

			///clear local storage
			self.clearLocallyStoredData()

			DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
				self.performSegue(withIdentifier: self.launchScreenController, sender: self)
			}
		}

		let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
		alertController.addAction(cancelAction)
		alertController.addAction(oKAction)
		self.present(alertController, animated: true, completion: nil)
	}

	func disableAccount() {
		let alertController = UIAlertController(title: "Deactivate Your Account",
																						message: "Are you sure?",
																						preferredStyle: .alert)
		let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
		let alertOk = UIAlertAction(title: "Deactivate", style: .destructive) { (_: UIAlertAction) in
			self.deactivateAccount()
		}

		alertController.addAction(cancel)
		alertController.addAction(alertOk)
		self.present(alertController, animated: true, completion: nil)
	}

	func deactivateAccount() {
		let params = ["userid": CurrentUserInfo.shared.userId()]
		_ = APIEndPoints.init()
		let deactivateAccountUrl = BaseAPIClient.urlFromPath(path: APIEndPoints.deactivateAccount)
		APIManager.shared.fetchGenericData(method: .post,
																			 urlString: deactivateAccountUrl,
																			 params: params as [String: AnyObject],
																			 headers: APIEndPoints.authorizationHeader) { (result: DeactivateAccount) in
																				if result.code == 200 {
																					//Clear local storage before user logout.
																					_ = try? self.keychain.remove("userId")
																					_ = try? self.keychain.remove("token")

																					//clear local storage
																					self.clearLocallyStoredData()

																					self.performSegue(withIdentifier: self.launchScreenController, sender: self)
																				} else {
																					SVProgressHUD.showError(withStatus: "Oops! an error occurred deactivating your account.")
																				}
		}
	}

	func clearLocallyStoredData() {
		preferences.removeObject(forKey: "recentlySearchedItems")
		preferences.removeObject(forKey: "itemMarkedAsSold")
		preferences.removeObject(forKey: "addedToWishlist")
		preferences.removeObject(forKey: "addedSearchedToWishlist")
		preferences.removeObject(forKey: "addedOtherItemToWishlist")
		preferences.removeObject(forKey: "similarProductsItemToWishlist")
	}
}
