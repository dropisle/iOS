//
//  Help&SupportController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-06-13.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit
import MessageUI

class HelpAndSupportController: UIViewController {

	var  helpAndSupportData = ["Guides & Tutorials", "Frequently Asked Questions", "Contact Us"]

	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "Help & Support"
		self.tabBarController?.tabBar.isHidden = true
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
	}
}

extension HelpAndSupportController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return helpAndSupportData.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "HelpAndSupportCell" , for: indexPath) as? HelpAndSupportCell else { return UITableViewCell() }
		cell.helpAndSupportTitles.text = helpAndSupportData[indexPath.row]
		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if indexPath.row == 0 {
			let storyBoard: UIStoryboard = UIStoryboard(name: "GuidesAndTutorialsStoryboard", bundle: nil)
			if let newViewController = storyBoard.instantiateViewController(withIdentifier: "GuidesAndTutorialsViewController") as? GuidesAndTutorialsViewController {
				navigationController?.pushViewController(newViewController, animated: true)
			}
		}

		if  indexPath.row == 1 {
			let storyBoard: UIStoryboard = UIStoryboard(name: "FrequentlyAskedQuestion", bundle: nil)
			if let newViewController = storyBoard.instantiateViewController(withIdentifier: "FrequentlyAskedQuestion") as? FrequentlyAskedQuestion {
				navigationController?.pushViewController(newViewController, animated: true)
			}
		}

		if  indexPath.row == 2 {
			contactUS()
		}
	}
}

extension HelpAndSupportController: MFMailComposeViewControllerDelegate {
	func contactUS() {
		if MFMailComposeViewController.canSendMail() {
			let mail = MFMailComposeViewController()
			mail.mailComposeDelegate = self
			mail.setToRecipients(["appsupport@dropisle.com"])
			mail.setMessageBody("<p>Tell us what we can help you with today.</p>", isHTML: true)

			let image = UIImage(named:"onBoardImage") // Your Image
			let imageData = image!.pngData() ?? nil
			let base64String = imageData?.base64EncodedString() ?? "" // Your String Image
			let _ = "<html><body><p>Header: Hello Test Email</p><p><b><img src='data:image/png;base64,\(base64String)' ></b></p></body></html>"
			let _ = "<html><body><p>Header: Hello Test Email</p><p><b><img src='data:image/png;base64,\(base64String)' width='\(50)' height='\(50)'></b></p></body></html>"
			///mail.setMessageBody(tml, isHTML: true)
			present(mail, animated: true)
		} else {
			///show failure alert
		}
	}

	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		controller.dismiss(animated: true)
	}
}
