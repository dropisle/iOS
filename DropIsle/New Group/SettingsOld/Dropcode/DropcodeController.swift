//
//  DropcodeController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-02-02.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDynamicLinks

class DropcodeController: UITableViewController {

	var dynamicURL: URL?
	var bottomView = UIView()

	@IBOutlet weak var containerView: CornerRect!
	@IBOutlet weak var dropCodeImage: UIImageView!
	@IBOutlet weak var SVLoadingViewContainer: UIView!
	@IBOutlet weak var scanIndicator: UIView!

	let iLabel: UILabel = {
		let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
		label.textColor = .darkGray
		label.backgroundColor = .clear
		label.numberOfLines = 0
		label.textAlignment = .center
		label.lineBreakMode = .byWordWrapping
		label.text = "Dropcode is linked with your profile and can be shared with other Dropislers. \n Get more info at www.dropisle.com"
		label.font = UIFont(name: "Avenir-Medium", size: 11)
		return label
	}()

	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "Dropcode"
		self.tabBarController?.tabBar.isHidden = true
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton

		bottomView.backgroundColor = .clear
		bottomView.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - 60, width: tableView.frame.size.width, height: 60)
		bottomView.addSubview(iLabel)
		navigationController?.view.addSubview(bottomView)

		if UIDevice.modelName == "iPhone SE" {
			bottomView.isHidden = true
		}

		self.scanIndicator.frame = CGRect(x: 0, y: -self.SVLoadingViewContainer.bounds.height / 10, width: 0, height: 0)
		UIView.animate(withDuration: 1.5, delay: 0, options: [.repeat, .autoreverse] , animations: {
			self.scanIndicator.frame = CGRect(x: 0, y: self.SVLoadingViewContainer.bounds.height, width: 0, height: 0)
		}) { (completed) in
		}

		self.showSpinner(onView: self.SVLoadingViewContainer)
		createDynamicLink()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if let tabBarController = self.tabBarController as? TabBarViewController {
		}

		///Analytics
		LoggerManager.shared.log(event: .dropCode)
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		bottomView.removeFromSuperview()
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
	}

	override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return CGFloat.leastNormalMagnitude
	}

	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return indexPath.row == 0 ? 400 : 60
	}

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		switch indexPath.section {
			case 0:
				switch indexPath.row {
					case 1:
						self.showShareSheet(url: nil, image: dropCodeImage.image)
					case 2:
						if let url = dynamicURL {
							self.showShareSheet(url: url, image: nil)
					}
					default: break
			}
			default: break
		}
	}
}

extension DropcodeController {
	func createQRCode(dynamicLink: URL) {
		DispatchQueue.global(qos: .background).async {
			let _ = UIColor(hexString: "#3293FC")
			let _ = UIImage(named: "scanLogo")!
			guard let qrURLImage = dynamicLink.qrImage(using: .black, logo: nil) else { return }
			DispatchQueue.main.async {
				self.dropCodeImage.image = self.convertCImage(cmage: qrURLImage)
				self.removeSpinner()
			}
		}
	}

	func createDynamicLink() {
		//Share Profile
		var components = URLComponents()
		components.scheme = APIEndPoints.scheme
		components.host = APIEndPoints.host
		components.path = APIEndPoints.profilePath

		let userId = URLQueryItem(name: "userId", value: CurrentUserInfo.shared.userId())
		components.queryItems = [userId]

		guard let linkParam = components.url else { return }
		print("I am sharing \(linkParam.absoluteString)")

		//Creating dynamic link
		guard let shareLink = DynamicLinkComponents.init(link: linkParam, domainURIPrefix: APIEndPoints.customDomain) else {
			print("Unable to creat dynamiclink")
			return
		}

		if let myBundleId = Bundle.main.bundleIdentifier {
			shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: myBundleId)
		}

		//Temporary redirection
		shareLink.iOSParameters?.appStoreID = APIEndPoints.appStoreId
		shareLink.iOSParameters?.fallbackURL = URL(string: APIEndPoints.host)
		shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
		shareLink.socialMetaTagParameters?.title = "\(GlobalProfileInfo.userNameForShareLink) | Dropisle"
		shareLink.socialMetaTagParameters?.descriptionText = "Dropisle is Africa’s best and beautiful new marketplace for buyers and sellers to do business. It’s trusted, free and simple to use."
		shareLink.socialMetaTagParameters?.imageURL = URL(string: "https://firebasestorage.googleapis.com/v0/b/iosdropisle.appspot.com/o/d3.png?alt=media&token=7f7f90be-4130-47c0-8034-1c2f5fa12059")

		DispatchQueue.global(qos: .background).async {
			shareLink.shorten { [weak self] (url, warnings, error) in
				guard let self = self else { return }
				if let error = error {
					print("There is an \(error)")
				}
				if let warnings = warnings {
					for warning in warnings {
						print("There is a warning \(warning)")
					}
				}
				guard let url = url else { return }
				print("The short url is \(url.absoluteString)")
				DispatchQueue.main.async {
					self.createQRCode(dynamicLink: url)
					self.dynamicURL = url
				}
			}
		}
	}

	func showShareSheet(url: URL?, image: UIImage?) {
		///Analytics
		let parameter = ["userId": CurrentUserInfo.shared.userId(), "url": url ?? ""] as [String : Any]
		LoggerManager.shared.log(event: .shareProduct, parameters: parameter)

		let promoText = "Hey there! Take a look at my profile and fantastic products on Dropisle Mobile App."
		var activityItems = [Any]()
		image == nil ? (activityItems = [promoText, url ?? URL(string: "") as Any]) : (activityItems = [promoText, image ?? UIImage(named: "scanLogo") as Any])
		let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
		if let popoverController = activityController.popoverPresentationController {
			popoverController.sourceView = self.view
			popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
			popoverController.permittedArrowDirections = []
		}
		self.present(activityController, animated: true, completion: nil)
	}
}

extension DropcodeController {
	func convertCImage(cmage: CIImage) -> UIImage {
		let context:CIContext = CIContext.init(options: nil)
		guard let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent) else { return UIImage() }
		let image:UIImage = UIImage.init(cgImage: cgImage)
		return image
	}
}

extension CIImage {
	/// Inverts the colors and creates a transparent image by converting the mask to alpha.
	/// Input image should be black and white.
	var transparent: CIImage? {
		return inverted?.blackTransparent
	}

	/// Inverts the colors.
	var inverted: CIImage? {
		guard let invertedColorFilter = CIFilter(name: "CIColorInvert") else { return nil }

		invertedColorFilter.setValue(self, forKey: "inputImage")
		return invertedColorFilter.outputImage
	}

	/// Converts all black to transparent.
	var blackTransparent: CIImage? {
		guard let blackTransparentFilter = CIFilter(name: "CIMaskToAlpha") else { return nil }
		blackTransparentFilter.setValue(self, forKey: "inputImage")
		return blackTransparentFilter.outputImage
	}

	/// Applies the given color as a tint color.
	func tinted(using color: UIColor) -> CIImage? {
		guard
			let transparentQRImage = transparent,
			let filter = CIFilter(name: "CIMultiplyCompositing"),
			let colorFilter = CIFilter(name: "CIConstantColorGenerator") else { return nil }

		let ciColor = CIColor(color: color)
		colorFilter.setValue(ciColor, forKey: kCIInputColorKey)
		let colorImage = colorFilter.outputImage

		filter.setValue(colorImage, forKey: kCIInputImageKey)
		filter.setValue(transparentQRImage, forKey: kCIInputBackgroundImageKey)

		return filter.outputImage!
	}
}

extension URL {
	/// Creates a QR code for the current URL in the given color.
	func qrImage(using color: UIColor) -> CIImage? {
		return qrImage?.tinted(using: color)
	}

	/// Returns a black and white QR code for this URL.
	var qrImage: CIImage? {
		guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return nil }
		let qrData = absoluteString.data(using: String.Encoding.ascii)
		qrFilter.setValue(qrData, forKey: "inputMessage")

		let qrTransform = CGAffineTransform(scaleX: 12, y: 12)
		return qrFilter.outputImage?.transformed(by: qrTransform)
	}
}

extension CIImage {
	/// Combines the current image with the given image centered.
	func combined(with image: CIImage) -> CIImage? {
		guard let combinedFilter = CIFilter(name: "CISourceOverCompositing") else { return nil }
		let centerTransform = CGAffineTransform(translationX: extent.midX - (image.extent.size.width / 2), y: extent.midY - (image.extent.size.height / 2))
		combinedFilter.setValue(image.transformed(by: centerTransform), forKey: "inputImage")
		combinedFilter.setValue(self, forKey: "inputBackgroundImage")
		return combinedFilter.outputImage!
	}
}

extension URL {
	/// Creates a QR code for the current URL in the given color.
	func qrImage(using color: UIColor, logo: UIImage? = nil) -> CIImage? {
		let tintedQRImage = qrImage?.tinted(using: color)
		guard let logo = logo?.cgImage else {
			return tintedQRImage
		}
		return tintedQRImage?.combined(with: CIImage(cgImage: logo))
	}
}


var vSpinner : UIView?
extension UIViewController {
	func showSpinner(onView : UIView) {
		let spinnerView = UIView.init(frame: onView.bounds)
		spinnerView.backgroundColor = UIColor(hexString: "#ececec")
		let ai = UIActivityIndicatorView.init(style: .gray)
		ai.startAnimating()
		ai.center = spinnerView.center

		DispatchQueue.main.async {
			spinnerView.addSubview(ai)
			onView.addSubview(spinnerView)
		}
		vSpinner = spinnerView
	}

	func removeSpinner() {
		DispatchQueue.main.async {
			vSpinner?.removeFromSuperview()
			vSpinner = nil
		}
	}
}
