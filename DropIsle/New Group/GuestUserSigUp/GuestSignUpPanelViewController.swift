//
//  GuestSignUpPanelViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-14.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol GuestUserSignUpDelegate: class {
	func dismissDarkBackground()
}

class GuestSignUpPanelViewController: UIViewController {

	var isSignUpOption = false
	var tappedFromTabBar = 1
	weak var delegate: GuestUserSignUpDelegate?
	static let signUpOptionController = "signUpOptionController"

    override func viewDidLoad() {
        super.viewDidLoad()
    }

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let touch = touches.first
		if touch?.view == self.view {
			 self.dismiss(animated: true, completion: nil)
		}
	}

	@IBAction func signUpOptions(_ sender: UIButton) {
		///Analytics
		LoggerManager.shared.log(event: .guestUserSignUpOptions)
		DispatchQueue.main.async(execute: {
			 self.isSignUpOption = true
			self.performSegue(withIdentifier: GuestSignUpPanelViewController.signUpOptionController, sender: self)
		})
	}

	@IBAction func signInOption(_ sender: UIButton) {
		///Analytics
		LoggerManager.shared.log(event: .guestUserSignInOptions)
		DispatchQueue.main.async(execute: {
			 self.isSignUpOption = false
			self.performSegue(withIdentifier: GuestSignUpPanelViewController.signUpOptionController, sender: self)
		})
	}

    @IBAction func closeView(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == GuestSignUpPanelViewController.signUpOptionController) {
				let option = segue.destination as? SignUpOptionController
				option?.isSignUpOption = isSignUpOption
			}
		}
	}
