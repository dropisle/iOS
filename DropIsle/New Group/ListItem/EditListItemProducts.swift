//
//  EditProducts.swift
//  DropIsle
//
//  Created by CtanLI on 2019-11-15.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.tt
//

import Foundation
import SVProgressHUD

extension ListItemController {

	///Update table for edit
	func updateListItemTableWithProductToEdit(product: ProductModel.content) {
		SVProgressHUD.show()
		isFromEdit = true
		DispatchQueue.main.asyncAfter(deadline: .now() + 1) {

//			firstCell.productTitle.text = product.name
//			let price = "\(product.price.price)".dropLast(2)
//			firstCell.productPrice.text = "\(price)"
//			firstCell.productDescription.text = product.description
//			firstCell.productDescription.textColor = .black

			let secondIndexPath = IndexPath(item: 0, section: 1)
			guard let secondCell = self.collectionView.cellForItem(at: secondIndexPath) as? SecondViewCell else {
			 return
		 }
			secondCell.category.setTitle(product.category, for: .normal)
			secondCell.itemCondition.setTitle(product.condition, for: .normal)
			product.price.negotiable == "YES" ? (secondCell.priceNegotiable.setTitle("Negotiable", for: .normal)) : (secondCell.priceNegotiable.setTitle("Fixed Price", for: .normal))			
			secondCell.stockQuantity.text! = "\(product.quantity)"
			SVProgressHUD.dismiss()
		}
	}

	///Update DB with updated table
	func editUserProducts() {
		let firstIndexPath = IndexPath(item: 0, section: 0)
		guard let firstCell = collectionView.cellForItem(at: firstIndexPath) as? FirstViewCell else {
			return
		}
		let title = firstCell.productTitle.text
		let description = firstCell.productDescription.text

		let secondIndexPath = IndexPath(item: 0, section: 1)
		guard let secondCell = collectionView.cellForItem(at: secondIndexPath) as? SecondViewCell else {
			return
		}
		let productCategory = secondCell.category.titleLabel?.text
		let itemCondition = secondCell.itemCondition.titleLabel?.text
		var priceNegotiable = secondCell.priceNegotiable.titleLabel?.text
		priceNegotiable == "Negotiable" ? (priceNegotiable = "YES") : (priceNegotiable = "NO")
		let quantity = secondCell.stockQuantity.text!

		guard  var price = firstCell.productPrice.text else { return }
		let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
		price = regex.stringByReplacingMatches(in: price, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, price.count), withTemplate: "")

		///getting currency symbol
		guard let currencySymbol = Locale.currency[CurrentUserInfo.shared.countryCode()]?.symbol else { return }

		guard let productTitle = firstCell.productTitle.text, !productTitle.isEmpty else {
			SVProgressHUD.showError(withStatus: "Please enter a title")
			return
		}
		guard let productPrice = firstCell.productPrice.text, !productPrice.isEmpty else {
			SVProgressHUD.showError(withStatus:"Please enter a price")
			return
		}
		guard firstCell.productDescription.text != descriptionPlaceHolder else {
			SVProgressHUD.showError(withStatus: "Please enter item description")
			return
		}
		guard productCategory != "Choose Category" else {
			SVProgressHUD.showError(withStatus: "Please select a category")
			return
		}
		guard itemCondition != "Condition" else {
			SVProgressHUD.showError(withStatus: "Please select item Condition")
			return
		}
		guard priceNegotiable != "Negotiable" else {
			SVProgressHUD.showError(withStatus: "Please select price negotiation")
			return
		}

		let location: [String: AnyObject] = ["countrycode": self.countryCode as AnyObject, "region": self.region as AnyObject, "city": self.city as AnyObject]
		let params: [String: Any] = ["name": title ?? "",
																 "itemid": productToEdit?.id ?? "",
																 "price": price,
																 "userid": CurrentUserInfo.shared.userId(),
																 "quantity": Int(quantity) ?? 0,
																 "currency": currencySymbol,
																 "negotiable": priceNegotiable?.uppercased() ?? "",
																 "description": description ?? "",
																 "condition": itemCondition ?? "",
																 "category": productCategory ?? "",
																 "shipping": false,
																 "location": location]
		ProfileService.sharedInstance.editUploadedItem(params: params as [String: AnyObject]) { (result) in
			if result.code == 200 {
				///Analytics
				let parameter = ["userId": CurrentUserInfo.shared.userId(), "productName": firstCell.productTitle.text ?? "", "productCategory": secondCell.category.titleLabel?.text ?? ""] as [String : Any]
				LoggerManager.shared.log(event: .listedProductEditConfirmed, parameters: parameter)
				self.clearFields(firstCell: firstCell, secondCell: secondCell)
				///
				self.dismiss(animated: true) {
					self.delegate?.ConfirmProductPage()
				}
			} else {
				SVProgressHUD.showError(withStatus: "Please try again \nSomething went wrong")
			}
		}
	}
}
