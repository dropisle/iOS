//
//  ListItemExtension.swift
//  DropIsle
//
//  Created by CtanLI on 2019-04-22.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import KeychainAccess
import SVProgressHUD

extension ListItemController {

	func uploadItem(sender: UIButton) {
		sender.bounceAnimation(value: 1.1)
		if productToEdit == nil {
			newItemUpload()
		} else {
			editUserProducts()
		}
	}

	func newItemUpload() {
		let firstIndexPath = IndexPath(item: 0, section: 0)
		let firstCell = collectionView.cellForItem(at: firstIndexPath) as? FirstViewCell
		let title = firstCell?.productTitle.text
		let description = firstCell?.productDescription.text

		let secondIndexPath = IndexPath(item: 0, section: 1)
		let secondCell = collectionView.cellForItem(at: secondIndexPath) as? SecondViewCell
		let itemCondition = secondCell?.itemCondition.titleLabel?.text

		var productCategory = secondCell?.category.titleLabel?.text
		let productLocation = secondCell?.location.titleLabel?.text

		var priceNegotiable = secondCell?.priceNegotiable.titleLabel?.text
		priceNegotiable == "Negotiable" ? (priceNegotiable = "YES") : (priceNegotiable = "NO")

		if let stock = secondCell?.stockQuantity.text {
			stock.isEmpty ? (quantity = "0") : (quantity = stock)
		}

		guard var price = firstCell?.productPrice.text else { return }
		let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
		price = regex.stringByReplacingMatches(in: price, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, price.count), withTemplate: "")

		///getting currency symbol
		guard let currencySymbol = Locale.currency[CurrentUserInfo.shared.countryCode()]?.symbol else { return }

		guard let main = self.mainImageArray, !main.isEmpty else {
			mainImageArray?.removeAll()
			SVProgressHUD.showError(withStatus: "Please drag and drop a main image")
			return
		}
		guard let productTitle = firstCell?.productTitle.text, !productTitle.isEmpty else {
			SVProgressHUD.showError(withStatus: "Please enter a title")
			return
		}
		guard let productPrice = firstCell?.productPrice.text, !productPrice.isEmpty else {
			SVProgressHUD.showError(withStatus: "Please enter a price")
			return
		}
		guard firstCell?.productDescription.text != descriptionPlaceHolder else {
			SVProgressHUD.showError(withStatus: "Please enter item description")
			return
		}
		guard productCategory != "Choose Category" else {
			SVProgressHUD.showError(withStatus: "Please select a category")
			return
		}
		guard itemCondition != "Set Condition" else {
			SVProgressHUD.showError(withStatus: "Please select item Condition")
			return
		}
		guard priceNegotiable != "Set Negotiation" else {
			SVProgressHUD.showError(withStatus: "Please select price negotiation")
			return
		}
		guard !productLocation!.isEmpty else { return }

		//MARK: Set main image
		let image = mainImageForDBUpload
		guard let imageData = self.compressImage(image: image) else { return }
		mainImage = imageData

		//MARK: Parameters
		let heightInPoints = "\(mainImageForDBUpload.size.height)"
		let widthInPoints = "\(mainImageForDBUpload.size.width)"

		///removing & and relacing with "and" before upload
		productCategory?.replace("&", with: "and")
		let photo: [String: AnyObject] = ["height": heightInPoints as AnyObject, "width": widthInPoints as AnyObject]
		let location: [String: AnyObject] = ["countrycode": self.countryCode as AnyObject, "region": self.region as AnyObject, "city": self.city as AnyObject]
		let params: [String: AnyObject] = ["name": title as AnyObject,
																		"price": price as AnyObject,
																		"userid": CurrentUserInfo.shared.userId() as AnyObject,
																		"quantity": Int(quantity) as AnyObject,
																		"currency": currencySymbol as AnyObject,
																		"negotiable": priceNegotiable?.uppercased() as AnyObject,
																		"description": description as AnyObject,
																		"condition": itemCondition as AnyObject,
																		"category": productCategory as AnyObject,
																		"photo": photo as AnyObject,
																		"shipping": false as AnyObject,
																		"location": location as AnyObject]
			let myGroup = DispatchGroup()
			myGroup.enter()
			DispatchQueue.global(qos: .background).async {
				guard let imagesForDBUpload = self.imagesForDBUpload else { return }
				for image in imagesForDBUpload {
					guard let imageData = self.compressImage(image: image) else { return }
					let _ = image.jpegData(compressionQuality: 0.5)
					print("Size of Image: \(imageData.count) bytes")
					self.imageArraySentToServer?.append(imageData)
				}
				myGroup.leave()
			}
			myGroup.notify(queue: .global()) {
			print("Finished all requests.")
				guard let fCell = firstCell, let sCell = secondCell else {
					return
				}
			self.postProducts(params: params, firstCell: fCell, secondCell: sCell)
			self.imagesForDBUpload?.removeAll()
		}
	}

	func updateHeaderCell() {
		let headerIndexPath = IndexPath(item: 0, section: 0)
		let _ = self.collectionView(self.listingCollectionView, viewForSupplementaryElementOfKind: UICollectionView.elementKindSectionHeader, at: headerIndexPath) as? ListItemHeaderReusableView
	}

	func postProducts(params: [String: AnyObject]?, firstCell: FirstViewCell, secondCell: SecondViewCell) {
		SVProgressHUD.loading(message: "Uploading...", backgroundColor: .white, textColor: .lightGray)
		let mainImageKey = "main"
		let imageArrayKey = "photo"
		_ = APIEndPoints.init()
		let addItemUrl = BaseAPIClient.urlFromPath(path: APIEndPoints.addItem)
		APIManager.shared.uploadProductToServer(method: .post, urlString: addItemUrl, mainImageKey: mainImageKey, mainData: mainImage, imageArrayKey: imageArrayKey, imageArrData: imageArraySentToServer, params: params, headers: APIEndPoints.authorizationHeader) { (result: AddItemModel) in
			if result.code == 200 {
				///Analytics
				let parameter = ["userId": CurrentUserInfo.shared.userId(), "productName": firstCell.productTitle.text ?? "", "productCategory": secondCell.category.titleLabel?.text ?? ""] as [String : Any]
				LoggerManager.shared.log(event: .listedProductConfirmed, parameters: parameter)
				self.collectionView.scrollToTop(true)
				self.clearFields(firstCell: firstCell, secondCell: secondCell)
				self.imageArraySentToServer?.removeAll()
				SVProgressHUD.dismiss(withDelay: 0.5)
				///
				self.dismiss(animated: true) {
					self.delegate?.ConfirmProductPage()
				}
			} else if result.code == 413 {
				self.imageArraySentToServer?.removeAll()
				self.clearFields(firstCell: firstCell, secondCell: secondCell)
				SVProgressHUD.showError(withStatus: "Please try again \nSomething went wrong")
			} else {
				SVProgressHUD.showError(withStatus: "Please try again \nSomething went wrong")
				SVProgressHUD.dismiss(withDelay: 0.5)
			}
		}
	}

	func clearFields(firstCell: FirstViewCell, secondCell: SecondViewCell) {
		firstCell.productTitle.text = String()
		firstCell.productPrice.text = String()
		firstCell.productDescription.delegate = self
		firstCell.productDescription.textColor = UIColor.lightGray.withAlphaComponent(0.5)
		firstCell.productDescription.text = String(descriptionPlaceHolder)
		secondCell.category.setTitle("Choose Category", for: .normal)
		secondCell.itemCondition.setTitle("Set Condition", for: .normal)
		secondCell.priceNegotiable.setTitle("Set Negotiation", for: .normal)
		secondCell.stockQuantity.text = String()
		city = String()
		region = String()
		countryCode = String()
		clearImageWhenViewDisappears()
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)

		///Clear product to edit object
		productToEdit = nil
		///collectionView.scrollToTop(true)

		///Skips clearing fields if image picker is open
		if !imagePickerIsOpen {
			let firstIndexPath = IndexPath(item: 0, section: 0)
			if let firstCell = collectionView.cellForItem(at: firstIndexPath) as? FirstViewCell {
				firstCell.productTitle.text = String()
				firstCell.productPrice.text = String()
				firstCell.productDescription.delegate = self
				firstCell.productDescription.textColor = UIColor.lightGray.withAlphaComponent(0.5)
				firstCell.productDescription.text = String(descriptionPlaceHolder)
			}
			let secondIndexPath = IndexPath(item: 0, section: 1)
			if let secondCell = collectionView.cellForItem(at: secondIndexPath) as? SecondViewCell {
				secondCell.category.setTitle("Choose Category", for: .normal)
				secondCell.itemCondition.setTitle("Set Condition", for: .normal)
				secondCell.priceNegotiable.setTitle("Set Negotiation", for: .normal)
				secondCell.stockQuantity.text = String()
			}

			///Reload collectionview header to reset images
			clearImageWhenViewDisappears()
		}
		
		///Remove observer after upload success
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "newListingAdded"), object: nil)
	}

	func textViewDidBeginEditing(_ textView: UITextView) {
		if textView.textColor == UIColor.lightGray.withAlphaComponent(0.5) {
			textView.text = nil
			textView.textColor = UIColor.black
		}
	}

	func textViewDidEndEditing(_ textView: UITextView) {
		if textView.text.isEmpty {
			textView.text = descriptionPlaceHolder
			textView.textColor = UIColor.lightGray.withAlphaComponent(0.5)
		}
	}

	func clearImageWhenViewDisappears() {

		let headerIndexPath = IndexPath(item: 0, section: 0)
		let cell = collectionView.supplementaryView(forElementKind: UICollectionView.elementKindSectionHeader, at: headerIndexPath) as? ListItemHeaderReusableView
		self.mainImageArray?.removeAll()
		cell?.mianImage.image = UIImage(named: "MainSection")

		cell?.firstImage.image = nil
		cell?.firstImage.image = placeholder
		if let firstImage = cell?.firstImage.image {
			if firstImage.isEqualToImage(placeholder) {
				_ = cell?.firstImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = true }
			}
		}
		cell?.secondImage.image = nil
		cell?.secondImage.image = placeholder
		if let secondImage = cell?.secondImage.image {
			if secondImage.isEqualToImage(placeholder) {
				_ = cell?.secondImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = true }
			}
		}
		cell?.thirdImage.image = nil
		cell?.thirdImage.image = placeholder
		if let thirdImage = cell?.thirdImage.image {
			if thirdImage.isEqualToImage(placeholder) {
				_ = cell?.thirdImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = true }
			}
		}
		cell?.fourthImage.image = nil
		cell?.fourthImage.image = placeholder
		if let fourthImage = cell?.firstImage.image {
			if fourthImage.isEqualToImage(placeholder) {
				_ = cell?.fourthImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = true }
			}
		}
	}

	//MARK: Conforming to delegate
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if  (segue.identifier == ListItemController.itemConditionController) {
			let viewController = segue.destination as? ItemConditionController
			viewController?.delegate = self
		}
		if  (segue.identifier == ListItemController.priceNegotiableController) {
				let viewController = segue.destination as? PriceNegotiableController
				viewController?.delegate = self
			}
		if  (segue.identifier == ListItemController.panelViewController) {
				let viewController = segue.destination as? PanelViewController
				viewController?.delegate = self
			}

		///
		if let nav = segue.destination as? UINavigationController,
			let citiesDetails = nav.topViewController as? CitiesController {
			citiesDetails.cities = cities
			citiesDetails.delegate = self
		}

		if let nav = segue.destination as? UINavigationController,
			let countryVC = nav.topViewController as? CountriesController {
			countryVC.delegate = self
		}

		if let nav = segue.destination as? UINavigationController,
			let regionVC = nav.topViewController as? RegionController, let region = self.region, let countryCode = self.countryCode, let country = self.countryText {
			regionVC.provinceOrState = region
			regionVC.countryCode = countryCode
			regionVC.country = country
			regionVC.delegate = self
		}
	}
}


