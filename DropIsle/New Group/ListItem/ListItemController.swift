//
//  ListItemController.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-28.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit
import AVFoundation
import YPImagePicker
import CoreLocation
import IQKeyboardManagerSwift
import CoreLocation

protocol ConfirmProductPageDelegate: class {
	func ConfirmProductPage()
}

class ListItemController: UICollectionViewController,
	UITextViewDelegate,
	UIGestureRecognizerDelegate,
	UIDropInteractionDelegate,
	UIDragInteractionDelegate,
	UICollectionViewDelegateFlowLayout,
	CLLocationManagerDelegate,
	CategoriesDelegate,
	ItemConditionDelegate,
	PriceNegotiableDelegate,
	UploadItemDelegate,
	SelectProductImageDelegate,
	ModalHandler, CitiesDelegate, RegionDelegate, OpenCountriesDelegate {

	weak var delegate: ConfirmProductPageDelegate?
	private var reusableview = UICollectionReusableView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
	private var config = YPImagePickerConfiguration()

	private let headerId = "headerId"
	private let footerId = "footerId"
	static let panelViewController = "PanelViewController"
	static let itemConditionController = "ItemConditionController"
	static let priceNegotiableController = "PriceNegotiableController"
	var descriptionPlaceHolder = "Describe your product here and include basic details so a buyer might take interest..."
	var imagePickerIsOpen = false
	var placeholder = UIImage(named: "placeholder")!

	var mainImage: Data?
	var mainImageForDBUpload = UIImage()
	var imageArraySentToServer: [Data]? = [Data]()
	var mainImageArray: [UIImage]? = [UIImage]()
	var imageArrayElements: [UIImage]? = [UIImage]()
	var imageArrayForUpdate: [UIImage]? = [UIImage]()
	var imagesForDBUpload: [UIImage]? = [UIImage]()
	var imageIndexButton = UIButton()
	var imageIndexButton2 = UIButton()
	var imageIndexButton3 = UIButton()
	var imageIndexButton4 = UIButton()
	var curreny: String?
	var quantity = String()
	var isFromEdit = false
	var locationGotFromDevice = Bool()
	var isDragActionAdded = true
	var setActualVCHeight = false
	var productToEdit: ProductModel.content?


	///Location
	var countriesController = "CountriesController"
	var citiesController = "CitiesController"
	var regionController = "RegionController"

	var city: String?
	var region: String?
	var countryCode: String?
	var countryText: String?
	var cities: Countries?

	@IBOutlet weak var listingCollectionView: UICollectionView!

	override func viewDidLoad() {
		super.viewDidLoad()

		listingCollectionView.delegate = self
		listingCollectionView.dataSource = self

		///Keyboard applied to all screens
		IQKeyboardManager.shared.enable = false

		let flowLayout = UICollectionViewFlowLayout()
		listingCollectionView.collectionViewLayout = flowLayout
		navigationItem.title = "Sell Items"
		navigationController?.navigationBar.barTintColor = UIColor(hexString: "#3293FC")

		if #available(iOS 11.0, *) {
			navigationController?.navigationBar.prefersLargeTitles = false
		} else {
			// Fallback on earlier versions
		}

		listingCollectionView.showsVerticalScrollIndicator = false
		collectionView?.alwaysBounceVertical = true
		let headerNib = UINib(nibName: "ListItemHeader", bundle: nil)
		collectionView?.register(headerNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)

		let footerNib = UINib(nibName: "ListItemFooter", bundle: nil)
		collectionView?.register(footerNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerId)

		imageIndexButton = {
			let button = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
			button.backgroundColor = .white
			button.layer.shadowOpacity = 0.7
			button.layer.shadowOffset = .zero
			button.layer.cornerRadius = 10
			let image = UIImage(named: "closeColored") as UIImage?
			button.setImage(image, for: .normal)
			return button
		}()

		imageIndexButton2 = {
			let button = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
			button.backgroundColor = .white
			button.layer.shadowOpacity = 0.7
			button.layer.shadowOffset = .zero
			button.layer.cornerRadius = 10
			let image = UIImage(named: "closeColored") as UIImage?
			button.setImage(image, for: .normal)
			return button
		}()

		imageIndexButton3 = {
			let button = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
			button.backgroundColor = .white
			button.layer.shadowOpacity = 0.7
			button.layer.shadowOffset = .zero
			button.layer.cornerRadius = 10
			let image = UIImage(named: "closeColored") as UIImage?
			button.setImage(image, for: .normal)
			return button
		}()

		imageIndexButton4 = {
			let button = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
			button.backgroundColor = .white
			button.layer.shadowOpacity = 0.7
			button.layer.shadowOffset = .zero
			button.layer.cornerRadius = 10
			let image = UIImage(named: "closeColored") as UIImage?
			button.setImage(image, for: .normal)
			return button
		}()

		config.isScrollToChangeModesEnabled = true
		config.onlySquareImagesFromCamera = true
		config.usesFrontCamera = true
		config.shouldSaveNewPicturesToAlbum = true
		config.albumName = "DefaultYPImagePickerAlbumName"
		config.startOnScreen = .library
		config.screens = [.library, .photo]
		config.showsCrop = .none
		config.targetImageSize = YPImageSize.original
		config.overlayView = UIView()
		config.hidesStatusBar = true
		config.hidesBottomBar = false
		config.preferredStatusBarStyle = UIStatusBarStyle.default
		config.bottomMenuItemSelectedTextColour = UIColor(red: 0.20, green: 0.58, blue: 0.99, alpha: 1.0)
		config.bottomMenuItemUnSelectedTextColour = .lightGray
		config.showsPhotoFilters = false

		//Library
		config.library.options = nil
		config.library.onlySquare = false
		config.library.minWidthForItem = nil
		config.library.mediaType = YPlibraryMediaType.photo
		config.library.maxNumberOfItems = 4
		config.library.minNumberOfItems = 1
		config.library.numberOfItemsInRow = 4
		config.library.spacingBetweenItems = 1.0
		config.library.skipSelectionsGallery = false
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Avenir-Black", size: 20)!]

		///Analytics
		LoggerManager.shared.log(event: .listingProduct)
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		// MARK: Check if item to edit exist
		if productToEdit != nil {
			//let _ = IndexPath(row: 0, section: 0)
			//collectionView.scrollToItem(at: indexPath, at: .top, animated: true)
			listingCollectionView.reloadData()
			updateListItemTableWithProductToEdit(product: productToEdit!)
		} else {
			let headerIndexPath = IndexPath(item: 0, section: 0)
			let cell = collectionView.supplementaryView(forElementKind: UICollectionView.elementKindSectionHeader, at: headerIndexPath) as? ListItemHeaderReusableView
			cell?.mianImage.image = UIImage(named: "MainSection")

			///Reload the listing page to show header after Editing a product.
			if isFromEdit {
				listingCollectionView.reloadData()
				isFromEdit = false
			} else {
				if let imageArray = self.imageArrayElements {
					updateImageViewWithImages(imageArray: imageArray)
					self.imageArrayElements?.removeAll()
				}
			}
		}
	}

	override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		if (kind == UICollectionView.elementKindSectionHeader) {
			let section = indexPath.section
			switch (section) {
				case 0:
					guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as? ListItemHeaderReusableView else {
						return UICollectionReusableView()
					}

					headerView.delegate = self
					if isDragActionAdded {
						let dragInteraction = UIDragInteraction(delegate: self)
						dragInteraction.isEnabled = true
						headerView.firstImage.addInteraction(dragInteraction)

						let dragInteraction3 = UIDragInteraction(delegate: self)
						dragInteraction3.isEnabled = true
						headerView.secondImage.addInteraction(dragInteraction3)

						let dragInteraction4 = UIDragInteraction(delegate: self)
						dragInteraction4.isEnabled = true
						headerView.thirdImage.addInteraction(dragInteraction4)

						let dragInteraction5 = UIDragInteraction(delegate: self)
						dragInteraction5.isEnabled = true
						headerView.fourthImage.addInteraction(dragInteraction5)
						isDragActionAdded = false
					}

					let dropInteraction = UIDropInteraction(delegate: self)
					headerView.mianImage.addInteraction(dropInteraction)

					headerView.mianImage.isUserInteractionEnabled = true
					headerView.firstImage.isUserInteractionEnabled = true
					headerView.secondImage.isUserInteractionEnabled = true
					headerView.thirdImage.isUserInteractionEnabled = true
					headerView.fourthImage.isUserInteractionEnabled = true

					imageIndexButton.addTarget(self, action: #selector(self.deleteImage(_:)), for: .touchUpInside)
					imageIndexButton.tag = 1

					imageIndexButton2.addTarget(self, action: #selector(self.deleteImage(_:)), for: .touchUpInside)
					imageIndexButton2.tag = 2

					imageIndexButton3.addTarget(self, action: #selector(self.deleteImage(_:)), for: .touchUpInside)
					imageIndexButton3.tag = 3

					imageIndexButton4.addTarget(self, action: #selector(self.deleteImage(_:)), for: .touchUpInside)
					imageIndexButton4.tag = 4

					imageIndexButton.frame = CGRect(x: headerView.firstImage.bounds.maxX - 25, y: headerView.firstImage.frame.size.height/25, width: 20, height: 20)
					imageIndexButton.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]

					imageIndexButton2.frame = CGRect(x: headerView.secondImage.bounds.maxX - 25, y: headerView.secondImage.frame.size.height/25, width: 20, height: 20)
					imageIndexButton2.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]

					imageIndexButton3.frame = CGRect(x: headerView.thirdImage.bounds.maxX - 25, y: headerView.thirdImage.frame.size.height/25, width: 20, height: 20)
					imageIndexButton3.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]

					imageIndexButton4.frame = CGRect(x: headerView.fourthImage.bounds.maxX - 25, y: headerView.fourthImage.frame.size.height/25, width: 20, height: 20)
					imageIndexButton4.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
					reusableview = headerView
				default:
					return reusableview
			}
		} else if (kind == UICollectionView.elementKindSectionFooter) {
			let section = indexPath.section
			switch (section) {
				case 1:
					let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerId, for: indexPath) as! ListItemFooterReusableView
					footerView.delegate = self
					reusableview = footerView
				default:
					return reusableview
			}
		}
		return reusableview
	}

	func selectProductImageAction() {
		self.showPicker()
	}

	func showPicker() {
		imagePickerIsOpen = true
		let picker = YPImagePicker(configuration: config)
		picker.didFinishPicking { [unowned picker] items, _ in
			for item in items {
				switch item {
					case .photo(let photo):
						print(photo.image)
						let image = photo.image
						self.imageArrayElements?.append(image)
					default: break
				}
			}
			self.updateImageViewWithImages(imageArray: self.imageArrayElements ?? [UIImage(named: "placeholder")!])
			self.imageArrayElements?.removeAll()
			picker.dismiss(animated: true, completion: {
				self.imagePickerIsOpen = false
			})
		}
		if #available(iOS 13.0, *) {
			if UIDevice.current.userInterfaceIdiom == .pad {
				picker.modalPresentationStyle = .overFullScreen
			} else {
				picker.modalPresentationStyle = .automatic
			}
		} else {
			// Fallback on earlier versions
			picker.modalPresentationStyle = .overFullScreen
		}
		picker.modalTransitionStyle = .coverVertical
		picker.transitioningDelegate = self
		present(picker, animated: true, completion: nil)
	}

	func updateImageViewWithImages(imageArray: [UIImage]) {
		let indexPath = IndexPath(item: 0, section: 0)
		guard let header = collectionView?.supplementaryView(forElementKind: UICollectionView.elementKindSectionHeader, at: indexPath) as? ListItemHeaderReusableView else { return }
		self.imageArrayForUpdate = imageArray
		imageIndexButton.frame = CGRect(x: header.firstImage.bounds.maxX - 25, y: header.firstImage.frame.size.height/25, width: 20, height: 20)
		imageIndexButton.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
		imageIndexButton2.frame = CGRect(x: header.secondImage.bounds.maxX - 25, y: header.secondImage.frame.size.height/25, width: 20, height: 20)
		imageIndexButton2.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
		imageIndexButton3.frame = CGRect(x: header.thirdImage.bounds.maxX - 25, y: header.thirdImage.frame.size.height/25, width: 20, height: 20)
		imageIndexButton3.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
		imageIndexButton4.frame = CGRect(x: header.fourthImage.bounds.maxX - 25, y: header.fourthImage.frame.size.height/25, width: 20, height: 20)
		imageIndexButton4.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]

		///Applying images to listing
		switch imageArrayForUpdate?.count {
			case 1:
				if let firstImage = header.firstImage.image {
					if firstImage.isEqualToImage(placeholder) {
						_ = header.firstImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.firstImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.firstImage.addSubview(imageIndexButton)
						imagesForDBUpload?.append(header.firstImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
				}
				if let secondImage = header.secondImage.image {
					if secondImage.isEqualToImage(placeholder) {
						guard !imageArrayForUpdate!.isEmpty else { return }
						_ = header.secondImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.secondImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.secondImage.addSubview(imageIndexButton2)
						imagesForDBUpload?.append(header.secondImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
				}
				if let thirdImage = header.thirdImage.image {
					if thirdImage.isEqualToImage(placeholder) {
						guard !imageArrayForUpdate!.isEmpty else { return }
						_ = header.thirdImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.thirdImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.thirdImage.addSubview(imageIndexButton3)
						imagesForDBUpload?.append(header.thirdImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
				}
				if let fourthImage = header.fourthImage.image {
					if fourthImage.isEqualToImage(placeholder) {
						guard !imageArrayForUpdate!.isEmpty else { return }
						_ = header.fourthImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.fourthImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.fourthImage.addSubview(imageIndexButton4)
						imagesForDBUpload?.append(header.fourthImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
			}
			case 2:
				if let firstImage = header.firstImage.image {
					if firstImage.isEqualToImage(placeholder) {
						_ = header.firstImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.firstImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.firstImage.addSubview(imageIndexButton)
						imagesForDBUpload?.append(header.firstImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
				}
				if let secondImage = header.secondImage.image {
					if secondImage.isEqualToImage(placeholder) {
						guard !imageArrayForUpdate!.isEmpty else { return }
						_ = header.secondImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.secondImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.secondImage.addSubview(imageIndexButton2)
						imagesForDBUpload?.append(header.secondImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
				}
				if let thirdImage = header.thirdImage.image {
					if thirdImage.isEqualToImage(placeholder) {
						guard !imageArrayForUpdate!.isEmpty else { return }
						_ = header.thirdImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.thirdImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.thirdImage.addSubview(imageIndexButton3)
						imagesForDBUpload?.append(header.thirdImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
				}
				if let fourthImage = header.fourthImage.image {
					if fourthImage.isEqualToImage(placeholder) {
						guard !imageArrayForUpdate!.isEmpty else { return }
						_ = header.fourthImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.fourthImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.fourthImage.addSubview(imageIndexButton4)
						imagesForDBUpload?.append(header.fourthImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
			}
			case 3:
				if let firstImage = header.firstImage.image {
					if firstImage.isEqualToImage(placeholder) {
						_ = header.firstImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.firstImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.firstImage.addSubview(imageIndexButton)
						imagesForDBUpload?.append(header.firstImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
				}
				if let secondImage = header.secondImage.image {
					if secondImage.isEqualToImage(placeholder) {
						guard !imageArrayForUpdate!.isEmpty else { return }
						_ = header.secondImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.secondImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.secondImage.addSubview(imageIndexButton2)
						imagesForDBUpload?.append(header.secondImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
				}
				if let thirdImage = header.thirdImage.image {
					if thirdImage.isEqualToImage(placeholder) {
						guard !imageArrayForUpdate!.isEmpty else { return }
						_ = header.thirdImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.thirdImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.thirdImage.addSubview(imageIndexButton3)
						imagesForDBUpload?.append(header.thirdImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
				}
				if let fourthImage = header.fourthImage.image {
					if fourthImage.isEqualToImage(placeholder) {
						guard !imageArrayForUpdate!.isEmpty else { return }
						_ = header.fourthImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.fourthImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.fourthImage.addSubview(imageIndexButton4)
						imagesForDBUpload?.append(header.fourthImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
			}
			case 4:
				if let firstImage = header.firstImage.image {
					if firstImage.isEqualToImage(placeholder) {
						_ = header.firstImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.firstImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.firstImage.addSubview(imageIndexButton)
						imagesForDBUpload?.append(header.firstImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
				}
				if let secondImage = header.secondImage.image {
					if secondImage.isEqualToImage(placeholder) {
						guard !imageArrayForUpdate!.isEmpty else { return }
						_ = header.secondImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.secondImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.secondImage.addSubview(imageIndexButton2)
						imagesForDBUpload?.append(header.secondImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
				}
				if let thirdImage = header.thirdImage.image {
					if thirdImage.isEqualToImage(placeholder) {
						guard !imageArrayForUpdate!.isEmpty else { return }
						_ = header.thirdImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.thirdImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.thirdImage.addSubview(imageIndexButton3)
						imagesForDBUpload?.append(header.thirdImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
				}
				if let fourthImage = header.fourthImage.image {
					if fourthImage.isEqualToImage(placeholder) {
						guard !imageArrayForUpdate!.isEmpty else { return }
						_ = header.fourthImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = false }
						header.fourthImage.image = imageArrayForUpdate?[0].resized(withPercentage: 0.5)
						header.fourthImage.addSubview(imageIndexButton4)
						imagesForDBUpload?.append(header.fourthImage.image!)
						imageArrayForUpdate = imageArrayForUpdate!.filter { $0 != imageArrayForUpdate?[0] }
					}
			}
			default: break
		}
	}

	///Deleting images from listing
	@objc func deleteImage(_ sender: UIButton) {
		let indexPath = IndexPath(item: 0, section: 0)
		guard let header = collectionView?.supplementaryView(forElementKind: UICollectionView.elementKindSectionHeader, at: indexPath) as? ListItemHeaderReusableView else { return }

		if sender.tag == 1 {
			for img in imagesForDBUpload! {
				if img.isEqualToImage(header.firstImage.image!) {
					imagesForDBUpload = imagesForDBUpload!.filter { $0 != img }
				}
			}
			header.firstImage.image = placeholder
			if (header.firstImage.image == placeholder) {
				UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
					header.firstImage.image = header.secondImage.image
					header.secondImage.image = header.thirdImage.image
					header.thirdImage.image = header.fourthImage.image

					//set last image to placeholder
					header.fourthImage.image = self.placeholder
					if let firstImage = header.firstImage.image, let secondImage = header.secondImage.image, let thirdImage = header.thirdImage.image, let fourthImage = header.fourthImage.image {
						if firstImage.isEqualToImage(self.placeholder) {
							_ = header.firstImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = true }
						}
						if secondImage.isEqualToImage(self.placeholder) {
							_ = header.secondImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = true }
						}
						if thirdImage.isEqualToImage(self.placeholder) {
							_ = header.thirdImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = true }
						}
						if fourthImage.isEqualToImage(self.placeholder) {
							_ = header.fourthImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = true }
						}
					}
				}) { (_) in }
			}
		}

		if sender.tag == 2 {
			for img in imagesForDBUpload! {
				if img.isEqualToImage(header.secondImage.image!) {
					imagesForDBUpload = imagesForDBUpload!.filter { $0 != img }
				}
			}
			header.secondImage.image = placeholder
			if (header.secondImage.image == placeholder) {
				UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
					header.secondImage.image = header.thirdImage.image
					header.thirdImage.image = header.fourthImage.image

					//set last image to placeholder
					header.fourthImage.image = self.placeholder
					if let secondImage = header.secondImage.image, let thirdImage = header.thirdImage.image, let fourthImage = header.fourthImage.image {
						if secondImage.isEqualToImage(self.placeholder) {
							_ = header.secondImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = true }
						}
						if thirdImage.isEqualToImage(self.placeholder) {
							_ = header.thirdImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = true }
						}
						if fourthImage.isEqualToImage(self.placeholder) {
							_ = header.fourthImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = true }
						}
					}
				}) { (_) in }
			}
		}

		if sender.tag == 3 {
			for img in imagesForDBUpload! {
				if img.isEqualToImage(header.thirdImage.image!) {
					imagesForDBUpload = imagesForDBUpload!.filter { $0 != img }
				}
			}
			header.thirdImage.image = placeholder
			if (header.thirdImage.image == placeholder) {
				UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
					header.thirdImage.image = header.fourthImage.image

					//set last image to placeholder
					header.fourthImage.image = self.placeholder
					if let thirdImage = header.thirdImage.image, let fourthImage = header.fourthImage.image {
						if thirdImage.isEqualToImage(self.placeholder) {
							_ = header.thirdImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = true }
						}
						if fourthImage.isEqualToImage(self.placeholder) {
							_ = header.fourthImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = true }
						}
					}
				}) { (_) in }
			}
		}

		if sender.tag == 4 {
			for img in imagesForDBUpload! {
				if img.isEqualToImage(header.fourthImage.image!) {
					imagesForDBUpload = imagesForDBUpload!.filter { $0 != img }
				}
			}
			_ = header.fourthImage.subviews.filter { $0 is UIButton }.map { $0.isHidden = true }
			header.fourthImage.image = placeholder
		}
	}

	// Drop Interaction
	func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
		let indexPath = IndexPath(item: 0, section: 0)
		guard let header = collectionView?.supplementaryView(forElementKind: UICollectionView.elementKindSectionHeader, at: indexPath) as? ListItemHeaderReusableView else {
			return
		}

		for dragItem in session.items {
			dragItem.itemProvider.loadObject(ofClass: UIImage.self, completionHandler: { object, error in
				guard error == nil else { return print("Failed to load our dragged item") }
				guard let draggedImage = object as? UIImage else { return }
				DispatchQueue.main.async {
					header.mianImage.image  = nil
					header.mianImage.image = draggedImage
					self.mainImageForDBUpload = draggedImage
					self.mainImageArray?.removeAll()
					self.mainImageArray?.append(draggedImage)
				}
			})
		}
	}

	func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
		return UIDropProposal(operation: .copy)
	}

	func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
		return session.canLoadObjects(ofClass: UIImage.self)
	}

	// Drag Interaction
	func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
		let indexPath = IndexPath(item: 0, section: 0)
		guard let header = collectionView?.supplementaryView(forElementKind: UICollectionView.elementKindSectionHeader, at: indexPath) as? ListItemHeaderReusableView else {
			return []
		}

		let firstTouchPoint = session.location(in: header.firstImage)
		if let touchImageView = header.firstImage.hitTest(firstTouchPoint, with: nil) as? UIImageView {
			let touchImage = touchImageView.image
			guard let image = touchImage else { return [] }
			if image.isEqualToImage(self.placeholder) { return [] }
			let itemProv = NSItemProvider(object: touchImage!)
			let dragItem = UIDragItem(itemProvider: itemProv)
			dragItem.localObject = touchImageView
			return [dragItem]
		}

		let secondTouchPoint = session.location(in: header.secondImage)
		if let touchImageView = header.secondImage.hitTest(secondTouchPoint, with: nil) as? UIImageView {
			let touchImage = touchImageView.image
			guard let image = touchImage else { return [] }
			if image.isEqualToImage(self.placeholder) { return [] }
			let itemProv = NSItemProvider(object: touchImage!)
			let dragItem = UIDragItem(itemProvider: itemProv)
			dragItem.localObject = touchImageView
			return [dragItem]
		}

		let thirdTouchPoint = session.location(in: header.thirdImage)
		if let touchImageView = header.thirdImage.hitTest(thirdTouchPoint, with: nil) as? UIImageView {
			let touchImage = touchImageView.image
			guard let image = touchImage else { return [] }
			if image.isEqualToImage(self.placeholder) { return [] }
			let itemProv = NSItemProvider(object: touchImage!)
			let dragItem = UIDragItem(itemProvider: itemProv)
			dragItem.localObject = touchImageView
			return [dragItem]
		}

		let fourthTouchPoint = session.location(in: header.fourthImage)
		if let touchImageView = header.fourthImage.hitTest(fourthTouchPoint, with: nil) as? UIImageView {
			let touchImage = touchImageView.image
			guard let image = touchImage else { return [] }
			if image.isEqualToImage(self.placeholder) { return [] }
			let itemProv = NSItemProvider(object: touchImage!)
			let dragItem = UIDragItem(itemProvider: itemProv)
			dragItem.localObject = touchImageView
			return [dragItem]
		}
		return []
	}

	///Add Category Name On Select
	func categories(categoryName: String) {
		let firstIndexPath = IndexPath(item: 0, section: 1)
		let secondCell = collectionView.cellForItem(at: firstIndexPath) as? SecondViewCell
		secondCell?.category.setTitle(categoryName, for: .normal)
	}

	func itemCondition(itemCondition: String) {
		let firstIndexPath = IndexPath(item: 0, section: 1)
		let secondCell = collectionView.cellForItem(at: firstIndexPath) as? SecondViewCell
		secondCell?.itemCondition.setTitle(itemCondition, for: .normal)
	}

	func priceNegotiable(priceNegotiable: String) {
		let firstIndexPath = IndexPath(item: 0, section: 1)
		let secondCell = collectionView.cellForItem(at: firstIndexPath) as? SecondViewCell
		secondCell?.priceNegotiable.setTitle(priceNegotiable, for: .normal)
	}

	func openCountry(cell: SecondViewCell) {
		self.performSegue(withIdentifier: countriesController, sender: self)
	}

	func modalDismissed(cities: Countries) {
		self.cities = cities
		self.performSegue(withIdentifier: self.citiesController, sender: self)
	}

	func usersCity(country: String?, city: String?, code: String?) {
		let firstIndexPath = IndexPath(item: 0, section: 1)
		let secondCell = collectionView.cellForItem(at: firstIndexPath) as? SecondViewCell
		self.city = String()
		self.region = city
		self.countryCode = code
		self.countryText = country
		secondCell?.location.setTitle("\(city ?? "")" + ", " + "\(country ?? "") " + " " + "\(code ?? "")", for: .normal)
	}

	///setting province or state name
	func setUserCity(city: String?, code: String?, country: String?) {
		self.region = city
		self.countryCode = code
		self.countryText = country
		self.performSegue(withIdentifier: self.regionController, sender: self)
	}

	///getting region name
	func getRegion(region: String?, code: String?, city: String?, country: String?) {
		let firstIndexPath = IndexPath(item: 0, section: 1)
		let secondCell = collectionView.cellForItem(at: firstIndexPath) as? SecondViewCell
		self.city = city
		self.region = region
		self.countryCode = code
		self.countryText = country
		secondCell?.location.setTitle("\(region ?? "")" + ", " + "\(city ?? "")" + ", " + "\(country ?? "")" + " " + "\(code ?? "")", for: .normal)
	}

	@IBAction func closeScreen(_ sender: UIBarButtonItem) {
		self.imageArrayElements?.removeAll()
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
			self.dismiss(animated: true, completion: nil)
		}
	}
}

class MyTapGesture: UITapGestureRecognizer {
	var position = Int()
}

extension ListItemController: UIViewControllerTransitioningDelegate {
	//	func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
	//		if !setActualVCHeight {
	//			return HalfSizePresentationController(presentedViewController: presented, presenting: presenting)
	//		}	else {
	//			return QuaterSizePresentationController(presentedViewController: presented, presenting: presenting)
	//		}
	//	}
}

extension ListItemController {

	override func numberOfSections(in collectionView: UICollectionView) -> Int {
		return Sections.numberOfSections
	}

	override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		guard let section = Sections(rawValue: section) else {
			return 0
		}

		switch section {
			case .firstCell:
				return 1
			case .secondCell:
				return 1
		}
	}

	override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let section = Sections(rawValue: indexPath.section) else {
			let cell = UICollectionViewCell()
			return cell
		}

		switch section {
			case .firstCell:
				guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FirstViewCell", for: indexPath) as? FirstViewCell else {
					return UICollectionViewCell()
				}
				cell.containerView.clipsToBounds = true
				cell.containerView.layer.cornerRadius = 20
				cell.containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
				return cell
			case .secondCell:
				guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SecondViewCell", for: indexPath) as? SecondViewCell else {
					return UICollectionViewCell()
				}
				cell.containerView.clipsToBounds = true
				cell.containerView.layer.cornerRadius = 20
				cell.containerView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
				cell.delegate = self

				self.city = CurrentUserInfo.shared.city()
				self.region = CurrentUserInfo.shared.region()
				self.countryCode = CurrentUserInfo.shared.countryCode()
				cell.location.setTitle("\(self.city ?? "")" + ", " +  "\(self.region ?? "")" +  " " +  "\(self.countryCode ?? "")", for: .normal)
				return cell
		}
	}
}

extension ListItemController {

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		guard let section = Sections(rawValue: indexPath.section) else {
			return CGSize(width: 0, height: 0)
		}

		if (section.rawValue == 0) {
			return CGSize(width: UIScreen.main.bounds.width, height: 312)
		} else {
			return CGSize(width: UIScreen.main.bounds.width, height: 420)
		}
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

		guard let section = Sections(rawValue: section) else {
			return CGSize(width: 0, height: 0)
		}

		if (section.rawValue == 0) {
			return productToEdit == nil ? CGSize(width: view.frame.width, height: view.frame.width) : CGSize(width: view.frame.width, height: 0)
		} else {
			return CGSize(width: 0, height: 0)
		}
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {

		guard let section = Sections(rawValue: section) else {
			return CGSize(width: 0, height: 0)
		}

		if (section.rawValue == 1) {
			return CGSize(width: view.frame.width, height: 100)
		} else {
			return CGSize(width: 0, height: 0)
		}
	}
}

enum Sections: Int {
	case firstCell
	case secondCell
	static var numberOfSections: Int {
		return 2
	}
}

extension UIView {
	func addTapGesture(tapNumber: Int, target: Any, action: Selector) {
		let tap = UITapGestureRecognizer(target: target, action: action)
		tap.numberOfTapsRequired = tapNumber
		addGestureRecognizer(tap)
		isUserInteractionEnabled = true
	}
}

extension ListItemController {

	override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		///Get user location from lat / lng
		if !locationGotFromDevice {
			///getCllocation()
			locationGotFromDevice = true
		}
	}

	func getCllocation() {
		guard let lat = Double(preferences[.latitude]!) else { return }
		guard let lng = Double(preferences[.longitude]!) else { return }
		let coord = CLLocation(latitude: lat, longitude: lng)
		updateLocation(location: coord)
	}

	func updateLocation(location: CLLocation?) {
		if let loc = location {
			let geocoder = CLGeocoder()
			geocoder.reverseGeocodeLocation(loc) { (placemarksArray, _) in
				guard let placeMark = placemarksArray else {
					return
				}
				if (placeMark.count) > 0 {
					let placemark = placemarksArray?.first
					let firstIndexPath = IndexPath(item: 0, section: 1)
					let _ = self.collectionView.cellForItem(at: firstIndexPath) as? SecondViewCell
					let _ = "\(placemark?.name ?? "")" + ", " +   "\(placemark?.locality ?? "")" + ", " +  "\(placemark?.administrativeArea ?? "")" +  ", " +  "\(placemark?.isoCountryCode ?? "")"
					///self.city = placemark?.locality
					///self.region = placemark?.administrativeArea
					///self.countryCode = placemark?.isoCountryCode
					///self.countryText = placemark?.country
				}
			}
		}
	}
}
