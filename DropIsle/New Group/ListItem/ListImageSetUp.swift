//
//  ListImageSetUp.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-04.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

class ListImageSetUp: UIImageView {

	let imageIndexLabel: UILabel = {
		let label = UILabel()
		label.textColor = .white
		label.text = "Main"
		label.font = UIFont.boldSystemFont(ofSize: 18)
		label.layer.shadowOpacity = 0.7
		label.layer.shadowOffset = .zero
		return label
	}()

	let imageIndexView: UIImageView = {
		let imageView = UIImageView()
		imageView.image = UIImage(named: "mainBanner")
		return imageView
	}()

	@IBInspectable
	var imageIndex: NSNumber? {
		didSet {
			layer.cornerRadius = 5
		}
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		addSubview(imageIndexView)
		imageIndexView.translatesAutoresizingMaskIntoConstraints = false
		imageIndexView.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
		imageIndexView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
	}
}

class HeaderImageIndex: UIImageView {
	@IBInspectable
	var imageIndex: NSNumber? {
		didSet {
			print(imageIndex ?? 0)
		}
	}
}
