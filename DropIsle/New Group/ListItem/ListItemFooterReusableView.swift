//
//  ListItemFooterReusableView.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-05.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol UploadItemDelegate: class {
	func uploadItem(sender: UIButton)
}

class ListItemFooterReusableView: UICollectionReusableView {
	weak var delegate: UploadItemDelegate?

	@IBOutlet weak var listButton: UIButton!

	@IBAction func listItem(_ sender: UIButton) {
		self.delegate?.uploadItem(sender: sender)
	}
}
