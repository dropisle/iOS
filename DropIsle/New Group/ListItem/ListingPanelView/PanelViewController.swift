//
//  PanelViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-05.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol CategoriesDelegate: class {
	func categories(categoryName: String)
}

class PanelViewController: UIViewController {

	weak var delegate: CategoriesDelegate?
	var categoryValue: String = ""

	@IBOutlet weak var containerView: UIView!

	override func viewDidLoad() {
		super.viewDidLoad()
			containerView.clipsToBounds = true
			containerView.layer.cornerRadius = 20
			containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

	@IBAction func Books(_ sender: UIButton) {
		self.delegate?.categories(categoryName: "Books & Arts")
		self.dismiss(animated: true, completion: nil)
	}

	@IBAction func BathAndHealth(_ sender: UIButton) {
		self.delegate?.categories(categoryName: "Beauty & Health")
		self.dismiss(animated: true, completion: nil)
	}

	@IBAction func apparel(_ sender: UIButton) {
		self.delegate?.categories(categoryName: "Fashion & Footwear")
		self.dismiss(animated: true, completion: nil)
	}

	@IBAction func ElectronicsAndAppliances(_ sender: UIButton) {
		self.delegate?.categories(categoryName: "Electronics & Appliances")
		self.dismiss(animated: true, completion: nil)
	}

	@IBAction func Household(_ sender: UIButton) {
		self.delegate?.categories(categoryName: "Household")
		self.dismiss(animated: true, completion: nil)
	}

	@IBAction func Jewelry(_ sender: UIButton) {
		self.delegate?.categories(categoryName: "Fresh Produce")
		self.dismiss(animated: true, completion: nil)
	}

	@IBAction func closeView(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}
}
