//
//  ItemConditionController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-02-29.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol ItemConditionDelegate: class {
	func itemCondition(itemCondition: String)
}

class ItemConditionController: UIViewController {
	
	weak var delegate: ItemConditionDelegate?

	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var newItem: UIButton!
	@IBOutlet weak var usedItem: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()
		containerView.clipsToBounds = true
		containerView.layer.cornerRadius = 20
		containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
	}
    
	@IBAction func newItemAction(_ sender: UIButton) {
		if let sender = sender.titleLabel!.text {
			self.delegate?.itemCondition(itemCondition: sender)
			self.dismiss(animated: true, completion: nil)
		}
	}

	@IBAction func usedItemAction(_ sender: UIButton) {
		if let sender = sender.titleLabel!.text {
			self.delegate?.itemCondition(itemCondition: sender)
			self.dismiss(animated: true, completion: nil)
		}
	}

	@IBAction func closeScreen(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}
}

