//
//  PriceNegotiableController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-02-29.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol PriceNegotiableDelegate: class {
	func priceNegotiable(priceNegotiable: String)
}

class PriceNegotiableController: UIViewController {

	weak var delegate: PriceNegotiableDelegate?

	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var fixedPrice: UIButton!
	@IBOutlet weak var negotiablePrice: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()
		containerView.clipsToBounds = true
		containerView.layer.cornerRadius = 20
		containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
	}

	@IBAction func fixedPriceAction(_ sender: UIButton) {
		if let sender = sender.titleLabel!.text {
				self.delegate?.priceNegotiable(priceNegotiable: sender)
				self.dismiss(animated: true, completion: nil)
			}
	}

	@IBAction func negotiablePriceAction(_ sender: UIButton) {
		if let sender = sender.titleLabel!.text {
				self.delegate?.priceNegotiable(priceNegotiable: sender)
				self.dismiss(animated: true, completion: nil)
			}
	}
	
	@IBAction func closeScreen(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}
}
