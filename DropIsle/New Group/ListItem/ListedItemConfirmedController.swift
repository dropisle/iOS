//
//  ListedItemConfirmedController.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-28.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol ListedItemConfirmedDelegate: class {
	func confirmUploadedProduct()
}

class ListedItemConfirmedController: UIViewController {

	weak var delegate: ListedItemConfirmedDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func confirmListing(_ sender: UIButton) {
			self.delegate?.confirmUploadedProduct()
			self.dismiss(animated: true, completion: nil)
    }

    @IBAction func closeScreen(_ sender: UIButton) {
        //self.dismiss(animated: true, completion: nil)
    }
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
