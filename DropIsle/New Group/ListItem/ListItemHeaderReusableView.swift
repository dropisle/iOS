//
//  ListItemHeaderReusableView.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-04.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol SelectProductImageDelegate: class {
	func selectProductImageAction()
}

class ListItemHeaderReusableView: UICollectionReusableView {

	weak var delegate: SelectProductImageDelegate?
	
	@IBOutlet weak var mianImage: UIImageView!
	@IBOutlet weak var firstImage: UIImageView!
	@IBOutlet weak var secondImage: UIImageView!
	@IBOutlet weak var thirdImage: UIImageView!
	@IBOutlet weak var fourthImage: UIImageView!
	@IBOutlet weak var notifiableText: UILabel!
	@IBOutlet weak var selectProductImage: UIButton!
	@IBOutlet weak var infoImage: UIImageView!

	override func awakeFromNib() {
		super.awakeFromNib()
		let tintedImage = UIImage(named: "addImage")?.withRenderingMode(.alwaysTemplate)
		selectProductImage.setImage(tintedImage, for: .normal)
		selectProductImage.tintColor = .white

		let info = UIImage(named: "info")?.withRenderingMode(.alwaysTemplate)
		infoImage.image = info
		infoImage.tintColor = .lightGray
	}

	@IBAction func selectProductImageAction(_ sender: UIButton) {
		self.delegate?.selectProductImageAction()
	}
}
