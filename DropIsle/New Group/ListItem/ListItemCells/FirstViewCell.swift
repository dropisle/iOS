//
//  FirstViewCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-04.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import CoreLocation

class FirstViewCell: UICollectionViewCell, UITextViewDelegate, UITextFieldDelegate {

	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var productTitle: UITextField!
	@IBOutlet weak var productPrice: UITextField!
	@IBOutlet weak var productDescription: UITextView!

	override func awakeFromNib() {
		super.awakeFromNib()
		self.productTitle.delegate = self
		self.productPrice.delegate = self
		self.productDescription.delegate = self
		self.productDescription.layer.cornerRadius = 5
		self.productDescription.layer.borderWidth = 0.9
		self.productDescription.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
		self.productDescription.text = "Describe your product here and include basic details so a buyer might take interest..."
		self.productDescription.textColor = UIColor.lightGray.withAlphaComponent(0.5)

		guard let currencySymbol = Locale.currency[CurrentUserInfo.shared.countryCode()]?.symbol else { return }
		self.productPrice.placeholder = currencySymbol

		productPrice.addTarget(self, action: #selector(myTextFieldDidChange), for: .editingChanged)
	}

	@objc func myTextFieldDidChange(_ textField: UITextField) {
		guard let currencySymbol = Locale.currency[CurrentUserInfo.shared.countryCode()]?.symbol else { return }
		if let amountString = textField.text?.currencyInputFormatting(currencySymbol: "\(currencySymbol) ") {
			textField.text = amountString
		}
	}

	func textFieldDidBeginEditing(_ textField: UITextField) {
		switch textField {
			case productPrice:
				textField.text = ""
			default: break
		}
	} 

	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		switch textField {
			case productTitle: _ = productPrice.becomeFirstResponder()
			case productPrice: _ = textField.resignFirstResponder()
			default: break
		}
		return true
	}

	func textViewDidBeginEditing(_ textView: UITextView) {
		if textView.textColor == UIColor.lightGray.withAlphaComponent(0.5) {
			textView.text = nil
			textView.textColor = UIColor.black
		}
	}

	func textViewDidEndEditing(_ textView: UITextView) {
		if textView.text.isEmpty {
			textView.text = "Describe your product here and include basic details so a buyer might take interest..."
			textView.textColor = UIColor.lightGray.withAlphaComponent(0.5)
		}
	}

	func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
		let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
		let numberOfChars = newText.count
		return numberOfChars < 1001
	}
}

