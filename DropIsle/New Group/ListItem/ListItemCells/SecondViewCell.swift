//
//  SecondViewCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-04.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol OpenCountriesDelegate: class {
	func openCountry(cell: SecondViewCell)
}

class SecondViewCell: UICollectionViewCell {

	weak var delegate: OpenCountriesDelegate?

	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var category: UIButton!
	@IBOutlet weak var stockQuantity: UITextField!
	@IBOutlet weak var itemCondition: UIButton!
	@IBOutlet weak var priceNegotiable: UIButton!
	@IBOutlet weak var location: UIButton!

	@IBAction func locationAction(_ sender: UIButton) {
		self.delegate?.openCountry(cell: self)
	}
}
