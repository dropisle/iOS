//
//  ConversationViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-07-23.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import MessageKit
//import MessageInputBar
import MapKit
import DropDown
import SVProgressHUD
import SDWebImage
import IQKeyboardManagerSwift
import KeychainAccess
import SwiftMessages

class ConversationViewController: MessagesViewController {

//}, InfoWarningViewDelegate, ReportUserDelegate {
//
//	let refreshControl = UIRefreshControl()
//	var dropDown = DropDown()
//	var appearance = DropDown.appearance()
//	var keychain = Keychain()
//	var messageList: [MessageModel] = []
//	var blockList = [BlockedUserListModel.content]()
//	var sellersName: String?
//	var isTyping = false
//	var productDetails: ProductDetailsModel?
//	var chatDetails: ChatModel?
//	var isChatSent = false
//	var isUserBlocked = String()
//
//	///Set variables on notification click
//	var productTitle = String()
//	var productIdentifier = String()
//	var otherUserId = String()
//
//	///UI attribute initializers
//	var customView = Bundle.main.loadNibNamed("Conversation", owner: self, options: nil)?.first as? ConversationView
//	var nameLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 5))
//	var imageview = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
//	lazy var formatter: DateFormatter = {
//		let formatter = DateFormatter()
//		formatter.dateStyle = .medium
//		return formatter
//	}()
//
//	override func loadView() {
//		super.loadView()
//		updateUserInfoAndLoadMessages(fromDetailPage: productDetails, fromChatPage: chatDetails)
//	}
//
//	override func viewDidLoad() {
//		super.viewDidLoad()
//		navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Avenir-Black", size: 15)!]
//		NotificationCenter.default.addObserver(self, selector: #selector(openMessageOnNotificationTapped), name: Notification.Name("ReceivedMessage"), object: nil)
//
//		// MARK: Get user details
//		getUserDetails(userId: CurrentUserInfo.shared.userId())
//
//		//MARK: Get blocked list
//		getBlockedUsersList()
//
//		///Adding custom header view to message conversation screen
//		let topBarHeight = UIApplication.shared.statusBarFrame.size.height + (self.navigationController?.navigationBar.frame.height ?? 0.0)
//		if let customView = customView {
//			self.view.addSubview(customView)
//			customView.delegate = self
//			customView.frame = CGRect(x: 0, y: topBarHeight, width: view.bounds.width, height: 80)
//			customView.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin]
//		}
//
//		messagesCollectionView.addSubview(refreshControl)
//		refreshControl.addTarget(self, action: #selector(ConversationViewController.loadMoreMessages), for: .valueChanged)
//
//		///Setting up nav bar buttons
//		let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "More")?.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(moreOption))
//		rightBarButtonItem.tintColor = UIColor.white
//		self.navigationItem.rightBarButtonItem = rightBarButtonItem
//
//		let back = UIImage(named: "ArrowLeft")!
//		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
//		backButton.tintColor = UIColor.white
//
//		let containView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
//		containView.layer.cornerRadius = containView.frame.size.width/2
//
//		///Set up and display seller name and image navigating from detail page
//		if let userImage = productDetails?.user.photo {
//			getProfilePhoto(photoImage: userImage) { [weak self] (imageData) in
//				guard let self = self else { return }
//				DispatchQueue.main.async {
//					self.imageview.contentMode = .scaleAspectFill
//					self.imageview.image = UIImage(data: imageData)
//				}
//			}
//		}
//		containView.addSubview(imageview)
//		imageview.makeRounded()
//
//		let leftImageItem = UIBarButtonItem(customView: containView)
//		nameLabel.text = "\(productDetails?.user.firstname ?? "") \(productDetails?.user.lastname ?? "")"
//		nameLabel.font = UIFont(name: "Avenir-Black", size: 15)
//		nameLabel.sizeToFit()
//		nameLabel.textColor = UIColor.white
//		let leftNameItem = UIBarButtonItem(customView: nameLabel)
//
//		navigationItem.leftBarButtonItems = [backButton, leftImageItem, leftNameItem]
//		navigationController?.navigationBar.barTintColor = UIColor(hexString: "#3293FC")
//		///end
//
//		messagesCollectionView.messagesDataSource = self
//		messagesCollectionView.messagesLayoutDelegate = self
//		messagesCollectionView.messagesDisplayDelegate = self
//		messagesCollectionView.messageCellDelegate = self
//		messageInputBar.delegate = self
//
//		messageInputBar.sendButton.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
//		scrollsToBottomOnKeyboardBeginsEditing = true
//		maintainPositionOnKeyboardFrameChanged = true
//
//		///Hide and Show custome view to avoid hiding messages.
//		self.messagesCollectionView.contentInset = UIEdgeInsets(top: 80, left: 0, bottom: 0, right: 0)
//	}
//
//	override func viewWillAppear(_ animated: Bool) {
//		super.viewWillAppear(animated)
//		///Keyboard disabled for current screen
//		IQKeyboardManager.shared.enable = false
//
//		if let tabBarController = self.tabBarController as? TabBarViewController {
//		  tabBarController.customListingButton.isHidden = true
//		}
//
//		///Ananlytics
//		LoggerManager.shared.log(event: .conversation)
//	}
//
//	override func viewWillDisappear(_ animated: Bool) {
//		super.viewWillDisappear(animated)
//		NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReceivedMessage"), object: nil)
//	}
//
//	override func viewDidAppear(_ animated: Bool) {
//		super.viewDidAppear(animated)
//		///Automatically show info warning message when conversation screen opens
//		customView?.infoButton.sendActions(for: .touchUpInside)
//	}
//
//	///Show safety info
//	func showInfo() {
//		let arrowPosition: InfoViewArrowPosition = .top
//		let formattedString = NSMutableAttributedString()
//		let normal = "\nMeet in a public place with other people around like a shopping mall, coffee shop or police station. \nDo transactions during the day or early evening."
//		let subBold = "\nGot it!"
//		let bold = "Safety tips for buying & selling"
//		DispatchQueue.main.async {
//			formattedString.bold(bold).normal(normal).bold(subBold)
//			let infoView = InfoView(text: formattedString, arrowPosition: arrowPosition, animation: .fadeIn, delegate: self)
//			if let customView = self.customView {
//				infoView.show(onView: self.view, centerView: customView)
//			}
//			DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
//			infoView.hide()
//			}
//		}
//	}
//
//	//MARK: Update conversation with new message if conversation screen is in background or forground.
//
//	@objc func openMessageOnNotificationTapped(_ notification: NSNotification) {
//		let userInfo = notification.userInfo
//		let dict = notification.object as? NSDictionary
//		let isNotificationClicked = dict?["isNotificationClicked"]
//		guard let fromUser = userInfo?[AnyHashable("fromuser")] as? String, let toUser = userInfo?[AnyHashable("touser")] as? String, let productId = userInfo?[AnyHashable("productid")] as? String  else { return }
//		guard let aps = userInfo?["aps"] as? NSDictionary, let alert = aps["alert"] as? NSDictionary, let _ = alert["body"] as? String, let title = alert["title"] as? String else { return }
//		productTitle = title
//		productIdentifier = productId
//		otherUserId = fromUser
//		fetchMessages(fromUser: fromUser, toUser: toUser, productId: productId)
//		if (isNotificationClicked != nil) {
//			fetchProductDetailForUsers(productId: productId)
//			///Analytics
//			LoggerManager.shared.log(event: .messageNotificationAutomaticTrigger)
//		} else {
//			///Analytics
//			LoggerManager.shared.log(event: .messageNotificationClicked)
//		}
//	}
//
//	//MARK: Open conversation screen when Notification is tapped and the app is in a killed state / In Foreground State or in Background state.
//
//	func MessageNotified(fromUser: String, toUser: String, productId: String, title: String) {
//		productTitle = title
//		productIdentifier = productId
//		otherUserId = fromUser
//		fetchProductDetailForUsers(productId: productId)
//		fetchMessages(fromUser: fromUser, toUser: toUser, productId: productId)
//
//		///Analytics
//		LoggerManager.shared.log(event: .messageNotificationClicked)
//	}
//
//	//MARK: Update user/product info when notification clicked when app is in background or terminated state
//
//	func fetchProductDetailForUsers(productId: String) {
//		ProductDetailService.sharedInstance.getProductById(id: productId) { [weak self] (productDetails) in
//			guard let self = self else { return }
//			self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//			DispatchQueue.main.async {
//				self.customView?.name.text = productDetails.name
//				let price = "\(productDetails.price.price)".dropLast(2)
//				self.customView?.price.text = "\(price)".currencyInputFormatting(currencySymbol: productDetails.price.currency)
//				self.customView?.productImage.loadImageUsingCacheWithURLString(urlString: productDetails.photo.url)
//
//				if let fName = productDetails.user.firstname, let lName = productDetails.user.lastname {
//					self.title = "\(fName + " " + lName)"
//				}
//				if let userImage = productDetails.user.photo {
//					self.getProfilePhoto(photoImage: userImage) { [weak self] (imageData) in
//						guard let self = self else { return }
//						self.imageview.contentMode = .scaleAspectFill
//						self.imageview.image = UIImage(data: imageData)
//					}
//				}
//			}
//		}
//	}
//
//	@objc func fetchMessages(fromUser: String, toUser: String, productId: String) {
//		MessageService.sharedInstance.getMessage(fromId: fromUser, toId: toUser, productId: productId) { (messages) in
//			SVProgressHUD.show()
//			if !messages.isEmpty {
//				DispatchQueue.main.async {
//					SVProgressHUD.dismiss()
//					self.messageList.removeAll()
//					self.messageList.append(contentsOf: messages)
//					self.messagesCollectionView.reloadData()
//					self.messagesCollectionView.scrollToBottom()
//				}
//			} else if messages.isEmpty {
//				SVProgressHUD.dismiss()
//			} else {
//				SVProgressHUD.dismiss()
//			}
//		}
//	}
//
//	@objc func loadMoreMessages() {
//		DispatchQueue.main.async {
//			//self.messageList.insert(contentsOf: messages, at: 0)
//			self.messagesCollectionView.reloadDataAndKeepOffset()
//			self.refreshControl.endRefreshing()
//		}
//	}
//
//	override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
//		if action == NSSelectorFromString("copy:") {
//			 return true
//		} else if action == NSSelectorFromString("delete:") {
//			let message = messageList[indexPath.section]
//			if isFromCurrentSender(message: message) {
//				return true
//			}
//			return false
//	   } else {
//		   return super.collectionView(collectionView, canPerformAction: action, forItemAt: indexPath, withSender: sender)
//	   }
//	}
//
//	override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
//		if action == NSSelectorFromString("delete:") {
//			let message = messageList[indexPath.section]
//			if isFromCurrentSender(message: message) {
//				let userId = CurrentUserInfo.shared.userId()
//				let params = ["userid": userId, "messageid": message.messageId]
//				MessageService.sharedInstance.deleteMessage(params: params as [String: AnyObject]) { (result: DeleteMessage) in
//					///Update success code for deleting messages
//					if result.code == 200 || result.code == 301 {
//						let indexPath = IndexPath(item: indexPath.item, section: indexPath.section)
//						self.messageList.remove(at: indexPath.section)
//						collectionView.performBatchUpdates({
//						collectionView.deleteSections([indexPath.section])
//						}) { (_) in
//						   collectionView.reloadItems(at: collectionView.indexPathsForVisibleItems)
//					   }}
//				}}
//	   } else {
//		   super.collectionView(collectionView, performAction: action, forItemAt: indexPath, withSender: sender)
//	   }
//	}
//}
//
//extension ConversationViewController {
//	@objc func moreOption() {
//		setupDropDown()
//		dropDown.show()
//	}
//
//	@objc func dismissScreen() {
//		self.navigationController?.popViewController(animated: true)
//	}
//}
//
//// MARK: - MessagesDataSource
//extension ConversationViewController: MessagesDataSource {
//
//	func currentSender() -> Sender {
//		return CurrentUserCheck.shared.currentSender
//	}
//
//	func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
//		return messageList.count
//	}
//
//	func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
//		return messageList[indexPath.section]
//	}
//
//	func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
//		if indexPath.section % 3 == 0 {
//			return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir", size: 10)!, NSAttributedString.Key.foregroundColor: UIColor.darkGray])
//		}
//		return nil
//	}
//
//	func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
//		let name = message.sender.displayName
//		return NSAttributedString(string: name, attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir", size: 10)!, NSAttributedString.Key.foregroundColor: UIColor.darkGray])
//	}
//
//	func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
//		let dateString = formatter.string(from: message.sentDate)
//		return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir", size: 10)!, NSAttributedString.Key.foregroundColor: UIColor.darkGray])
//	}
//}
//
//// MARK: - MessagesDisplayDelegate
//extension ConversationViewController: MessagesDisplayDelegate {
//
//	// MARK: - Text Messages
//	func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
//		return isFromCurrentSender(message: message) ? .white : .darkText
//	}
//
//	func detectorAttributes(for detector: DetectorType, and message: MessageType, at indexPath: IndexPath) -> [NSAttributedString.Key: Any] {
//		return MessageLabel.defaultAttributes
//	}
//
//	func enabledDetectors(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> [DetectorType] {
//		return [.url, .address, .phoneNumber, .date, .transitInformation]
//	}
//
//	// MARK: - All Messages
//	func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
//		return isFromCurrentSender(message: message) ? UIColor(red: 75/255, green: 160/255, blue: 252/255, alpha: 1) : UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
//	}
//
//	func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
//		let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
//		return .bubbleTail(corner, .pointedEdge)
//	}
//
//	func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
//		_ = CurrentUserCheck.shared.getAvatarFor(sender: message.sender as! Sender)
//		let initial = message.sender.displayName
//		_ = isFromCurrentSender(message: message) ? avatarView.set(avatar: Avatar(initials: initial.prefix(1).uppercased())) : avatarView.set(avatar: Avatar(initials: initial.prefix(1).uppercased()))
//	}
//
//	// MARK: - Location Messages
//	func annotationViewForLocation(message: MessageType, at indexPath: IndexPath, in messageCollectionView: MessagesCollectionView) -> MKAnnotationView? {
//		let annotationView = MKAnnotationView(annotation: nil, reuseIdentifier: nil)
//		let pinImage = #imageLiteral(resourceName: "Delete location")
//		annotationView.image = pinImage
//		annotationView.centerOffset = CGPoint(x: 0, y: -pinImage.size.height / 2)
//		return annotationView
//	}
//
//	func animationBlockForLocation(message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> ((UIImageView) -> Void)? {
//		return { view in
//			view.layer.transform = CATransform3DMakeScale(0, 0, 0)
//			view.alpha = 0.0
//			UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: [], animations: {
//				view.layer.transform = CATransform3DIdentity
//				view.alpha = 1.0
//			}, completion: nil)
//		}
//	}
//
//	func snapshotOptionsForLocation(message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> LocationMessageSnapshotOptions {
//		 return LocationMessageSnapshotOptions()
//	}
//
//	func configureAccessoryView(_ accessoryView: UIView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
//		// Cells are reused, so only add a button here once. For real use you would need to
//		// ensure any subviews are removed if not needed
//		accessoryView.subviews.forEach { $0.removeFromSuperview() }
//		accessoryView.isHidden = false
//		_ = messageList[indexPath.section]
//	}
//}
//
//// MARK: - MessagesLayoutDelegate
//extension ConversationViewController: MessagesLayoutDelegate {
//	func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
//		if indexPath.section % 3 == 0 {
//			return 10
//		}
//		return 0
//	}
//
//	func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
//		return 16
//	}
//
//	func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
//		return 16
//	}
//}
//
//// MARK: - MessageCellDelegate
//extension ConversationViewController: MessageCellDelegate {
//	func didTapAvatar(in cell: MessageCollectionViewCell) {}
//	func didTapMessage(in cell: MessageCollectionViewCell) {}
//	func didTapCellTopLabel(in cell: MessageCollectionViewCell) {}
//	func didTapMessageTopLabel(in cell: MessageCollectionViewCell) {}
//	func didTapMessageBottomLabel(in cell: MessageCollectionViewCell) {}
//}
//
//// MARK: - MessageLabelDelegate
//extension ConversationViewController: MessageLabelDelegate {
//	func didSelectAddress(_ addressComponents: [String: String]) {}
//	func didSelectDate(_ date: Date) {}
//	func didSelectPhoneNumber(_ phoneNumber: String) {}
//	func didSelectURL(_ url: URL) {}
//	func didSelectTransitInformation(_ transitInformation: [String: String]) {}
//}
//
//// MARK: - MessageInputBarDelegate
//extension ConversationViewController: InputBarAccessoryViewDelegate {
//	func messageInputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
//		for component in inputBar.inputTextView.components {
//			if let text = component as? String {
//				let attributedText = NSAttributedString(string: text, attributes: [.font: UIFont.preferredFont(forTextStyle: .body), .foregroundColor: UIColor.white])
//				let randomInt = arc4random()
//				let message = MessageModel(attributedText: attributedText, sender: currentSender(), messageId: "\(randomInt)", date: Date())
//				inputBar.inputTextView.text = String()
//				for isUserBlocked in blockList {
//					if isUserBlocked.userid == otherUserId {
//						self.messageList.append(message)
//						self.messagesCollectionView.insertSections([self.messageList.count - 1])
//						inputBar.inputTextView.text = String()
//						self.messagesCollectionView.scrollToBottom()
//						return
//					}
//				}
//				sendMessage(messageId: "\(randomInt)", messageText: attributedText.string, messageModel: message, completion: {[unowned self] (success) -> Void in
//					if success {
//						DispatchQueue.main.async {
//							self.messageList.append(message)
//							self.messagesCollectionView.insertSections([self.messageList.count - 1])
//							inputBar.inputTextView.text = String()
//							self.messagesCollectionView.scrollToBottom()
//						}
//					} else {
//						DispatchQueue.main.async {
//							self.messageList.append(message)
//							self.messagesCollectionView.insertSections([self.messageList.count - 1])
//							inputBar.inputTextView.text = String()
//							self.messagesCollectionView.scrollToBottom()
//						}
//					}
//				})
//			}
//		}
//	}
//
//	func sendMessage(messageId: String, messageText: String, messageModel: MessageModel, completion: @escaping (Bool) -> Void) {
//		var params: [String: AnyObject]
//		if chatDetails != nil {
//			params = ["id": messageId as AnyObject, "title": chatDetails?.name as AnyObject, "message": messageText as AnyObject, "from": CurrentUserInfo.shared.userId() as AnyObject, "to": chatDetails?.otherid as AnyObject, "productid": chatDetails?.productId as AnyObject]
//		} else if productDetails != nil {
//			params = ["id": messageId as AnyObject, "title": productDetails?.name as AnyObject, "message": messageText as AnyObject, "from": CurrentUserInfo.shared.userId() as AnyObject, "to": productDetails?.user.userid as AnyObject, "productid": productDetails?.id as AnyObject]
//		} else {
//			params = ["id": messageId as AnyObject, "title": productTitle as AnyObject, "message": messageText as AnyObject, "from": CurrentUserInfo.shared.userId() as AnyObject, "to": otherUserId as AnyObject, "productid": productIdentifier as AnyObject]
//		}
//		MessageService.sharedInstance.sendMessage(params: params) { (response) in
//			if response.code == 200 {
//				completion(true)
//			} else {
//				completion(false)
//			}
//		}
//	}
//
//	func sentMessageFailed(message: MessageModel) -> Bool {
//		return isChatSent
//	}
//}
//
//extension ConversationViewController {
//	func customizeDropDown() {
//		appearance.cellHeight = 45
//		appearance.backgroundColor = UIColor(white: 1, alpha: 1)
//		appearance.selectionBackgroundColor = .clear
//		appearance.separatorColor = UIColor(white: 0.0, alpha: 0.1)
//		appearance.cornerRadius = 0.0
//		appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
//		appearance.shadowOpacity = 0.9
//		appearance.shadowRadius = 20
//		appearance.animationduration = 0.30
//		if let font = UIFont(name: "Avenir", size: 14.0) {
//			appearance.textFont = font
//		}
//	}
//
//	func setupDropDown() {
//		self.appearance.textColor = .darkGray
//		dropDown.bottomOffset = CGPoint(x: -75, y: 43)
//		dropDown.anchorView = navigationItem.rightBarButtonItem
//		dropDown.width = 120
//		dropDown.dataSource = ["Report", "Block"]
//		for blo in blockList {
//			if blo.userid == otherUserId {
//				dropDown.dataSource = ["Report", "Unblock"]
//			}
//		}
//		dropDown.cellNib = UINib(nibName: "CountryDropDownCell", bundle: nil)
//		dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
//			guard let cell = cell as? CountryDropDownCell else { return }
//			if index == 0 {
//				cell.countryFlagImage.image = UIImage(named: "Flag")?.withRenderingMode(.alwaysTemplate)
//				cell.countryFlagImage.tintColor = .lightGray
//			} else {
//				cell.countryFlagImage.image = UIImage(named: "Block")?.withRenderingMode(.alwaysTemplate)
//				cell.countryFlagImage.tintColor = .lightGray
//			}
//		}
//
//		///Action triggered on selection
//		dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
//			print("Selected item: \(item) at index: \(index)")
//			if index == 0 {
//				self.setUpAlertSheets()
//			} else {
//				self.blockUser()
//			}
//		}
//	}
//
//	func blockUser() {
//		var blockValue = String()
//		blockValue = "block"
//		for blo in blockList {
//			if blo.userid == otherUserId {
//				blockValue = "unblock"
//			}
//		}
//		let params = ["userid": CurrentUserInfo.shared.userId(), "other": otherUserId, "type": blockValue]
//		MessageService.sharedInstance.blockUser(params: params as [String: AnyObject]) { (result: BlockUserModel) in
//			if result.code == 200 {
//				self.alert(message: result.message, title: "")
//				self.getBlockedUsersList()
//			} else {
//				SVProgressHUD.showError(withStatus: "Please try again \nSomething went wrong")
//			}
//		}
//	}
//
//	func reportDescritption(_ report: String) {
//		let userId = CurrentUserInfo.shared.userId()
//		let params = ["userid": userId, "other": otherUserId, "itemid": productIdentifier, "type": "Message", "comment": report]
//		ProductDetailService.sharedInstance.reportProductAndMessage(params: params as [String: AnyObject]) { (result: ReportProductAndMessage) in
//			if result.code == 200 {
//				self.alert(message: "\(result.message) thank you. \nThis will be reviewed and if necessary actions will be taken.", title: "")
//			} else {
//				SVProgressHUD.showError(withStatus: "Please try again \nSomething went wrong")
//			}
//		}
//	}
//}
//
//extension ConversationViewController {
//	// MARK: Get users conversations from product page or chat page
//	func updateUserInfoAndLoadMessages(fromDetailPage: ProductDetailsModel?, fromChatPage: ChatModel?) {
//		if fromDetailPage != nil {
//			DispatchQueue.main.async {
//				self.customView?.productImage.loadImageUsingCacheWithURLString(urlString: fromDetailPage?.photo.url ?? "")
//				self.customView?.name.text = fromDetailPage?.name
//				if let initialPrice = fromDetailPage?.price.price {
//					let price = "\(initialPrice)".dropLast(2)
//					self.customView?.price.text = "\(price)".currencyInputFormatting(currencySymbol: fromDetailPage?.price.currency ?? "")
//				}
//			}
//
//			//fetch messages
//			if let toUser = fromDetailPage?.user.userid, let productId = fromDetailPage?.id {
//				otherUserId = toUser
//				productIdentifier = productId
//				fetchMessages(fromUser: CurrentUserInfo.shared.userId(), toUser: toUser, productId: productId)
//			}
//		}
//
//		if fromChatPage != nil {
//			// Fetch user details and display
//			if let fcp = fromChatPage {
//				fetchProductDetailForUsers(productId: fcp.productId)
//			}
//
//			//fetch messages
//			if let toUser = fromChatPage?.otherid, let productId = fromChatPage?.productId {
//				otherUserId = toUser
//				productIdentifier = productId
//				fetchMessages(fromUser: CurrentUserInfo.shared.userId(), toUser: toUser, productId: productId)
//			}
//		}
//	}
//
//	// MARK: Get user details for current user
//	func getUserDetails(userId: String) {
//		ProfileService.sharedInstance.getUserDetails( userId: userId, completion: {[weak self] (response) in
//			guard let self = self else { return }
//			DispatchQueue.main.async {
//				let name = response.firstname + " " + response.lastname
//				self.keychain["userName"] = name
//			}
//		})
//	}
//
//	// MARK: Get user image
//	func getProfilePhoto(photoImage: String, completion: @escaping (Data) -> Void) {
//		APIManager.shared.getUserProfileImage(photoImage: photoImage) { (imageData) in
//			DispatchQueue.main.async {
//				completion(imageData)
//			}
//		}
//	}
//
//	func getBlockedUsersList() {
//		_ = APIEndPoints.init()
//		let urlString = String(format: APIEndPoints.getblockUsers, CurrentUserInfo.shared.userId())
//		let getblockUsers = BaseAPIClient.urlFromPath(path: urlString)
//		APIManager.shared.fetchGenericData(method: .get, urlString: getblockUsers, params: nil, headers: APIEndPoints.authorizationHeader) { (result: BlockedUserListModel) in
//			if !result.content.isEmpty {
//				self.blockList.removeAll()
//				self.blockList.append(contentsOf: result.content)
//			} else if result.content.isEmpty {
//			} else {
//				SVProgressHUD.showError(withStatus: "Unable to block user at this time.")
//			}
//		}
//	}
//
//	///Report User
//	func setUpAlertSheets() {
//		let destinationVC = ReportUserMessageController.instantiate()
//		destinationVC.delegate = self
//		let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: destinationVC)
//		segue.configure(layout: .centered)
//		segue.messageView.backgroundHeight = 500
//		segue.perform()
//	}
//
//	func reportUser(report: String) {
//		self.reportDescritption(report)
//	}
//}
//
//extension MessageCollectionViewCell {
//	override open func delete(_ sender: Any?) {
//			// Get the collectionView
//			if let collectionView = self.superview as? UICollectionView {
//			// Get indexPath
//			if let indexPath = collectionView.indexPath(for: self) {
//				// Trigger action
//				collectionView.delegate?.collectionView?(collectionView, performAction: NSSelectorFromString("delete:"), forItemAt: indexPath, withSender: sender)
//			}
//		}
//	}
//}
//
//extension ConversationViewController: InfoViewDelegate {
//    func infoViewDidHide(view: InfoView) {}
//    func infoViewDidShow(view: InfoView) { }
//    func infoViewWillHide(view: InfoView) { }
//    func infoViewWillShow(view: InfoView) {}
//}
}
