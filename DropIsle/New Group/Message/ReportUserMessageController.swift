//
//  ReportUserMessageController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-03-05.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol ReportUserDelegate: class {
	func reportUser(report: String)
}

class ReportUserMessageController: UIViewController {

	weak var delegate: ReportUserDelegate?
	static func instantiate() -> ReportUserMessageController {
		return (UIStoryboard(name: "ConversationView", bundle: nil).instantiateViewController(withIdentifier: "ReportUserMessageController") as? ReportUserMessageController)!
	}

	override func viewDidLoad() {
		super.viewDidLoad()
	}

	@IBAction func reportUserAction(_ sender: UIButton) {
		if let sender = sender.titleLabel?.text {
			delegate?.reportUser(report: sender)
		}
	}

	@IBAction func dismissScreen(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}


	/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
