//
//  ChatsController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-11-03.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import SVProgressHUD

class ChatsController: UITableViewController {

	private var emptyBackgroundView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))

	@IBOutlet weak var chatsTableView: UITableView!

	var chats = [ChatModel]() {
		didSet {
			DispatchQueue.main.async {
				self.chatsTableView.reloadData()
			}
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "Chats"
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton
		setBackgroundView()

		//get chat for users
		getChats()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Avenir-Black", size: 20)!]
		if let tabBarController = self.tabBarController as? TabBarViewController {
		}
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
	}

	// MARK: Api calls
	func getChats() {
		MessageService.sharedInstance.getChats { (result) in
			if result.count != 0 {
				self.chats.removeAll()
				self.chats.append(contentsOf: result)
			}
		}
	}

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
			if chats.count == 0 {
				DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
					guard self.chats.count != 0 else {
						self.chatsTableView.backgroundView = self.emptyBackgroundView
						return
					}
				}
				SVProgressHUD.show()
			}
			SVProgressHUD.dismiss(withDelay: 0.5)
			return chats.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "ChatsCell", for: indexPath) as? ChatsCell
			cell?.chat = chats[indexPath.row]
        return cell!
    }

	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 83
	}

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let cell = tableView.cellForRow(at: indexPath) as? ChatsCell
		let newViewController = ConversationViewController()
		///newViewController.chatDetails = cell?.chat
		navigationController?.pushViewController(newViewController, animated: true)
	}
}

extension ChatsController {
	private func setBackgroundView() {
		let stackView = UIStackView()
		emptyBackgroundView.backgroundColor = UIColor.white
		let view1 = UIImageView.noChatImage(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
		view1.heightAnchor.constraint(equalToConstant: 200).isActive = true
		view1.widthAnchor.constraint(equalToConstant: 200).isActive = true

		let view2 = UILabel.noChatAttributeLabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 48))
		view2.heightAnchor.constraint(equalToConstant: 48).isActive = true
		view2.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true

		stackView.axis = .vertical
		stackView.distribution = .equalSpacing
		stackView.alignment = .center
		stackView.spacing = 20

		stackView.addArrangedSubview(view1)
		stackView.addArrangedSubview(view2)

		stackView.translatesAutoresizingMaskIntoConstraints = false
		emptyBackgroundView.addSubview(stackView)
		stackView.centerXAnchor.constraint(equalTo: emptyBackgroundView.centerXAnchor).isActive = true
		stackView.centerYAnchor.constraint(equalTo: emptyBackgroundView.centerYAnchor).isActive = true
	}
}
