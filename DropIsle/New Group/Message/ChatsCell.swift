//
//  ChatsCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-11-03.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import SDWebImage

class ChatsCell: UITableViewCell {

	@IBOutlet weak var name: UILabel!
	@IBOutlet weak var productName: UILabel!
	@IBOutlet weak var productImage: UIImageView!

	var chat: ChatModel? {
		didSet {
			name.text = chat?.name
			productName.text = chat?.productName
			weak var weakSelf = self
			productImage?.sd_imageTransition = SDWebImageTransition.fade
			if let imageUrl = chat?.productImage {
				weakSelf?.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"), options: .fromCacheOnly) { (image, error, cacheType, url) in
					self.productImage.image = image
				}
			}
		}
	}

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		productImage.makeRounded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
