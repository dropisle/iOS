//
//  MessageService.swift
//  DropIsle
//
//  Created by CtanLI on 2019-10-14.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

class MessageService {
static let sharedInstance = MessageService()
//	func getMessage(fromId: String, toId: String, productId: String, completion: @escaping ([MessageModel]) -> Void) {
//		_ = APIEndPoints.init()
//		let urlString = String(format: APIEndPoints.getMessages, fromId, toId, productId)
//		let getMeesageUrl = BaseAPIClient.urlFromPath(path: urlString)
//		APIManager.shared.fetchMessages(method: .get, urlString: getMeesageUrl, params: nil, headers: APIEndPoints.authorizationHeader) { (messages, _) in
//			completion(messages ?? [])
//		}
//	}

	func sendMessage(params: [String: AnyObject], completion: @escaping (MessageSentModel) -> Void) {
		_ = APIEndPoints.init()
		let urlString = String(format: APIEndPoints.sendMessage)
		let getMeesageUrl = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .post, urlString: getMeesageUrl, params: params, headers: APIEndPoints.authorizationHeader) { (response) in
			completion(response)
		}
	}

	func getChats(completion: @escaping ([ChatModel]) -> Void) {
		_ = APIEndPoints.init()
		let urlString = String(format: APIEndPoints.getChats, CurrentUserInfo.shared.userId())
		let getChatsUrl = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .get, urlString: getChatsUrl, params: nil, headers: APIEndPoints.authorizationHeader) { (response) in
			completion(response)
		}
	}

	func blockUser(params: [String: AnyObject], completion: @escaping (BlockUserModel) -> Void) {
		_ = APIEndPoints.init()
		let urlString = String(format: APIEndPoints.blockUser)
		let blockUserUrl = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .post, urlString: blockUserUrl, params: params, headers: APIEndPoints.authorizationHeader) { (response) in
			completion(response)
		}
	}

	func deleteMessage(params: [String: AnyObject], completion: @escaping (DeleteMessage) -> Void) {
		_ = APIEndPoints.init()
		let urlString = String(format: APIEndPoints.deleteMessage)
		let deleteMessageUrl = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .post, urlString: deleteMessageUrl, params: params, headers: APIEndPoints.authorizationHeader) { (response) in
			completion(response)
		}
	}
}
