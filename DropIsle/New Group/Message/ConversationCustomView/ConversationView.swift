//
//  ConversationHeaderView.swift
//  DropIsle
//
//  Created by CtanLI on 2019-07-24.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol InfoWarningViewDelegate: class {
	func showInfo()
}

class ConversationView: UIView {

	weak var delegate: InfoWarningViewDelegate?

	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var infoButton: UIButton!
	@IBOutlet weak var productImage: UIImageView!
	@IBOutlet weak var price: UILabel!
	@IBOutlet weak var name: UILabel!

	override func awakeFromNib() {
		super.awakeFromNib()
		let origImage = UIImage(named: "info")
		let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
		infoButton.setImage(tintedImage, for: .normal)
		infoButton.tintColor = .darkGray
	}

	@IBAction func infoViewAction(_ sender: UIButton) {
		self.delegate?.showInfo()
	}
}
