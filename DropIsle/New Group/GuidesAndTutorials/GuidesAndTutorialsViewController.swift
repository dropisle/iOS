//
//  GuidesAndTutorialsViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-03-30.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import SVProgressHUD

class GuidesAndTutorialsViewController: UITableViewController {

    override func viewDidLoad() {
			super.viewDidLoad()
			self.title = "Guides and Tutorials"
			addBarButtons()
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
	}

	func addBarButtons() {
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton
	}

	func showLoadIndicator() {
		SVProgressHUD.show(withStatus: "Loading...")
	}

	@objc func dismissScreen(sender: AnyObject) {
		self.navigationController?.popViewController(animated: true)
	}
}
