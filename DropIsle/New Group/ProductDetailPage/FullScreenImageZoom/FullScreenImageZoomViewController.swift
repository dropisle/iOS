//
//  FullScreenImageZoomViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-04-29.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import SDWebImage

class FullScreenImageZoomViewController: UIViewController, UIScrollViewDelegate {

	var fullZoomImageArray: [String] = []

	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var closeButton: UIButton!

	var overlay: UIView = {
	   let view = UIView(frame: UIScreen.main.bounds)
	   view.alpha = 0
	   view.backgroundColor = .black
	   return view
	 }()

	override func viewDidLoad() {
		super.viewDidLoad()
		setupImageViewConstraints()

		scrollView.delegate = self
		scrollView.minimumZoomScale = 0.5
		scrollView.maximumZoomScale = 2.0

		setCloseButtonColor()
		displayFullScreenImage()

		let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(dismissView))
		swipeUp.direction = .up
		view.addGestureRecognizer(swipeUp)

		let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissView))
		swipeDown.direction = .down
		view.addGestureRecognizer(swipeDown)
    }

	@objc func dismissView(gesture: UISwipeGestureRecognizer) {
		UIView.animate(withDuration: 0.4) {
			if gesture.direction == .up {
					self.dismiss(animated: true, completion: nil)
			  } else if gesture.direction == .down {
					self.dismiss(animated: true, completion: nil)
			  }
		}
	}

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		fullZoomImageArray.removeAll()
	}

	/// Setup ImageView constraints
	func setupImageViewConstraints() {
	  // Disable Autoresizing Masks into Constraints
	  scrollView.translatesAutoresizingMaskIntoConstraints = false
	  // Constraints
	  NSLayoutConstraint.activate([
		  scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
		  scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
		  scrollView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
	  ])
	  view.layoutIfNeeded()
	}

	func setCloseButtonColor() {
		let origImage = UIImage(named: "Close")
		let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
		closeButton.setImage(tintedImage, for: .normal)
		closeButton.tintColor = UIColor(hexString: "#ffffff")
	}

	func displayFullScreenImage() {
		for (index, value) in fullZoomImageArray.enumerated() {
			let imageView = UIImageView()
			let x = self.view.frame.size.width * CGFloat(index)
			imageView.frame = CGRect(x: x, y: 0, width: self.view.frame.width, height: self.view.frame.height)
			imageView.contentMode = .scaleAspectFit
			let imageUrl = URL(string: value)
			imageView.sd_setImage(with: imageUrl) { (image, error, cacheType, url) in }
			scrollView.contentSize.width = scrollView.frame.size.width * CGFloat(index + 1)
			scrollView.addSubview(imageView)
			scrollView.isUserInteractionEnabled = true
			imageView.isUserInteractionEnabled = true

			let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(saveImage))
			let pinch = UIPinchGestureRecognizer(target: self, action: #selector(handleZoom(_:)))
			let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))

			// Use 2 thingers to move the view
			 pan.minimumNumberOfTouches = 2
			 pan.maximumNumberOfTouches = 2

			 // We delegate gestures so we can
			 // perform both at the same time
			 pan.delegate = self
			 pinch.delegate = self

			// Add the gestures to our target (imageView)
			 imageView.addGestureRecognizer(longPressRecognizer)
			 scrollView.addGestureRecognizer(pinch)
			 scrollView.addGestureRecognizer(pan)
		}
	}

	@objc func saveImage(_ gesture: UILongPressGestureRecognizer) {
		guard let sender = gesture.view as? UIImageView else { return }
		guard let _ = URL(string: "dropisle.com") else { return }
		let activityItems = [sender.image as Any]
		let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
		if let popoverController = activityController.popoverPresentationController {
				popoverController.sourceView = self.view
				popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
				popoverController.permittedArrowDirections = []
			}
			self.present(activityController, animated: true, completion: nil)
	}

	@IBAction func closeFullScreen(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}
}

extension FullScreenImageZoomViewController: UIGestureRecognizerDelegate {

    // that method make it works
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    @objc func handleZoom(_ gesture: UIPinchGestureRecognizer) {
        switch gesture.state {
        case .began, .changed:

            // Only zoom in, not out
            if gesture.scale >= 1 {

                // Get the scale from the gesture passed in the function
                let scale = gesture.scale

                // use CGAffineTransform to transform the imageView
                gesture.view!.transform = CGAffineTransform(scaleX: scale, y: scale)
            }

            // Show the overlay
            UIView.animate(withDuration: 0.2) {
                self.overlay.alpha = 0.8
            }
            break
        default:
            // If the gesture has cancelled/terminated/failed or everything else that's not performing
            // Smoothly restore the transform to the "original"
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
              gesture.view!.transform = .identity
            }) { _ in
              // Hide the overlay
              UIView.animate(withDuration: 0.2) {
                self.overlay.alpha = 0
              }
            }
        }
    }

    @objc func handlePan(_ gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began, .changed:
            // Get the touch position

			let translation = gesture.translation(in: scrollView)
            // Edit the center of the target by adding the gesture position
			gesture.view!.center = CGPoint(x: scrollView.center.x + translation.x, y: scrollView.center.y + translation.y)
            gesture.setTranslation(.zero, in: scrollView)

            // Show the overlay
            UIView.animate(withDuration: 0.2) {
                self.overlay.alpha = 0.8
            }
            break
        default:
            // If the gesture has cancelled/terminated/failed or everything else that's not performing
            // Smoothly restore the transform to the "original"
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                gesture.view!.center = self.view.center
                gesture.setTranslation(.zero, in: self.view)
            }) { _ in
              // Hide the overaly
              UIView.animate(withDuration: 0.2) {
                self.overlay.alpha = 0
              }
            }
            break
        }
    }
}
