//
//  CustomCollectionViewCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-07.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import SDWebImage

protocol AddSuggestedItemsToWishListDelegate: class {
	func addToWishList(cell: CustomCollectionViewCell)
}

class CustomCollectionViewCell: UICollectionViewCell {

	weak var delegate: AddSuggestedItemsToWishListDelegate?

	@IBOutlet weak var productName: UILabel!
	@IBOutlet weak var productImage: UIImageView!
	@IBOutlet weak var addToWishListButton: UIButton!
	@IBOutlet weak var productPrice: UILabel!

	var product: ProductModel.content? {
		didSet {
			guard let product = product else {
				return
			}
			weak var weakSelf = self
			productImage?.sd_imageTransition = SDWebImageTransition.fade
			weakSelf?.productImage.sd_setImage(with: URL(string: product.photo.url), placeholderImage: UIImage(named: "placeholder"), options: .fromCacheOnly) { (image, error, cacheType, url) in
				self.productImage.image = image
			}
			productName.text = product.name
			let price = "\(product.price.price)".dropLast(2)
			productPrice.text = "\(price)".currencyInputFormatting(currencySymbol: product.price.currency)
			product.watch == "YES" ? setAddToWishList(wishListImage: UIImage(named: "addToWishListSolid")) : setAddToWishList(wishListImage: UIImage(named: "addToWishListEmpty"))
		}
	}

	class var CustomCell: CustomCollectionViewCell {
		let cell = Bundle.main.loadNibNamed("CustomCollectionViewCell", owner: self, options: nil)?.last
		return cell as! CustomCollectionViewCell
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}

	func setAddToWishList(wishListImage: UIImage?) {
		let tintedImage = wishListImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
		addToWishListButton.setImage(tintedImage, for: .normal)
		addToWishListButton.tintColor = .lightGray
	}

	@IBAction func addToWishListAction(_ sender: UIButton) {
		self.delegate?.addToWishList(cell: self)
	}
}
