//
//  WrapperTableViewCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-07.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol SimilarProductDelegate: class {
	func openProductDetails(cell: CustomCollectionViewCell, productId: String)
	func checkIfUserIsLoggedIn(product: ProductModel.content)
}

class WrapperTableViewCell: UITableViewCell,
																							UICollectionViewDataSource,
																							UICollectionViewDelegate,
																							UICollectionViewDataSourcePrefetching,
																							UICollectionViewDelegateFlowLayout,
																							UIViewControllerTransitioningDelegate,
																							AddSuggestedItemsToWishListDelegate {

	var productDetails: ProductDetailsModel?
	weak var delegate: SimilarProductDelegate?
	private let cellReuseId = "CollectionViewCell"
	private var dict = [String: String]()
	private var jsonObj = [String: Any]()
	private var wishlistState = String()

	@IBOutlet weak var collectionView: UICollectionView!

	var product = [ProductModel]()
	var content = [ProductModel.content]() {
		didSet {
			DispatchQueue.main.async {
				self.collectionView.reloadData()
			}
		}
	}

	class var customCell: WrapperTableViewCell {
		let cell = Bundle.main.loadNibNamed("WrapperTableViewCell", owner: self, options: nil)?.first as! WrapperTableViewCell
			return cell
	}

	override func awakeFromNib() {
		super.awakeFromNib()
			let flowLayout = UICollectionViewFlowLayout()
			flowLayout.scrollDirection = .horizontal
			flowLayout.itemSize = CGSize(width: 150, height: 200)
			flowLayout.minimumLineSpacing = 10.0
			flowLayout.minimumInteritemSpacing = 10.0
			collectionView.collectionViewLayout = flowLayout

			//Comment if you set Datasource and delegate in .xib
			collectionView.dataSource = self
			collectionView.delegate = self
			collectionView.prefetchDataSource = self

			collectionView.backgroundColor = UIColor(hexString: "#f6f6f6")
			collectionView.showsHorizontalScrollIndicator = false

			//register the xib for collection view cell
			let cellNib = UINib(nibName: "CustomCollectionView", bundle: nil)
			self.collectionView.register(cellNib, forCellWithReuseIdentifier: cellReuseId)
    }

	// MARK: Instance Methods get product for category
	func updateCellWith(content: [ProductModel.content], product: [ProductModel]) {
		self.product.append(contentsOf: product)
		self.content.append(contentsOf: content)
	}

	private func fetchProductBasedOnCategory(categoryName: String, page: String) {
		DispatchQueue.global(qos: .background).async {
			HomeService.sharedInstance.getProductByCategory(category: categoryName, page: page) { [weak self] (products) in
				guard let self = self else { return }
				DispatchQueue.main.async {
					self.product.removeAll()
					let prod = products.content.filter { $0.status != APIEndPoints.soldSate}
					let shuffledProducts = prod.shuffled()
					self.product.append(products)
					self.content.append(contentsOf: shuffledProducts)
				}
			}
		}
	}

	private func fetchProductByCategoryAndUserId(category: String, page: String) {
		DispatchQueue.global(qos: .background).async {
			HomeService.sharedInstance.getProductByCategoryAndUserId(category: category, page: page) { [weak self] (products) in
				guard let self = self else { return }
				DispatchQueue.main.async {
					self.product.removeAll()
					let prod = products.content.filter { $0.status != APIEndPoints.soldSate}
					let shuffledProducts = prod.shuffled()
					self.product.append(products)
					self.content.append(contentsOf: shuffledProducts)
				}
			}
		}
	}

	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		///Configure the view for the selected state
	}

	//MARK: Collection view datasource and Delegate
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		if let cell = collectionView.cellForItem(at: indexPath) as? CustomCollectionViewCell {
			///Show Detail Screen
			let parameter = ["productName": cell.product?.name, "productCategory": cell.product?.category]
			LoggerManager.shared.log(event: .showDetailScreen, parameters: parameter as [String : Any])

			self.delegate?.openProductDetails(cell: cell, productId: cell.product?.id ?? String())
		}
	}

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if content.count == 0 {
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
			guard self.content.count != 0 else {
				self.collectionView.setEmptyMessage("No similar products exist at the moment. \nEngage by selling and buying products!")
				return
				}
			}
		} else {
			self.collectionView.restore()
		}
			return content.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseId, for: indexPath) as? CustomCollectionViewCell else { return UICollectionViewCell() }
		cell.setUpMargins()
		cell.delegate = self
		cell.product = content[indexPath.row]

		//Check if the product added to wishlist id's are in storage and update the button when scrolling.
		if let productId = cell.product?.id {
			if preferences[.similarProductsItemToWishlist].contains(productId) {
				cell.setAddToWishList(wishListImage: UIImage(named: "addToWishListSolid"))
			} 
		}
		return cell
	}

	func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
		for indexPath in indexPaths {
			guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseId, for: indexPath) as? CustomCollectionViewCell else { return }
			let model = content[indexPath.row]
			cell.product = model
		}
	}

	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		if indexPath.row == content.count - 1 {
			guard product.first?.numberOfElements == 10 else {
				return
			}
			if var pageNumber = self.product.first?.pageable.pageNumber {
				pageNumber += 2
				let category = content[indexPath.row].category
				CurrentUserInfo.shared.userId().isEmpty ? fetchProductBasedOnCategory(categoryName: category, page: "\(pageNumber)") : fetchProductByCategoryAndUserId(category: category, page: "\(pageNumber)")
			}
		}
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
	}

	override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame = newFrame
            frame.origin.x += 15
            frame.size.width -= 2 * 15
            super.frame = frame
        }
    }
}

extension WrapperTableViewCell {

	func addToWishList(cell: CustomCollectionViewCell) {
		if let product = cell.product {
			CurrentUserInfo.shared.userId().isEmpty ? delegate?.checkIfUserIsLoggedIn(product: product) : AddAndRemoveFromWishList(cell: cell)
		}
	}

	private func AddAndRemoveFromWishList(cell: CustomCollectionViewCell) {

		///Animating button on click to verify action
		cell.addToWishListButton.bounceAnimation(value: 0.1)

		guard let indexPath = self.collectionView.indexPath(for: cell) else {
			return
		}
		let cell = collectionView.cellForItem(at: indexPath) as? CustomCollectionViewCell
		let userId = CurrentUserInfo.shared.userId()
		guard let itemCode = cell?.product?.id else {
			return
		}

		if preferences[.similarProductsItemToWishlist].contains(itemCode) {
			self.wishlistState = "remove"
		}

		if !preferences[.similarProductsItemToWishlist].contains(itemCode) {
			self.wishlistState = "add"
		}

		dict.updateValue(itemCode, forKey: "itemcode")
		dict.updateValue(userId, forKey: "userid")
		dict.updateValue(wishlistState, forKey: "type")
		if let theJSONData = try?  JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
			if let jsonObject = try? JSONSerialization.jsonObject(with: theJSONData, options: []) as? [String: Any] {
				jsonObj = jsonObject
			}
		}
		HomeService.sharedInstance.addAndRemoveProductFromWishlist(params: jsonObj as [String: AnyObject]) { (result) in
			if result.code == 200 {
				SVProgressHUD.dismiss(withDelay: 0.5)

				///Check if wishlist already added by user
				if result.status == "added" {
					///Analytics
					let parameter = ["userId": CurrentUserInfo.shared.userId(), "productName": cell?.product?.name ?? "", "productCategory": cell?.product?.category ?? ""] as [String : Any]
					LoggerManager.shared.log(event: .addToWishlist, parameters: parameter)

					cell?.setAddToWishList(wishListImage: UIImage(named: "addToWishListSolid"))
					///Post a notification
					NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyWishlistScreen"), object: nil, userInfo: nil)
					if let productId = cell?.product?.id {
						preferences[.similarProductsItemToWishlist].append(productId)
					}
				} else {
					///Analytics
					let parameter = ["userId": CurrentUserInfo.shared.userId(), "productName": cell?.product?.name ?? "", "productCategory": cell?.product?.category ?? ""] as [String : Any]
					LoggerManager.shared.log(event: .removeFromWishlist, parameters: parameter)

					cell?.setAddToWishList(wishListImage: UIImage(named: "addToWishListEmpty"))
					if let productId = cell?.product?.id {
						let storedWishlistProductId = preferences[.similarProductsItemToWishlist]
						let availableProductId = storedWishlistProductId.filter { $0 != productId }
						preferences.removeObject(forKey: "similarProductsItemToWishlist")
						preferences[.similarProductsItemToWishlist].append(contentsOf: availableProductId)
					}
				}
			} else {
				SVProgressHUD.dismiss(withDelay: 0.5)
			}
		}
	}
}

//			if let wishlistImage = cell.addToWishListButton.currentImage?.isEqual(UIImage(named: "addToWishListSolid")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)) {
//				if wishlistImage  {
//					self.wishlistState = "remove"
//				}else {
//					self.wishlistState = "add"
//				}
//			}


