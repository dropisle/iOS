//
//  ProductLocationController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-09-22.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class ProductLocationController: UIViewController, MKMapViewDelegate {

	var productAddress = String()
	var locationCordinate: CLLocationCoordinate2D?
	private var locationManager = CLLocationManager()
	private let location = LocationMonitor.SharedManager

	@IBOutlet weak var mapView: MKMapView!
	@IBOutlet weak var distanceInfo: UIView!
	@IBOutlet weak var distanceText: UILabel!

	override func viewDidLoad() {
		super.viewDidLoad()
		title = "Location"
		self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: "Avenir-Black", size: 20)!]
		navigationController?.navigationBar.barTintColor = UIColor(hexString: "#E4E4E4")

		distanceInfo.clipsToBounds = true
		distanceInfo.layer.cornerRadius = 10
		distanceInfo.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
		location.startMonitoring()

		mapView.delegate = self
		mapView.showsScale = true
		mapView.showsPointsOfInterest = true
		mapView.showsUserLocation = true
		mapView.userTrackingMode = .followWithHeading

		let address = productAddress
		let geoCoder = CLGeocoder()
		geoCoder.geocodeAddressString(address) { (placemarks, _) in
			guard let placemarks = placemarks, let location = placemarks.first?.location else {
				return
			}

			//Adding Annotation
			self.locationCordinate = location.coordinate
			self.addAnnotationOnLocation(pointAtCoordinate: location.coordinate)

			//Adding Circlar Overlay
			self.showCircle(coordinate: location.coordinate, radius: 10000)

			//Zooming To Location
			let viewRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 50000, longitudinalMeters: 50000)
			self.mapView.setRegion(viewRegion, animated: true)

			//Show distance to product
			self.showDistanceToProductLocation()
		}
	}

	func showDistanceToProductLocation() {
		guard let userLoc = location.locationManager.location?.coordinate else { return }
		guard let productLoc = locationCordinate else { return }
		let point1 = MKMapPoint(userLoc)
		let point2 = MKMapPoint(productLoc)
		let distanceInMeters = point1.distance(to: point2)
		let kilometers = distanceInMeters.inKilometers()
		let valueDistance = String.init(format: "%.3f km", kilometers)
		distanceText.text = valueDistance
	}

	func showCircle(coordinate: CLLocationCoordinate2D, radius: CLLocationDistance) {
		let circle = MKCircle(center: coordinate, radius: radius)
		mapView.addOverlay(circle)
	}

	func addAnnotationOnLocation(pointAtCoordinate: CLLocationCoordinate2D) {
		let annotation = MKPointAnnotation()
		annotation.coordinate = pointAtCoordinate
		mapView.addAnnotation(annotation)
	}

	func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
		// If you want to include other shapes, then this check is needed.
		// If you only want circles, then remove it.
		if let circleOverlay = overlay as? MKCircle {
			let circleRenderer = MKCircleRenderer(overlay: circleOverlay)
			circleRenderer.fillColor = UIColor(hexString: "#3293FC")
			circleRenderer.alpha = 0.2
			return circleRenderer
		}
		return MKOverlayRenderer()
	}

	@IBAction func closeScreen(_ sender: UIBarButtonItem) {
		self.dismiss(animated: true, completion: nil)
	}
}

extension CLLocationDistance {
	func inMiles() -> CLLocationDistance {
		return self * 0.00062137
	}

	func inKilometers() -> CLLocationDistance {
		return self / 1000.0
	}
}
