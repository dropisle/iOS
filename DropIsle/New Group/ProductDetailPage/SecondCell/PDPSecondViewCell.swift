//
//  PDPSecondViewCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-09.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol ProductLocationDelegate: class {
	func openMap(cell: PDPSecondViewCell, sender: UIButton)
}

class PDPSecondViewCell: UITableViewCell {

	weak var delegate: ProductLocationDelegate?
	var productDetails: ProductDetailsModel!

	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var itemNegotiable: UILabel!
	@IBOutlet weak var itemCondition: UILabel!
	@IBOutlet weak var itemCategory: UILabel!
	@IBOutlet weak var location: UILabel!
	@IBOutlet weak var productPrice: UILabel!

	class var customCell: PDPSecondViewCell {
		let cell = Bundle.main.loadNibNamed("PDPSecondViewCell", owner: self, options: nil)?.last
		return cell as! PDPSecondViewCell
	}

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.containerView.layer.borderWidth = 0
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
			super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

	@IBAction func openMap(_ sender: UIButton) {
		self.delegate?.openMap(cell: self, sender: sender)
	}

//	override var frame: CGRect {
//        get {
//            return super.frame
//        }
//        set (newFrame) {
//            var frame = newFrame
//            frame.origin.x += 15
//            frame.size.width -= 2 * 15
//            super.frame = frame
//        }
//    }
}
