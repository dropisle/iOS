//
//  WraapedCommentCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-01-24.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol CommentHeightDeleagte: class {
	func setCommentHeight(value: CGFloat)
}

class WrapperCommentCell: UITableViewCell,
																						UICollectionViewDataSource,
																						UICollectionViewDelegate,
																						UICollectionViewDelegateFlowLayout,
UIViewControllerTransitioningDelegate {

	let cellReuseId = "CollectionViewCell"
	var cellHeights: [CGFloat] = []
	var lHeight: CGFloat = 0
	var commentModel = [String]()
	weak var delegate: CommentHeightDeleagte?

	@IBOutlet weak var collectionView: UICollectionView!

	class var customCell: WrapperCommentCell {
		let cell = Bundle.main.loadNibNamed("WrapperCommentCell", owner: self, options: nil)?.first as! WrapperCommentCell
		return cell
	}

	override func awakeFromNib() {
	super.awakeFromNib()
		//Comment if you set Datasource and delegate in .xib
		collectionView.dataSource = self
		collectionView.delegate = self
		collectionView.showsVerticalScrollIndicator = false

		let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
		layout?.minimumLineSpacing = 4

		//register the xib for collection view cell
		let cellNib = UINib(nibName: "CommentView", bundle: nil)
		self.collectionView.register(cellNib, forCellWithReuseIdentifier: cellReuseId)
	}

	func getComments(comments: [String]) {
		commentModel.append(contentsOf: comments)
	}

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return commentModel.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseId, for: indexPath) as? CustomCommentCell else { return UICollectionViewCell() }
		cell.layer.cornerRadius = 10
		cell.commentText.sizeToFit()
		cell.commentText.text = commentModel[indexPath.row]
		delegate?.setCommentHeight(value: collectionView.contentSize.height)
		return cell
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseId, for: indexPath) as? CustomCommentCell {
			cell.commentText.text = commentModel[indexPath.row]
			//lHeight = cell.commentText.textHeight(withWidth: collectionView.bounds.size.width)
		}
		cellHeights.append(lHeight + 15)
		return CGSize(width: collectionView.bounds.size.width , height: cellHeights[indexPath.row])
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
	}

	override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame = newFrame
            frame.origin.x += 15
            frame.size.width -= 2 * 15
            super.frame = frame
        }
    }
}
