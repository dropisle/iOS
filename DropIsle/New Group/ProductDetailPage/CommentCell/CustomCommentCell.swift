//
//  CommentCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-01-24.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class CustomCommentCell: UICollectionViewCell {

	class var CustomCell: CustomCommentCell {
		let cell = Bundle.main.loadNibNamed("CustomCommentCell", owner: self, options: nil)?.last
		return cell as! CustomCommentCell
	}
	
	@IBOutlet weak var commentText: UILabel!

	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
