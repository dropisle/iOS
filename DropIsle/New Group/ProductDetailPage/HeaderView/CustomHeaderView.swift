//
//  CustomHeaderView.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-07.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol PinchToZoomDelegate: class {
	func pinchToZoom(sender: UITapGestureRecognizer)
	func fullScreenImageZoom(sender: UITapGestureRecognizer)
}

class CustomHeaderView: UITableViewHeaderFooterView, UIGestureRecognizerDelegate {

	weak var delegate: PinchToZoomDelegate!

	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var imageViewer: UIView!
	@IBOutlet weak var pageControlView: UIView!

	class var customView: CustomHeaderView {
		let cell = Bundle.main.loadNibNamed("CustomHeaderView", owner: self, options: nil)?.last
		return cell as! CustomHeaderView
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		contentView.backgroundColor = UIColor.white

		let tap = UITapGestureRecognizer(target: self, action: #selector(zoomImage))
		tap.numberOfTapsRequired = 2
		tap.delegate = self
		self.imageViewer.addGestureRecognizer(tap)

		let fullScreen = UITapGestureRecognizer(target: self, action: #selector(fullScreenZoom))
		fullScreen.numberOfTapsRequired = 1
		fullScreen.delegate = self
		imageViewer.addGestureRecognizer(fullScreen)
	}

	@objc func zoomImage(sender: UITapGestureRecognizer) {
		delegate?.pinchToZoom(sender: sender)
	}

	@objc func fullScreenZoom(sender: UITapGestureRecognizer) {
		delegate?.fullScreenImageZoom(sender: sender)
	}
}

