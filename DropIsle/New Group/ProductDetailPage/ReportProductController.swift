//
//  ReportProductController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-03-07.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol ReportProductDelegate: class {
	func reportProduct(report: String)
}

class ReportProductController: UIViewController, UITextViewDelegate {

	weak var delegate: ReportProductDelegate?
	static func instantiate() -> ReportProductController {
		return (UIStoryboard(name: "ProductDetailPage", bundle: nil).instantiateViewController(withIdentifier: "ReportProductController") as? ReportProductController)!
	}

	@IBOutlet weak var otherText: UITextView!

	override func viewDidLoad() {
		super.viewDidLoad()
			otherText.delegate = self
			otherText.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
			otherText.text = "Other Please describe"
			otherText.textColor = UIColor.lightGray.withAlphaComponent(0.5)
	}

	func textViewDidBeginEditing(_ textView: UITextView) {
		if textView.textColor == UIColor.lightGray.withAlphaComponent(0.5) {
			textView.text = nil
			textView.textColor = UIColor.black
		}
	}

	func textViewDidEndEditing(_ textView: UITextView) {
		if textView.text.isEmpty {
			textView.text = "Other Please describe"
			textView.textColor = UIColor.lightGray.withAlphaComponent(0.5)
		}
	}

	func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
		let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
		let numberOfChars = newText.count
		return numberOfChars < 201
	}

	@IBAction func reportUserAction(_ sender: UIButton) {
		if let sender = sender.titleLabel!.text {
			delegate?.reportProduct(report: sender)
			self.dismiss(animated: true, completion: nil)
		}
	}

	@IBAction func closeScreen(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}

	@IBAction func reportOtherAction(_ sender: UIButton) {
		if let sender = otherText.text {
			delegate?.reportProduct(report: sender)
			self.dismiss(animated: true, completion: nil)
		}
	}
	
	/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
