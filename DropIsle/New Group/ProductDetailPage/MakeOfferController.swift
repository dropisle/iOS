//
//  MkaeOfferController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-03-05.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit
import SwiftMessages
import SVProgressHUD

protocol MakeOfferDelegate: class {
	func makeOfferAction(value: String, userIsRegistered: Bool)
}

class MakeOfferController: UIViewController, UITextFieldDelegate {

	@IBOutlet weak var offerValue: UITextField!
	@IBOutlet weak var offerPanelTitle: UILabel!

	weak var delegate: MakeOfferDelegate?
	var offerPriceValue: String?
	var askingPrice: String?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		offerValue.delegate = self
		offerValue.tintColor = .lightGray
		offerValue.backgroundColor = .white
		offerValue.keyboardType = .numberPad
		offerValue.placeholder = offerPriceValue
		offerPanelTitle.text = askingPrice
	}
    
	@IBAction func makeOfferAction(_ sender: UIButton) {
		if let senderText = offerValue.text {
			guard  !senderText.isEmpty else { return SVProgressHUD.showError(withStatus: "Please enter a price!") }
			CurrentUserInfo.shared.userId().isEmpty ? delegate?.makeOfferAction(value: senderText, userIsRegistered: false) : delegate?.makeOfferAction(value: senderText, userIsRegistered: true)
			self.dismiss(animated: true, completion: nil)
		}
	}

	@IBAction func cancelOffer(_ sender: UIButton) {
			self.dismiss(animated: true, completion: nil)
	}
}
