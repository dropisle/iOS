//
//  ProductDetailPageController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-07.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import FSPagerView
import CoreLocation
import DropDown
import IQKeyboardManagerSwift
import SDWebImage
import SVProgressHUD
import FirebaseDynamicLinks
import MessageKit
import SwiftMessages
import MessageUI
import FacebookShare 

struct Global {
	static var productOnRegister: ProductModel.content?
	static var productIfOpenWithDynamicLink: ProductDetailsModel?
}

class ProductDetailPageController: UIViewController,
	UITableViewDelegate,
	UITableViewDataSource,
	UIScrollViewDelegate,
	MFMessageComposeViewControllerDelegate,
	SharingDelegate,
	FSPagerViewDataSource,
	FSPagerViewDelegate,
	PinchToZoomDelegate,
	ShowUserProfileDelegate,
	RatingControlDelegate,
	UserProductDescriptionDelegate,
	ProductLocationDelegate,
	SimilarProductDelegate,
	CommentHeightDeleagte,
	MakeOfferDelegate,
	ReportProductDelegate {
	
	var commentCellHeight = CGFloat()
	let headerReuseId = "TableHeaderViewReuseId"
	let productLocationController = "ProductLocationController"
	let makeOfferController = "MakeOfferController"
	var productObject: ProductModel.content?
	var productDetailsObject: ProductDetailsModel?
	var productObjId: String?
	var productObjCategory: String?
	var productIdFromDeepLink = String()
	var productCategoryFromDeepLink = String()
	var isFromSearchController = false
	var likeState = String()
	var imageData = UIImage()
	var userId = String()
	var productId = String()
	var dropDown = DropDown()
	var appearance = DropDown.appearance()
	var fullZoomImageArray: [String] = []
	var pagerView = FSPagerView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIDevice.current.userInterfaceIdiom == .pad ? 650 : 380))
	var pageControl = FSPageControl()
	var productAddress = String()
	var moreDetailsUpdated = Bool()
	var shortURL = URL(string: "")
	
	///To be implemented
	var commentModel = [String]()
	
	///Similar product
	var product = [ProductModel]()
	var content = [ProductModel.content]()
	
	var productDetails = [ProductDetailsModel]() {
		didSet {
			DispatchQueue.main.async {
				self.tableView.reloadData()
			}
		}
	}
	
	@IBOutlet weak var containerBottomView: UIView!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var productAvalability: UIButton!
	@IBOutlet weak var makeOffer: UIButton!
	@IBOutlet weak var subContainerView: UIView!
	@IBOutlet weak var shareProductButton: UIButton!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.navigationBar.barTintColor = UIColor(hexString: "#3293FC")
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton
		
		let headerNib = UINib(nibName: "CustomHeaderView", bundle: nil)
		tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: headerReuseId)
		tableView.showsVerticalScrollIndicator = false
		//tableView.reloadData()
		tableView.backgroundColor = UIColor(hexString: "#f6f6f6")
		
		//set border to botton container
		containerBottomView.layer.borderWidth = 0
		containerBottomView.layer.borderColor = UIColor.lightGray.cgColor
		containerBottomView.layer.shadowColor = UIColor.lightGray.cgColor
		containerBottomView.layer.shadowOpacity = 0.5
		containerBottomView.layer.shadowOffset = .zero
		containerBottomView.layer.shadowRadius = 3
		containerBottomView.layer.shouldRasterize = true
		containerBottomView.layer.rasterizationScale = UIScreen.main.scale
		
		makeOffer.layer.borderWidth = 0
		makeOffer.layer.borderColor = UIColor.lightGray.cgColor
		makeOffer.layer.shadowColor = UIColor.lightGray.cgColor
		makeOffer.layer.shadowOpacity = 0.5
		makeOffer.layer.shadowOffset = .zero
		makeOffer.layer.shadowRadius = 3
		makeOffer.layer.shouldRasterize = true
		makeOffer.layer.rasterizationScale = UIScreen.main.scale
		
		///Drop down apperance
		customizeDropDown()
		
		productObject == nil ? (productObjId = productDetailsObject?.id) : (productObjId = productObject?.id)
		productObject == nil ? (productObjCategory = productDetailsObject?.category) : (productObjCategory = productObject?.category)
		
		///Get Product Details
		CurrentUserInfo.shared.userId().isEmpty ?
			fetchProductDetailForGuestUsers(productId: productObjId ?? productIdFromDeepLink) :
			fetchProductDetailForRegisteredUsers(productId: productObjId ?? productIdFromDeepLink, userId: CurrentUserInfo.shared.userId())
		
		//MARK: Prefetch Similar products before view loads
		fetchCategory(category: productObjCategory ?? productCategoryFromDeepLink)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.tabBarController?.tabBar.isHidden = true
		self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Avenir-Black", size: 20)!]
		if let tabBarController = self.tabBarController as? TabBarViewController {
		}
		
		///Ananlytics
		LoggerManager.shared.log(event: .productDetail)
	}
	
	///Set Up image Container
	func setUpImageContainer(imageCount: Int) {
		//Mark pageviewer
		self.pagerView.dataSource = self
		self.pagerView.delegate = self
		self.pagerView.itemSize = FSPagerView.automaticSize
		self.pagerView.interitemSpacing = 10
		self.pagerView.isInfinite = false
		self.pagerView.transformer = FSPagerViewTransformer(type: .crossFading)
		self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
		
		// Create a page control
		self.pageControl = FSPageControl(frame: CGRect(x: 0, y: 0, width: 100, height: 10))
		self.pageControl.numberOfPages = imageCount
		self.pageControl.contentHorizontalAlignment = .center
		self.pageControl.hidesForSinglePage = true
		self.pageControl.backgroundColor = .clear
		self.pageControl.setStrokeColor(.clear, for: .normal)
		self.pageControl.setStrokeColor(.clear, for: .selected)
		self.pageControl.setFillColor(UIColor.lightGray, for: .normal)
		self.pageControl.setFillColor(UIColor(red: 0.20, green: 0.58, blue: 0.99, alpha: 1.0), for: .selected)
		self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		//Clear Zoom Image Array
		fullZoomImageArray.removeAll()
	}
	
	private func addBarButtons(likeImage: String) {
		let likeImage = UIImage(named: likeImage)!
		let moreImage = UIImage(named: "More")!
		
		let likeButton = UIBarButtonItem(image: likeImage, style: .plain, target: self, action: #selector((didTapLike)))
		likeButton.tintColor = UIColor.white
		let moreButton = UIBarButtonItem(image: moreImage, style: .plain, target: self, action: #selector((didTapMore)))
		moreButton.tintColor = UIColor.white
		navigationItem.rightBarButtonItems = [moreButton, likeButton]
	}
	
	@objc func didTapMore(sender: AnyObject) {
		setupDropDown()
		dropDown.show()
	}
	
	@objc func dismissScreen(sender: AnyObject) {
		//Remove product objects to be passed if detatil screen is dismissmed
		Global.productOnRegister = nil
		Global.productIfOpenWithDynamicLink = nil
		if isFromSearchController {
			self.dismiss(animated: true, completion: nil)
		} else {
			self.navigationController?.popViewController(animated: true)
		}
	}
	
	func showUserProfile(sender: UITapGestureRecognizer) {
		///Analytics
		LoggerManager.shared.log(event: .showUserProfile)
		
		let storyBoard: UIStoryboard = UIStoryboard(name: "ProfilePage", bundle: nil)
		if let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController {
			if let userIdfromHomePage = productDetailsObject, let userId = userIdfromHomePage.user.userid {
				newViewController.userIdentifier = userId
			}
			self.navigationController?.pushViewController(newViewController, animated: true)
		}
	}
	
	func setupDropDown() {
		// Drop down
		dropDown.bottomOffset = CGPoint(x: -75, y: 43)
		appearance.textColor = .darkGray
		dropDown.anchorView = navigationItem.rightBarButtonItem
		dropDown.width = 100
		dropDown.dataSource = ["Report"]
		dropDown.cellNib = UINib(nibName: "CountryDropDownCell", bundle: nil)
		dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
			guard let cell = cell as? CountryDropDownCell else { return }
			if index == 0 {
				cell.countryFlagImage.image = UIImage(named: "Flag")?.withRenderingMode(.alwaysTemplate)
				cell.countryFlagImage.tintColor = .lightGray
			} else {
				//cell.countryFlagImage.image = UIImage(named: "nigeria")
			}
		}
		
		///Action triggered on selection
		dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
			print("Selected item: \(item) at index: \(index)")
			if index == 0 {
				self.setUpAlertSheets()
			}
		}
	}
	
	///Report Product
	func setUpAlertSheets() {
		let destinationVC = ReportProductController.instantiate()
		destinationVC.delegate = self
		let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: destinationVC)
		segue.configure(layout: .centered)
		segue.messageView.backgroundHeight = 460
		segue.keyboardTrackingView = KeyboardTrackingView()
		segue.perform()
	}
	
	func customizeDropDown() {
		appearance.cellHeight = 45
		appearance.backgroundColor = UIColor(white: 1, alpha: 1)
		appearance.selectionBackgroundColor = .clear
		appearance.separatorColor = UIColor(white: 0.0, alpha: 0.1)
//		appearance.cornerRadius = 0.0
//		appearance.shadowOpacity = 0.9
//		appearance.shadowRadius = 20
		appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
		appearance.animationduration = 0.30
		if let font = UIFont(name: "Avenir", size: 14.0) {
			appearance.textFont = font
		}
	}
	
	@IBAction func contactSeller(_ sender: UIButton) {
		CurrentUserInfo.shared.userId().isEmpty ? CheckIfUserIsRegistered(product: productObject, productDetails: productDetailsObject) : showConversationViewController()
	}
	
	func showConversationViewController() {
		///Analytics
		LoggerManager.shared.log(event: .showConversation)
		
		let newViewController = ConversationViewController()
		//newViewController.productDetails = productDetails.first
		navigationController?.pushViewController(newViewController, animated: true)
	}
}

extension ProductDetailPageController {
	
	func contactSellerState() {
		subContainerView.isHidden = true
	}
	
	func itemSoldState() {
		subContainerView.isHidden = false
		shareProductButton.setTitle("SOLD", for: .normal)
		shareProductButton.setTitleColor(.white, for: .normal)
		shareProductButton.isEnabled = false
		shareProductButton.backgroundColor = UIColor(hexString: "#84befd")
	}
	
	func userUploadedItem() {
		subContainerView.isHidden = false
		shareProductButton.setTitle("Promote by sharing", for: .normal)
		shareProductButton.setTitleColor(.white, for: .normal)
		shareProductButton.isEnabled = false
		shareProductButton.backgroundColor = UIColor(hexString: "#84befd")
	}
	
	private func fetchProductDetailForGuestUsers(productId: String) {
		ProductDetailService.sharedInstance.getProductById(id: productId) { [weak self] (productDetails) in
			guard let self = self else { return }
			
			//Set Up image Container
			self.setUpImageContainer(imageCount: productDetails.images.count)
			
			self.productDetails.append(productDetails)
			self.productDetailsObject = productDetails
			
			//Disable contact seller button if item is sold
			if productDetails.status == APIEndPoints.soldSate {
				self.itemSoldState()
			} else {
				self.contactSellerState()
			}
			
			// Display price on the nav view
			let price = "\(productDetails.price.price)".dropLast(2)
			self.title = "\(price)".currencyInputFormatting(currencySymbol: productDetails.price.currency)
			
			///get sharable link
			self.getShareURL()
			
			if let profileImage = productDetails.user.photo {
				self.getProfilePhoto(photoImage: profileImage) { (data) in
					guard let data = UIImage(data: data) else {
						self.imageData = UIImage(named: "userPlaceholder")!
						let indexPath = IndexPath(item: 0, section: 2)
						self.tableView.reloadRows(at: [indexPath], with: .automatic)
						return
					}
					self.imageData = data
					let indexPath = IndexPath(item: 0, section: 2)
					self.tableView.reloadRows(at: [indexPath], with: .automatic)
				}
			}
			
			//Check if item already liked by user
			self.addBarButtons(likeImage: "emptyLike")
		}
	}
	
	private func fetchProductDetailForRegisteredUsers(productId: String, userId: String) {
		ProductDetailService.sharedInstance.getProductByIdAndUserId(productId: productId, userId: userId) { [weak self] (productDetails) in
			guard let self = self else { return }
			
			///Set Up image Container
			self.setUpImageContainer(imageCount: productDetails.images.count)
			
			self.productDetails.append(productDetails)
			self.productDetailsObject = productDetails
			
			if productDetails.status == APIEndPoints.soldSate {
				self.itemSoldState()
			} else if productDetails.user.userid == CurrentUserInfo.shared.userId() {
				self.userUploadedItem()
			} else {
				self.contactSellerState()
			}
			
			///Display price on the nav view
			let price = "\(productDetails.price.price)".dropLast(2)
			self.title = "\(price)".currencyInputFormatting(currencySymbol: productDetails.price.currency)
			
			///get sharable link
			self.getShareURL()
			
			if let profileImage = productDetails.user.photo {
				self.getProfilePhoto(photoImage: profileImage) { (data) in
					DispatchQueue.main.async {
						if let data = UIImage(data: data)  {
							self.imageData = data
							let indexPath = IndexPath(item: 0, section: 2)
							self.tableView.reloadRows(at: [indexPath], with: .automatic)
						}
					}
				}
			}
			
			///Check if item already liked by user
			if productDetails.like == "NO" {
				self.likeState = "like"
				self.addBarButtons(likeImage: "emptyLike")
			} else {
				self.likeState = "unlike"
				self.addBarButtons(likeImage: "filledLike")
			}
		}
	}
}

extension ProductDetailPageController {
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		var headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerReuseId) as? CustomHeaderView
		if section == 0 {
			if headerView == nil {
				headerView = CustomHeaderView.customView
			}
			headerView?.delegate = self
			headerView?.imageViewer.addSubview(pagerView)
			
			pageControl.center = CGPoint(x: (headerView?.pageControlView.frame.size.width)! / 2, y: (headerView?.pageControlView.frame.size.height)! / 2)
			headerView?.pageControlView.addSubview(pageControl)
			headerView?.scrollView.delegate = self
			headerView?.scrollView.minimumZoomScale = 1.0
			headerView?.scrollView.maximumZoomScale = 2.0
			
			//Add image string to array and pass for full view
			_ = productDetails.map { imagesValues in
				let imageString = imagesValues.images
				for image in  imageString {
					fullZoomImageArray.append(image)
				}
			}
			return headerView
		}
		return headerView
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return PDPSections.numberOfSections
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let section = PDPSections(rawValue: section) else {
			return 0
		}
		switch section {
			case .firstCell:
				return productDetails.count
			case .secondCell:
				return productDetails.count
			case .fourthCell:
				return productDetails.count
			case .footerCell:
				return productDetails.count
			case .commentCell:
				return 1
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		guard let section = PDPSections(rawValue: indexPath.section) else {
			let cell = UITableViewCell()
			return cell
		}
		
		switch section {
			case .firstCell:
				var cell = tableView.dequeueReusableCell(withIdentifier: "PDPFirstViewCell") as? PDPFirstViewCell
				if cell == nil {
					cell = PDPFirstViewCell.customCell
				}
				
				cell?.clipsToBounds = true
				cell?.layer.cornerRadius = 20
				cell?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
				
				cell?.delegate = self
				cell?.titleLabel.text = productDetails[indexPath.row].name
				
				///Read more text added.
				cell?.descriptionLabel.text = productDetails[indexPath.row].description
				let readmoreFont = UIFont(name: "Helvetica-Oblique", size: 11)
				cell?.descriptionLabel.addTrailing(with: "...", moreText: "more", moreTextFont: readmoreFont!, moreTextColor: .lightGray)
				return cell!
			case .secondCell:
				var cell = tableView.dequeueReusableCell(withIdentifier: "PDPSecondViewCell") as? PDPSecondViewCell
				if cell == nil {
					cell = PDPSecondViewCell.customCell
				}
				cell?.itemCondition.text = productDetails[indexPath.row].condition
				cell?.itemCategory.text = productDetails[indexPath.row].category
				let price = "\(productDetails[indexPath.row].price.price)".dropLast(2)
				cell?.productPrice.text = "\(price)".currencyInputFormatting(currencySymbol: productDetails[indexPath.row].price.currency)
				
				if productDetails[indexPath.row].price.negotiable == "YES" {
					cell?.itemNegotiable.text = "Flexible"
				} else {
					cell?.itemNegotiable.text = "Fixed Price"
				}
				
				cell?.delegate = self
				if let countrycode = productDetails[indexPath.row].location?.countrycode, let region = productDetails[indexPath.row].location?.region, let city = productDetails[indexPath.row].location?.city {
					let location = (region + ", " + city + " " + countrycode)
					cell?.location.text = location
				}
				
				//passing product details to cell
				cell?.productDetails = productDetails[indexPath.row]
				return cell!
			case .fourthCell:
				var cell = tableView.dequeueReusableCell(withIdentifier: "PDPFourthViewCell") as? PDPFourthViewCell
				if cell == nil {
					cell = PDPFourthViewCell.customCell
				}
				
				cell?.clipsToBounds = true
				cell?.layer.cornerRadius = 20
				cell?.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
				
				cell?.delegate = self
				cell?.userProfileImage.image = imageData
				if let fName = productDetails[indexPath.row].user.firstname, let lName = productDetails[indexPath.row].user.lastname {
					cell?.userName.text = "\(fName + " " + lName)"
				}
				
				// MARK: Disabling rating for more precise implementation.
				//			let ratingValue = Double(productDetails[indexPath.row].rating)
				//			cell?.ratingControl.rating = Int(ratingValue ?? 0)
				//			cell?.ratingControl.delegate = self
				
				//assign product to user info cellpassing product details to cell
				cell?.productDetails = productDetails[indexPath.row]
				return cell!
			case .footerCell:
				var cell = tableView.dequeueReusableCell(withIdentifier: "WrapperTableViewCell") as? WrapperTableViewCell
				if cell == nil {
					cell = WrapperTableViewCell.customCell
				}
				cell?.updateCellWith(content: content, product: product)
				cell?.delegate = self
				return cell!
			case .commentCell:
				var cell = tableView.dequeueReusableCell(withIdentifier: "WrapperCommentCell") as? WrapperCommentCell
				if cell == nil {
					cell = WrapperCommentCell.customCell
				}
				cell?.clipsToBounds = true
				cell?.layer.cornerRadius = 20
				cell?.backgroundColor = .clear
				cell?.getComments(comments: commentModel)
				cell?.delegate = self
				return cell!
		}
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if indexPath.section == 1 {
			return 150
		} else if indexPath.section == 2 {
			return 185
		} else if indexPath.section == 3 {
			return 200
		} else if indexPath.section == 4 {
			return commentCellHeight == 0.0 ? UITableView.automaticDimension : commentCellHeight
		} else {
			return UITableView.automaticDimension
		}
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		guard let section = PDPSections(rawValue: section) else {
			return 0
		}
		switch section {
			case .firstCell:
				if UIDevice.current.userInterfaceIdiom == .pad {
					return 650
				} else {
					return 380
			}
			case .secondCell:
				return CGFloat.leastNormalMagnitude
			case .fourthCell:
				return CGFloat.leastNormalMagnitude
			case .footerCell:
				return CGFloat.leastNormalMagnitude
			case .commentCell:
				return CGFloat.leastNormalMagnitude
		}
	}
	
	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		guard let section = PDPSections(rawValue: section) else {
			return 0
		}
		switch section {
			case .firstCell:
				return 2
			case .secondCell:
				return 2
			case .fourthCell:
				return 50
			case .footerCell:
				if !commentModel.isEmpty {
					return 45
				}
				return CGFloat.leastNormalMagnitude
			case .commentCell:
				return CGFloat.leastNormalMagnitude
		}
	}
	
	func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
		if section == 2 {
			//Set to check and avoid test to display before page loads.
			if !productDetails.isEmpty {
				return "Similar Products"
			}
		} else if section == 3 {
			if !commentModel.isEmpty {
				return "Reviews"
			}
		}
		return String()
	}
	
	func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
		if section == 2 || section == 3 {
			let header = view as? UITableViewHeaderFooterView
			header?.textLabel?.textColor = .black
			header?.textLabel?.font = UIFont(name: "Avenir-Black", size: 18)
		}
	}
	
	// MARK: - Navigation
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == productLocationController {
			if let nav = segue.destination as? UINavigationController, let address = nav.topViewController as? ProductLocationController {
				address.productAddress = productAddress
			}
		}

		if (segue.identifier == makeOfferController) {
			if let segue = segue as? SwiftMessagesSegue {
				let viewController = segue.destination as? MakeOfferController
				if let actualPrice = productDetailsObject {
					let price = "\(actualPrice.price.price)".dropLast(2)
					let value = "\(price)".currencyInputFormatting(currencySymbol: actualPrice.price.currency)
					viewController?.offerPriceValue = value
					viewController?.askingPrice = "This is selling for \(value), you are offering?"
				}
				viewController?.delegate = self
				segue.keyboardTrackingView = KeyboardTrackingView()
			}
		}
	}
}

// Handling images on detail page
extension ProductDetailPageController {
	
	func numberOfItems(in pagerView: FSPagerView) -> Int {
		let detailImages = productDetails.map { $0.images }.flatMap { $0 }
		return detailImages.count
	}
	
	func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
		let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
		let detailImages = productDetails.map { $0.images }.flatMap { $0 }
		let imageLocation = detailImages[index]
		cell.imageView?.contentMode = .scaleAspectFill
		cell.imageView?.clipsToBounds = false
		cell.imageView?.sd_imageTransition = SDWebImageTransition.fade
		cell.imageView?.sd_setImage(with: URL(string: imageLocation), placeholderImage: UIImage(named: "largePlaceholder"), options: .fromCacheOnly, completed:  { (image, error, cacheType, url) in
			cell.imageView?.image = image
		})
		return cell
	}
	
	func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
		pagerView.deselectItem(at: index, animated: true)
		pagerView.scrollToItem(at: index, animated: true)
	}
	
	func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
		self.pageControl.currentPage = targetIndex
	}
	
	func pinchToZoom(sender: UITapGestureRecognizer) {
		let view = tableView.headerView(forSection: 0) as! CustomHeaderView
		
		if (view.scrollView.zoomScale < 1.5) {
			view.scrollView.setZoomScale(view.scrollView.maximumZoomScale, animated: true)
		} else {
			view.scrollView.setZoomScale(view.scrollView.minimumZoomScale, animated: true)
		}
	}
	
	@objc func pinchGesture(sender: UIPinchGestureRecognizer) {
		sender.view?.transform = (sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale))!
		sender.scale = 1.0
	}
	
	func fullScreenImageZoom(sender: UITapGestureRecognizer) {
		let storyboard = UIStoryboard(name: "FullScreenImageZoomStoryboard", bundle: nil)
		if let pvc = storyboard.instantiateViewController(withIdentifier: "FullScreenImageZoomViewController") as? FullScreenImageZoomViewController {
			pvc.fullZoomImageArray = fullZoomImageArray
			pvc.modalPresentationStyle = UIModalPresentationStyle.custom
			pvc.modalTransitionStyle = .crossDissolve
			pvc.transitioningDelegate = self
			self.present(pvc, animated: true, completion: nil)
		}
	}
}

extension ProductDetailPageController: UIViewControllerTransitioningDelegate {
	func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
		return FullSizePresentationController(presentedViewController: presented, presenting: presenting)
	}
}

extension ProductDetailPageController {
	///Report Product
	func reportProduct(report: String) {
		let desc = "Other Please describe"
		guard report != desc else {
			alert(message: "Please select or describe the issue.")
			return
		}
		let userId = CurrentUserInfo.shared.userId()
		let params = ["userid": userId, "other": productDetailsObject?.user.userid, "itemid": productDetailsObject?.id, "type": "Product", "comment": report]
		ProductDetailService.sharedInstance.reportProductAndMessage(params: params as [String: AnyObject]) { (result: ReportProductAndMessage) in
			if result.code == 200 {
				self.alert(message: "\(result.message) thank you. \nThis will be looked into and if necessary actions will be taken.", title: "")
			} else {
				SVProgressHUD.showError(withStatus: "Please try again \nSomething went wrong")
			}
		}
	}
	
	///Get comment collectionview height
	func setCommentHeight(value: CGFloat) {
		commentCellHeight =  value
	}
	
	///Rating Seller to be done.
	func rateSeller(cell: PDPFourthViewCell, sender: UIButton) {
		guard let indexPath = self.tableView.indexPath(for: cell) else {
			return
		}
		_ = tableView.cellForRow(at: indexPath) as? PDPFourthViewCell
	}
	
	///Show more action.
	func showMore(cell: PDPFirstViewCell, sender: UIButton) {
		guard let indexPath = self.tableView.indexPath(for: cell) else {
			return
		}
		if #available(iOS 11.0, *) {
			let cell = tableView.cellForRow(at: indexPath) as? PDPFirstViewCell
			tableView.performBatchUpdates({() -> Void in
				cell?.descriptionLabel.text = productDetails[indexPath.row].description
			}, completion: {(_ finished: Bool) -> Void in })
		} else {
			tableView.beginUpdates()
			cell.descriptionLabel.text = productDetails[indexPath.row].description
			tableView.endUpdates()
		}
	}
	
	///Open Map Show User Area Location
	func openMap(cell: PDPSecondViewCell, sender: UIButton) {
		///Analytics
		LoggerManager.shared.log(event: .mapView)
		if let region = cell.productDetails.location?.region, let city = cell.productDetails.location?.city, let countryCode = cell.productDetails.location?.countrycode {
			productAddress = region + ", " + city + " " + countryCode
		}
		self.performSegue(withIdentifier: productLocationController, sender: self)
	}
	
	///Get rating when star is clicked
	func ratingButtonTapped(_ ratingControl: RatingControl) {
		let indexPath = IndexPath(row: 0, section: 2)
		guard let cell = tableView.cellForRow(at: indexPath) as? PDPFourthViewCell else {
			return
		}
		let params: [String: AnyObject] = ["itemcode": cell.productDetails.id as AnyObject, "userid": cell.productDetails.user.userid as AnyObject, "rating": ratingControl.rating as AnyObject]
		ProductDetailService.sharedInstance.ratingUserProduct(params: params) { (response) in
			if response.code == 200 {
				cell.ratingControl.shake()
			} else {
				SVProgressHUD.showError(withStatus: "Please try again \nSomething went wrong")
			}
		}
	}
	
	///Check if user is logged in before adding or removing from wishlist for Similar product page
	func checkIfUserIsLoggedIn(product: ProductModel.content) {
		CheckIfUserIsRegistered(product: product, productDetails: nil)
	}
	
	func openProductDetails(cell: CustomCollectionViewCell, productId: String) {
		let storyBoard: UIStoryboard = UIStoryboard(name: "ProductDetailPage", bundle: nil)
		if let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProductDetailPageController") as? ProductDetailPageController {
			newViewController.productObject = cell.product
			navigationController?.pushViewController(newViewController, animated: true)
		}
	}
	
	@objc func didTapLike(sender: AnyObject) {
		CurrentUserInfo.shared.userId().isEmpty ? CheckIfUserIsRegistered(product: productObject, productDetails: productDetailsObject) : likeUserAndUnlikeUser()
	}
	
	func likeUserAndUnlikeUser() {
		let userId = CurrentUserInfo.shared.userId()
		var dict = [String: String]()
		var params = [String: AnyObject]()
		dict.updateValue(productDetailsObject?.id ?? "", forKey: "itemcode")
		dict.updateValue(userId, forKey: "userid")
		dict.updateValue(self.likeState, forKey: "type")
		if let theJSONData = try?  JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
			if let jsonObject = try? JSONSerialization.jsonObject(with: theJSONData, options: []) as? [String: AnyObject] {
				params = jsonObject
			}
		}
		ProductDetailService.sharedInstance.likeProduct(params: params) { (response) in
			if response.code == 200 {
				if response.status == "liked" {
					self.addBarButtons(likeImage: "filledLike")
					self.likeState = "unlike"
					
					///Analytics
					let parameter = ["userId": CurrentUserInfo.shared.userId(), "productName": self.productDetailsObject?.name ?? "", "productCategory": self.productDetailsObject?.category ?? ""] as [String : Any]
					LoggerManager.shared.log(event: .like, parameters: parameter)
				} else {
					self.addBarButtons(likeImage: "emptyLike")
					self.likeState = "like"
					
					///Analytics
					let parameter = ["userId": CurrentUserInfo.shared.userId(), "productName": self.productDetailsObject?.name ?? "", "productCategory": self.productDetailsObject?.category ?? ""] as [String : Any]
					LoggerManager.shared.log(event: .unlike, parameters: parameter)
				}
			} else {
				SVProgressHUD.showError(withStatus: "Please try again \nSomething went wrong")
			}
		}
	}
}

extension ProductDetailPageController {
	///Share Product
	func getShareURL() {
		var components = URLComponents()
		components.scheme = APIEndPoints.scheme
		components.host = APIEndPoints.host
		components.path = APIEndPoints.productPath
		
		let productId = URLQueryItem(name: "id", value: productDetails.first?.id)
		let productCategory = URLQueryItem(name: "category", value: productDetails.first?.category)
		components.queryItems = [productId, productCategory]
		
		guard let linkParam = components.url else { return }
		print("I am sharing \(linkParam.absoluteString)")
		
		///Creating dynamic link
		guard let shareLink = DynamicLinkComponents.init(link: linkParam, domainURIPrefix: APIEndPoints.customDomain) else {
			print("Unable to creat dynamiclink")
			return
		}
		
		if let myBundleId = Bundle.main.bundleIdentifier {
			shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: myBundleId)
		}
		
		///Temporary redirection
		shareLink.iOSParameters?.appStoreID = APIEndPoints.appStoreId
		shareLink.iOSParameters?.fallbackURL = URL(string: APIEndPoints.host)
		shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
		shareLink.socialMetaTagParameters?.title = "\(productDetails.first?.name ?? "Dropisle")"
		shareLink.socialMetaTagParameters?.descriptionText = productDetails.first?.description
		if let photo = productDetails.first?.photo.url {
			shareLink.socialMetaTagParameters?.imageURL = URL(string: photo)
		}
		
		DispatchQueue.global(qos: .background).async {
			shareLink.shorten { [weak self] (url, warnings, error) in
				guard let self = self else { return }
				if let error = error {
					print("There is an \(error)")
				}
				if let warnings = warnings {
					for warning in warnings {
						print("There is a warning \(warning)")
					}
				}
				guard let url = url else { return }
				print("The short url is \(url.absoluteString)")
				self.shortURL = url
			}
		}
	}

	///Share on iMessage
	func iMessageShare(cell: PDPFourthViewCell, sender: UIButton) {
		guard MFMessageComposeViewController.canSendText() else {
			return
		}
		
		let messageVC = MFMessageComposeViewController()
		if let link = shortURL {
			messageVC.body = "Check out this deal on Dropisle! \n\(link)"
			messageVC.messageComposeDelegate = self
			self.present(messageVC, animated: false, completion: nil)
		}
	}
	
	func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
		switch (result.rawValue) {
			case MessageComposeResult.cancelled.rawValue:
				print("Message was cancelled")
				self.dismiss(animated: true, completion: nil)
			case MessageComposeResult.failed.rawValue:
				print("Message failed")
				self.dismiss(animated: true, completion: nil)
			case MessageComposeResult.sent.rawValue:
				print("Message was sent")
				self.dismiss(animated: true, completion: nil)
			default:
				break
		}
	}

	///Share on Whatsapp
	func whatsappShare(cell: PDPFourthViewCell, sender: UIButton) {
		if let link = shortURL {
			let urlWhats = "whatsapp://send?text=Check out this deal on Dropisle! \n\(link)"
			if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
				if let whatsappURL = NSURL(string: urlString) {
					if UIApplication.shared.canOpenURL(whatsappURL as URL) {
						UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: { (Bool) in })
					} else {
						// Handle a problem
					}
				}
			}
		}
	}

	///Share on Facebook
	func shareTextOnFaceBook() {
		let shareContent = ShareLinkContent()
		if let link = shortURL {
			shareContent.contentURL = link 	
			shareContent.quote =  "Check out this deal on Dropisle!"
			ShareDialog(fromViewController: self, content: shareContent, delegate: self).show()
		}
	}
	
	func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
		if sharer.shareContent.pageID != nil {
			print("Share: Success")
		}
	}
	
	func sharer(_ sharer: Sharing, didFailWithError error: Error) {
		print("Share: Fail")
	}
	
	func sharerDidCancel(_ sharer: Sharing) {
		print("Share: Cancel")
	}

	///Share on Facebook
	func facebookShare(cell: PDPFourthViewCell, sender: UIButton) {
		shareTextOnFaceBook()
	}

	///Copy and Paste on any platform
	func copyPasteShare(cell: PDPFourthViewCell, sender: UIButton) {
		if let link = shortURL {
			UIPasteboard.general.string = "Check out this deal on Dropisle! \n\(link)"
			self.alert(message: "The product's sharable link has been copied to the clipboard and can now be shared with your friends.", title: "Copied")
		}
	}

	///Share on Twitter
	func twitterShare(cell: PDPFourthViewCell, sender: UIButton) {
		if let link = shortURL {
			let tweetText = "Check out this deal on Dropisle!"
			let shareString = "https://twitter.com/intent/tweet?text=\(tweetText)&url=\(link)"
			// encode a space to %20 for example
			let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
			// cast to an url
			let url = URL(string: escapedShareString)
			// open in safari
			UIApplication.shared.open(url!, options: [:], completionHandler: { (Bool) in })
		}
	}

	///Share on all Platforms
	func shareOption(cell: PDPFourthViewCell, sender: UIButton) {
		let indexPath = IndexPath(row: 0, section: 2)
		guard let cell = tableView.cellForRow(at: indexPath) as? PDPFourthViewCell else {
			return
		}
		var components = URLComponents()
		components.scheme = APIEndPoints.scheme
		components.host = APIEndPoints.host
		components.path = APIEndPoints.productPath
		
		let productId = URLQueryItem(name: "id", value: cell.productDetails.id)
		let productCategory = URLQueryItem(name: "category", value: cell.productDetails.category)
		components.queryItems = [productId, productCategory]
		
		guard let linkParam = components.url else { return }
		print("I am sharing \(linkParam.absoluteString)")
		
		///Creating dynamic link
		guard let shareLink = DynamicLinkComponents.init(link: linkParam, domainURIPrefix: APIEndPoints.customDomain) else {
			print("Unable to creat dynamiclink")
			return
		}
		
		if let myBundleId = Bundle.main.bundleIdentifier {
			shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: myBundleId)
		}
		
		///Temporary redirection
		shareLink.iOSParameters?.appStoreID = APIEndPoints.appStoreId
		shareLink.iOSParameters?.fallbackURL = URL(string: APIEndPoints.host)
		shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
		shareLink.socialMetaTagParameters?.title = "\(cell.productDetails.name)"
		shareLink.socialMetaTagParameters?.descriptionText = cell.productDetails.description
		shareLink.socialMetaTagParameters?.imageURL = URL(string: cell.productDetails.photo.url)
		
		DispatchQueue.global(qos: .background).async {
			shareLink.shorten { [weak self] (url, warnings, error) in
				guard let self = self else { return }
				if let error = error {
					print("There is an \(error)")
				}
				if let warnings = warnings {
					for warning in warnings {
						print("There is a warning \(warning)")
					}
				}
				guard let url = url else { return }
				print("The short url is \(url.absoluteString)")
				DispatchQueue.main.async {
					self.showShareSheet(url: url, productName: cell.productDetails.name)
				}
			}
		}
	}
	
	//		guard let LongUrl = shareLink.url else { return }
	//		print("The long url is \(LongUrl.absoluteString)")
	
	func showShareSheet(url: URL, productName: String) {
		///Analytics
		let parameter = ["userId": CurrentUserInfo.shared.userId(), "productName": productName, "url": url] as [String : Any]
		LoggerManager.shared.log(event: .shareProduct, parameters: parameter)
		
		let promoText = "Hey there! Take a look at this fantastic product \(productName) on Dropisle Mobile App."
		let activityItems = [promoText, url] as [Any]
		let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
		if let popoverController = activityController.popoverPresentationController {
			popoverController.sourceView = self.view
			popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
			popoverController.permittedArrowDirections = []
		}
		self.present(activityController, animated: true, completion: nil)
	}
}

///get user detail image but does not work if viewing in guest mode
extension ProductDetailPageController {
	func getProfilePhoto(photoImage: String, completion: @escaping (Data) -> Void) {
		APIManager.shared.getUserProfileImage(photoImage: photoImage) { (imageData) in
			completion(imageData)
		}
	}
}

extension ProductDetailPageController {
	private  func CheckIfUserIsRegistered(product: ProductModel.content?, productDetails: ProductDetailsModel?) {
		///Show Guest User Screen
		LoggerManager.shared.log(event: .guestUserCreateOrSignInScreen)
		
		let storyboard = UIStoryboard(name: "GuestUserSignUpStoryboard", bundle: nil)
		if let pvc = storyboard.instantiateVC() as? GuestSignUpPanelViewController {
			pvc.modalPresentationStyle = UIModalPresentationStyle.custom
			pvc.modalTransitionStyle = .crossDissolve
			pvc.transitioningDelegate = self
			Global.productOnRegister = product
			Global.productIfOpenWithDynamicLink = productDetails
			self.present(pvc, animated: true, completion: nil)
		}
	}
}

extension ProductDetailPageController {
	
	func makeOfferAction(value: String, userIsRegistered: Bool) {
		if userIsRegistered {
			///Analytics
			let parameter = ["userId": CurrentUserInfo.shared.userId(), "productName": productDetailsObject?.name, "productCategory": productDetailsObject?.category]
			LoggerManager.shared.log(event: .makeOfferClicked, parameters: parameter as [String : Any])
			
			if let currencySymbol = Locale.current.currencySymbol {
				self.makeOfferOption(offerText: "I will like to make an offer of \(currencySymbol)\(value) for this product.")
			}
		} else {
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
				self.CheckIfUserIsRegistered(product: self.productObject, productDetails: self.productDetailsObject)
			}
		}
	}
	
//	func currentSender() -> Sender {
//		return CurrentUserCheck.shared.currentSender
//	}
	
	@objc func makeOfferOption(offerText: String?) {
		///Analytics
		let parameter = ["userId": CurrentUserInfo.shared.userId(), "productName": productDetailsObject?.name, "productCategory": productDetailsObject?.category, "offerPrice": offerText]
		LoggerManager.shared.log(event: .makeOfferConfirmed, parameters: parameter as [String : Any])
		
		var params: [String: AnyObject]
		let randomInt = arc4random()
		if let details = productDetailsObject {
			params = ["id": "\(randomInt)" as AnyObject, "title": details.name as AnyObject, "message": offerText as AnyObject, "from": CurrentUserInfo.shared.userId() as AnyObject, "to": details.user.userid as AnyObject, "productid": details.id as AnyObject]
			MessageService.sharedInstance.sendMessage(params: params) { (response) in
				if response.code == 200 {
					SVProgressHUD.showSuccess(withStatus: "Done")
				} else {
					SVProgressHUD.showError(withStatus: "Something went wrong try again.")
				}}}}}

enum PDPSections: Int {
	case firstCell
	case secondCell
	case fourthCell
	case footerCell
	case commentCell
	static var numberOfSections: Int {
		return 5
	}
}

extension ProductDetailPageController {
	
	func fetchCategory(category: String) {
		CurrentUserInfo.shared.userId().isEmpty ? fetchProductBasedOnCategory(categoryName: category, page: "\(1)") : fetchProductByCategoryAndUserId(category: category, page: "\(1)")
	}
	
	func fetchProductBasedOnCategory(categoryName: String, page: String) {
		DispatchQueue.global(qos: .background).async {
			HomeService.sharedInstance.getProductByCategory(category: categoryName, page: page) { [weak self] (products) in
				guard let self = self else { return }
				DispatchQueue.main.async {
					self.product.removeAll()
					let prod = products.content.filter { $0.status != APIEndPoints.soldSate }
					let shuffledProducts = prod.shuffled()
					self.product.append(products)
					self.content.append(contentsOf: shuffledProducts)
					
					if UIDevice.current.userInterfaceIdiom == .pad {
						let indexPath = IndexPath(item: 0, section: 3)
						self.tableView.reloadRows(at: [indexPath], with: .automatic)
					}}}}}
	
	func fetchProductByCategoryAndUserId(category: String, page: String) {
		DispatchQueue.global(qos: .background).async {
			HomeService.sharedInstance.getProductByCategoryAndUserId(category: category, page: page) { [weak self] (products) in
				guard let self = self else { return }
				DispatchQueue.main.async {
					self.product.removeAll()
					let prod = products.content.filter { $0.status != APIEndPoints.soldSate }
					let shuffledProducts = prod.shuffled()
					self.product.append(products)
					self.content.append(contentsOf: shuffledProducts)
					
					if UIDevice.current.userInterfaceIdiom == .pad {
						let indexPath = IndexPath(item: 0, section: 3)
						self.tableView.reloadRows(at: [indexPath], with: .automatic)
					}}}}}}
