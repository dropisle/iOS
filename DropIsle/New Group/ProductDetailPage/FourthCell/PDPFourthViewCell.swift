//
//  PDPFourthViewCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-09.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol ShowUserProfileDelegate: class {
	func showUserProfile(sender: UITapGestureRecognizer)
	func rateSeller(cell: PDPFourthViewCell, sender: UIButton)
	func shareOption(cell: PDPFourthViewCell, sender: UIButton)
	func whatsappShare(cell: PDPFourthViewCell, sender: UIButton)
	func iMessageShare(cell: PDPFourthViewCell, sender: UIButton)
	func copyPasteShare(cell: PDPFourthViewCell, sender: UIButton)
	func twitterShare(cell: PDPFourthViewCell, sender: UIButton)
	func facebookShare(cell: PDPFourthViewCell, sender: UIButton)
}

class PDPFourthViewCell: UITableViewCell {
	weak var delegate: ShowUserProfileDelegate!
	var productDetails: ProductDetailsModel!

	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var userProfileImage: UIImageView!
	@IBOutlet weak var userName: UILabel!
	@IBOutlet weak var ratingControl: RatingControl!
	@IBOutlet weak var shareButton: UIButton!
	@IBOutlet weak var iMessageButton: UIButton!
	@IBOutlet weak var copyPasteButton: UIButton!
	@IBOutlet weak var twitterButton: UIButton!
	@IBOutlet weak var facebookButton: UIButton!

	class var customCell: PDPFourthViewCell {
		let cell = Bundle.main.loadNibNamed("PDPFourthViewCell", owner: self, options: nil)?.last
		return cell as! PDPFourthViewCell
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
		containerView.layer.borderWidth = 0
		containerView.layer.borderColor = UIColor.lightGray.cgColor

		userName.isUserInteractionEnabled = true
		userProfileImage.isUserInteractionEnabled = true
		userProfileImage.layer.borderWidth = 5
		userProfileImage.layer.borderColor = UIColor.white.cgColor

		let imageTap = UITapGestureRecognizer(target: self, action: #selector(showProfile))
		imageTap.numberOfTapsRequired = 1
		imageTap.delegate = self

		let nameTap = UITapGestureRecognizer(target: self, action: #selector(showProfile))
		nameTap.numberOfTapsRequired = 1
		nameTap.delegate = self

		userName.addGestureRecognizer(nameTap)
		userProfileImage.addGestureRecognizer(imageTap)

		let origImage = UIImage(named: "share")
		let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
		self.shareButton.setImage(tintedImage, for: .normal)
		self.shareButton.tintColor = UIColor(hexString: "#FFFFFF")

		let iMessageImage = UIImage(named: "iMessage")
		let  iMessageTintedImage = iMessageImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
		self.iMessageButton.setImage(iMessageTintedImage, for: .normal)
		self.iMessageButton.tintColor = UIColor(hexString: "#FFFFFF")

		let copyPasteImage = UIImage(named: "copyPaste")
		let  copyPasteTintedImage = copyPasteImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
		self.copyPasteButton.setImage(copyPasteTintedImage, for: .normal)
		self.copyPasteButton.tintColor = UIColor(hexString: "#FFFFFF")

		let twitterImage = UIImage(named: "twitter")
		let  twitterTintedImage = twitterImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
		self.twitterButton.setImage(twitterTintedImage, for: .normal)
		self.twitterButton.tintColor = UIColor(hexString: "#FFFFFF")

		let facebookImage = UIImage(named: "facebook")
		let  facebookTintedImage = facebookImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
		self.facebookButton.setImage(facebookTintedImage, for: .normal)
		self.facebookButton.tintColor = UIColor(hexString: "#FFFFFF")
	}

	@objc func showProfile(sender: UITapGestureRecognizer) {
		delegate?.showUserProfile(sender: sender)
	}

	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		// Configure the view for the selected state
	}

	@IBAction func shareOptions(_ sender: UIButton) {
		delegate?.shareOption(cell: self, sender: sender)
	}

	@IBAction func whatsappShare(_ sender: UIButton) {
		delegate.whatsappShare(cell: self, sender: sender)
	}

	@IBAction func iMessage(_ sender: UIButton) {
		delegate.iMessageShare(cell: self, sender: sender)
	}
	
	@IBAction func copyPaste(_ sender: UIButton) {
		delegate.copyPasteShare(cell: self, sender: sender)
	}

	@IBAction func twitter(_ sender: UIButton) {
		delegate.twitterShare(cell: self, sender: sender)
	}

	@IBAction func facebook(_ sender: UIButton) {
		delegate.facebookShare(cell: self, sender: sender)
	}

	//	override var frame: CGRect {
	//        get {
	//            return super.frame
	//        }
	//        set (newFrame) {
	//            var frame = newFrame
	//            frame.origin.x += 15
	//            frame.size.width -= 2 * 15
	//            super.frame = frame
	//        }
	//    }
}
