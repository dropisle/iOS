//
//  ProductDetailService.swift
//  DropIsle
//
//  Created by CtanLI on 2019-08-20.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation

class ProductDetailService {
	private init() {}
	static let sharedInstance = ProductDetailService()
	
	func getProductById(id: String, completion: @escaping (ProductDetailsModel) -> Void) {
		let urlString = String(format: APIEndPoints.getProductById, id)
		let getProductUrl = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .get, urlString: getProductUrl, params: nil, headers: nil) { (response: ProductDetailsModel) in
			completion(response)
		}
	}

	func getProductByIdAndUserId(productId: String, userId: String, completion: @escaping (ProductDetailsModel) -> Void) {
		let urlString = String(format: APIEndPoints.getProductByIdAndUserId, productId, userId)
		let getProductUrl = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .get, urlString: getProductUrl, params: nil, headers: nil) { (response: ProductDetailsModel) in
			completion(response)
		}
	}

	func ratingUserProduct(params: [String: AnyObject], completion: @escaping (RatingModel) -> Void) {
		let urlString = String(format: APIEndPoints.ratingProduct)
		_ = APIEndPoints.init()
		let getProductUrl = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .post, urlString: getProductUrl, params: params, headers: APIEndPoints.authorizationHeader) { (response: RatingModel) in
			completion(response)
		}
	}

	func likeProduct(params: [String: AnyObject], completion: @escaping (LikeAndUnlikeProduct) -> Void) {
		let urlString = String(format: APIEndPoints.likeAndUlikeProduct)
		_ = APIEndPoints.init()
		let likeAndUlikeProduct = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .post, urlString: likeAndUlikeProduct, params: params, headers: APIEndPoints.authorizationHeader) { (response: LikeAndUnlikeProduct) in
			completion(response)
		}
	}

	func reportProductAndMessage(params: [String: AnyObject], completion: @escaping (ReportProductAndMessage) -> Void) {
		let urlString = String(format: APIEndPoints.reportProductAndMessage)
		_ = APIEndPoints.init()
		let reportProductAndMessage = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .post, urlString: reportProductAndMessage, params: params, headers: APIEndPoints.authorizationHeader) { (response: ReportProductAndMessage) in
			completion(response)
		}
	}
}
