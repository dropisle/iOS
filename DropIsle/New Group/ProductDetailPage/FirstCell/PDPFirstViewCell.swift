//
//  FirstViewCell.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-07.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol UserProductDescriptionDelegate: class {
	func showMore(cell: PDPFirstViewCell, sender: UIButton)
}

class PDPFirstViewCell: UITableViewCell {

	weak var delegate: UserProductDescriptionDelegate!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var titleLabel: UILabel!

	class var customCell: PDPFirstViewCell {
		let cell = Bundle.main.loadNibNamed("PDPFirstViewCell", owner: self, options: nil)?.last
		return cell as! PDPFirstViewCell
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		///Initialization code
		let tap = UITapGestureRecognizer(target: self, action: #selector(self.showMoreDecription))
		tap.numberOfTapsRequired = 1
		descriptionLabel.isUserInteractionEnabled = true
		descriptionLabel.addGestureRecognizer(tap)
	}

	@objc func showMoreDecription(_ sender: UIButton) {
		delegate?.showMore(cell: self, sender: sender)
	}

    override func setSelected(_ selected: Bool, animated: Bool) {
			super.setSelected(selected, animated: animated)
    }

//	override var frame: CGRect {
//        get {
//            return super.frame
//        }
//        set (newFrame) {
//            var frame = newFrame
//            frame.origin.x += 15
//            frame.size.width -= 2 * 15
//            super.frame = frame
//        }
//    }
}
