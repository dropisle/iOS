//
//  SustainableCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-15.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class SustainableCatCell: UITableViewCell, Reusable {

	let typeText = UILabel()
	let subText = UILabel()
	let switcher = UISwitch()
	let topView = UIView()

	override func awakeFromNib() {
		super.awakeFromNib()
		//Initialization code
	}

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		typeText.translatesAutoresizingMaskIntoConstraints = false
		switcher.translatesAutoresizingMaskIntoConstraints = false
		subText.translatesAutoresizingMaskIntoConstraints = false
		topView.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(typeText)
		contentView.addSubview(switcher)
		contentView.addSubview(subText)
		contentView.addSubview(topView)


		NSLayoutConstraint.activate([
			topView.topAnchor.constraint(equalTo: topAnchor),
			topView.heightAnchor.constraint(equalToConstant: 0.3),
			topView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			topView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			typeText.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -15),
			subText.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 20),

			typeText.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			subText.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			subText.trailingAnchor.constraint(equalTo: switcher.leadingAnchor, constant: -20),

			switcher.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			switcher.centerYAnchor.constraint(equalTo: centerYAnchor),
			switcher.heightAnchor.constraint(equalToConstant: 25),
			switcher.widthAnchor.constraint(equalToConstant: 55)
		])

		topView.backgroundColor = .lightGray

		typeText.textColor = .label
		typeText.text = "Sustainable Art"
		typeText.font =  UIFont(name: "AppleSDGothicNeo-Bold", size: 18)

		subText.textColor = .label
		subText.lineBreakMode = .byWordWrapping
		subText.numberOfLines = 0
		subText.text = "Art with a special attention to reduce carbon footprint...."
		subText.font =  UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
