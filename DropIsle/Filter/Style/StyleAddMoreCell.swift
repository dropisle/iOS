//
//  StyleAddMoreCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-10.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol LoadMoreStyleDelegate {
	func loadMoreStyle()
}

class StyleAddMoreCell: UITableViewCell, Reusable {

	let loadMore = UIButton()
	let underlineView = UIView()
	let sperator = UIView()
	var delegate: LoadMoreStyleDelegate?

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		loadMore.addTarget(self, action: #selector(loadMorePerform), for: .touchUpInside)
		loadMore.translatesAutoresizingMaskIntoConstraints = false
		underlineView.translatesAutoresizingMaskIntoConstraints = false
		sperator.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(loadMore)
		contentView.addSubview(underlineView)
		contentView.addSubview(sperator)

		NSLayoutConstraint.activate([
			loadMore.topAnchor.constraint(equalTo: topAnchor),
			loadMore.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			
			underlineView.heightAnchor.constraint(equalToConstant: 0.8),
			underlineView.leadingAnchor.constraint(equalTo: loadMore.leadingAnchor),
			underlineView.trailingAnchor.constraint(equalTo: loadMore.trailingAnchor),
			underlineView.topAnchor.constraint(equalTo: loadMore.bottomAnchor, constant: 0),

			sperator.heightAnchor.constraint(equalToConstant: 0.2),
			sperator.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			sperator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			sperator.bottomAnchor.constraint(equalTo: bottomAnchor)
		])

		sperator.backgroundColor = .lightGray
		underlineView.backgroundColor = .label
		loadMore.setTitle("Show More Styles...", for: .normal)
		loadMore.setTitleColor(.label, for: .normal)
		loadMore.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 12)
		loadMore.titleLabel?.textAlignment = .left
		loadMore.titleLabel?.lineBreakMode = .byWordWrapping
		loadMore.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: -10, right: 0)
	}

	@objc func loadMorePerform() {
		delegate?.loadMoreStyle()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		// Configure the view for the selected state
	}
}
