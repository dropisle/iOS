//
//  StyleCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-09.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class StyleCell: UITableViewCell, Reusable {

	let typeText = UILabel()
	let selector = UIButton()
	let checkMark = UIImageView()

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		typeText.translatesAutoresizingMaskIntoConstraints = false
		selector.translatesAutoresizingMaskIntoConstraints = false
		checkMark.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(typeText)
		contentView.addSubview(selector)
		selector.addSubview(checkMark)

		NSLayoutConstraint.activate([
			typeText.topAnchor.constraint(equalTo: topAnchor, constant: 10),
			typeText.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
			typeText.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			selector.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			selector.centerYAnchor.constraint(equalTo: centerYAnchor),
			selector.heightAnchor.constraint(equalToConstant: 25),
			selector.widthAnchor.constraint(equalToConstant: 25),

			checkMark.centerXAnchor.constraint(equalTo: selector.centerXAnchor),
			checkMark.centerYAnchor.constraint(equalTo: selector.centerYAnchor),
		])

		typeText.textColor = .label
		selector.borderWidth = 0.3
		selector.borderColor = .lightGray
		selector.cornerRadius = 4

		let image = UIImage(systemName: "checkmark", withConfiguration: UIImage.SymbolConfiguration(pointSize: 13, weight: .regular, scale: .medium))?.withTintColor(.secondarySystemGroupedBackground, renderingMode: .alwaysOriginal)
		checkMark.image = image
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
