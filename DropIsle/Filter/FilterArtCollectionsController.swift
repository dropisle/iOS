//
//  FilterArtCollectionsController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-09.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class FilterArtCollectionsController: UIViewController, UITableViewDataSource, UITableViewDelegate {

	var typeCategory = TypeViewModel()
	var styleCategory = StyleViewModel()
	var subjectCategory = SubjectViewModel()
	var artistCategory = ArtistViewModel()
	var countrySection = CountryViewModel()
	var filteredData = CountryViewModel()

	var arrSelectedRows = [String]()
	var typeCount = 5
	var styleCount = 5
	var subjectCount = 5
	var countryCount = 5

	var tableView: UITableView!
	var topView = UIView()
	var closeButton = UIButton()
	var topTitle = UILabel()
	var topSeperator = UIView()

	var countryCatCell = CountryCatCell()

	override func viewDidLoad() {
		super.viewDidLoad()
		setupCollectionView()

		//Keyboard applied to all screens
		IQKeyboardManager.shared.enable = true
		IQKeyboardManager.shared.enableAutoToolbar = false

		closeButton.addTarget(self, action: #selector(closeScreen), for: .touchUpInside)
	}

	func setupCollectionView() {
		topTitle.text = "Filters"
		topTitle.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 20)
		topSeperator.backgroundColor = .lightGray

		let image = UIImage(systemName: "multiply", withConfiguration: UIImage.SymbolConfiguration(pointSize: 18, weight: .regular, scale: .medium))?.withTintColor(.label, renderingMode: .alwaysOriginal)
		closeButton.setImage(image, for: .normal)

		tableView = UITableView.init(frame: .zero)
		tableView.dataSource = self
		tableView.delegate = self
		tableView.backgroundColor = .secondarySystemGroupedBackground
		tableView.separatorStyle = .none
		tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		tableView.keyboardDismissMode = .onDrag

		view.addSubview(tableView)
		view.addSubview(topView)
		topView.addSubview(closeButton)
		topView.addSubview(topTitle)
		topView.addSubview(topSeperator)

		topView.translatesAutoresizingMaskIntoConstraints = false
		tableView.translatesAutoresizingMaskIntoConstraints = false
		closeButton.translatesAutoresizingMaskIntoConstraints = false
		topTitle.translatesAutoresizingMaskIntoConstraints = false
		topSeperator.translatesAutoresizingMaskIntoConstraints = false

		NSLayoutConstraint.activate([
			topTitle.centerXAnchor.constraint(equalTo: topView.centerXAnchor),
			topTitle.bottomAnchor.constraint(equalTo: topView.bottomAnchor, constant: -20),

			topSeperator.heightAnchor.constraint(equalToConstant: 0.2),
			topSeperator.leadingAnchor.constraint(equalTo: topView.leadingAnchor),
			topSeperator.trailingAnchor.constraint(equalTo: topView.trailingAnchor),
			topSeperator.bottomAnchor.constraint(equalTo: topView.bottomAnchor),

			closeButton.heightAnchor.constraint(equalToConstant: 25),
			closeButton.widthAnchor.constraint(equalToConstant: 25),
			closeButton.leadingAnchor.constraint(equalTo: topView.leadingAnchor, constant: 20),
			closeButton.bottomAnchor.constraint(equalTo: topView.bottomAnchor, constant: -20),

			topView.topAnchor.constraint(equalTo: view.topAnchor),
			topView.heightAnchor.constraint(equalToConstant: 80),
			topView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			topView.trailingAnchor.constraint(equalTo: view.trailingAnchor),

			tableView.topAnchor.constraint(equalTo: topView.bottomAnchor),
			tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])

		//Registering all Cells and Classes we will need
		tableView.register(TypeCell.self, forCellReuseIdentifier: TypeCell.identifier)
		tableView.register(StyleCell.self, forCellReuseIdentifier: StyleCell.identifier)
		tableView.register(SubjectCell.self, forCellReuseIdentifier: SubjectCell.identifier)
		tableView.register(CountryCatCell.self, forCellReuseIdentifier: CountryCatCell.identifier)
		tableView.register(ArtistCatCell.self, forCellReuseIdentifier: ArtistCatCell.identifier)
		tableView.register(SustainableCatCell.self, forCellReuseIdentifier: SustainableCatCell.identifier)
		tableView.register(FooterCell.self, forCellReuseIdentifier: FooterCell.identifier)

		tableView.register(TypeAddMoreCell.self, forCellReuseIdentifier: TypeAddMoreCell.identifier)
		tableView.register(StyleAddMoreCell.self, forCellReuseIdentifier: StyleAddMoreCell.identifier)
		tableView.register(SubjectAddMoreCell.self, forCellReuseIdentifier: SubjectAddMoreCell.identifier)
		tableView.register(CountryCatAddMoreCell.self, forCellReuseIdentifier: CountryCatAddMoreCell.identifier)
		tableView.register(FilterHeader.self, forHeaderFooterViewReuseIdentifier: FilterHeader.identifier)
	}

	@objc func closeScreen() {
		self.dismiss(animated: true, completion: nil)
	}

	func numberOfSections(in tableView: UITableView) -> Int {
		return FSections.numberOfSections
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let section = FSections(rawValue: section) else {
			return 0
		}
		switch section {
			case .firstCell:
				return typeCategory.items.prefix(typeCount).count + 1
			case .secondCell:
				return styleCategory.items.prefix(styleCount).count + 1
			case .thirdCell:
				return subjectCategory.items.prefix(subjectCount).count + 1
			case .fourthCell:
				return countrySection.items.prefix(countryCount).count + 1
			case .fifthCell:
				return artistCategory.items.count
			case .sixthCell:
			    return 1
			case .seventhCell:
				return 1
		}
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let section = FSections(rawValue: indexPath.section) else {
			let cell = UITableViewCell()
			return cell
		}
		switch section {
			case .firstCell:
				if (indexPath.row == typeCategory.items.prefix(typeCount).count) {
					let cell = tableView.dequeueReusableCell(withIdentifier: TypeAddMoreCell.identifier) as! TypeAddMoreCell
					cell.selectionStyle = .none
					cell.delegate = self
					return cell
				} else {
					let cell = tableView.dequeueReusableCell(withIdentifier: TypeCell.identifier) as! TypeCell
					cell.selectionStyle = .none
					let typeArr = typeCategory.items.prefix(typeCount)
					cell.typeText.text = typeArr[indexPath.row].title
					guard let value = cell.typeText.text else { return cell }
					if !arrSelectedRows.contains(value) {
						cell.selector.backgroundColor = .secondarySystemGroupedBackground
					} else {
						cell.selector.backgroundColor = .label
					}
					return cell
				}
			case .secondCell:
				if (indexPath.row == styleCategory.items.prefix(styleCount).count) {
					let cell = tableView.dequeueReusableCell(withIdentifier: StyleAddMoreCell.identifier) as! StyleAddMoreCell
					cell.selectionStyle = .none
					cell.delegate = self
					return cell
				} else {
					let cell = tableView.dequeueReusableCell(withIdentifier: StyleCell.identifier) as! StyleCell
					cell.selectionStyle = .none
					let typeArr = styleCategory.items.prefix(styleCount)
					cell.typeText.text = typeArr[indexPath.row].title

					guard let value = cell.typeText.text else { return cell }
					if !arrSelectedRows.contains(value) {
						cell.selector.backgroundColor = .secondarySystemGroupedBackground
					} else {
						cell.selector.backgroundColor = .label
					}
					return cell
				}
			case .thirdCell:
				if (indexPath.row == subjectCategory.items.prefix(subjectCount).count) {
					let cell = tableView.dequeueReusableCell(withIdentifier: SubjectAddMoreCell.identifier) as! SubjectAddMoreCell
					cell.selectionStyle = .none
					cell.delegate = self
					return cell
				} else {
					let cell = tableView.dequeueReusableCell(withIdentifier: SubjectCell.identifier) as! SubjectCell
					cell.selectionStyle = .none
					let typeArr = subjectCategory.items.prefix(subjectCount)
					cell.typeText.text = typeArr[indexPath.row].title

					guard let value = cell.typeText.text else { return cell }
					if !arrSelectedRows.contains(value) {
						cell.selector.backgroundColor = .secondarySystemGroupedBackground
					} else {
						cell.selector.backgroundColor = .label
					}
					return cell
				}
			case .fourthCell:
				if (indexPath.row == countrySection.items.prefix(countryCount).count) {
					let cell = tableView.dequeueReusableCell(withIdentifier: CountryCatAddMoreCell.identifier) as! CountryCatAddMoreCell
					cell.selectionStyle = .none
					cell.delegate = self
					return cell
				} else {
					let cell = tableView.dequeueReusableCell(withIdentifier: CountryCatCell.identifier) as! CountryCatCell
					cell.selectionStyle = .none
					let typeArr = countrySection.items.prefix(countryCount)
					cell.typeText.text = typeArr[indexPath.row].title

					guard let value = cell.typeText.text else { return cell }
					if !arrSelectedRows.contains(value) {
						cell.selector.backgroundColor = .secondarySystemGroupedBackground
					} else {
						cell.selector.backgroundColor = .label
					}
					return cell
				}
			case .fifthCell:
				let cell = tableView.dequeueReusableCell(withIdentifier: ArtistCatCell.identifier) as! ArtistCatCell
				cell.selectionStyle = .none
				cell.typeText.text = artistCategory.items[indexPath.row].title
				guard let value = cell.typeText.text else { return cell }
				if !arrSelectedRows.contains(value) {
					cell.selector.backgroundColor = .secondarySystemGroupedBackground
				} else {
					cell.selector.backgroundColor = .label
				}
				return cell
			case .sixthCell:
				let cell = tableView.dequeueReusableCell(withIdentifier: SustainableCatCell.identifier) as! SustainableCatCell
				return cell
			case .seventhCell:
				let cell = tableView.dequeueReusableCell(withIdentifier: FooterCell.identifier) as! FooterCell
				return cell
		}
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if indexPath.section == 5 || indexPath.section == 6 {
			return 100
		} else {
			return 50
		}
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let section = FSections(rawValue: indexPath.section) else { return }
		switch section {
			case .firstCell:
				guard let cell = tableView.cellForRow(at: indexPath) as? TypeCell else { return }
				guard let value = cell.typeText.text else { return }
				cellActions(value: value) ? (cell.selector.backgroundColor = .label) : (cell.selector.backgroundColor = .secondarySystemGroupedBackground)
			case .secondCell:
				guard let cell = tableView.cellForRow(at: indexPath) as? StyleCell else { return }
				guard let value = cell.typeText.text else { return }
				cellActions(value: value) ? (cell.selector.backgroundColor = .label) : (cell.selector.backgroundColor = .secondarySystemGroupedBackground)
			case .thirdCell:
				guard let cell = tableView.cellForRow(at: indexPath) as? SubjectCell else { return }
				guard let value = cell.typeText.text else { return }
				cellActions(value: value) ? (cell.selector.backgroundColor = .label) : (cell.selector.backgroundColor = .secondarySystemGroupedBackground)
			case .fourthCell:
				guard let cell = tableView.cellForRow(at: indexPath) as? CountryCatCell else { return }
				guard let value = cell.typeText.text else { return }
				cellActions(value: value) ? (cell.selector.backgroundColor = .label) : (cell.selector.backgroundColor = .secondarySystemGroupedBackground)
			case .fifthCell:
				guard let cell = tableView.cellForRow(at: indexPath) as? ArtistCatCell else { return }
				guard let value = cell.typeText.text else { return }
				cellActions(value: value) ? (cell.selector.backgroundColor = .label) : (cell.selector.backgroundColor = .secondarySystemGroupedBackground)
			case .sixthCell: break
			case .seventhCell: break
		}
	}

	func cellActions(value: String) -> Bool {
		var isUpdated = Bool()
		if !arrSelectedRows.contains(value) {
			arrSelectedRows.append(value)
			isUpdated = true
		} else {
			let result = arrSelectedRows.filter {$0 != value}
			arrSelectedRows.removeAll()
			arrSelectedRows.append(contentsOf: result)
			isUpdated = false
		}
		return isUpdated
	}
}

extension FilterArtCollectionsController {
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		if section == 3 {
			return 100
		} else if section == 5 || section == 6 {
			return 0
		} else {
			return 44
		}
	}

	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let customHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: FilterHeader.identifier) as! FilterHeader
		// set up your footer view
		customHeaderView.delegate = self
		guard let section = FSections(rawValue: section) else {
			let cell = UITableViewCell()
			return cell
		}
		switch section {
			case .firstCell:
				customHeaderView.title.text = "Type of Category"
				customHeaderView.searchBar.isHidden = true
				customHeaderView.searchBarTitle.isHidden = true
				customHeaderView.title.isHidden  = false
			case .secondCell:
				customHeaderView.title.text = "Style"
				customHeaderView.searchBar.isHidden = true
				customHeaderView.searchBarTitle.isHidden = true
				customHeaderView.title.isHidden  = false
			case .thirdCell:
				customHeaderView.title.text = "Subject"
				customHeaderView.searchBar.isHidden = true
				customHeaderView.searchBarTitle.isHidden = true
				customHeaderView.title.isHidden  = false
			case .fourthCell:
				customHeaderView.searchBarTitle.text = "Country"
				customHeaderView.title.isHidden  = true
				customHeaderView.searchBar.isHidden = false
				customHeaderView.searchBarTitle.isHidden = false
			case .fifthCell:
				customHeaderView.title.text = "Type of Artist"
				customHeaderView.searchBar.isHidden = true
				customHeaderView.searchBarTitle.isHidden = true
				customHeaderView.title.isHidden  = false
			case .sixthCell:
				customHeaderView.searchBar.isHidden = true
				customHeaderView.searchBarTitle.isHidden = true
				customHeaderView.title.isHidden  = true
			case .seventhCell: break
		}
		return customHeaderView
	}
}

extension FilterArtCollectionsController: LoadMoreTypeDelegate, LoadMoreStyleDelegate, LoadMoreSubjectDelegate, LoadMoreCountryDelegate {
	func loadMoreType() {
		typeCount += 5
		let lastRow = self.tableView.numberOfRows(inSection: 0) - 1
		if typeCategory.items.count != lastRow {
			tableView.reloadSections(IndexSet(integer: 0), with: .none)
		}
	}

	func loadMoreStyle() {
		styleCount += 5
		if styleCategory.items.count != self.tableView.numberOfRows(inSection: 1) - 1 {
			tableView.reloadSections(IndexSet(integer: 1), with: .none)
			DispatchQueue.main.async {
				let lastRow = self.tableView.numberOfRows(inSection: 1) - 1
				let indexPath = IndexPath(row: lastRow, section: 1)
				self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
			}
		}
	}

	func loadMoreSubject() {
		subjectCount += 5
		if subjectCategory.items.count != self.tableView.numberOfRows(inSection: 2) - 1 {
			tableView.reloadSections(IndexSet(integer: 2), with: .none)
			DispatchQueue.main.async {
				let lastRow = self.tableView.numberOfRows(inSection: 2) - 1
				let indexPath = IndexPath(row: lastRow, section: 2)
				self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
			}
		}
	}

	func loadMoreCountry() {
		countryCount += 5
		if countrySection.items.count != self.tableView.numberOfRows(inSection: 3) - 1 {
			tableView.reloadSections(IndexSet(integer: 3), with: .none)
			DispatchQueue.main.async {
				let lastRow = self.tableView.numberOfRows(inSection: 3) - 1
				let indexPath = IndexPath(row: lastRow, section: 3)
				self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
			}
		}
	}
}

extension FilterArtCollectionsController: SearchBarDelegate {
	func cancelSearch(value: String) {
		countrySection.items.removeAll()
		countrySection.items.append(contentsOf: filteredData.items)
		if countrySection.items.count != self.tableView.numberOfRows(inSection: 3) - 1 {
			tableView.reloadSections(IndexSet(integer: 3), with: .none)
			DispatchQueue.main.async {
				let lastRow = self.tableView.numberOfRows(inSection: 3) - 1
				let indexPath = IndexPath(row: lastRow, section: 3)
				self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
			}
		}
	}

	func searchCountry(value: String) {
		countrySection.items = filteredData.items.filter ({( region: CountryViewModelItem) -> Bool in
			return region.title.lowercased().contains(value.lowercased())
		})
		tableView.reloadSections(IndexSet(integer: 3), with: .none)
		let header = tableView.headerView(forSection: 3) as! FilterHeader
		header.searchBar.text = value
	}
}

enum FSections: Int {
	case firstCell
	case secondCell
	case thirdCell
	case fourthCell
	case fifthCell
	case sixthCell
	case seventhCell
	static var numberOfSections: Int {
		return 7
	}
}
