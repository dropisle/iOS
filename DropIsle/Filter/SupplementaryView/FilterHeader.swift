//
//  TypeHeader.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-12.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

protocol SearchBarDelegate {
	func searchCountry(value: String)
	func cancelSearch(value: String)
}

class FilterHeader: UITableViewHeaderFooterView, Reusable {

	let title = UILabel()
	let searchBarTitle = UILabel()
	let searchBar = UITextField()
	let searchIcon = UIImageView()
	let closeSearch = UIButton()

	var delegate: SearchBarDelegate?

	override init(reuseIdentifier: String?) {
		super.init(reuseIdentifier: reuseIdentifier)
		searchBar.delegate = self
		closeSearch.addTarget(self, action: #selector(cancelSearch), for: .touchUpInside)
		setup() 
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func setup() {
		title.translatesAutoresizingMaskIntoConstraints = false
		searchBar.translatesAutoresizingMaskIntoConstraints = false
		searchIcon.translatesAutoresizingMaskIntoConstraints = false
		closeSearch.translatesAutoresizingMaskIntoConstraints = false
		searchBarTitle.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(title)
		contentView.addSubview(searchBar)
		contentView.addSubview(searchBarTitle)
		searchBar.addSubview(searchIcon)
		searchBar.addSubview(closeSearch)

		NSLayoutConstraint.activate([
			title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			title.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			title.topAnchor.constraint(equalTo: topAnchor),
			title.bottomAnchor.constraint(equalTo: bottomAnchor),


			searchBarTitle.leadingAnchor.constraint(equalTo: searchBar.leadingAnchor),
			searchBarTitle.bottomAnchor.constraint(equalTo: searchBar.topAnchor, constant: -15),


			searchBar.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			searchBar.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			searchBar.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
			searchBar.heightAnchor.constraint(equalToConstant: 40),

			searchIcon.heightAnchor.constraint(equalToConstant: 20),
			searchIcon.widthAnchor.constraint(equalToConstant: 20),
			searchIcon.leadingAnchor.constraint(equalTo: searchBar.leadingAnchor, constant: 20),
			searchIcon.centerYAnchor.constraint(equalTo: searchBar.centerYAnchor),

			closeSearch.heightAnchor.constraint(equalToConstant: 20),
			closeSearch.widthAnchor.constraint(equalToConstant: 20),
			closeSearch.trailingAnchor.constraint(equalTo: searchBar.trailingAnchor, constant: -20),
			closeSearch.centerYAnchor.constraint(equalTo: searchBar.centerYAnchor)
		])


		searchBarTitle.tintColor = .label
		searchBarTitle.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 20)

		searchBar.backgroundColor = UIColor(hexString: "#f1f1f1")
		searchBar.cornerRadius = 20
		searchBar.textColor = .black
		searchBar.setLeftPaddingPoints(50)
		searchBar.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		searchBar.placeholder = "Search Country"

		searchIcon.clipsToBounds = true
		searchIcon.contentMode = .scaleAspectFill
		searchIcon.layer.cornerRadius = 10
		searchIcon.image = UIImage(systemName: "magnifyingglass", withConfiguration: UIImage.SymbolConfiguration(pointSize: 20, weight: .regular, scale: .medium))?.withTintColor(.black, renderingMode: .alwaysOriginal)

		closeSearch.layer.cornerRadius = 10
		let image = UIImage(systemName: "multiply", withConfiguration: UIImage.SymbolConfiguration(pointSize: 15, weight: .regular, scale: .medium))?.withTintColor(.black, renderingMode: .alwaysOriginal)
		closeSearch.setImage(image, for: .normal)

		title.tintColor = .label
		title.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 20)
		contentView.backgroundColor = .secondarySystemGroupedBackground
	}
}

extension FilterHeader: UITextFieldDelegate {

	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		guard let text = textField.text else { return false }
		delegate?.searchCountry(value: text)
		return true
	}

	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		guard let _ = textField.text else { return false }
		return true
	}

	@objc func cancelSearch() {
		guard let text = searchBar.text else { return }
		delegate?.cancelSearch(value: text)
	}
}
