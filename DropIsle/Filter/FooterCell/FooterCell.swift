//
//  FooterCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-18.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class FooterCell: UITableViewCell, Reusable {

	let clearAll = UIButton()
	let action = UIButton()
	let topView = UIView()

	override func awakeFromNib() {
		super.awakeFromNib()
		//Initialization code
	}

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		clearAll.translatesAutoresizingMaskIntoConstraints = false
		action.translatesAutoresizingMaskIntoConstraints = false
		topView.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(clearAll)
		contentView.addSubview(action)
		contentView.addSubview(topView)

		NSLayoutConstraint.activate([
			topView.topAnchor.constraint(equalTo: topAnchor),
			topView.heightAnchor.constraint(equalToConstant: 0.3),
			topView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			topView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			
			clearAll.centerYAnchor.constraint(equalTo: centerYAnchor),
			clearAll.heightAnchor.constraint(equalToConstant: 50),
			clearAll.widthAnchor.constraint(equalToConstant: 150),
			clearAll.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),

			action.centerYAnchor.constraint(equalTo: centerYAnchor),
			action.heightAnchor.constraint(equalToConstant: 50),
			action.widthAnchor.constraint(equalToConstant: 150),
			action.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
		])

		topView.backgroundColor = .lightGray

		clearAll.setTitle("Clear all", for: .normal)
		clearAll.setTitleColor(.label, for: .normal)
		clearAll.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18)
		clearAll.backgroundColor = .secondarySystemGroupedBackground

		action.setTitle("Apply", for: .normal)
		action.setTitleColor(.secondarySystemGroupedBackground, for: .normal)
		action.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18)
		action.backgroundColor = UIColor.NavyBlueDark
		action.cornerRadius = 4
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
