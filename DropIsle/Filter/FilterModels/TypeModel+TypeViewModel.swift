//
//  Model+ViewModel.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-12.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import Foundation
import UIKit

let dataArray = [TypeModel(title: "Mordern Art"), TypeModel(title: "Grafiti"), TypeModel(title: "Abstract Art"), TypeModel(title: "Pop Art"), TypeModel(title: "Paintings"), TypeModel(title: "Sculpture"), TypeModel(title: "Photography")]

struct TypeModel {
	var title: String
}

class TypeViewModelItem {
	private var item: TypeModel

	var title: String {
		return item.title
	}

	init(item: TypeModel) {
		self.item = item
	}
}

class TypeViewModel: NSObject {

	var reloadList = {() -> () in }
	var items: [TypeViewModelItem] = [] {
		didSet{
			reloadList()
		}
	}

	override init() {
		super.init()
		items = dataArray.map { TypeViewModelItem(item: $0) }
	}
}
