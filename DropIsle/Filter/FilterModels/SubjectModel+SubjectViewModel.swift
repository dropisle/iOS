//
//  SubjectModel+SubjectViewModel.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-13.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import Foundation
import UIKit

let subjectDataArray = [SubjectModel(title: "Abstract & geometric"), SubjectModel(title: "Animal"), SubjectModel(title: "Anime & cartoon"), SubjectModel(title: "Architecture & cityscape"), SubjectModel(title: "Beach & tropical"), SubjectModel(title: "Comics & manga"), SubjectModel(title: "Fantasy & Sci Fi"), SubjectModel(title: "Fashion"), SubjectModel(title: "Flowers"), SubjectModel(title: "Food & drink"), SubjectModel(title: "Geography & locale"), SubjectModel(title: "Horror & gothic"), SubjectModel(title: "Humorous saying"), SubjectModel(title: "Inspirational saying"), SubjectModel(title: "Landscape & scenery"), SubjectModel(title: "LGBTQ pride"), SubjectModel(title: "Love & friendship"), SubjectModel(title: "Military"), SubjectModel(title: "Movie"), SubjectModel(title: "Music"), SubjectModel(title: "Nautical"), SubjectModel(title: "Nudes"), SubjectModel(title: "Patriotic & flags"), SubjectModel(title: "People & portrait"), SubjectModel(title: "Pet portrait"), SubjectModel(title: "Phrase & saying"), SubjectModel(title: "Plants & trees"), SubjectModel(title: "Religious"), SubjectModel(title: "Science & tech"), SubjectModel(title: "Sports & fitness"), SubjectModel(title: "Stars & celestial"), SubjectModel(title: "Steampunk"), SubjectModel(title: "Superhero"), SubjectModel(title: "Travel & transportation"), SubjectModel(title: "TV"), SubjectModel(title: "Video game"), SubjectModel(title: "Western & cowboy"), SubjectModel(title: "Zodiac")]

struct SubjectModel {
	var title: String
}

class SubjectViewModelItem {
	private var item: SubjectModel

	var title: String {
		return item.title
	}

	init(item: SubjectModel) {
		self.item = item
	}
}

class SubjectViewModel: NSObject {

	var reloadList = {() -> () in }
	var items: [SubjectViewModelItem] = [] {
		didSet{
			reloadList()
		}
	}

	override init() {
		super.init()
		items = subjectDataArray.map { SubjectViewModelItem(item: $0) }
	}
}
