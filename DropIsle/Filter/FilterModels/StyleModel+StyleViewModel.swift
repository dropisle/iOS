//
//  StyleModel+StyleViewModel.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-12.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import Foundation
import UIKit

let styleDataArray = [StyleModel(title: "Art deco"), StyleModel(title: "Art nouveau"), StyleModel(title: "Bohemian & eclectic"), StyleModel(title: "Coastal & tropical"), StyleModel(title: "Contemporary"), StyleModel(title: "Country & farmhouse"), StyleModel(title: "Gothic"), StyleModel(title: "Industrial & utility"), StyleModel(title: "Lodge"), StyleModel(title: "Mid-century"), StyleModel(title: "Minimalistic"), StyleModel(title: "Rustic"), StyleModel(title: "Southwestern"), StyleModel(title: "victorian"), StyleModel(title: "Figurative"), StyleModel(title: "Expressionism"), StyleModel(title: "Realism"), StyleModel(title: "Conceptual"), StyleModel(title: "Impressionism"), StyleModel(title: "Surrealism"), StyleModel(title: "Portairure"), StyleModel(title: "Street Art"), StyleModel(title: "Documentary"), StyleModel(title: "Folk"), StyleModel(title: "Cubism")]

struct StyleModel {
	var title: String
}

class StyleViewModelItem {
	private var item: StyleModel

	var title: String {
		return item.title
	}

	init(item: StyleModel) {
		self.item = item
	}
}

class StyleViewModel: NSObject {

	var reloadList = {() -> () in }
	var items: [StyleViewModelItem] = [] {
		didSet{
			reloadList()
		}
	}

	override init() {
		super.init()
		items = styleDataArray.map { StyleViewModelItem(item: $0) }
	}
}
