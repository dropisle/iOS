//
//  ArtistModel+ArtistViewModel.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-15.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import Foundation
import UIKit

let artistDataArray = [ArtistModel(title: "Upcoming"), ArtistModel(title: "Renowned")]

struct ArtistModel {
	var title: String
}

class ArtistViewModelItem {
	private var item: ArtistModel

	var title: String {
		return item.title
	}

	init(item: ArtistModel) {
		self.item = item
	}
}

class ArtistViewModel: NSObject {

	var reloadList = {() -> () in }
	var items: [ArtistViewModelItem] = [] {
		didSet{
			reloadList()
		}
	}

	override init() {
		super.init()
		items = artistDataArray.map { ArtistViewModelItem(item: $0) }
	}
}
