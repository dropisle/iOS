//
//  CountryModel+CountryViewModel.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-13.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import Foundation
import UIKit

struct CountryModel {
	var title: String
}

class CountryViewModelItem {
	private var item: String

	var title: String {
		return item
	}

	init(item: String) {
		self.item = item
	}
}

class CountryViewModel: NSObject {

	var reloadList = {() -> () in }
	var items: [CountryViewModelItem] = [] {
		didSet{
			reloadList()
		}
	}

	override init() {
		super.init()
		let sortedCountries = GetCountries.countries.sorted { $0 < $1 }
		items = sortedCountries.map { CountryViewModelItem(item: $0) }
	}
}
