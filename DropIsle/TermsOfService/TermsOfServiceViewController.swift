//
//  TermsOfServiceViewController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-03-29.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class TermsOfServiceViewController: UIViewController {

    let urlString = "https://www.dropisle.com/Conditions"
		var isFromSettingsPage = Bool()
    @IBOutlet weak var webView: WKWebView!

    override func viewDidLoad() {
			addBarButtons()
			super.viewDidLoad()
				self.title = "Conditions Of Use"
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		showLoadIndicator()
		DispatchQueue.main.async { [weak self] in
			guard let self = self else { return }
			guard let url = URL(string: self.urlString) else { return }
			let request = URLRequest(url: url)
			self.webView.load(request)
			SVProgressHUD.dismiss(withDelay: 1.0)
		}
	}

	func getConditions() {
		let urlString = String(format: APIEndPoints.termsOfService)
		let termsUrl = BaseAPIClient.urlFromPath(path: urlString)
		APIManager.shared.fetchGenericData(method: .get, urlString: termsUrl, params: nil, headers: nil) { ( result: TermsOfService ) in
			self.webView.loadHTMLString(result.conditions, baseURL: nil)
		}
	}

	func showLoadIndicator() {
		SVProgressHUD.show(withStatus: "Loading...")
	}

	func addBarButtons() {
		let back = UIImage(named: "ArrowLeft")!
		let backButton = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector((dismissScreen)))
		backButton.tintColor = UIColor.white
		navigationItem.leftBarButtonItem = backButton
	}

	@objc func dismissScreen(sender: AnyObject) {
		if isFromSettingsPage {
			self.navigationController?.popViewController(animated: true)
		} else {
			self.dismiss(animated: true, completion: nil)
		}
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
