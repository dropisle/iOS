//
//  RecommendedPresenter.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-04.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class RecommendedPresenter {

	var dataSource: [RecommendedSection] = []

	// We will initialise the dataSource in the init method.
	init() {
		// Second section: This weeks favorites
		let recommended = [App(id: 4, type: "Photo", name: "Map Painting", subTitle: "$120 / Oil on Canvas", image: UIImage(named: "Recommend2")!, hasIAP: true),
						 App(id: 5, type: "Shopping", name: "Map Painting", subTitle: "$120 / Sustainable Art", image: UIImage(named: "Recommend2")!, hasIAP: false),
						 App(id: 6, type: "Notes", name: "Map Painting", subTitle: "$120 / Oil on Canvas", image: UIImage(named: "Recommend1")!, hasIAP: true),
						 App(id: 7, type: "Photo", name: "Map Painting", subTitle: "$120 / Oil on Canvas", image: UIImage(named: "Recommend2")!, hasIAP: true),
						 App(id: 8, type: "Shopping", name: "Map Painting", subTitle: "$120 / Oil on Canvas", image: UIImage(named: "Recommend1")!, hasIAP: false),
						 App(id: 9, type: "Meditation", name: "Map Painting", subTitle: "$120 / Oil on Canvas", image: UIImage(named: "Recommend2")!, hasIAP: true),
						 App(id: 10, type: "Learning", name: "Map Painting", subTitle: "$120 / Oil on Canvas", image: UIImage(named: "Recommend1")!, hasIAP: false)]
		let section2 = RecommendedSection(id: 2, type: .singleSectionList, title: "Explore Ar you favourited....", subtitle: "Brief description....", data: recommended, search: false)

		dataSource.append(section2)
	}

	// MARK: Collection View

	/// Returns the number of sections in the dataSource
	var numberOfSections: Int {
		return dataSource.count
	}

	/// Returns the number of items for the given section
	func numberOfItems(for sectionIndex: Int) -> Int {
		let section = dataSource[sectionIndex]
		return section.data.count
	}

	// MARK: Cells

	/// Configures the given item with the app appropriate for the given IndexPath
	func configure(item: AppConfigurable, for indexPath: IndexPath) {
		let section = dataSource[indexPath.section]
		if let app = section.data[indexPath.row] as? App {
			item.configure(with: app)
		} else {
			print("Error getting app for indexPath: \(indexPath)")
		}
	}

	/// Configures the given item with the category appropriate for the given IndexPath
	func configure(item: CategoryConfigurable, for indexPath: IndexPath) {
		let section = dataSource[indexPath.section]
		if let category = section.data[indexPath.row] as? Category {
			item.configure(with: category)
		} else {
			print("Error getting category for indexPath: \(indexPath)")
		}
	}

	/// Returns the SectionType for the given sectionIndex
	func sectionType(for sectionIndex: Int) -> SingleSectionType {
		let section = dataSource[sectionIndex]
		return section.type
	}

	// MARK: Supplementary Views

	/// Returns the title for the given sectionIndex
	func title(for sectionIndex: Int) -> String? {
		let section = dataSource[sectionIndex]
		return section.title
	}

	/// Returns the subtitle for the given sectionIndex
	func subtitle(for sectionIndex: Int) -> String? {
		let section = dataSource[sectionIndex]
		return section.subtitle
	}

	func search(for sectionIndex: Int) -> Bool? {
		let section = dataSource[sectionIndex]
		return section.search
	}
}
