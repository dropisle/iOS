//
//  AppPresenter.swift
//  AppStore
//
//  Created by Bob De Kort on 07/01/2020.
//  Copyright © 2020 Nodes. All rights reserved.
//

import UIKit

/* NOTE
 This is the minimal data layer of this ViewController.
 In a normal project depending on your architecture this would be in the Presenter,
 ViewModel, the ViewController itself or anywhere else to your liking.
 
 This will use similar methods and variables that we would use in a Presenter in the VIPER
 architecture
 */

class AppsPresenter {
    
	var dataSource: [Section] = []
    
    // We will initialise the dataSource in the init method.
    init() {
        // First section: Featured
        let featuredApps = [App(id: 1, type: "New This Week", name: "", subTitle: "Discover art to decorate your home, \nroom etc", image: UIImage(named: "desert")!, hasIAP: true)]
		let section1 = Section(id: 1, type: .singleList, title: "Graffiti Art", subtitle: "Let yourself into an art", data: featuredApps, search: true)
        
        dataSource.append(section1)
        
        // Second section: This weeks favorites
        let favorites = [App(id: 4, type: "Photo", name: "FilmStuff", subTitle: "Photo effects and filters", image: UIImage(named: "food")!, hasIAP: true),
                         App(id: 5, type: "Shopping", name: "Get Shopping", subTitle: "Simple grocery list", image: UIImage(named: "temple")!, hasIAP: false),
                         App(id: 6, type: "Notes", name: "White Board", subTitle: "Notes, Sketching, ...", image: UIImage(named: "desert")!, hasIAP: true),]
//                         App(id: 7, type: "Photo", name: "Video maker", subTitle: "Make simple videos", image: UIImage(named: "containers")!, hasIAP: true),
//                         App(id: 8, type: "Shopping", name: "NaNzone", subTitle: "Buy everything here", image: UIImage(named: "temple")!, hasIAP: false),
//                         App(id: 9, type: "Meditation", name: "MUtopia", subTitle: "Relax, breathe", image: UIImage(named: "oldTemple")!, hasIAP: true),
//                         App(id: 10, type: "Learning", name: "Memo", subTitle: "Remind your self of your routine", image: UIImage(named: "food")!, hasIAP: false)]
		let section2 = Section(id: 2, type: .doubleList, title: "Recommended For You", subtitle: "Explore All", data: favorites, search: false)
        
        dataSource.append(section2)
        
        // Third section: Learn something
        let learning = [App(id: 11, type: "Learning", name: "Sustainable Art", subTitle: "Find sustainable art sourced naturally that \nmakes the mother earth proud.", image: UIImage(named: "containers")!, hasIAP: true)]
		let section3 = Section(id: 3, type: .tripleList, title: "Learn something", subtitle: "Self development is key", data: learning, search: false)
        
        dataSource.append(section3)
        
        // Fourth section: Categories
        let categories = [App(id: 5, type: "", name: "", subTitle: "", image: UIImage(named: "nature3")!, hasIAP: false)]
		let section4 = Section(id: 4, type: .categoryList, title: "Featured", subtitle: "Dope Setup conins", data: categories, search: false)
        
        dataSource.append(section4)

		// Second section: This weeks favorites
		let artist = [App(id: 1, type: "Learning", name: "Artist", subTitle: "Meet Alex, 16 years artist specializing", image: UIImage(named: "containers")!, hasIAP: true),
					  App(id: 2, type: "Notes", name: "White Board", subTitle: "Notes, Sketching, ...", image: UIImage(named: "desert")!, hasIAP: true),
					  App(id: 3, type: "Photo", name: "Video maker", subTitle: "Make simple videos", image: UIImage(named: "containers")!, hasIAP: true),
					  App(id: 4, type: "Shopping", name: "NaNzone", subTitle: "Buy everything here", image: UIImage(named: "temple")!, hasIAP: false),
					  App(id: 5, type: "Meditation", name: "MUtopia", subTitle: "Relax, breathe", image: UIImage(named: "oldTemple")!, hasIAP: true),
					  App(id: 6, type: "Learning", name: "Memo", subTitle: "Remind your self of your routine", image: UIImage(named: "food")!, hasIAP: false)]
		let section5 = Section(id: 5, type: .artist, title: "Explore All", subtitle: "All start base", data: artist, search: false)

		dataSource.append(section5)

		let auction = [App(id: 1, type: "Learning", name: "Artist", subTitle: "Meet Alex, 16 years artist specializing", image: UIImage(named: "containers")!, hasIAP: true),
					  App(id: 2, type: "Notes", name: "White Board", subTitle: "Notes, Sketching, ...", image: UIImage(named: "desert")!, hasIAP: true),
					  App(id: 3, type: "Photo", name: "Video maker", subTitle: "Make simple videos", image: UIImage(named: "containers")!, hasIAP: true),
					  App(id: 4, type: "Shopping", name: "NaNzone", subTitle: "Buy everything here", image: UIImage(named: "temple")!, hasIAP: false),
					  App(id: 5, type: "Meditation", name: "MUtopia", subTitle: "Relax, breathe", image: UIImage(named: "oldTemple")!, hasIAP: true),
					  App(id: 6, type: "Learning", name: "Memo", subTitle: "Remind your self of your routine", image: UIImage(named: "food")!, hasIAP: false)]
		let section6 = Section(id: 6, type: .auction, title: "Upcoming Auctions", subtitle: "Bid for exclusive art ......", data: auction, search: false)

		dataSource.append(section6)
    }
    
    // MARK: Collection View
    
    //Returns the number of sections in the dataSource
    var numberOfSections: Int {
        return dataSource.count
    }
    
    //Returns the number of items for the given section
    func numberOfItems(for sectionIndex: Int) -> Int {
        let section = dataSource[sectionIndex]
        return section.data.count 
    }
    
    // MARK: Cells
    
    //Configures the given item with the app appropriate for the given IndexPath
    func configure(item: AppConfigurable, for indexPath: IndexPath) {
        let section = dataSource[indexPath.section]
        if let app = section.data[indexPath.row] as? App {
            item.configure(with: app)
        } else {
            print("Error getting app for indexPath: \(indexPath)")
        }
    }
    
    //Configures the given item with the category appropriate for the given IndexPath
    func configure(item: CategoryConfigurable, for indexPath: IndexPath) {
        let section = dataSource[indexPath.section]
        if let category = section.data[indexPath.row] as? Category {
            item.configure(with: category)
        } else {
            print("Error getting category for indexPath: \(indexPath)")
        }
    }
    
    //Returns the SectionType for the given sectionIndex
    func sectionType(for sectionIndex: Int) -> SectionType {
        let section = dataSource[sectionIndex]
        return section.type
    }

    // MARK: Supplementary Views
    
    //Returns the title for the given sectionIndex
    func title(for sectionIndex: Int) -> String? {
        let section = dataSource[sectionIndex]
        return section.title
    }
    
    //Returns the subtitle for the given sectionIndex
    func subtitle(for sectionIndex: Int) -> String? {
        let section = dataSource[sectionIndex]
        return section.subtitle
    }

	func search(for sectionIndex: Int) -> Bool? {
		let section = dataSource[sectionIndex]
		return section.search
	}
}
