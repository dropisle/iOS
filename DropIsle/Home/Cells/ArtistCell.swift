//
//  ArtistCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-11-28.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class ArtistCell: UICollectionViewCell, AppConfigurable {

	let imageView = UIImageView()
	let backView = UIView()
	let nameLabel = UILabel()
	let subtitleLabel = UILabel()

	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}

	private func setup() {
		backView.translatesAutoresizingMaskIntoConstraints = false
		imageView.translatesAutoresizingMaskIntoConstraints = false
		nameLabel.translatesAutoresizingMaskIntoConstraints = false
		subtitleLabel.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(backView)
		backView.addSubview(imageView)
		backView.addSubview(nameLabel)
		backView.addSubview(subtitleLabel)

		NSLayoutConstraint.activate([
			nameLabel.topAnchor.constraint(equalTo: backView.topAnchor, constant: 20),
			nameLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: 10),

			subtitleLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4),
			subtitleLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: 10),

			imageView.leadingAnchor.constraint(equalTo: backView.leadingAnchor),
			imageView.trailingAnchor.constraint(equalTo: backView.trailingAnchor),
			imageView.topAnchor.constraint(equalTo: backView.topAnchor, constant: 90),
			imageView.bottomAnchor.constraint(equalTo: backView.bottomAnchor, constant: -20),

			backView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
			backView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
			backView.topAnchor.constraint(equalTo: contentView.topAnchor),
			backView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
		])
		style()
	}

	private func style() {
		nameLabel.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 20)
		nameLabel.textColor = .white
		nameLabel.numberOfLines = 0

		subtitleLabel.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		subtitleLabel.textColor = .white
		subtitleLabel.numberOfLines = 0

		imageView.layer.cornerRadius = 10
		imageView.clipsToBounds = true
		imageView.contentMode = .scaleAspectFill

		backView.clipsToBounds = true
		backView.contentMode = .scaleAspectFill
	}

	func configure(with app: App) {
		nameLabel.text = app.name
		subtitleLabel.text = app.subTitle
		imageView.image = app.image
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
