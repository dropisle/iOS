//
//  SustainableCell.swift
//  AppStore
//
//  Created by Bob De Kort on 07/01/2020.
//  Copyright © 2020 Nodes. All rights reserved.
//

import UIKit

class SustainableCell: UICollectionViewCell, AppConfigurable {
    
    let imageView = UIImageView()
	let backImage = UIImageView()
    let nameLabel = UILabel()
    let subtitleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    private func setup() {
		backImage.translatesAutoresizingMaskIntoConstraints = false
		imageView.translatesAutoresizingMaskIntoConstraints = false
		nameLabel.translatesAutoresizingMaskIntoConstraints = false
		subtitleLabel.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(backImage)
		backImage.addSubview(imageView)
		backImage.addSubview(nameLabel)
		backImage.addSubview(subtitleLabel)
        
        NSLayoutConstraint.activate([
			nameLabel.topAnchor.constraint(equalTo: backImage.topAnchor, constant: 12),
			nameLabel.leadingAnchor.constraint(equalTo: backImage.leadingAnchor, constant: 10),

			subtitleLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4),
			subtitleLabel.leadingAnchor.constraint(equalTo: backImage.leadingAnchor, constant: 10),

			imageView.leadingAnchor.constraint(equalTo: backImage.leadingAnchor, constant: 10),
			imageView.trailingAnchor.constraint(equalTo: backImage.trailingAnchor, constant: -10),
			imageView.topAnchor.constraint(equalTo: backImage.topAnchor, constant: 100),
			imageView.bottomAnchor.constraint(equalTo: backImage.bottomAnchor, constant: -20),

			backImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
			backImage.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
			backImage.topAnchor.constraint(equalTo: contentView.topAnchor),
			backImage.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
        style()
    }
    
    private func style() {
        nameLabel.font = UIFont(name: "SavoyeLetPlain", size: 35)
        nameLabel.textColor = .darkGray
        nameLabel.numberOfLines = 0
        
        subtitleLabel.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)
        subtitleLabel.textColor = .black
        subtitleLabel.numberOfLines = 0
        
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill

		backImage.clipsToBounds = true
		backImage.contentMode = .scaleAspectFill
		backImage.image = UIImage(named: "nature1")
    }
    
    func configure(with app: App) {
        nameLabel.text = app.name
        subtitleLabel.text = app.subTitle
        imageView.image = app.image
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
