 //
//  RecommendedCell.swift
//  AppStore
//
//  Created by Bob De Kort on 06/01/2020.
//  Copyright © 2020 Nodes. All rights reserved.
//

import UIKit

class RecommendedCell: UICollectionViewCell, AppConfigurable {
    
    let imageView = UIImageView()
	let favourited = HeartButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    private func setup() {
        let outerStackView = UIStackView(arrangedSubviews: [imageView])
        outerStackView.alignment = .center
        outerStackView.spacing = 10
        outerStackView.distribution = .fillEqually
        
        outerStackView.translatesAutoresizingMaskIntoConstraints = false
		favourited.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(outerStackView)
		imageView.addSubview(favourited)
        
        NSLayoutConstraint.activate([
            outerStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            outerStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            outerStackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            outerStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

			favourited.trailingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: -10),
			favourited.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 10),
			favourited.heightAnchor.constraint(equalToConstant: 20),
			favourited.widthAnchor.constraint(equalToConstant: 23)
        ])
        style()
    }
    
    private func style() {
        imageView.layer.cornerRadius = 15
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill

		favourited.filled = true
		favourited.strokeColor = .white
		favourited.fillColor = .systemRed
    }
    
    func configure(with app: App) {
        imageView.image = app.image
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
