//
//  ThisWeekCell.swift
//  AppStore
//
//  Created by Bob De Kort on 06/01/2020.
//  Copyright © 2020 Nodes. All rights reserved.
//

import UIKit

class ThisWeekCell: UICollectionViewCell, AppConfigurable {

    // UI components
    let typeLabel = UILabel()
    let nameLabel = UILabel()
    let subtitleLabel = UILabel()
    let imageView = UIImageView()
	let explore = UIButton()
	let exploreImage = UIImageView()
    
    // Initialiser
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    //Sets up the UI components for this cell and adds them to the contentview
    private func setup() {
		let exploreSeparator = UIView(frame: .zero)
		exploreSeparator.translatesAutoresizingMaskIntoConstraints = false
		exploreSeparator.backgroundColor = .separator

		typeLabel.translatesAutoresizingMaskIntoConstraints = false
		imageView.translatesAutoresizingMaskIntoConstraints = false
		explore.translatesAutoresizingMaskIntoConstraints = false
		explore.translatesAutoresizingMaskIntoConstraints = false
		subtitleLabel.translatesAutoresizingMaskIntoConstraints = false

        // Constructing our main stack view with all the UI components we defined above
        let stackView = UIStackView(arrangedSubviews: [imageView])
        stackView.axis = .vertical
        
        // We will define our own constraints
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        // Add the stackview to the content view
		imageView.addSubview(typeLabel)
		imageView.addSubview(explore)
		imageView.addSubview(subtitleLabel)
		explore.addSubview(exploreSeparator)
        contentView.addSubview(stackView)
        
        // Constraint the stackview to all 4 edges of the content view
        // Constraint the separator height to 0.5
        NSLayoutConstraint.activate([
			typeLabel.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 10),
			typeLabel.centerXAnchor.constraint(equalTo: imageView.centerXAnchor),

			subtitleLabel.topAnchor.constraint(equalTo: typeLabel.topAnchor, constant: 30),
			subtitleLabel.centerXAnchor.constraint(equalTo: imageView.centerXAnchor),
			subtitleLabel.trailingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 10),
			subtitleLabel.leadingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: -10),

			exploreSeparator.heightAnchor.constraint(equalToConstant: 60),
			exploreSeparator.widthAnchor.constraint(equalToConstant: 0.5),
			exploreSeparator.trailingAnchor.constraint(equalTo: explore.trailingAnchor, constant: -35),

			explore.centerYAnchor.constraint(equalTo: imageView.centerYAnchor),
			explore.heightAnchor.constraint(equalToConstant: 60),
			explore.widthAnchor.constraint(equalToConstant: 120),
			explore.trailingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: -20),

            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        stackView.setCustomSpacing(10, after: subtitleLabel)
        
        style()
    }
    
    /// Styles all the UI components in the cell
    private func style() {
        typeLabel.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 20)
        typeLabel.textColor = .white
        
        nameLabel.font = UIFont(name: "AppleSDGothicNeo-Thin", size: 10)
        nameLabel.textColor = .label
        
        subtitleLabel.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 15)
        subtitleLabel.textColor = .white
		subtitleLabel.textAlignment = .center
		subtitleLabel.lineBreakMode = .byWordWrapping
		subtitleLabel.numberOfLines = 0
        
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill

		exploreImage.clipsToBounds = true
		exploreImage.contentMode = .scaleAspectFill

		let image = UIImage(systemName: "arrow.right", withConfiguration: UIImage.SymbolConfiguration(pointSize: 15, weight: .semibold, scale: .medium))?.withTintColor(.black, renderingMode: .alwaysOriginal)
		explore.setTitle("Explore All", for: .normal)
		explore.setTitleColor(.black, for: .normal)
		explore.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 14)
		explore.backgroundColor = .white
		explore.layer.cornerRadius = 10
		explore.setImage(image, for: .normal)
		explore.alignImageRight()
    }
    
    /// Configures the cell with a given app
    func configure(with app: App) {
        typeLabel.text = app.type
        nameLabel.text = app.name
        subtitleLabel.text = app.subTitle
        imageView.image = app.image
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UIButton {
   //Makes the ``imageView`` appear just to the right of the ``titleLabel``.
   func alignImageRight() {
	if let titleLabel = self.titleLabel, let imageView = self.imageView {
		   // Force the label and image to resize.
		   titleLabel.sizeToFit()
		   imageView.sizeToFit()
		   imageView.contentMode = .scaleAspectFit

		   // Set the insets so that the title appears to the left and the image appears to the right.
		   // Make the image appear slightly off the top/bottom edges of the button.
		   self.titleEdgeInsets = UIEdgeInsets(top: 0, left: -2 * imageView.frame.size.width, bottom: 0, right: imageView.frame.size.width)
		   self.imageEdgeInsets = UIEdgeInsets(top: 4, left: titleLabel.frame.size.width, bottom: 4, right: -1 * titleLabel.frame.size.width + -1 * imageView.frame.size.width)
		 }
	   }
	}


