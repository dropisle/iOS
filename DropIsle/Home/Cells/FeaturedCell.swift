//
//  FeaturedCell.swift
//  AppStore
//
//  Created by Bob De Kort on 07/01/2020.
//  Copyright © 2020 Nodes. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class FeaturedCell: UICollectionViewCell, AppConfigurable {
    
    let nameLabel = UILabel()
    let playerView = PlayerViewClass()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
		playerView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(playerView)
        
        NSLayoutConstraint.activate([
			playerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
			playerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
			playerView.topAnchor.constraint(equalTo: contentView.topAnchor),
			playerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
        style()
    }
    
    private func style() {
        nameLabel.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 20)
        nameLabel.textColor = .label
		playerView.clipsToBounds = true
		playerView.backgroundColor = .secondarySystemGroupedBackground
    }
    
    func configure(with app: App) {
        nameLabel.text = app.name
    }
}

class PlayerViewClass: UIView {
	override static var layerClass: AnyClass {
		return AVPlayerLayer.self
	}

	var playerLayer: AVPlayerLayer {
		return layer as! AVPlayerLayer
	}

	var player: AVPlayer? {
		get {
			return playerLayer.player
		}
		set {
			playerLayer.player = newValue
		}
	}
}
