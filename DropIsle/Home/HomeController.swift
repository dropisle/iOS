//
//  HomeController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-11-25.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import FirebaseDynamicLinks

class HomeController: UIViewController {

	// MARK: - Properties -

	var searchButton = UIButton()
	var searchIcon = UIImageView()
	private var presenter = AppsPresenter()         // Handles all the data
	private var collectionView: UICollectionView!

	// MARK: - Life cycle -
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.navigationItem.title = ""
		navigationController?.navigationBar.prefersLargeTitles = false
		navigationItem.largeTitleDisplayMode = .never

		setupCollectionView()
		collectionView.delegate = self
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		setUpSearchBar()
		
		navigationItem.setHidesBackButton(false, animated: animated)
		navigationController?.setNavigationBarHidden(false, animated: animated)
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		searchButton.removeFromSuperview()
	}

	// MARK: - Setup methods -
	//Constructs the UICollectionView and adds it to the view.
	// Registers all the Cells and Views that the UICollectionView will need
	private func setupCollectionView() {
		// Initialises the collection view with a CollectionViewLayout which we will define
		collectionView = UICollectionView.init(frame: .zero,
											   collectionViewLayout: makeLayout())
		// Assigning data source and background color
		collectionView.dataSource = self
		collectionView.backgroundColor = .secondarySystemGroupedBackground
		// Adding the collection view to the view
		view.addSubview(collectionView)

		// This line tells the system we will define our own constraints
		collectionView.translatesAutoresizingMaskIntoConstraints = false

		// Constraining the collection view to the 4 edges of the view
		NSLayoutConstraint.activate([
			collectionView.topAnchor.constraint(equalTo: view.topAnchor),
			collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])

		// Registering all Cells and Classes we will need
		collectionView.register(SectionHeader.self,
								forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
								withReuseIdentifier: SectionHeader.identifier)
		collectionView.register(SectionFooter.self,
								forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
								withReuseIdentifier: SectionFooter.identifier)
		collectionView.register(ThisWeekCell.self,
								forCellWithReuseIdentifier: ThisWeekCell.identifier)
		collectionView.register(RecommendedCell.self,
								forCellWithReuseIdentifier: RecommendedCell.identifier)
		collectionView.register(SustainableCell.self,
								forCellWithReuseIdentifier: SustainableCell.identifier)
		collectionView.register(FeaturedCell.self,
								forCellWithReuseIdentifier: FeaturedCell.identifier)
		collectionView.register(ArtistCell.self,
								forCellWithReuseIdentifier: ArtistCell.identifier)
		collectionView.register(AuctionsCell.self,
								forCellWithReuseIdentifier: AuctionsCell.identifier)
	}


	// MARK: - Collection View Helper Methods -
	// In this section you can find all the layout related code

	/// Creates the appropriate UICollectionViewLayout for each section type
	private func makeLayout() -> UICollectionViewLayout {
		// Constructs the UICollectionViewCompositionalLayout
		let layout = UICollectionViewCompositionalLayout { (sectionIndex: Int, layoutEnv: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
			let isWideView = layoutEnv.container.effectiveContentSize.width > 500
			switch self.presenter.sectionType(for: sectionIndex) {
				case .singleList:   return self.createNewThisWeekSection()
				case .doubleList:   return self.createRecommendedForYouSection(isWide: isWideView)
				case .tripleList:   return self.createSustainableSection(isWide: isWideView)
				case .categoryList: return self.createFeaturedSection()
				case .artist: return self.createArtistSection(isWide: isWideView)
				case .auction: return self.createAuctionSection(isWide: isWideView)
			}
		}

		/// Configure the Layout with interSectionSpacing
		let config = UICollectionViewCompositionalLayoutConfiguration()
		config.interSectionSpacing = 20
		layout.configuration = config
		layout.register(SectionBackgroundView.self, forDecorationViewOfKind: "background")
		return layout
	}

	//Creates the layout for the Featured styled sections
	private func createNewThisWeekSection() -> NSCollectionLayoutSection {
		// Defining the size of a single item in this layout
		let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
											  heightDimension: .fractionalHeight(1))
		// Construct the Layout Item
		let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)

		// Configuring the Layout Item
		layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: 10, bottom: 0, trailing: 10)

		// Defining the size of a group in this layout
		let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .absolute(collectionView.frame.width), //.fractionalWidth(0.95),
													 heightDimension: .estimated(550))

		// Constructing the Layout Group
		let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize,
															 subitems: [layoutItem])

		// Constructing the Layout Section
		let layoutSection = NSCollectionLayoutSection(group: layoutGroup)

		// Configuring the Layout Section
		//layoutSection.orthogonalScrollingBehavior = .paging

		// Constructing the Section Header
		let _ = createSectionHeader()

		// Adding the Section Header to the Section Layout
		//layoutSection.boundarySupplementaryItems = [layoutSectionHeader]

		return layoutSection
	}

	//Creates a layout that shows 2 items per group and scrolls horizontally
	private func createRecommendedForYouSection(isWide: Bool) -> NSCollectionLayoutSection {
		// Defining the size of a single item in this layout
		let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
											  heightDimension: .fractionalHeight(1))
		// Construct the Layout Item
		let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)

		// Configure the Layout Item
		layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: 1, bottom: 0, trailing: 10)

		// Defining the size of a group in this layout
		let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .absolute(isWide ? 300 : 160), heightDimension: .estimated(isWide ? 450 : 250))

		// Constructing the Layout Group
		let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize, subitems: [layoutItem])

		// Constructing the Layout Section
		let layoutSection = NSCollectionLayoutSection(group: layoutGroup)

		// Configuring the Layout Section
		layoutSection.orthogonalScrollingBehavior = .continuous

		// Constructing the Section Header
		let layoutSectionHeader = createSectionHeader()

		// Adding the Section Header to the Section Layout
		layoutSection.boundarySupplementaryItems = [layoutSectionHeader]

		return layoutSection
	}

	//Creates a layout that shows 3 items per group and scrolls horizontally
	private func createSustainableSection(isWide: Bool) -> NSCollectionLayoutSection {
		// Defining the size of a single item in this layout
		let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
											  heightDimension: .fractionalHeight(1))
		// Construct the Layout Item
		let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)

		// Configure the Layout Item
		layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: 0, bottom: 0, trailing: 0)

		// Defining the size of a group in this layout
		let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .absolute(collectionView.frame.width),
													 heightDimension: .estimated(isWide ? 700 : 580))
		// Constructing the Layout Group
		let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize, subitems: [layoutItem])

		// Constructing the Layout Section
		let layoutSection = NSCollectionLayoutSection(group: layoutGroup)

		// Configuring the Layout Section
		layoutSection.orthogonalScrollingBehavior = .groupPagingCentered

		return layoutSection
	}

	//Creates a layout that shows a list of small items based on the given amount.
	private func createFeaturedSection() -> NSCollectionLayoutSection {
		// Defining the size of a single item in this layout
		let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
											  heightDimension: .fractionalHeight(1))
		// Constructing the Layout Item
		let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)

		// Configuring the Layout Item
		// We use a leading of 20 here and not 5 as the other layouts because we do not use the
		// orthogonalScrollingBehavior of groupPagingCentered, which handled part of the insets.
		layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 0, bottom: 20, trailing: 0)

		// Defining the size of a group in this layout
		let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .absolute(collectionView.frame.width),
													 heightDimension: .estimated(500))
		// Constructing the Layout Group
		let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize,
														   subitems: [layoutItem])

		// Constructing the Layout Section
		let layoutSection = NSCollectionLayoutSection(group: layoutGroup)

		// Constructing the Section header
		let layoutSectionHeader = createSectionHeader()

		// Adding the Section Header to the Layout Section
		layoutSection.boundarySupplementaryItems = [layoutSectionHeader]

		return layoutSection
	}

	//Creates a layout that shows 3 items per group and scrolls horizontally
	private func createArtistSection(isWide: Bool) -> NSCollectionLayoutSection {
		// Defining the size of a single item in this layout
		let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
											  heightDimension: .fractionalHeight(1))
		// Construct the Layout Item
		let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)

		// Configure the Layout Item
		layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 0)

		// Defining the size of a group in this layout
		let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .absolute(isWide ? 600 : 320), heightDimension: .estimated(isWide ? 600 : 360))
		
		// Constructing the Layout Group
		let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize, subitems: [layoutItem])

		// Constructing the Layout Section
		let layoutSection = NSCollectionLayoutSection(group: layoutGroup)

		// Configuring the Layout Section
		layoutSection.orthogonalScrollingBehavior = .groupPaging

		let backgroundItem = NSCollectionLayoutDecorationItem.background(elementKind: "background")
		backgroundItem.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0)
		layoutSection.decorationItems = [backgroundItem]

		// Constructing the Section footer
		let layoutSectionFooter = createSectionFooter()

		// Adding the Section Header to the Layout Section
		layoutSection.boundarySupplementaryItems = [layoutSectionFooter]

		return layoutSection
	}

	//Creates a layout that shows 3 items per group and scrolls horizontally
	private func createAuctionSection(isWide: Bool) -> NSCollectionLayoutSection {
		// Defining the size of a single item in this layout
		let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
											  heightDimension: .fractionalHeight(1))
		// Construct the Layout Item
		let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)

		// Configure the Layout Item
		layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 40, leading: 10, bottom: 30, trailing: 0)

		// Defining the size of a group in this layout
		let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .absolute(isWide ? 600 : 340), heightDimension: .estimated(isWide ? 500 : 335))

		// Constructing the Layout Group
		let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize, subitems: [layoutItem])

		// Constructing the Layout Section
		let layoutSection = NSCollectionLayoutSection(group: layoutGroup)

		// Configuring the Layout Section
		layoutSection.orthogonalScrollingBehavior = .groupPaging

		// Constructing the Section footer
		let layoutSectionFooter = createSectionHeader()

		// Adding the Section Header to the Layout Section
		layoutSection.boundarySupplementaryItems = [layoutSectionFooter]

		return layoutSection
	}

	//Creates a Layout for the SectionHeader
	private func createSectionHeader() -> NSCollectionLayoutBoundarySupplementaryItem {
		// Define size of Section Header
		let layoutSectionHeaderSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.95), heightDimension: .estimated(60))
		// Construct Section Header Layout
		let layoutSectionHeader = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: layoutSectionHeaderSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
		return layoutSectionHeader
	}

	//Creates a Layout for the SectionFooter
	private func createSectionFooter() -> NSCollectionLayoutBoundarySupplementaryItem {
		// Define size of Section Footer
		let layoutSectionFooterSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(72))
		// Construct Section Footer Layout
		let layoutSectionFooter = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: layoutSectionFooterSize, elementKind: UICollectionView.elementKindSectionFooter, alignment: .bottom)
		return layoutSectionFooter
	}
}

// MARK: - Delegates Actions -
extension HomeController: ExploreAllDelegate {
	func exploreAll(isHeaderPressed: Bool) {
		if isHeaderPressed {
		self.performSegue(withIdentifier: segues.recommendeForYou.rawValue, sender: self)
		} else {
			self.performSegue(withIdentifier: segues.artistsController.rawValue, sender: self)
		}
	}
}


// MARK: - UICollectionViewDataSource -

extension HomeController: UICollectionViewDataSource {
	//Tells the UICollectionView how many sections are needed
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return presenter.numberOfSections
	}

	//Tells the UICollectionView how many items the requested sections needs
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return presenter.numberOfItems(for: section)
	}

	//Constructs and configures the item needed for the requested IndexPath
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		// Checks what section type we should use for this indexPath so we use the right cells for that section
		switch presenter.sectionType(for: indexPath.section) {
			case .singleList:
				guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ThisWeekCell.identifier, for: indexPath) as? ThisWeekCell else {
					fatalError("Could not dequeue FeatureCell")
				}
				
				presenter.configure(item: cell, for: indexPath)

				return cell

			case .doubleList:
				guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RecommendedCell.identifier, for: indexPath) as? RecommendedCell else {
					fatalError("Could not dequeue MediumAppCell")
				}

				presenter.configure(item: cell, for: indexPath)

				return cell
			case .tripleList:
				guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SustainableCell.identifier, for: indexPath) as? SustainableCell else {
					fatalError("Could not dequeue SmallAppCell")
				}

				presenter.configure(item: cell, for: indexPath)

				return cell
			case .categoryList:
				guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeaturedCell.identifier, for: indexPath) as? FeaturedCell else {
					fatalError("Could not dequeue SmallCategoryCell")
				}

				//Video player
				guard let path = Bundle.main.path(forResource: "sculpture", ofType:"mp4") else {
					return UICollectionViewCell()
				}

				DispatchQueue.main.async {
					let avPlayer = AVPlayer(url: URL(fileURLWithPath: path))
					cell.playerView.playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
					cell.playerView.playerLayer.frame = cell.playerView.frame
					cell.playerView.playerLayer.player = avPlayer
				}

				presenter.configure(item: cell, for: indexPath)
				return cell
			case .artist:
				guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ArtistCell.identifier, for: indexPath) as? ArtistCell else {
					fatalError("Could not dequeue SmallCategoryCell")
				}

				presenter.configure(item: cell, for: indexPath)

				return cell
			case .auction:
				guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AuctionsCell.identifier, for: indexPath) as? AuctionsCell else {
					fatalError("Could not dequeue SmallCategoryCell")
				}

				presenter.configure(item: cell, for: indexPath)

				return cell
		}
	}

	//Constructs and configures the Supplementary Views for the UICollectionView
	//In this project only used for the Section Headers
	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		if kind == UICollectionView.elementKindSectionHeader {
			guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SectionHeader.identifier, for: indexPath) as? SectionHeader else {
				fatalError("Could not dequeue SectionHeader")
			}
			
			headerView.delegate = self
			if indexPath.section == 0 {
				if let _ = presenter.search(for: indexPath.section) {
					headerView.titleLabel.isHidden = true
					headerView.subtitleLabel.isHidden = true
					headerView.explore.isHidden = true
				}
			}

			if indexPath.section == 1 {
				if let title = presenter.title(for: indexPath.section),  let subtitle = presenter.subtitle(for: indexPath.section) {
					headerView.titleLabel.text = title
					headerView.titleLabel.isHidden = false
					headerView.explore.setTitle(subtitle, for: .normal)
					headerView.explore.isHidden = false
					headerView.subtitleLabel.isHidden = true
				}
			}

			if indexPath.section == 3 {
				if let title = presenter.title(for: indexPath.section) {
					headerView.titleLabel.text = title
					headerView.titleLabel.isHidden = false
					headerView.explore.isHidden = true
					headerView.subtitleLabel.isHidden = true
				}
			}

			if indexPath.section == 5 {
				if let subtitle = presenter.subtitle(for: indexPath.section), let title = presenter.title(for: indexPath.section) {
					headerView.titleLabel.text = title
					headerView.subtitleLabel.text = subtitle
					headerView.subtitleLabel.isHidden = false
					headerView.explore.isHidden = true
				}
			}
			return headerView
		}

		if kind == UICollectionView.elementKindSectionFooter {
			guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SectionFooter.identifier, for: indexPath) as? SectionFooter else {
				fatalError("Could not dequeue SectionHeader")
			}
			footerView.delegate = self
			if let title = presenter.title(for: indexPath.section) {
				footerView.explore.setTitle(title, for: .normal)
			}

			return footerView
		}
		return UICollectionReusableView()
	}
}


extension HomeController: UICollectionViewDelegate {

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		switch presenter.sectionType(for: indexPath.section) {
			case .singleList:
				self.performSegue(withIdentifier: segues.newThisWeek.rawValue, sender: self)
			case .doubleList:
				break
			case .tripleList:
				self.performSegue(withIdentifier: segues.sustainableArt.rawValue, sender: self)
			case .categoryList:
				break
			case .artist:
				break
			case .auction:
				break
		}
	}
}

extension HomeController: UIScrollViewDelegate {
	//CGFloat yOrigin = [tableView.visibleCells[1] frame].origin.y - tableView.contentOffset.y
	func hope() {
		let yOrigin1: CGFloat = collectionView.visibleCells[0].frame.origin.y - collectionView.contentOffset.y
		let yOrigin2: CGFloat = collectionView.visibleCells[1].frame.origin.y - collectionView.contentOffset.y
		print(yOrigin1)
		print(yOrigin2)
	}

	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		collectionView.visibleCells.forEach { cell in
			// TODO: write logic to stop the video before it begins scrolling
			guard let indexPath = collectionView.indexPath(for: cell) else { return }
			let cell = collectionView.cellForItem(at: indexPath) as? FeaturedCell
			DispatchQueue.main.async {
				cell?.playerView.player?.play()
				NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: cell?.playerView.player?.currentItem, queue: nil) { (_) in
				cell?.playerView.player?.seek(to: CMTime.zero)
				cell?.playerView.player?.play()
				}
			}
		}
	}

	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		collectionView.visibleCells.forEach { cell in
			// TODO: write logic to start the video after it ends scrolling
			guard let indexPath = collectionView.indexPath(for: cell) else { return }
			let _ = collectionView.cellForItem(at: indexPath) as? FeaturedCell
			DispatchQueue.main.async {
				//cell?.playerView.player?.pause()
			}
		}
	}
}

extension HomeController {
	func setUpSearchBar() {
		let searchIconImage = UIImage(systemName: "magnifyingglass", withConfiguration: UIImage.SymbolConfiguration(pointSize: 15, weight: .semibold, scale: .medium))?.withTintColor(.black, renderingMode: .alwaysOriginal)
		searchIcon.image = searchIconImage
		searchButton.translatesAutoresizingMaskIntoConstraints = false
		searchIcon.translatesAutoresizingMaskIntoConstraints = false

		navigationController?.navigationBar.addSubview(searchButton)
		searchButton.addSubview(searchIcon)

		guard let navBar = navigationController?.navigationBar else  { return }
		NSLayoutConstraint.activate([
			searchButton.trailingAnchor.constraint(equalTo: navBar.trailingAnchor, constant: -20),
			searchButton.leadingAnchor.constraint(equalTo: navBar.leadingAnchor, constant: 20),
			searchButton.heightAnchor.constraint(equalToConstant: 50),
			searchButton.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 2),

			searchIcon.heightAnchor.constraint(equalToConstant: 20),
			searchIcon.widthAnchor.constraint(equalToConstant: 20),
			searchIcon.leadingAnchor.constraint(equalTo: searchButton.leadingAnchor, constant: 20),
			searchIcon.centerYAnchor.constraint(equalTo: searchButton.centerYAnchor)
		])

		searchButton.setTitle("Search by artist, country, style etc.", for: .normal)
		searchButton.setTitleColor(.black, for: .normal)
		searchButton.layer.cornerRadius = 25
		searchButton.layer.borderWidth = 1
		searchButton.layer.borderColor = UIColor.clear.cgColor
		searchButton.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14)
		searchButton.backgroundColor = UIColor(hexString: "#f1f1f1")
		searchButton.layer.shadowOpacity = 0.3
		searchButton.layer.shadowOffset = CGSize.zero
		searchButton.layer.shadowRadius = 10
	}
}

