//
//  BackgroundView.swift
//  AppStore
//
//  Created by CtanLI on 2020-11-26.
//  Copyright © 2020 Nodes. All rights reserved.
//

import UIKit

class SectionBackgroundView: UICollectionReusableView {
		static let reuseIdentifier = "BackView"
		override init(frame: CGRect) {
			super.init(frame: frame)

			layer.cornerRadius = 0
			backgroundColor = UIColor.black
		}

		required init?(coder: NSCoder) {
			fatalError("init(coder:) has not been implemented")
		}

}
