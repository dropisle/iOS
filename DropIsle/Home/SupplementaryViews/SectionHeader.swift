//
//  SectionHeader.swift
//  AppStore
//
//  Created by Bob De Kort on 06/01/2020.
//  Copyright © 2020 Nodes. All rights reserved.
//

import UIKit

class SectionHeader: UICollectionReusableView {
	
	let titleLabel = UILabel()
	let subtitleLabel = UILabel()
	let explore = UIButton()
	var delegate: ExploreAllDelegate?
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		explore.addTarget(self, action: #selector(exploreAllRecommended), for: .touchUpInside)
		setup()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	@objc func exploreAllRecommended() {
		delegate?.exploreAll(isHeaderPressed: true)
	}
	
	private func setup() {
		let separator = UIView(frame: .zero)
		separator.translatesAutoresizingMaskIntoConstraints = false
		separator.backgroundColor = .clear

		let stackView = UIStackView(arrangedSubviews: [separator, titleLabel])
		stackView.axis = .vertical

		explore.translatesAutoresizingMaskIntoConstraints = false
		titleLabel.translatesAutoresizingMaskIntoConstraints = false
		subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
		stackView.translatesAutoresizingMaskIntoConstraints = false

		addSubview(stackView)
		addSubview(explore)
		addSubview(subtitleLabel)

		NSLayoutConstraint.activate([
			separator.heightAnchor.constraint(equalToConstant: 1),
			stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
			stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
			stackView.topAnchor.constraint(equalTo: topAnchor),
			stackView.bottomAnchor.constraint(equalTo: bottomAnchor),

			titleLabel.centerYAnchor.constraint(equalTo: stackView.centerYAnchor),
			subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
			subtitleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
			subtitleLabel.heightAnchor.constraint(equalToConstant: 20),

			explore.topAnchor.constraint(equalTo: titleLabel.topAnchor),
			explore.bottomAnchor.constraint(equalTo: titleLabel.bottomAnchor),
			explore.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: -8)
		])
		stackView.setCustomSpacing(15, after: separator)
		style()
	}
	
	private func style() {
		titleLabel.textColor = .label
		titleLabel.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18)

		subtitleLabel.textColor = .label
		subtitleLabel.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)

		let color = UIColor(red: 253/255.0, green: 99/255.0, blue: 102/255.0, alpha: 1.0)
		explore.setTitleColor(color, for: .normal)
		explore.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 15)
	}
}

