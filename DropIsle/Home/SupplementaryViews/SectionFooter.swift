//
//  SectionFooter.swift
//  DropIsle
//
//  Created by CtanLI on 2020-11-28.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class SectionFooter: UICollectionReusableView {

	let explore = UIButton()
	var delegate: ExploreAllDelegate?

	override init(frame: CGRect) {
		super.init(frame: frame)
		explore.addTarget(self, action: #selector(exploreAllRecommended), for: .touchUpInside)
		setup()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	@objc func exploreAllRecommended() {
		delegate?.exploreAll(isHeaderPressed: false)
	}

	private func setup() {
		let container = UIView()
		container.translatesAutoresizingMaskIntoConstraints = false
		explore.translatesAutoresizingMaskIntoConstraints = false
		addSubview(container)
		container.addSubview(explore)

		NSLayoutConstraint.activate([
			explore.heightAnchor.constraint(equalToConstant: 55),
			explore.widthAnchor.constraint(equalToConstant: 170),
			explore.topAnchor.constraint(equalTo: container.topAnchor),
			explore.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 20),

			container.heightAnchor.constraint(equalToConstant: 72),
			container.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
			container.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
			container.topAnchor.constraint(equalTo: topAnchor),
			container.bottomAnchor.constraint(equalTo: bottomAnchor),
		])
		style()
	}

	private func style() {
		explore.setTitleColor(.black, for: .normal)
		explore.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 20)
		explore.backgroundColor = UIColor(hexString: "1B292E")
		explore.layer.cornerRadius = 10
		explore.setTitleColor(.white, for: .normal)
	}
}
