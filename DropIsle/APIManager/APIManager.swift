//
//  APIManager.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-11.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import Foundation
import Alamofire
import MessageKit

class APIManager {
	private init() {}
	private let baseURL = "http://ec2-3-140-104-94.us-east-2.compute.amazonaws.com:5000/api"
	private let stripeURL = "http://ec2-3-140-104-94.us-east-2.compute.amazonaws.com:5000"
	static let `shared` = APIManager()
	class func headers() -> [String : String] {
		//guard  let token = preferences[.header] else { return nil }
		let httpHeaders = ["x-auth-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MDNhOWQzY2RhNGFmNDRlMmY1Nzc2OTIiLCJuYW1lIjoiTGlTdGFuIiwiZW1haWwiOiJMaVN0YW5AZ21haWwuY29tIiwic2lnbmVkSW5BcyI6IlVzZXIiLCJpYXQiOjE2MTQ0NTQxMDR9.ftQzYukHqAuW-R3PVanYXtokS6knNfUeiAoKZPgfv6k", "Content-Type": "application/json"]
		return httpHeaders
	}

	func requestCallWithOutHeaders<T: Decodable>(method: HTTPMethod, path: String, params: Parameters, completion: @escaping (Result<T, Error>) -> Void) {
		guard let JSONData = try?  JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) else { return }
		let url = URL(string: baseURL+path)!
		var request = URLRequest(url: url, timeoutInterval: Double.infinity)
		request.addValue("application/json", forHTTPHeaderField: "Content-Type")
		request.httpMethod = method.rawValue
		request.httpBody = JSONData
		AF.request(request).validate().responseData { (responseObject) in
			if responseObject.error != nil, let httpRes = responseObject.response, httpRes.statusCode != 200 {
				if let data = responseObject.data, let message = String(data: data, encoding: String.Encoding.utf8) {
					completion(.failure(errorMessage.custom(message)))
				}
			} else {
				do {
					guard let data = responseObject.data else { return }
					let responseResult = try JSONDecoder.init().decode(T.self, from: data)
					completion(.success(responseResult))
				} catch let err {
					completion(.failure(err.localizedDescription as! Error))
				}
			}
		}
	}

	func requestCallWithHeaders<T: Decodable>(method: HTTPMethod, path: String, params: Parameters, completion: @escaping (Result<T, Error>) -> Void) {
		guard let JSONData = try?  JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) else { return }
		var url: URL!
		if path.contains("stripe") {
			url = URL(string: stripeURL+path)
		} else {
			url = URL(string: baseURL+path)
		}
		//let url = URL(string: baseURL+path)!
		var request = URLRequest(url: url, timeoutInterval: Double.infinity)
		request.allHTTPHeaderFields = APIManager.headers()
		request.httpMethod = method.rawValue
		request.httpBody = JSONData
		
		AF.request(request).validate().responseData { (responseObject) in
			if responseObject.error != nil, let httpRes = responseObject.response, httpRes.statusCode != 200 {
				if let data = responseObject.data, let message = String(data: data, encoding: String.Encoding.utf8) {
					completion(.failure(errorMessage.custom(message)))
				}
			} else {
				do {
					guard let data = responseObject.data else { return }
					let responseResult = try JSONDecoder.init().decode(T.self, from: data)
					completion(.success(responseResult))
				} catch let err {
					completion(.failure(err))
				}
			}
		}
	}

	func requestCallWithUrlParams<T: Decodable>(method: HTTPMethod, path: String, params: Parameters, completion: @escaping (Result<T, Error>) -> Void) {
		guard let urlQuery = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
		guard let _  = URL(string: urlQuery) else { return }
		var urlComp = URLComponents(string: baseURL+path)!
		var items = [URLQueryItem]()
		for (key, value) in params {
			items.append(URLQueryItem(name: key, value: "\(value)"))
		}
		items = items.filter{!$0.name.isEmpty}
		if !items.isEmpty {
			urlComp.queryItems = items
		}
		var headers = HTTPHeaders()
		APIManager.headers().forEach({headers.add(name: $0.key, value: $0.value)})
		AF.request(urlComp, method: method, parameters: nil, encoding: JSONEncoding.default, headers:  headers).responseData { (responseObject) in
			if responseObject.error != nil, let httpRes = responseObject.response, httpRes.statusCode != 200 {
				if let data = responseObject.data, let message = String(data: data, encoding: String.Encoding.utf8) {
					completion(.failure(errorMessage.custom(message)))
				}
			} else {
				do {
					guard let data = responseObject.data else { return }
					let responseResult = try JSONDecoder.init().decode(T.self, from: data)
					completion(.success(responseResult))
				} catch let err {
					print(err.localizedDescription)
					completion(.failure(err))
				}
			}
		}
	}
}


