//
//  LaunchScreenController.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-09.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit
import KeychainAccess
import SVProgressHUD
import CoreLocation

class LaunchScreenController: UIViewController, UpdateLocationDelegate {

	static let onBoardingController = "OnBoardingController"
	static let tabBarViewController = "TabBarViewController"
	var keychain = Keychain()
	var address = String()
	var locationManager = CLLocationManager()
	let location = LocationMonitor.SharedManager

	override func viewDidLoad() {
		super.viewDidLoad()

		///Location manager delegate
		location.delegate = self

		///Check for user current location OR Use selected stored location
		checkForUserLocation()

		//MARK: Check life span of token if expired request a fresh token.
		guard let storedDate = try? self.keychain.get("tokenCreationdDate") else { return }
		if	let StartDate = getDate(stringDate: storedDate) {
			if calculateDaysBetweenTwoDates(start: StartDate, end: Date()) >= 1705 {
				refreshTokenIfExpired()
			}
		}
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		if CurrentUserInfo.shared.userId().isEmpty {
			DispatchQueue.main.async(execute: {
				self.performSegue(withIdentifier: LaunchScreenController.onBoardingController, sender: self)
			})
		} else {
			DispatchQueue.main.async(execute: {
				self.performSegue(withIdentifier: LaunchScreenController.tabBarViewController, sender: self)
			})
		}

		///launch events
		guard let countryCode = Locale.current.regionCode else { return }
		LoggerManager.shared.log(event: .launch, parameters: ["userId": CurrentUserInfo.shared.userId(), "region": countryCode])
	}

	func refreshTokenIfExpired() {
		let params = ["userid": CurrentUserInfo.shared.userId(), "refreshtoken": CurrentUserInfo.shared.refreshToken()]
		let refreshTokenUrl = BaseAPIClient.urlFromPath(path: APIEndPoints.tokenRefresh)
		APIManager.shared.fetchGenericData(method: .post, urlString: refreshTokenUrl, params: params as [String : AnyObject], headers: nil) { (result: TokenRefresh) in
			if result.code == 200 {
				///Ananlytics
				LoggerManager.shared.log(event: .tokenRefresh)
				
				self.keychain["token"] = result.accesstoken
				self.keychain["tokenCreationdDate"] = self.getDateString()
			} else {
				SVProgressHUD.showError(withStatus: "")
			}
		}
	}

	func calculateDaysBetweenTwoDates(start: Date, end: Date) -> Int {
		let currentCalendar = Calendar.current
		guard let start = currentCalendar.ordinality(of: .day, in: .era, for: start) else { return 0 }
		guard let end = currentCalendar.ordinality(of: .day, in: .era, for: end) else { return 0 }
		return end - start
	}
}

extension LaunchScreenController {
	///Check if location enabled and append users current location
	func checkForUserLocation() {
		switch CLLocationManager.authorizationStatus() {
			case .authorizedWhenInUse: location.startMonitoring()
				break
			case .denied: getLatLng()
				break
			case .notDetermined: getLatLng()
				break
			case .restricted: getLatLng()
				break
			default: break
		}
	}

	///get location if enabled
	func updateLocation() {
		_ = ("\(location.currentLocation?.coordinate.latitude ?? 0), \(location.currentLocation?.coordinate.longitude ?? 0)")
		if let _ = location.currentLocation?.coordinate { }
		if let currentLocation = location.currentLocation {
			preferences[.latitude] = "\(currentLocation.coordinate.latitude)"
			preferences[.longitude] = "\(currentLocation.coordinate.longitude)"
		}
		location.stopMonitoring()
	}

	///get location if not enabled
	func getLatLng() {
		if CurrentUserInfo.shared.userId().isEmpty {
			let countryLocale = NSLocale.current
			address = countryLocale.regionCode ?? ""
		} else {
			address = CurrentUserInfo.shared.city() + ", " + CurrentUserInfo.shared.country() + " " + CurrentUserInfo.shared.countryCode()
		}
		
		let geoCoder = CLGeocoder()
		geoCoder.geocodeAddressString(address) { (placemarks, error) in
			guard let placemarks = placemarks, let location = placemarks.first?.location else { return }
			preferences[.latitude] = "\(location.coordinate.latitude)"
			preferences[.longitude] = "\(location.coordinate.longitude)"
		}
	}

	func openCountryList() { }
}
