//
//  HalfSizePresentationController.swift
//  DropIsle
//
//  Created by CtanLI on 2019-02-05.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import UIKit

class FullSizePresentationController: UIPresentationController {
	override var frameOfPresentedViewInContainerView: CGRect {
		get {
			guard let theView = containerView else {
				return CGRect.zero
			}
			return CGRect(x: 0, y: 0, width: theView.bounds.width, height: theView.bounds.height)
		}
	}
}

class HalfSizePresentationController: UIPresentationController {
	override var frameOfPresentedViewInContainerView: CGRect {
		get {
			guard let theView = containerView else {
				return CGRect.zero
			}
			//return CGRect(x: 0, y: theView.bounds.height - theView.bounds.height/2, width: theView.bounds.width, height: theView.bounds.height/2)
			return CGRect(x: 0, y: theView.bounds.height - 395, width: theView.bounds.width, height: 395)
		}
	}
	override func presentationTransitionWillBegin() {
		containerView?.frame = frameOfPresentedViewInContainerView
	}
}

class QuaterSizePresentationController: UIPresentationController {
	override var frameOfPresentedViewInContainerView: CGRect {
		get {
			guard let theView = containerView else {
				return CGRect.zero
			}
			return CGRect(x: 0, y: theView.bounds.height - 170, width: theView.bounds.width, height: 170)
		}
	}
	override func presentationTransitionWillBegin() {
		containerView?.frame = frameOfPresentedViewInContainerView
	}
}
