//
//  ErrorViewCell.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-14.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import UIKit
import DropDown

class ErrorViewCell: DropDownCell {

    @IBOutlet weak var errorIcon: UIImageView!

}
