//
//  BackgroundView.swift
//  DropIsle
//
//  Created by CtanLI on 2019-11-28.
//  Copyright © 2019 DropIsle, Inc. All rights reserved.
//

import Foundation
import UIKit

/**
 Background view that handle touches
 */
internal protocol BackgroundViewDelegate: class {
    func pressedBackgorund(view: BackgroundView)
}

internal class BackgroundView: UIView {

    weak var delegate: BackgroundViewDelegate?

    // MARK: Lifecycle
    override init(frame: CGRect) {
			super.init(frame: frame)
			customize()
    }

    required init?(coder aDecoder: NSCoder) {
			super.init(coder: aDecoder)
			customize()
    }

    private func customize() {
		isUserInteractionEnabled = true
			let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(pressedBackground(sender:)))
			addGestureRecognizer(tapRecognizer)
    }

    @objc func pressedBackground(sender: AnyObject?) {
			delegate?.pressedBackgorund(view: self)
    }
}
