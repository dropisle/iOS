//
//  SwiftMessage.swift
//  DropIsle
//
//  Created by CtanLI on 2020-03-05.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import SwiftMessages

class SwiftMessagesBottomCardSegue: SwiftMessagesSegue {
	override public init(identifier: String?, source: UIViewController, destination: UIViewController) {
		super.init(identifier: identifier, source: source, destination: destination)
		configure(layout: .bottomCard)
		if (destination.isKind(of: ItemConditionController.self)) ||
			(destination.isKind(of: PriceNegotiableController.self))  {
			messageView.backgroundHeight = 150
		} else if (destination.isKind(of: MakeOfferController.self)) {
			messageView.backgroundHeight = 200
		} else {
			///Panel view height
			messageView.backgroundHeight = 260
		}
	}
}

class SwiftMessagesCenteredSegue: SwiftMessagesSegue {
	override public init(identifier: String?, source: UIViewController, destination: UIViewController) {
		super.init(identifier: identifier, source: source, destination: destination)
		configure(layout: .centered)
		if (destination.isKind(of: SortingPriceController.self)) ||
				(destination.isKind(of: PriceNegotiableController.self)) {
			messageView.backgroundHeight = 150
		} else if (destination.isKind(of: SortingDateController.self)) {
			messageView.backgroundHeight = 90
		}
	}
}

class SwiftMessagesBottomMessageSegue: SwiftMessagesSegue {
	override public init(identifier: String?, source: UIViewController, destination: UIViewController) {
		super.init(identifier: identifier, source: source, destination: destination)
		configure(layout: .bottomMessage)
		if (destination.isKind(of: PickerController.self)) {
		   messageView.backgroundHeight = 250
	   }
	}
}
