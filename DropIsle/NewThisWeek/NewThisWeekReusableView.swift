//
//  NewThisWeekReusableView.swift
//  DropIsle
//
//  Created by CtanLI on 2020-11-30.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class NewThisWeekReusableView: UICollectionReusableView {

	let backgroundImage: UIImageView = {
		let iv = UIImageView()
		iv.contentMode = .scaleAspectFill
		iv.backgroundColor = .green
		iv.clipsToBounds = true
		iv.image = UIImage(named: "desert")
		return iv
	}()

	let pageTitle = UILabel()
	let subPageTitle = UILabel()
	
	let title = UILabel()
	let subTitle = UILabel()
	let containerView = UIView()

	override init(frame: CGRect) {
		super.init(frame: frame)
		style()
		setup()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func setup() {
		backgroundImage.translatesAutoresizingMaskIntoConstraints = false
		containerView.translatesAutoresizingMaskIntoConstraints = false
		title.translatesAutoresizingMaskIntoConstraints = false
		subTitle.translatesAutoresizingMaskIntoConstraints = false
		pageTitle.translatesAutoresizingMaskIntoConstraints = false
		subPageTitle.translatesAutoresizingMaskIntoConstraints = false

		addSubview(backgroundImage)
		addSubview(pageTitle)
		addSubview(subPageTitle)
		addSubview(containerView)
		containerView.addSubview(title)
		containerView.addSubview(subTitle)

		NSLayoutConstraint.activate([
			backgroundImage.leadingAnchor.constraint(equalTo: leadingAnchor),
			backgroundImage.trailingAnchor.constraint(equalTo: trailingAnchor),
			backgroundImage.topAnchor.constraint(equalTo: topAnchor),
			backgroundImage.bottomAnchor.constraint(equalTo: containerView.topAnchor),

			pageTitle.topAnchor.constraint(equalTo: topAnchor, constant: 120),
			subPageTitle.topAnchor.constraint(equalTo: pageTitle.bottomAnchor, constant: 5),
			pageTitle.centerXAnchor.constraint(equalTo: centerXAnchor),
			subPageTitle.centerXAnchor.constraint(equalTo: centerXAnchor),

			containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
			containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
			containerView.bottomAnchor.constraint(equalTo: bottomAnchor),
			containerView.heightAnchor.constraint(equalToConstant: 70),

			title.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
			subTitle.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 5)
		])
	}

	private func style() {
		pageTitle.text = "New This Week"
		pageTitle.textColor = .white
		pageTitle.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 20)

		subPageTitle.text = "Discover art to decorate your home, \nrooms, etc"
		subPageTitle.numberOfLines = 0
		subPageTitle.lineBreakMode = .byWordWrapping
		subPageTitle.textAlignment = .center
		subPageTitle.textColor = .white
		subPageTitle.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)

		title.textColor = .label
		title.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18)

		subTitle.textColor = .label
		subTitle.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
		backgroundImage.fillSuperview()
	}
}

extension UIView {
	func fillSuperview(padding: UIEdgeInsets = .zero) {
		translatesAutoresizingMaskIntoConstraints = false
		if let superviewTopAnchor = superview?.topAnchor {
			topAnchor.constraint(equalTo: superviewTopAnchor, constant: padding.top).isActive = true
		}

		if let superviewBottomAnchor = superview?.bottomAnchor {
			bottomAnchor.constraint(equalTo: superviewBottomAnchor, constant: -padding.bottom).isActive = true
		}

		if let superviewLeadingAnchor = superview?.leadingAnchor {
			leadingAnchor.constraint(equalTo: superviewLeadingAnchor, constant: padding.left).isActive = true
		}

		if let superviewTrailingAnchor = superview?.trailingAnchor {
			trailingAnchor.constraint(equalTo: superviewTrailingAnchor, constant: -padding.right).isActive = true
		}
	}
}

