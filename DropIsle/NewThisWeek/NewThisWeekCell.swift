//
//  NewThisWeekCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-11-30.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class NewThisWeekCell: UICollectionViewCell, AppConfigurable {
	
	let imageView = UIImageView()
	let backView = UIView()
	let nameLabel = UILabel()
	let bottomView = UIView()

	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}

	private func setup() {
		backView.translatesAutoresizingMaskIntoConstraints = false
		imageView.translatesAutoresizingMaskIntoConstraints = false
		nameLabel.translatesAutoresizingMaskIntoConstraints = false
		bottomView.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(backView)
		backView.addSubview(imageView)
		backView.addSubview(bottomView)
		bottomView.addSubview(nameLabel)

		NSLayoutConstraint.activate([
			nameLabel.centerYAnchor.constraint(equalTo: bottomView.centerYAnchor),
			nameLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: 20),

			imageView.leadingAnchor.constraint(equalTo: backView.leadingAnchor),
			imageView.trailingAnchor.constraint(equalTo: backView.trailingAnchor),
			imageView.topAnchor.constraint(equalTo: backView.topAnchor),
			imageView.bottomAnchor.constraint(equalTo: bottomView.topAnchor),

			bottomView.leadingAnchor.constraint(equalTo: backView.leadingAnchor),
			bottomView.trailingAnchor.constraint(equalTo: backView.trailingAnchor),
			bottomView.topAnchor.constraint(equalTo: imageView.bottomAnchor),
			bottomView.bottomAnchor.constraint(equalTo: bottomAnchor),
			bottomView.heightAnchor.constraint(equalToConstant: 70),

			backView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
			backView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
			backView.topAnchor.constraint(equalTo: contentView.topAnchor),
			backView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
		])
		style()
	}

	private func style() {
		nameLabel.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18)
		nameLabel.textColor = .black
		nameLabel.numberOfLines = 0

		imageView.clipsToBounds = true
		imageView.contentMode = .scaleAspectFill

		bottomView.backgroundColor = .white

		backView.clipsToBounds = true
		backView.layer.cornerRadius = 10
		backView.borderColor = UIColor.lightGray
		backView.borderWidth = 0.5
	}

	func configure(with app: App) {
		nameLabel.text = app.name
		imageView.image = app.image
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
