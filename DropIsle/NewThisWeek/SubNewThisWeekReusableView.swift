//
//  SubNewThisWeekReusableView.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-01.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class SubNewThisWeekReusableView: UICollectionReusableView {

	let title = UILabel()
	let subTitle = UILabel()
	let containerView = UIView()

	override init(frame: CGRect) {
		super.init(frame: frame)
		style()
		setup()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func setup() {
		title.translatesAutoresizingMaskIntoConstraints = false
		subTitle.translatesAutoresizingMaskIntoConstraints = false
		containerView.translatesAutoresizingMaskIntoConstraints = false
		addSubview(containerView)
		containerView.addSubview(title)
		containerView.addSubview(subTitle)

		NSLayoutConstraint.activate([
			containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
			containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
			containerView.topAnchor.constraint(equalTo: topAnchor),
			containerView.bottomAnchor.constraint(equalTo: bottomAnchor),
			
			title.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
			subTitle.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 5),

		])
	}

	private func style() {
		title.textColor = .label
		title.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18)

		subTitle.textColor = .label
		subTitle.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
	}
}

