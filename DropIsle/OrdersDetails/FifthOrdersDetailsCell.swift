//
//  FifthOrdersDetailsCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-26.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class FifthOrdersDetailsCell: UITableViewCell, Reusable {

	let priceDetails = UILabel()
	let itemCost = UILabel()
	let shippingCost = UILabel()
	let totalBeforeTax = UILabel()
	let gstHst = UILabel()

	let itemCostValue = UILabel()
	let shippingCostValue = UILabel()
	let totalBeforeTaxValue = UILabel()
	let gstHstValue = UILabel()

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		//Initialization code
		setUp()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func setUp() {
		priceDetails.translatesAutoresizingMaskIntoConstraints = false
		itemCost.translatesAutoresizingMaskIntoConstraints = false
		shippingCost.translatesAutoresizingMaskIntoConstraints = false
		totalBeforeTax.translatesAutoresizingMaskIntoConstraints = false
		gstHst.translatesAutoresizingMaskIntoConstraints = false
		itemCostValue.translatesAutoresizingMaskIntoConstraints = false
		shippingCostValue.translatesAutoresizingMaskIntoConstraints = false
		totalBeforeTaxValue.translatesAutoresizingMaskIntoConstraints = false
		gstHstValue.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(priceDetails)
		contentView.addSubview(itemCost)
		contentView.addSubview(shippingCost)
		contentView.addSubview(totalBeforeTax)
		contentView.addSubview(gstHst)
		contentView.addSubview(itemCostValue)
		contentView.addSubview(shippingCostValue)
		contentView.addSubview(totalBeforeTaxValue)
		contentView.addSubview(gstHstValue)

		NSLayoutConstraint.activate([
			priceDetails.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			priceDetails.topAnchor.constraint(equalTo: topAnchor, constant: 20),

			itemCost.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			itemCost.topAnchor.constraint(equalTo: priceDetails.bottomAnchor, constant: 15),

			itemCostValue.centerYAnchor.constraint(equalTo: itemCost.centerYAnchor),
			itemCostValue.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			shippingCost.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			shippingCost.topAnchor.constraint(equalTo: itemCost.bottomAnchor, constant: 15),

			shippingCostValue.centerYAnchor.constraint(equalTo: shippingCost.centerYAnchor),
			shippingCostValue.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			totalBeforeTax.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			totalBeforeTax.topAnchor.constraint(equalTo: shippingCostValue.bottomAnchor, constant: 10),

			totalBeforeTaxValue.centerYAnchor.constraint(equalTo: totalBeforeTax.centerYAnchor),
			totalBeforeTaxValue.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

			gstHst.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			gstHst.topAnchor.constraint(equalTo: totalBeforeTaxValue.bottomAnchor, constant: 10),

			gstHstValue.centerYAnchor.constraint(equalTo: gstHst.centerYAnchor),
			gstHstValue.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
		])

		priceDetails.text = "Price details"
		priceDetails.textColor = .label
		priceDetails.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 15)

		itemCost.text = "Item"
		itemCost.textColor = .label
		itemCost.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		itemCostValue.text = "CAD $340"
		itemCostValue.textColor = .label
		itemCostValue.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		shippingCost.text = "Shipping"
		shippingCost.textColor = .label
		shippingCost.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		shippingCostValue.text = "CAD $340"
		shippingCostValue.textColor = .label
		shippingCostValue.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		totalBeforeTax.text = "Totlal before tax"
		totalBeforeTax.textColor = .label
		totalBeforeTax.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		totalBeforeTaxValue.text = "CAD $340"
		totalBeforeTaxValue.textColor = .label
		totalBeforeTaxValue.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		gstHst.text = "Estimated GST/HST"
		gstHst.textColor = .label
		gstHst.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		gstHstValue.text = "CAD $340"
		gstHstValue.textColor = .label
		gstHstValue.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)
	}
}
