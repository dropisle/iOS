//
//  SecondOrdersDetailsCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-26.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class SecondOrdersDetailsCell: UITableViewCell, Reusable {

	let orderSummary = UILabel()
	let continerView = UIView()
	let hStack = UIStackView()

	let firstView = UIView()
	let topFirstView = UIView()

	let secondView = UIView()
	let thirdView = UIView()
	let fourthView = UIView()
	let fifthView = UIView()
	let sixthView = UIView()
	let seventhView = UIView()

	let processingImg = UIImageView()
	let processing = UILabel()
	let packingImg = UIImageView()
	let packing = UILabel()
	let shippedImg = UIImageView()
	let shipped = UILabel()
	let deliveredImg = UIImageView()
	let delivered = UILabel()
	let divider1 = UIView()
	let divider2 = UIView()
	let divider3 = UIView()

	let trackingTitle = UILabel()
	let trackingNumber = UILabel()
	let trackOrder = UIButton()
	let devider = UIView()

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		//Initialization code
		setUp()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func setUp() {
		hStack.axis = .horizontal
		hStack.distribution = .fillEqually
		hStack.alignment = .fill
		hStack.spacing = 0

		orderSummary.translatesAutoresizingMaskIntoConstraints = false
		continerView.translatesAutoresizingMaskIntoConstraints = false
		hStack.translatesAutoresizingMaskIntoConstraints = false
		firstView.translatesAutoresizingMaskIntoConstraints = false
		secondView.translatesAutoresizingMaskIntoConstraints = false
		thirdView.translatesAutoresizingMaskIntoConstraints = false
		fourthView.translatesAutoresizingMaskIntoConstraints = false
		fifthView.translatesAutoresizingMaskIntoConstraints = false
		sixthView.translatesAutoresizingMaskIntoConstraints = false
		seventhView.translatesAutoresizingMaskIntoConstraints = false
		processingImg.translatesAutoresizingMaskIntoConstraints = false
		processing.translatesAutoresizingMaskIntoConstraints = false
		packingImg.translatesAutoresizingMaskIntoConstraints = false
		packing.translatesAutoresizingMaskIntoConstraints = false
		shippedImg.translatesAutoresizingMaskIntoConstraints = false
		shipped.translatesAutoresizingMaskIntoConstraints = false
		deliveredImg.translatesAutoresizingMaskIntoConstraints = false
		delivered.translatesAutoresizingMaskIntoConstraints = false
		divider1.translatesAutoresizingMaskIntoConstraints = false
		divider2.translatesAutoresizingMaskIntoConstraints = false
		divider3.translatesAutoresizingMaskIntoConstraints = false
		topFirstView.translatesAutoresizingMaskIntoConstraints = false

		trackingTitle.translatesAutoresizingMaskIntoConstraints = false
		trackingNumber.translatesAutoresizingMaskIntoConstraints = false
		trackOrder.translatesAutoresizingMaskIntoConstraints = false
		devider.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(orderSummary)
		contentView.addSubview(continerView)
		continerView.addSubview(hStack)
		hStack.addArrangedSubview(firstView)
		hStack.addArrangedSubview(secondView)
		hStack.addArrangedSubview(thirdView)
		hStack.addArrangedSubview(fourthView)
		hStack.addArrangedSubview(fifthView)
		hStack.addArrangedSubview(sixthView)
		hStack.addArrangedSubview(seventhView)
		firstView.addSubview(topFirstView)
		firstView.addSubview(processingImg)
		firstView.addSubview(processing)
		secondView.addSubview(divider1)
		thirdView.addSubview(packingImg)
		thirdView.addSubview(packing)
		fourthView.addSubview(divider2)
		fifthView.addSubview(shippedImg)
		fifthView.addSubview(shipped)
		sixthView.addSubview(divider3)
		seventhView.addSubview(deliveredImg)
		seventhView.addSubview(delivered)

		contentView.addSubview(trackingTitle)
		contentView.addSubview(trackingNumber)
		contentView.addSubview(trackOrder)
		contentView.addSubview(devider)

		NSLayoutConstraint.activate([
			orderSummary.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			orderSummary.topAnchor.constraint(equalTo: topAnchor, constant: 10),

			continerView.topAnchor.constraint(equalTo: orderSummary.bottomAnchor, constant: 10),
			continerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			continerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			continerView.heightAnchor.constraint(equalToConstant: 100),

			hStack.topAnchor.constraint(equalTo: continerView.topAnchor),
			hStack.bottomAnchor.constraint(equalTo: continerView.bottomAnchor),
			hStack.leadingAnchor.constraint(equalTo: continerView.leadingAnchor),
			hStack.trailingAnchor.constraint(equalTo: continerView.trailingAnchor),

			divider1.heightAnchor.constraint(equalToConstant: 1),
			divider1.leadingAnchor.constraint(equalTo: secondView.leadingAnchor, constant: 5),
			divider1.trailingAnchor.constraint(equalTo: secondView.trailingAnchor, constant: -5),
			divider1.centerYAnchor.constraint(equalTo: secondView.centerYAnchor),
			divider1.centerXAnchor.constraint(equalTo: secondView.centerXAnchor),

			divider2.heightAnchor.constraint(equalToConstant: 1),
			divider2.leadingAnchor.constraint(equalTo: fourthView.leadingAnchor, constant: 5),
			divider2.trailingAnchor.constraint(equalTo: fourthView.trailingAnchor, constant: -5),
			divider2.centerYAnchor.constraint(equalTo: fourthView.centerYAnchor),
			divider2.centerXAnchor.constraint(equalTo: fourthView.centerXAnchor),

			divider3.heightAnchor.constraint(equalToConstant: 1),
			divider3.leadingAnchor.constraint(equalTo: sixthView.leadingAnchor, constant: 5),
			divider3.trailingAnchor.constraint(equalTo: sixthView.trailingAnchor, constant: -5),
			divider3.centerYAnchor.constraint(equalTo: sixthView.centerYAnchor),
			divider3.centerXAnchor.constraint(equalTo: sixthView.centerXAnchor),

			processingImg.centerXAnchor.constraint(equalTo: firstView.centerXAnchor),
			processing.centerXAnchor.constraint(equalTo: firstView.centerXAnchor),
			processingImg.centerYAnchor.constraint(equalTo: firstView.centerYAnchor, constant: -15),
			processing.centerYAnchor.constraint(equalTo: firstView.centerYAnchor, constant: 15),

			packingImg.centerXAnchor.constraint(equalTo: thirdView.centerXAnchor),
			packing.centerXAnchor.constraint(equalTo: thirdView.centerXAnchor),
			packingImg.centerYAnchor.constraint(equalTo: thirdView.centerYAnchor, constant: -15),
			packing.centerYAnchor.constraint(equalTo: thirdView.centerYAnchor, constant: 15),

			shippedImg.centerXAnchor.constraint(equalTo: fifthView.centerXAnchor),
			shipped.centerXAnchor.constraint(equalTo: fifthView.centerXAnchor),
			shippedImg.centerYAnchor.constraint(equalTo: fifthView.centerYAnchor, constant: -15),
			shipped.centerYAnchor.constraint(equalTo: fifthView.centerYAnchor, constant: 15),

			deliveredImg.centerXAnchor.constraint(equalTo: seventhView.centerXAnchor),
			delivered.centerXAnchor.constraint(equalTo: seventhView.centerXAnchor),
			deliveredImg.centerYAnchor.constraint(equalTo: seventhView.centerYAnchor, constant: -15),
			delivered.centerYAnchor.constraint(equalTo: seventhView.centerYAnchor, constant: 15),

			topFirstView.leadingAnchor.constraint(equalTo: firstView.leadingAnchor),
			topFirstView.trailingAnchor.constraint(equalTo: firstView.trailingAnchor),
			topFirstView.topAnchor.constraint(equalTo: firstView.topAnchor),
			topFirstView.bottomAnchor.constraint(equalTo: firstView.bottomAnchor),

			trackingTitle.topAnchor.constraint(equalTo: continerView.bottomAnchor, constant: 10),
			trackingTitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),

			trackingNumber.topAnchor.constraint(equalTo: trackingTitle.bottomAnchor, constant: 5),
			trackingNumber.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),

			trackOrder.topAnchor.constraint(equalTo: continerView.bottomAnchor, constant: 10),
			trackOrder.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			trackOrder.widthAnchor.constraint(equalToConstant: 140),
			trackOrder.heightAnchor.constraint(equalToConstant: 45),

			devider.heightAnchor.constraint(equalToConstant: 0.3),
			devider.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			devider.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			devider.bottomAnchor.constraint(equalTo: bottomAnchor),
		])

		orderSummary.textColor = .label
		orderSummary.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 15)
		orderSummary.text = "Order Status"

		processingImg.image = UIImage(named: "package")
		processingImg.contentMode = .scaleAspectFill

		processing.textColor = .label
		processing.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 10)
		processing.text = "Processing"

		packingImg.image = UIImage(named: "package")
		packingImg.contentMode = .scaleAspectFill

		packing.textColor = .label
		packing.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 10)
		packing.text = "Packing"

		shippedImg.image = UIImage(named: "package")
		shippedImg.contentMode = .scaleAspectFill

		shipped.textColor = .label
		shipped.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 10)
		shipped.text = "Shipped"

		deliveredImg.image = UIImage(named: "package")
		deliveredImg.contentMode = .scaleAspectFill

		delivered.textColor = .label
		delivered.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 10)
		delivered.text = "Delivered"

		divider1.backgroundColor = .label
		divider2.backgroundColor = .label
		divider3.backgroundColor = .label

		topFirstView.backgroundColor = UIColor.orderStatusModeColor().withAlphaComponent(0.5)
		firstView.bringSubviewToFront(topFirstView)

		trackingTitle.textColor = .label
		trackingTitle.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 16)
		trackingTitle.text = "Tracking Number"

		trackingNumber.textColor = .label
		trackingNumber.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 15)
		trackingNumber.text = "ANF-0003742386"

		trackOrder.cornerRadius = 7
		trackOrder.borderColor = .secondarySystemGroupedBackground
		trackOrder.borderWidth = 2
		trackOrder.setTitle("Track Order", for: .normal)
		trackOrder.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 22)
		trackOrder.backgroundColor = UIColor.NavyBlueDark

		devider.backgroundColor = .lightGray
	}
}
