//
//  FirstOrdersDetailsCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-26.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class FirstOrdersDetailsCell: UITableViewCell, Reusable {

	let orderSummary = UILabel()
	let title = UILabel()
	let price = UILabel()
	let estDeliveryDate = UILabel()
	let devider = UIView()

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		//Initialization code
		setUp()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func setUp() {
		orderSummary.translatesAutoresizingMaskIntoConstraints = false
		title.translatesAutoresizingMaskIntoConstraints = false
		price.translatesAutoresizingMaskIntoConstraints = false
		estDeliveryDate.translatesAutoresizingMaskIntoConstraints = false
		devider.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(orderSummary)
		contentView.addSubview(title)
		contentView.addSubview(price)
		contentView.addSubview(estDeliveryDate)
		contentView.addSubview(devider)

		NSLayoutConstraint.activate([
			orderSummary.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			orderSummary.topAnchor.constraint(equalTo: topAnchor, constant: 10),

			title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			title.topAnchor.constraint(equalTo: orderSummary.bottomAnchor, constant: 10),
			title.trailingAnchor.constraint(equalTo: price.leadingAnchor, constant: -10),

			price.topAnchor.constraint(equalTo: title.topAnchor),
			price.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			price.heightAnchor.constraint(equalToConstant: 14),
			price.widthAnchor.constraint(equalToConstant: 110),

			estDeliveryDate.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			estDeliveryDate.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 10),

			devider.heightAnchor.constraint(equalToConstant: 0.3),
			devider.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			devider.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			devider.bottomAnchor.constraint(equalTo: bottomAnchor),
		])

		devider.backgroundColor = .lightGray

		orderSummary.textColor = .label
		orderSummary.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 15)
		orderSummary.text = "Order Summary"

		title.textColor = .label
		title.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 15)
		title.text = "One unique painting by Nadi Bay One unique painting by Nadi Bay One unique painting by Nadi Bay"
		title.lineBreakMode = .byTruncatingTail
		title.numberOfLines = 2

		price.textColor = .label
		price.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 15)
		price.text = "CAD 350.67"
		price.textAlignment = .right

		estDeliveryDate.textColor = .label
		estDeliveryDate.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 15)
		estDeliveryDate.text = "Estimated delivery: Oct 20, 2020"
	}
}
