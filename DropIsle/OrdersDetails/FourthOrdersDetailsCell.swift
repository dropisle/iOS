//
//  FourthOrdersDetailsCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-26.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class FourthOrdersDetailsCell: UITableViewCell, Reusable {

	let shippingAddTitle = UILabel()
	let shippingName = UILabel()
	let shippingStreet = UILabel()
	let shippingCity = UILabel()
	let shippingState = UILabel()
	let shippingPostalOrZipCode = UILabel()
	let shippingCountry = UILabel()
	let shippingContact = UILabel()
	let devider = UIView()

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		//Initialization code
		setUp()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func setUp() {
		shippingAddTitle.translatesAutoresizingMaskIntoConstraints = false
		shippingName.translatesAutoresizingMaskIntoConstraints = false
		shippingStreet.translatesAutoresizingMaskIntoConstraints = false
		shippingCity.translatesAutoresizingMaskIntoConstraints = false
		shippingState.translatesAutoresizingMaskIntoConstraints = false
		shippingPostalOrZipCode.translatesAutoresizingMaskIntoConstraints = false
		shippingCountry.translatesAutoresizingMaskIntoConstraints = false
		shippingContact.translatesAutoresizingMaskIntoConstraints = false
		devider.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(shippingAddTitle)
		contentView.addSubview(shippingName)
		contentView.addSubview(shippingStreet)
		contentView.addSubview(shippingCity)
		contentView.addSubview(shippingState)
		contentView.addSubview(shippingPostalOrZipCode)
		contentView.addSubview(shippingCountry)
		contentView.addSubview(shippingContact)
		contentView.addSubview(devider)

		NSLayoutConstraint.activate([
			shippingAddTitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			shippingAddTitle.topAnchor.constraint(equalTo: topAnchor, constant: 20),

			shippingName.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			shippingName.topAnchor.constraint(equalTo: shippingAddTitle.bottomAnchor, constant: 15),
			shippingStreet.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			shippingStreet.topAnchor.constraint(equalTo: shippingName.bottomAnchor, constant: 5),

			shippingCity.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			shippingCity.topAnchor.constraint(equalTo: shippingStreet.bottomAnchor, constant: 5),
			shippingState.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			shippingState.topAnchor.constraint(equalTo: shippingCity.bottomAnchor, constant: 5),

			shippingPostalOrZipCode.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			shippingPostalOrZipCode.topAnchor.constraint(equalTo: shippingState.bottomAnchor, constant: 5),
			shippingCountry.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			shippingCountry.topAnchor.constraint(equalTo: shippingPostalOrZipCode.bottomAnchor, constant: 5),
			shippingContact.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			shippingContact.topAnchor.constraint(equalTo: shippingCountry.bottomAnchor, constant: 5),

			devider.heightAnchor.constraint(equalToConstant: 0.3),
			devider.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			devider.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			devider.bottomAnchor.constraint(equalTo: bottomAnchor),
		])

		shippingAddTitle.text = "Billing address"
		shippingAddTitle.textColor = .label
		shippingAddTitle.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 15)

		shippingName.text = "Sandra Parker"
		shippingName.textColor = .label
		shippingName.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		shippingStreet.text = "2 Capistro Street"
		shippingStreet.textColor = .label
		shippingStreet.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		shippingCity.text = "Toronto"
		shippingCity.textColor = .label
		shippingCity.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		shippingState.text = "Ontario"
		shippingState.textColor = .label
		shippingState.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		shippingPostalOrZipCode.text = "M6H 3T6"
		shippingPostalOrZipCode.textColor = .label
		shippingPostalOrZipCode.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		shippingCountry.text = "Canada"
		shippingCountry.textColor = .label
		shippingCountry.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		shippingContact.text = "555 666 7777"
		shippingContact.textColor = .label
		shippingContact.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		devider.backgroundColor = .lightGray
	}
}
