//
//  OrdersDetailsController.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-26.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class OrdersDetailsController: UIViewController, UITableViewDataSource, UITableViewDelegate {

	var tableView: UITableView!

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		title = "#Order 1234"
		setUpTableView()
	}

	func setUpTableView() {
		tableView = UITableView.init(frame: .zero)
		tableView.dataSource = self
		tableView.delegate = self
		tableView.separatorStyle = .none
		tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.backgroundColor = .secondarySystemGroupedBackground
		view.addSubview(tableView)

		NSLayoutConstraint.activate([
			tableView.topAnchor.constraint(equalTo: view.topAnchor),
			tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])

		tableView.register(FirstOrdersDetailsCell.self, forCellReuseIdentifier: FirstOrdersDetailsCell.identifier)
		tableView.register(SecondOrdersDetailsCell.self, forCellReuseIdentifier: SecondOrdersDetailsCell.identifier)
		tableView.register(ThirdOrdersDetailsCell.self, forCellReuseIdentifier: ThirdOrdersDetailsCell.identifier)
		tableView.register(FourthOrdersDetailsCell.self, forCellReuseIdentifier: FourthOrdersDetailsCell.identifier)
		tableView.register(FifthOrdersDetailsCell.self, forCellReuseIdentifier: FifthOrdersDetailsCell.identifier)
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 5
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		switch indexPath.row {
			case 0:
				let cell = tableView.dequeueReusableCell(withIdentifier: FirstOrdersDetailsCell.identifier ) as! FirstOrdersDetailsCell
				cell.selectionStyle = .none
				return cell
			case 1:
				let cell = tableView.dequeueReusableCell(withIdentifier: SecondOrdersDetailsCell.identifier ) as! SecondOrdersDetailsCell
				cell.selectionStyle = .none
				return cell
			case 2:
				let cell = tableView.dequeueReusableCell(withIdentifier: ThirdOrdersDetailsCell.identifier ) as! ThirdOrdersDetailsCell
				cell.selectionStyle = .none
				return cell
			case 3:
				let cell = tableView.dequeueReusableCell(withIdentifier: FourthOrdersDetailsCell.identifier ) as! FourthOrdersDetailsCell
				cell.selectionStyle = .none
				return cell
			case 4:
				let cell = tableView.dequeueReusableCell(withIdentifier: FifthOrdersDetailsCell.identifier ) as! FifthOrdersDetailsCell
				cell.selectionStyle = .none
				return cell
			default: break
		}
		return UITableViewCell()
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if indexPath.row == 0 {
			return 120
		} else if indexPath.row == 1 {
			return 205
		} else if indexPath.row == 2 {
			return 320
		} else if indexPath.row == 3 {
			return 225
		}
		return 180
	}

	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return .leastNormalMagnitude
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
	}
}
