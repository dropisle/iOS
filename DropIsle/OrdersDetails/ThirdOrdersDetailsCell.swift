//
//  ThirdOrdersDetailsCell.swift
//  DropIsle
//
//  Created by CtanLI on 2020-12-26.
//  Copyright © 2020 DropIsle, Inc. All rights reserved.
//

import UIKit

class ThirdOrdersDetailsCell: UITableViewCell, Reusable {

	let paymentInfo = UILabel()
	let title = UILabel()
	let cardImage = UIImageView()
	let cardInfo = UILabel()
	let billingAddress = UILabel()

	let cardTypeImage = UIImageView()
	let cardEndingNumber = UILabel()
	let billingAddTitle = UILabel()

	let billingName = UILabel()
	let billingStreet = UILabel()
	let billingCity = UILabel()
	let billingState = UILabel()
	let billingPostalOrZipCode = UILabel()
	let billingCountry = UILabel()
	let billingContact = UILabel()
	let devider = UIView()

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		//Initialization code
		setUp()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func setUp() {
		paymentInfo.translatesAutoresizingMaskIntoConstraints = false
		title.translatesAutoresizingMaskIntoConstraints = false
		cardImage.translatesAutoresizingMaskIntoConstraints = false
		cardInfo.translatesAutoresizingMaskIntoConstraints = false
		billingAddress.translatesAutoresizingMaskIntoConstraints = false
		cardTypeImage.translatesAutoresizingMaskIntoConstraints = false
		cardEndingNumber.translatesAutoresizingMaskIntoConstraints = false
		billingAddTitle.translatesAutoresizingMaskIntoConstraints = false

		billingName.translatesAutoresizingMaskIntoConstraints = false
		billingStreet.translatesAutoresizingMaskIntoConstraints = false
		billingCity.translatesAutoresizingMaskIntoConstraints = false
		billingState.translatesAutoresizingMaskIntoConstraints = false
		billingPostalOrZipCode.translatesAutoresizingMaskIntoConstraints = false
		billingCountry.translatesAutoresizingMaskIntoConstraints = false
		billingContact.translatesAutoresizingMaskIntoConstraints = false
		devider.translatesAutoresizingMaskIntoConstraints = false

		contentView.addSubview(paymentInfo)
		contentView.addSubview(title)
		contentView.addSubview(cardImage)
		contentView.addSubview(cardInfo)
		contentView.addSubview(billingAddress)
		contentView.addSubview(cardTypeImage)
		contentView.addSubview(cardEndingNumber)
		contentView.addSubview(billingAddTitle)

		contentView.addSubview(billingName)
		contentView.addSubview(billingStreet)
		contentView.addSubview(billingCity)
		contentView.addSubview(billingState)
		contentView.addSubview(billingPostalOrZipCode)
		contentView.addSubview(billingCountry)
		contentView.addSubview(billingContact)
		contentView.addSubview(devider)

		NSLayoutConstraint.activate([
			paymentInfo.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			paymentInfo.topAnchor.constraint(equalTo: topAnchor, constant: 10),

			title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			title.topAnchor.constraint(equalTo: paymentInfo.bottomAnchor, constant: 15),

			cardTypeImage.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 15),
			cardTypeImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			cardTypeImage.heightAnchor.constraint(equalToConstant: 20),
			cardTypeImage.widthAnchor.constraint(equalToConstant: 50),

			cardEndingNumber.leadingAnchor.constraint(equalTo: cardTypeImage.trailingAnchor, constant: 5),
			cardEndingNumber.centerYAnchor.constraint(equalTo: cardTypeImage.centerYAnchor),

			billingAddTitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			billingAddTitle.topAnchor.constraint(equalTo: cardEndingNumber.bottomAnchor, constant: 20),

			billingName.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			billingName.topAnchor.constraint(equalTo: billingAddTitle.bottomAnchor, constant: 15),
			billingStreet.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			billingStreet.topAnchor.constraint(equalTo: billingName.bottomAnchor, constant: 5),

			billingCity.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			billingCity.topAnchor.constraint(equalTo: billingStreet.bottomAnchor, constant: 5),
			billingState.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			billingState.topAnchor.constraint(equalTo: billingCity.bottomAnchor, constant: 5),

			billingPostalOrZipCode.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			billingPostalOrZipCode.topAnchor.constraint(equalTo: billingState.bottomAnchor, constant: 5),
			billingCountry.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			billingCountry.topAnchor.constraint(equalTo: billingPostalOrZipCode.bottomAnchor, constant: 5),
			billingContact.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			billingContact.topAnchor.constraint(equalTo: billingCountry.bottomAnchor, constant: 5),

			devider.heightAnchor.constraint(equalToConstant: 0.3),
			devider.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			devider.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			devider.bottomAnchor.constraint(equalTo: bottomAnchor),
		])

		paymentInfo.textColor = .label
		paymentInfo.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 15)
		paymentInfo.text = "Payment information"

		title.textColor = .label
		title.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 15)
		title.text = "Payment method"
		title.lineBreakMode = .byTruncatingTail
		title.numberOfLines = 2

		cardTypeImage.contentMode = .scaleAspectFill
		cardTypeImage.image = #imageLiteral(resourceName: "visaIcon")

		cardEndingNumber.text = "ending with 6678"
		cardEndingNumber.textColor = .label
		cardEndingNumber.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		billingAddTitle.text = "Billing address"
		billingAddTitle.textColor = .label
		billingAddTitle.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 15)

		billingName.text = "Sandra Parker"
		billingName.textColor = .label
		billingName.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		billingStreet.text = "2 Capistro Street"
		billingStreet.textColor = .label
		billingStreet.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		billingCity.text = "Toronto"
		billingCity.textColor = .label
		billingCity.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		billingState.text = "Ontario"
		billingState.textColor = .label
		billingState.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		billingPostalOrZipCode.text = "M6H 3T6"
		billingPostalOrZipCode.textColor = .label
		billingPostalOrZipCode.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		billingCountry.text = "Canada"
		billingCountry.textColor = .label
		billingCountry.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		billingContact.text = "555 666 7777"
		billingContact.textColor = .label
		billingContact.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)

		devider.backgroundColor = .lightGray
	}
}
