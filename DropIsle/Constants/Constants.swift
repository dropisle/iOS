//
//  Constants.swift
//  DropIsle
//
//  Created by CtanLI on 2018-12-11.
//  Copyright © 2018 DropIsle, Inc. All rights reserved.
//

import Foundation
import Alamofire
import KeychainAccess

class BaseAPIClient {
	static var shared: AppDelegate? {
		if Thread.isMainThread {
			return UIApplication.shared.delegate as? AppDelegate
		}
		var appDelegate: AppDelegate?
		DispatchQueue.main.sync {
			appDelegate = UIApplication.shared.delegate as? AppDelegate
		}
		return appDelegate
	}

	class func urlFromPath(path: String) -> String {
		let del = BaseAPIClient.shared
		if del?.endPointURL == String() {
			return ""
		}
		let requestUrl = String(format: "%@%@", del?.endPointURL ?? "http://3.13.36.128/api", path)
		return  requestUrl
	}
}

struct APIEndPoints {

	private var keychain = Keychain()
	public static var savedToken: String?

	init() {
		if let token = try? keychain.get("token") {
			APIEndPoints.savedToken = token
		}
	}

	static let phoneVerificationURL = "https://api.authy.com/protected/json/phones/verification/start"
	static let twilioPhoneVerificationHeader = ["X-Authy-API-Key": "wvZOW45Nm6mZ48OOtWlogoW5YO8OgkgZ"]
	static let authorizationHeader = ["Content-Type": "application/json", "Authorization": "Bearer \(APIEndPoints.savedToken ?? "")"]

	//Categorires
	static let categories: Array<(String, UIImage)> = [("Picks for you", UIImage(named: "Picks")!),
													   ("Fresh Produce", UIImage(named: "fresh")!),
													   ("Household", UIImage(named: "Household")!),
													   ("Fashion & Footwear", UIImage(named: "Apparel")!),
													   ("Beauty & Health", UIImage(named: "Beauty")!),
													   ("Electronics & Appliances", UIImage(named: "Electronics")!),
													   ("Books & Arts", UIImage(named: "Books")!)]

	static let soldSate = "UNAVAILABLE"
	static let blocked = "NO"

	//DeepLinks
	static let scheme = "https"
	static let host = "www.dropisle.com"
	static let productPath = "/product"
	static let profilePath = "/profile"
	static let customDomain = "https://share.dropisle.com"

	//AppStore ID
	static let appStoreId = "1488234122"

	//Dropisle API paths...

	//Authentication
	static let signUp = "/user/adduser"
	static let login = "/user/login"
	static let phoneLogin =  "/user/sendcode"

	//tokenRefresh
	static let tokenRefresh =  "/user/token"

	//Countires
	static let contries =  "/user/countries"

	//Region
	static let region =  "/user/cities/country/%@/state/%@"

	//addProduct
	static let addItem = "/product/additem"

	//getProduct
	static let getProductById = "/product/item/id/%@"

	//getProduct
	static let getProductByIdAndUserId = "/product/item/%@/user/%@"

	//Rating Products
	static let ratingProduct = "/user/item/rate"

	//addProductToWishlit
	static let addProductToWishlit = "/user/item/watch"

	//likeAndUlikeProduct
	static let likeAndUlikeProduct = "/user/item/like"

	//getProductByUserId
	static let getProductByOwnerId = "/product/item/user/%@/page/%@"

	//getProductByOwnerAndUserId
	static let getProductByOwnerAndUserId = "/product/item/owner/%@/user/%@/page/%@"

	//getUserWishlist
	static let getUserWishlist =  "/user/watch/userid/%@/page/%@"

	//forgotPassword
	static let forgotPassword =  "/user/forgot"

	//removeUserUploadedItem
	static let removeUserUploadedItem =  "/product/removeitem"

	//getActivities
	static let getActivities =  "/user/activity/user/%@"

	//getMessages
	static let getMessages =  "/user/messages/%@/other/%@/product/%@"

	//sendMessage
	static let sendMessage =  "/user/sendmessage"

	//getChats
	static let getChats = "/user/chats/%@"

	//getUserDetails
	static let getUserDetails =  "/user/userid/%@"

	//updateToken
	static let updateToken = "/user/addtoken"

	//termsOfService
	static let termsOfService = "/user/conditions"

	//editProduct
	static let editProduct = "/product/edititem"

	//reportProduct&User
	static let reportProductAndMessage = "/user/reportuser"

	//blockUser
	static let blockUser = "/user/blockuser"

	//blockUserList
	static let getblockUsers = "/user/block/userid/%@"

	//addUserImage
	static let addUserImage = "/user/addphoto"

	//updateuser
	static let updateUser = "/user/updateuser"

	//changepassword
	static let changePassword = "/user/changepassword"

	//deletemessage
	static let deleteMessage = "/user/deletemessage"

	//deletemessage
	static let deactivateAccount = "/user/deleteuser"


	//NEW END POINT UPDATE
	//filterGuestProducts & filterUserRegister
	static let filterProductsUsers = "/product/filter/page/%@"

	//searchProducts
	static let registeredUserSearch = "/product/search/lat/%@/lng/%@/user/%@/page/%@"

	//guestUserSearch
	static let guestUserSearch = "/product/search/lat/%@/lng/%@/page/%@"

	//getProductByCategory
	static let getProductByCategory = "/product/item/category/%@/lat/%@/lng/%@/page/%@"

	//getProductByCategoryAndUserId
	static let getProductByCategoryAndUserId = "/product/item/category/%@/lat/%@/lng/%@/user/%@/page/%@"

	//getPrdcuct
	static let getProductForGuestUsers = "/product/item/lat/%@/lng/%@/page/%@"

	static let getProductForRegisteredUsers = "/product/item/lat/%@/lng/%@/user/%@/page/%@"

}

//v2
enum segues: String {
	case newThisWeek = "NewThisWeekController"
	case recommendeForYou = "RecommendeForYouController"
	case sustainableArt = "SustainableArtController"
	case artistsController = "ArtistsController"
	case filterArtCollections = "FilterArtCollectionsController"
	case productDetails  = "ProductDetailsController"
	case ordersDetails  = "OrdersDetailsController"
	case reviewOrder = "ReviewOrderController"
	case shippingAddress = "ShippingAddressController"
	case profile = "ProfileController"
	case editProfile = "EditProfileController"

	case addShippingAddress = "AddShippingAddressController"
	case payment = "PaymentController"

	case picker = "PickerController"

}

struct GetCountries {
	static var countries: [String] = NSLocale.isoCountryCodes.map { (code: String) -> String in
		let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
		return NSLocale(localeIdentifier: "en_US").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
	}
}

enum urlPath: String {
	case signUp = "/users"
	case signIn = "/auth"

	//product
	case getProduct = "/products"
	case getProductImage = "/productimages"

	//store
	case createAStore = "/stores/"

	//cart
	case getCart = "/carts"
	case deleteItemInCart = "/carts/deleteitems"
	case addItemToCart = "/carts/additems"
	case checkout = "/carts/checkout"

	//stripe
	case paymentintent = "/stripe/paymentintent"
}

enum errorMessage: Error {
	case custom(String)
	case other
}

extension errorMessage: LocalizedError {
	var errorDescription: String? {
		switch self {
			case .other:
				return "Something went wrong"
			case .custom(let message):
				return message
		}
	}
}

enum appConstants {
	static let plus = "plus"
	static let minus = "minus"
}
